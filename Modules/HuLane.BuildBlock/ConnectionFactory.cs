﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.BuildBlock
{
    public class ConnectionFactory : IConnectionFactory
    {
        public IDbConnection CreateBackendDBConnection()
        {
            string connStr = ConfigurationManager.ConnectionStrings["backendDB"].ConnectionString;

            return new SqlConnection(connStr);
        }

        public IDbConnection CreateProductDBConnection()
        {
            string connStr = ConfigurationManager.ConnectionStrings["productDB"].ConnectionString;

            return new SqlConnection(connStr);
        }
    }
}
