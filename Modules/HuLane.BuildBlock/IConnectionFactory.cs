﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.BuildBlock
{
    public interface IConnectionFactory
    {
        IDbConnection CreateProductDBConnection();
        IDbConnection CreateBackendDBConnection();
    }
}
