﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.BuildBlock
{
    public class RepositoryBase
    {
        public IConnectionFactory ConnectionFactory { get; set; }
        public RepositoryBase(IConnectionFactory connectionFactory) 
        {
            ConnectionFactory = connectionFactory;
        }
    }
}
