﻿using FluentMigrator;
using FluentMigrator.SqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.Main.Migration
{
    [Migration(2021071201)]
    public class CreateArticles : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Table("Article");
            Delete.Table("Section");
            Delete.Table("Section_lang");
        }

        public override void Up()
        {
            Create.Table("Article")
                .WithColumn("id").AsInt32().Identity().PrimaryKey()
                .WithColumn("name").AsString().NotNullable().Unique()
                .WithColumn("status").AsBoolean().NotNullable().WithDefaultValue(true).WithColumnDescription("是否出現")
                .WithColumn("defaultLang").AsString(2).NotNullable().WithColumnDescription("預設語言，找不到會用預設的")
                .WithColumn("create_date").AsDateTime().NotNullable().WithColumnDescription("創建時間")
                .WithColumn("modify_date").AsDateTime().Nullable().WithColumnDescription("修改時間");

            Create.Table("Section")
               .WithColumn("id").AsInt32().Identity().PrimaryKey()
               .WithColumn("article_id").AsInt32().NotNullable()
               .WithColumn("name").AsString().NotNullable()
               .WithColumn("status").AsBoolean().NotNullable().WithDefaultValue(true).WithColumnDescription("是否出現")
               .WithColumn("sortIndex").AsInt32().NotNullable().WithColumnDescription("頁面排序")
               .WithColumn("parentId").AsInt32().Nullable().WithColumnDescription("上層SectionId")
               .WithColumn("create_date").AsDateTime().NotNullable().WithColumnDescription("創建時間")
               .WithColumn("modify_date").AsDateTime().Nullable().WithColumnDescription("修改時間");

            Create.Table("Section_lang")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("section_id").AsInt32().NotNullable()
                .WithColumn("lang").AsString(2).NotNullable()
                .WithColumn("content").AsCustom("nvarchar(max)").WithColumnDescription("多語系內文");

            InsertAbout();

            InsertHistory();

            InsertOverview();

            InsertStronghold();

            InsertTechnology();

            InsertOrganization();

            InsertTeam();

            
        }

        private void InsertAbout() 
        {
            Insert
                .IntoTable("Article")
                .WithIdentityInsert()
                .Row(new { id = 0,name = "About", status = true, defaultLang = "tw", create_date = DateTime.Now });

            Insert
               .IntoTable("Section")
               .WithIdentityInsert()
               .Row(new { id= 0,name = "Default", article_id = 0, status = true, sortIndex = 0, create_date = DateTime.Now });

            Insert
                .IntoTable("Section_lang")
                .Row(new { section_id = 0, lang = "tw", content = @"<div class=""max1200"">
                        <h2 class=""font-30 font-weight-bold border-bottom-red pb-2 pb-sm-3 pb-lg-4 mb-2 mb-sm-3 mb-lg-4\"" data-aos=""fade-up"">關於胡連</h2>
                        <div class=""pt-lg-2\"">
                            <h3 class=""font-26 font-weight-bold"" data-aos=""fade-up"">Since 1977</h3> 
                            <p data-aos=""fade-up"">胡連精密成立於1977年，創立時從事金屬沖壓端子零件產品，於2004年涉入塑膠連接器產品，如今已成為汽機車業界專業連接器廠；並跨足醫療、太陽能等各方面領域。胡連不斷努力精進提高模具、製程等相關技術開發能力，包括點焊端子、電動車連接器、保險絲盒、壓著機、橡膠件之自製開發產品，並拓展銷售於全球市場。</p>
                            <div class=""background-no-repeat background-position-center border-bottom-C8C8C8 pt-lg-2 my-4 my-lg-5"" style=""background-image:url(styles/images/about/global.svg);background-size:80%"">
                                <ul class=""row noneStyle text-center justify-center"">
                                    <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
                                        <div class=""shadow-style-1 mb-2 pb-sm-1"">
                                            <img src = ""/styles/images/about/address-1.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""font-20 font-weight-bold lh14 mb-0"">台北總公司</p>
                                        <p class=""mb-0"">TAIPEI Headquarters</p>
                                    </li>
                                    <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
                                        <div class=""shadow-style-1 mb-2 pb-sm-1"">
                                            <img src = ""/styles/images/about/address-2.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""font-20 font-weight-bold lh14 mb-0"">台北新廠</p>
                                        <p class=""mb-0"">TAIPEI New Plant</p>
                                    </li>
                                    <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
                                        <div class=""shadow-style-1 mb-2 pb-sm-1"">
                                            <img src = ""/styles/images/about/address-3.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""font-20 font-weight-bold lh14 mb-0"">南京廠</p>
                                        <p class=""mb-0"">Nanjing Plant</p>
                                    </li>
                                    <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
                                        <div class=""shadow-style-1 mb-2 pb-sm-1"">
                                            <img src = ""/styles/images/about/address-4.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""font-20 font-weight-bold lh14 mb-0"">東莞廠</p>
                                        <p class=""mb-0"">Dongguan Plant</p>
                                    </li>
                                    <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
                                        <div class=""shadow-style-1 mb-2 pb-sm-1"">
                                            <img src = ""/styles/images/about/address-5.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""font-20 font-weight-bold lh14 mb-0"">越南廠</p>
                                        <p class=""mb-0"">Vietnam Plant</p>
                                    </li>
                                    <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
                                        <div class=""shadow-style-1 mb-2 pb-sm-1"">
                                            <img src = ""/styles/images/about/address-6.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""font-20 font-weight-bold lh14 mb-0"">印尼廠</p>
                                        <p class=""mb-0"">Indonesia Plant</p>
                                    </li>
                                    <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
                                        <div class=""shadow-style-1 mb-2 pb-sm-1"">
                                            <img src = ""/styles/images/about/address-7.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""font-20 font-weight-bold lh14 mb-0"">歐洲辦事處</p>
                                        <p class=""mb-0"">EU Sales Office</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class=""row"">
                            <div class=""col-12 col-lg-7 pr-xl-0 mb-3 mb-sm-4 mb-lg-0"">
                                <h3 class=""font-26 font-weight-bold"" data-aos=""fade-up"">創新胡連，連接世界 Connecting Worldwide</h3>
                                <p class=""mb-2"" data-aos=""fade-up"">胡連擁有數十年技術經驗及高效率之銷售服務團隊，以台北為總公司，大陸於南京及東莞、東南亞於越南、印尼設立工廠及銷售團隊，形成堅強全球團隊，在業界建立良好商譽。為提供可靠及實用的產品，除了管理體系通過多項國際品質管理認證外，產品亦符合RoHS等相關標準，給予客戶品質信賴的產品。</p>
                                <p class=""mb-2"" data-aos=""fade-up"">胡連秉持「誠信務實、技術創新、客戶滿意、共榮共存、放眼天下」之經營理念；「同仁團隊合作、勇於挑戰、追求卓越」的企業精神、及四大發展方針『品質』、『專業』、『效率』、『服務』作為動力，持續向上成長。為滿足客戶的期望努力、求新求變實現 “Connecting Worldwide”，讓胡連連接器永續發展，連結全世界！</p>
                            </div>
                            <div class=""col-12 col-lg-5"">
                                <div class=""pl-xl-5"">
                                    <div data-aos=""fade-up"">
                                        <div class=""video-element"">
                                            <iframe class=""border-0 w-100"" src=""https://www.youtube.com/embed/ykEbgOojWiU?loop=1&playlist=ykEbgOojWiU"" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>" });
        }

        private void InsertHistory()
        {
            Insert
             .IntoTable("Article")
             .WithIdentityInsert()
             .Row(new { id = 1, name = "History", status = true, defaultLang = "tw", create_date = DateTime.Now });

            Insert
            .IntoTable("Section")
            .WithIdentityInsert()
            .Row(new { id = 1000, name = "2011 - Now", article_id = 1, status = true, sortIndex = 0, create_date = DateTime.Now })
            .Row(new { id = 1001, name = "2021", article_id = 1, status = true, sortIndex = 0, parentId = 1000,create_date = DateTime.Now })
            .Row(new { id = 1002, name = "2019", article_id = 1, status = true, sortIndex = 1, parentId = 1000,create_date = DateTime.Now })
            .Row(new { id = 1003, name = "2018", article_id = 1, status = true, sortIndex = 2, parentId = 1000, create_date = DateTime.Now })
            .Row(new { id = 1004, name = "2017", article_id = 1, status = true, sortIndex = 3, parentId = 1000, create_date = DateTime.Now })
            .Row(new { id = 1005, name = "2016", article_id = 1, status = true, sortIndex = 4, parentId = 1000, create_date = DateTime.Now })
            .Row(new { id = 1006, name = "2015 - 2017", article_id = 1, status = true, sortIndex = 5, parentId = 1000, create_date = DateTime.Now })
            .Row(new { id = 1007, name = "2015", article_id = 1, status = true, sortIndex = 6, parentId = 1000, create_date = DateTime.Now })
            .Row(new { id = 1008, name = "2014", article_id = 1, status = true, sortIndex = 7, parentId = 1000, create_date = DateTime.Now })
            .Row(new { id = 1009, name = "2013", article_id = 1, status = true, sortIndex = 8, parentId = 1000, create_date = DateTime.Now })
            .Row(new { id = 1010, name = "2012", article_id = 1, status = true, sortIndex = 9, parentId = 1000, create_date = DateTime.Now })
            .Row(new { id = 1011, name = "2011", article_id = 1, status = true, sortIndex = 10, parentId = 1000, create_date = DateTime.Now })
            .Row(new { id = 1100, name = "2001 - 2010", article_id = 1, status = true, sortIndex = 1, create_date = DateTime.Now })
            .Row(new { id = 1101, name = "2010", article_id = 1, status = true, sortIndex = 0, parentId = 1100, create_date = DateTime.Now })
            .Row(new { id = 1102, name = "2009", article_id = 1, status = true, sortIndex = 1, parentId = 1100, create_date = DateTime.Now })
            .Row(new { id = 1103, name = "2008", article_id = 1, status = true, sortIndex = 2, parentId = 1100, create_date = DateTime.Now })
            .Row(new { id = 1104, name = "2007", article_id = 1, status = true, sortIndex = 3, parentId = 1100, create_date = DateTime.Now })
            .Row(new { id = 1105, name = "2006", article_id = 1, status = true, sortIndex = 4, parentId = 1100, create_date = DateTime.Now })
            .Row(new { id = 1106, name = "2005", article_id = 1, status = true, sortIndex = 5, parentId = 1100, create_date = DateTime.Now })
            .Row(new { id = 1107, name = "2004", article_id = 1, status = true, sortIndex = 6, parentId = 1100, create_date = DateTime.Now })
            .Row(new { id = 1108, name = "2003", article_id = 1, status = true, sortIndex = 7, parentId = 1100, create_date = DateTime.Now })
            .Row(new { id = 1109, name = "2002", article_id = 1, status = true, sortIndex = 8, parentId = 1100, create_date = DateTime.Now })
            .Row(new { id = 1110, name = "2001", article_id = 1, status = true, sortIndex = 9, parentId = 1100, create_date = DateTime.Now })
            .Row(new { id = 1200, name = "1991 - 2000", article_id = 1, status = true, sortIndex = 2, create_date = DateTime.Now })
            .Row(new { id = 1201, name = "2000", article_id = 1, status = true, sortIndex = 0, parentId = 1200, create_date = DateTime.Now })
            .Row(new { id = 1202, name = "1997", article_id = 1, status = true, sortIndex = 1, parentId = 1200, create_date = DateTime.Now })
            .Row(new { id = 1203, name = "1996", article_id = 1, status = true, sortIndex = 2, parentId = 1200, create_date = DateTime.Now })
            .Row(new { id = 1204, name = "1995", article_id = 1, status = true, sortIndex = 3, parentId = 1200, create_date = DateTime.Now })
            .Row(new { id = 1205, name = "1994", article_id = 1, status = true, sortIndex = 4, parentId = 1200, create_date = DateTime.Now })
            .Row(new { id = 1300, name = "1977 - 1990", article_id = 1, status = true, sortIndex = 3, create_date = DateTime.Now })
            .Row(new { id = 1301, name = "1990", article_id = 1, status = true, sortIndex = 0, parentId = 1300, create_date = DateTime.Now })
            .Row(new { id = 1302, name = "1989", article_id = 1, status = true, sortIndex = 1, parentId = 1300, create_date = DateTime.Now })
            .Row(new { id = 1303, name = "1982", article_id = 1, status = true, sortIndex = 2, parentId = 1300, create_date = DateTime.Now })
            .Row(new { id = 1304, name = "1981", article_id = 1, status = true, sortIndex = 3, parentId = 1300, create_date = DateTime.Now })
            .Row(new { id = 1305, name = "1979", article_id = 1, status = true, sortIndex = 4, parentId = 1300, create_date = DateTime.Now })
            .Row(new { id = 1306, name = "1977", article_id = 1, status = true, sortIndex = 5, parentId = 1300, create_date = DateTime.Now });


            Insert
                .IntoTable("Section_lang")
                .Row(new
                {
                    section_id = 1001,
                    lang = "tw",
                    content = @"
                                <div class=""row align-center"">
                                    <div class=""col-12 col-sm-7 mb-3 mb-sm-0"">
                                        <ul class=""noneStyle ul-style-2"">
                                            <li>台北新廠擴廠落成。</li>
                                        </ul>
                                    </div>
                                    <div class=""col-12 col-sm-5 text-center"">
                                        <div class=""slider-mode-1 dots-style-2"">
                                            <div>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/1.jpg"" alt=""胡連精密"">
                                            </div>
                                            <div>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/1.jpg"" alt=""胡連精密"">
                                            </div>
                                            <div>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/1.jpg"" alt=""胡連精密"">
                                            </div>
                                        </div>
                                    </div>
                                </div>"
                })
                .Row(new
                {
                    section_id = 1002,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">設立歐洲辦事處。</li>
                                    <li class=""mb-2 pb-1 d-flex text-darkBlue"">
                                        <div class=""flex-shrink-0 mr-2"">
                                            <img src=""/styles/images/about/trophy.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""mb-0"">
                                            (7月) 胡連電子(南京)有限公司獲得南京市工業企業技術裝備投入普惠性獎。
                                        </p>
                                    </li>
                                    <li class=""mb-2 pb-1 d-flex text-darkBlue"">
                                        <div class=""flex-shrink-0 mr-2"">
                                            <img src=""/styles/images/about/trophy.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""mb-0"">
                                            (7月) 胡連電子(南京)有限公司獲得南京市工業企業技術裝備投入普惠性獎。
                                        </p>
                                    </li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1003,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">東莞胡連電子科技有限公司通過ISO-14001:2015認證。</li>
                                    <li class=""mb-2"">東莞胡連電子科技有限公司通過IATF-16949:2016認證。</li>
                                    <li class=""mb-2"">胡連電子(越南)責任有限公司通過IATF-16949:2016認證。</li>
                                    <li class=""mb-2 pb-1 d-flex text-darkBlue"">
                                        <div class=""flex-shrink-0 mr-2"">
                                            <img src=""/styles/images/about/trophy.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""mb-0"">
                                            (11月) 東莞胡連電子科技有限公司通過全國高新技術企業認證。
                                        </p>
                                    </li>
                                    <li class=""mb-2 pb-1 d-flex text-darkBlue"">
                                        <div class=""flex-shrink-0 mr-2"">
                                            <img src=""/styles/images/about/trophy.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""mb-0"">
                                            (12月) 胡連電子(南京)有限公司通過全國高新技術企業認證。
                                        </p>
                                    </li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1004,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2 pb-1 d-flex text-darkBlue"">
                                        <div class=""flex-shrink-0 mr-2"">
                                            <img src=""/styles/images/about/trophy.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""mb-0"">
                                            胡連集團推展品管圈活動，東莞胡連電子科技有限公司 獲得第51屆健峰全國品管圈大會之「石川獎」及「健峰獎」。
                                        </p>
                                    </li>
                                    <li class=""mb-2"">東莞胡連電子科技有限公司通過TÜV NORD ISO/TS 16949:2009 認證。</li>
                                    <li class=""mb-2"">胡連電子(南京)有限公司通過VDA 6.1認證</li>
                                    <li class=""mb-2"">成立印尼胡連科技製造有限責任公司</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1005,
                    lang = "tw",
                    content = @"
                                <div class=""row align-center"">
                                    <div class=""col-12 col-sm-7 mb-3 mb-sm-0"">
                                        <ul class=""noneStyle ul-style-2"">
                                            <li class=""mb-2"">台北總公司通過ISO-14001:2015認證。</li>
                                            <li class=""mb-2 pb-1 d-flex text-darkBlue"">
                                                <div class=""flex-shrink-0 mr-2"">
                                                    <img src=""/styles/images/about/trophy.png"" alt=""胡連精密"">
                                                </div>
                                                <p class=""mb-0"">
                                                    東莞胡連普光貿易有限公司 獲頒 比亞迪汽車工業有限公司第十五事業部之「優質供應商」。
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class=""col-12 col-sm-5 text-center"">
                                       <div class=""slider-mode-1 dots-style-2"">
                                            <div>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/2.jpg"" alt=""胡連精密"">
                                            </div>
                                            <div>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/2.jpg"" alt=""胡連精密"">
                                            </div>
                                            <div>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/2.jpg"" alt=""胡連精密"">
                                            </div>
                                        </div>
                                    </div>"
                })
                .Row(new
                {
                    section_id = 1006,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li>胡連集團全面導入六標準差活動並深化應用至日常產品設計開發和問題解決。</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1007,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li>東莞胡連電子科技有限公司 東莞廠區正式落成並量產。</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1008,
                    lang = "tw",
                    content = @"
                                <div class=""row align-center"">
                                    <div class=""col-12 col-sm-7 mb-3 mb-sm-0"">
                                        <ul class=""noneStyle ul-style-2"">
                                            <li class=""mb-2"">胡連電子(越南)責任有限公司通過ISO- 14001:2004認證。</li>
                                            <li class=""mb-2 pb-1 d-flex text-darkBlue"">
                                                <div class=""flex-shrink-0 mr-2"">
                                                    <img src=""/styles/images/about/trophy.png"" alt=""胡連精密"">
                                                </div>
                                                <p class=""mb-0"">
                                                    榮獲經濟部第二屆中堅企業獎。
                                                </p>
                                            </li>
                                            <li class=""mb-2"">胡連電子(南京)有限公司自動倉儲系統完成啟用。</li>
                                        </ul>
                                    </div>
                                    <div class=""col-12 col-sm-5 text-center"">
                                        <div class=""slider-mode-1 dots-style-2"">
                                            <div>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/3.jpg"" alt=""胡連精密"">
                                            </div>
                                            <div>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/3.jpg"" alt=""胡連精密"">
                                            </div>
                                            <div>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/3.jpg"" alt=""胡連精密"">
                                            </div>
                                        </div>
                                    </div>
                                </div>"
                })
                .Row(new
                {
                    section_id = 1009,
                    lang = "tw",
                    content = @"
                                <div class=""row"">
                                    <div class=""col-12 col-sm-7 mb-3 mb-sm-0"">
                                        <ul class=""noneStyle ul-style-2"">
                                            <li class=""mb-2"">胡連電子(越南)責任有限公司通過TS- 16949:2009認證。</li>
                                            <li class=""mb-2"">辦理員工認股權憑證及現金增資，使資本額提高至9.71億元。</li>
                                            <li class=""mb-2"">胡連電子(南京)有限公司建置成型機與沖壓機台即時生產監控系統。</li>
                                            <li class=""mb-2"">東莞胡連電子科技有限公司正式投產。</li>
                                            <li class=""mb-2 pb-1 d-flex text-darkBlue"">
                                                <div class=""flex-shrink-0 mr-2"">
                                                    <img src=""/styles/images/about/trophy.png"" alt=""胡連精密"">
                                                </div>
                                                <p class=""mb-0"">
                                                    胡連電子(南京)有限公司12月獲頒長城汽車質量學院第一屆零缺陷工程獎(為唯一獲獎連接器廠商)。
                                                </p>
                                            </li>
                                            <li class=""mb-2"">胡連電子(南京)有限公司二期擴廠完成。</li>
                                            <li class=""mb-2"">首次出版胡連企業社會責任報告書。</li>
                                            <li class=""mb-2"">取得一宏鋼鐵股份有限公司100%股權。</li>
                                        </ul>
                                    </div>
                                    <div class=""col-12 col-sm-5 text-center align-self-start"">
                                        <ul class=""noneStyle slider-mode-1 dots-style-2"">
                                            <li>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/4.jpg"" alt=""胡連精密"">
                                            </li>
                                            <li>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/4.jpg"" alt=""胡連精密"">
                                            </li>
                                            <li>
                                                <img class=""mx-auto"" src=""/styles/images/about/history/4.jpg"" alt=""胡連精密"">
                                            </li>
                                        </ul>
                                    </div>
                                </div>"
                })
                .Row(new
                {
                    section_id = 1010,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">辦理盈餘轉增資1,733萬元及員工認股權憑證轉換股份181.1萬元，使資本額提高至8.84億元。</li>
                                    <li class=""mb-2"">獲得經濟部工業局電動機車電池交換站科專計劃補助，新開發的接觸式連接器成功的應用於此計劃。</li>
                                    <li class=""mb-2"">成立東莞胡連普光貿易有限公司。</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1011,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">開發完成電動車高電壓連接器。</li>
                                    <li class=""mb-2"">投資成立東莞胡連電子科技有限公司。</li>
                                    <li class=""mb-2"">胡連(越南)電子責任有限公司正式落成及量產。</li>
                                    <li class=""mb-2"">公司ERP系統導入SAP系統。</li>
                                    <li class=""mb-2"">辦理盈餘轉增資4,104萬元及員工認股權憑證轉換股份313萬元，使資本額提高至8.64億元。</li>
                                    <li class=""mb-2 pb-1 d-flex text-darkBlue"">
                                        <div class=""flex-shrink-0 mr-2"">
                                            <img src=""/styles/images/about/trophy.png"" alt=""胡連精密"">
                                        </div>
                                        <p class=""mb-0"">
                                            榮獲證券櫃檯買賣中心「上櫃公司金桂獎」-卓越永續經營獎殊榮。

                                        </p>
                                    </li>
                                    <li class=""mb-2"">氣動壓線模具開發成功，並正式量產買賣銷售。</li>
                                    <li class=""mb-2"">胡連電子(南京)有限公司獲得南京市企業技術中心。</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1101,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">榮獲遠見雜誌第285期報導第六屆企業社會責任獎(CSR)，傑出企業65強上櫃公司5強之一。</li>
                                    <li class=""mb-2"">高階點焊端子開發完成並獲得工業局CITD專案補助。</li>
                                    <li class=""mb-2"">辦理現金增資8,390萬元，使資本額提高至8.21億元。</li>
                                    <li class=""mb-2"">投資成立香港福茂發展有限公司。</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1102,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">榮獲遠見雜誌第273期報導第五屆企業社會責任獎(CSR)，傑出企業70強上櫃公司10強之一。</li>
                                    <li class=""mb-2"">南京實驗室通過CNAS國家實驗室認證(ISO 17025)。</li>
                                    <li class=""mb-2"">辦理盈餘轉增資3,508萬元，使資本額提高至7.36億元。</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1103,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">5月完成產品壽命管理系統(PLM)建置及導入。</li>
                                    <li class=""mb-2"">7月與北科大建置完成電性自動化量測系統。</li>
                                    <li class=""mb-2"">7月與北科大合作專案獲得教育部補助。</li>
                                    <li class=""mb-2"">通過上海通用汽車對南京廠實驗室的認證 (GP-10)。</li>
                                    <li class=""mb-2"">上海辦公室成立。</li>
                                    <li class=""mb-2"">辦理盈餘轉增資3,713萬元，使資本額提高至7.02億元。</li>
                                    <li class=""mb-2"">成立胡連電子(越南)股份有限公司。</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1104,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">本公司、胡連電子深圳子公司及胡連電子南京子公司通過TUV認証ISO-14001:2004環境保護體系。</li>
                                    <li class=""mb-2"">本公司實驗室通過 ISO/IEC 17025:2005 長度校正實驗室認證(編號1779)。</li>
                                    <li class=""mb-2"">與義大利ELMAC S.R.L.公司簽立代理銷售合約。</li>
                                    <li class=""mb-2"">胡連電子(南京)有限公司正式建廠完成。</li>
                                    <li class=""mb-2"">辦理盈餘轉增資3,138萬元，使資本額提高至6.64億元。</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1105,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">本公司及胡連電子深圳子公司通過TUV認証 TS-16949:2002品質保証體系。</li>
                                    <li class=""mb-2"">辦理現金增資3,000萬使資本額提升至6.33億元。</li>
                                    <li class=""mb-2"">越南辦事處成立。</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1106,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">辦理盈餘轉增資2,117萬元，資本公積轉增資5,292萬元使資本額提 高至60,325萬元。</li>
                                    <li class=""mb-2"">轉投資南京子公司正式開工興建。</li>
                                    <li class=""mb-2"">南京子公司通過TUV ISO9001:2000認証。</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1107,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">轉投資南京廠，正式成立胡連電子 ( 南京 ) 有限公司</li>
                                    <li class=""mb-2"">榮獲國家頒發盤石獎獎項</li>
                                    <li class=""mb-2"">正式與捷運工業股份有限公司完成合併，使本公司股本提高至48,996萬元</li>
                                    <li class=""mb-2"">辦理盈餘轉增資3,920萬元使公司資本額提高至52,916萬元</li>
                                    <li class=""mb-2"">投資印尼多達胡連太平洋股份有限公司取得該公司股權40%</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1108,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">於 11 月正式上櫃</li>
                                    <li class=""mb-2"">辦理盈餘轉增資7,605萬元，使本公司資本額提高至 38,025 萬元</li>
                                    <li class=""mb-2"">正式導入 TIPTOP 系統，進行 ERP 系統資源整合</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1109,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">辦理盈餘轉增資 5,070 萬元，使本公司資本額提高至 30,420 萬元</li>
                                    <li class=""mb-2"">榮獲經濟部頒發中小企業「小巨人」獎項</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1110,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">辦理現金增資 4,971 萬元及盈餘轉增資 4,509 萬元，使本公司資本額提高至 19,500 萬元，並經股東會通過更名為胡連精密股份有限公司</li>
                                    <li class=""mb-2"">辦理盈餘轉增資 5,850 萬元，使本公司資本額提高至 25,350 萬元</li>
                                    <li class=""mb-2"">發行新股暨補辦公開發行，並獲主管機關同意公開發行股份</li>
                                    <li class=""mb-2"">成立香港分公司</li>
                                    <li class=""mb-2"">取得胡連電子(深圳)有限公司及連盈電子(深圳)有限公司100%股權</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1201,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">為因應 WTO 提升競爭力，導入績效管理制度</li>
                                    <li class=""mb-2"">研發開發端子自動影像感知器 (LCD) 成功</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1202,
                    lang = "tw",
                    content = @" 
                                <ul class=""noneStyle ul-style-2"">
                                    <li>辦理資本公積轉增資 3,700 萬元及盈餘轉增資 1,310 萬元，使資本額提高至 10,020 萬元</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1203,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li>為提昇本公司管理效率，與顧問公司簽約輔導，並導入日常管理制度建立 ISO-9002 制度</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1204,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li>本公司辦理現金增資 1,620 萬元及盈餘轉增資 420 萬元，使資本額提高 至 5,010 萬元</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1205,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">本公司通過 UL 認證，使產品品質提升至國際水準</li>
                                    <li class=""mb-2"">為提高營運及生產效率，本公司更換大型電腦系統</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1301,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">本公司由胡連企業有限公司更名為胡連工業股份有限公司</li>
                                    <li class=""mb-2"">為營運之需，辦理現金增資新台幣 2,470 萬元，使本公司資本額提高至 2,970 萬元</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1302,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">環河街新廠落成啟用，導入全廠MRP(資訊管理系統)</li>
                                    <li class=""mb-2"">本公司通過 CSA 認證</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1303,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li class=""mb-2"">開發壓線模具，並正式量產銷售</li>
                                    <li class=""mb-2"">總公司由台北市遷址至汐止市</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1304,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li>辦理現金增資 450 萬元，使本公司資本額提高至 500 萬元</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1305,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li>開發壓線模具，並正式量產買賣銷售</li>
                                </ul>"
                })
                .Row(new
                {
                    section_id = 1306,
                    lang = "tw",
                    content = @"
                                <ul class=""noneStyle ul-style-2"">
                                    <li>設立胡連企業有限公司，從事汽機車電子用端子零件製造及銷售，設立時資本額為新台幣 50 萬元</li>
                                </ul>"
                })
                ;

        }

        private void InsertOverview() 
        {
            Insert
               .IntoTable("Article")
               .WithIdentityInsert()
               .Row(new { id = 2, name = "Overview", status = true, defaultLang = "tw", create_date = DateTime.Now });

            Insert
               .IntoTable("Section")
               .WithIdentityInsert()
               .Row(new { id = 2, name = "Default", article_id = 2, status = true, sortIndex = 0, create_date = DateTime.Now });

            Insert
                .IntoTable("Section_lang")
                .Row(new { section_id = 2, lang = "tw", content = @"<div class=""max1200"">
                <h2 class=""font-30 font-weight-bold border-bottom-red pb-2 pb-sm-3 pb-lg-4 mb-2 mb-sm-3 mb-lg-4"" data-aos=""fade-up"">企業願景</h2>
                <div class=""pt-lg-4"">
                    <p class=""font-26 font-weight-bold mb-1"" data-aos=""fade-up"">創新胡連，連接世界 Connecting Worldwide</p>
                    <p class=""mb -0"" data-aos=""fade-up"">胡連以 「誠信務實、技術創新、客戶滿意、共榮共存、放眼天下」之經營理念及「以人為本、信任支持、權責清楚、賞罰分明、成果分享」之管理原則，持續不斷地推動公司的發展性方針與基礎性方針為依據。</p>
                    <p class=""mb -0"" data-aos=""fade-up"">同時秉持團隊合作、勇於挑戰與追求卓越的企業精神，在接插件相關事業領域上，全員追求貫徹以「成為全球前十大汽機車連接器供應商」為使命立基，進而實現「創新胡連，連接世界」的願景。</p>
                    <img class=""my -4 my-lg-5 py-sm-2"" src=""/styles/images/about/overview/img.png"" alt=""胡連精密"" data-aos=""fade-up"">
                    <p class=""mb -0"" data-aos=""fade-up"">胡連以上述之經營理念及管理原則作為指導並持續不斷地推動公司的發展性方針與基礎性方針為依據。</p>
                    <p class=""mb -0"" data-aos=""fade-up"">同時秉持團隊合作、勇於挑戰與追求卓越的企業精神，在接插件相關事業領域上，全員追求貫徹以「成為全球前十大汽機車連接器供應商」為使命利基，進而實現「創新胡連，連接世界」的願景。</p>
                </div>

            </div>" });
        }

        private void InsertOrganization() 
        {
            Insert
              .IntoTable("Article")
              .WithIdentityInsert()
              .Row(new { id = 6, name = "Organization", status = true, defaultLang = "tw", create_date = DateTime.Now });

            Insert
               .IntoTable("Section")
               .WithIdentityInsert()
               .Row(new { id = 6, name = "Default", article_id = 6, status = true, sortIndex = 0, create_date = DateTime.Now });

            Insert
                .IntoTable("Section_lang")
                .Row(new { section_id = 6, lang = "tw", content = @"<div class=""max1200"">
                <h2 class=""font-30 font-weight-bold border-bottom-red pb-2 pb-sm-3 pb-lg-4 mb-0"" data-aos=""fade-up"">胡連組織</h2>

                <div class=""bg -F5F5F5 rounded-bottom p-3 p-sm-4"">
                    <div class=""px-sm-3 pt-sm-3 pb-sm-4"" data-aos=""fade-up"">
                        <picture>
                            <source media = ""(min-width: 576px)"" srcset=""/styles/images/about/organization/img.png"" />
                            <source media = ""(max-width: 575px)"" srcset=""/styles/images/about/organization/img-mobile.png"" />
                            <img src = ""#"" alt=""胡連精密"" />
                        </picture>
                    </div>
                </div>
            </div>" });
        }

        private void InsertTechnology() 
        {
            Insert
             .IntoTable("Article")
             .WithIdentityInsert()
             .Row(new { id = 5, name = "Technology", status = true, defaultLang = "tw", create_date = DateTime.Now });

            Insert
               .IntoTable("Section")
               .WithIdentityInsert()
               .Row(new { id = 5, name = "Default", article_id = 5, status = true, sortIndex = 0, create_date = DateTime.Now });

            Insert
                .IntoTable("Section_lang")
                .Row(new { section_id = 5, lang = "tw", content = @"<div class=""max1200"">
                <h2 class=""font-30 font-weight-bold border-bottom-red pb-2 pb-sm-3 pb-lg-4 mb-0"" data-aos=""fade-up"">技術發展</h2>
                <div class=""position-relative pb-sm-5 mb-4"">
                    <div class=""position-lg-absolute text-center w-100 pt-3 pt-sm-4 px-sm-4 mt-sm-2"">
                        <p class=""font-26 font-weight-bold mb-1"" data-aos=""fade-up"">胡連產品藍圖</p>
                        <p data-aos=""fade-up"">滿足客戶與使用人之需求，使用核心技術，研發高品質且多樣化之產品，並開發新能源電動車系連接器及往高端技術連接器自主開發設計為目標。</p>
                    </div>
                    <img src = ""/styles/images/about/technology/1.jpg"" alt=""胡連精密"" data-aos=""fade-up"">
                </div>
                <div class=""pb-sm-5 mb-4"">
                    <p class=""font-26 font-weight-bold text-center mb-2 mb-sm-4"" data-aos=""fade-up"">開發設計-分析技術</p>
                    <img src = ""/styles/images/about/technology/2.png"" alt=""胡連精密"" data-aos=""fade-up"">
                </div>
                <div>
                    <div class=""pb-sm-4"">
                        <p class=""font-26 font-weight-bold text-center mb-1"" data-aos=""fade-up"">實驗驗證</p>
                        <p class=""text-center"" data-aos=""fade-up"">強化驗證能力及開發高效能的檢測設備以達到產品品質要求，是胡連技術發展的目標。</p>
                    </div>
                    <div class=""bg-F5F5F5 rounded-25 pt-4 pb-2 px-3 p-sm-4 p-xl-5"">
                        <div class=""py-sm-2 px-xl-1"">
                            <div class=""row g-1 mb-4 mb-sm-5 pb-sm-2"">
                                <div class=""col-4 col-sm-2 px-1 mb-3 mb-sm-0"" data-aos=""fade-up"">
                                    <img src = ""/styles/images/about/technology/3-1-1.png"" alt=""胡連精密"">
                                </div>
                                <div class=""col-4 col-sm-2 px-1 mb-3 mb-sm-0"" data-aos=""fade-up"">
                                    <img src = ""/styles/images/about/technology/3-1-2.png"" alt=""胡連精密"">
                                </div>
                                <div class=""col-4 col-sm-2 px-1 mb-3 mb-sm-0"" data-aos=""fade-up"">
                                    <img src = ""/styles/images/about/technology/3-1-3.png"" alt=""胡連精密"">
                                </div>
                                <div class=""col-4 col-sm-2 px-1 mb-3 mb-sm-0"" data-aos=""fade-up"">
                                    <img src = ""/styles/images/about/technology/3-1-4.png"" alt=""胡連精密"">
                                </div>
                                <div class=""col-4 col-sm-2 px-1 mb-3 mb-sm-0"" data-aos=""fade-up"">
                                    <img src = ""/styles/images/about/technology/3-1-5.png"" alt=""胡連精密"">
                                </div>
                                <div class=""col-4 col-sm-2 px-1 mb-3 mb-sm-0"" data-aos=""fade-up"">
                                    <img src = ""/styles/images/about/technology/3-1-6.png"" alt=""胡連精密"">
                                </div>
                            </div>
                            <div class=""row g-2"">
                                <div class=""col-6 col-sm-3 px-2 mb-3 mb-sm-0"" data-aos=""fade-up"">
                                    <div class=""box-shadow-style-2 rounded-25"">
                                        <img class=""w-100"" src=""/styles/images/about/technology/3-2-1.png"" alt=""胡連精密"">
                                    </div>
                                </div>
                                <div class=""col-6 col-sm-3 px-2 mb-3 mb-sm-0"" data-aos=""fade-up"">
                                    <div class=""box-shadow-style-2 rounded-25"">
                                        <img class=""w-100"" src=""/styles/images/about/technology/3-2-2.png"" alt=""胡連精密"">
                                    </div>
                                </div>
                                <div class=""col-6 col-sm-3 px-2 mb-3 mb-sm-0"" data-aos=""fade-up"">
                                    <div class=""box-shadow-style-2 rounded-25"">
                                        <img class=""w-100"" src=""/styles/images/about/technology/3-2-3.png"" alt=""胡連精密"">
                                    </div>
                                </div>
                                <div class=""col-6 col-sm-3 px-2 mb-3 mb-sm-0"" data-aos=""fade-up"">
                                    <div class=""box-shadow-style-2 rounded-25"">
                                        <img class=""w-100"" src=""/styles/images/about/technology/3-2-4.png"" alt=""胡連精密"">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>" });
        }

        private void InsertTeam() 
        {
            Insert
             .IntoTable("Article")
             .WithIdentityInsert()
             .Row(new { id = 7, name = "Team", status = true, defaultLang = "tw", create_date = DateTime.Now });

            Insert
               .IntoTable("Section")
               .WithIdentityInsert()
               .Row(new { id = 7, name = "Default", article_id = 7, status = true, sortIndex = 0, create_date = DateTime.Now });

            Insert
                .IntoTable("Section_lang")
                .Row(new { section_id = 7, lang = "tw", content = @"<div class=""max1200"">
                <h2 class=""font-30 font-weight-bold pb-2 pb-sm-3 pb-lg-4 mb-0"" data-aos=""fade-up"">經營團隊</h2>
                <div>
                    <table class=""table-style-2 table-style-2-2"">
                        <thead data-aos=""fade-up"">
                            <tr class=""text-center"">
                                <th style = ""width:15%;""> 職稱 </th >
                                <th style=""width:15%;"">姓名</th>
                                <th style = ""width:15%;"" > 學歷 </th >
                                <th style=""width:55%;"">經歷</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">董事長</td>
                                <td class=""text-md-center"" data-title=""姓名"">張子雄</td>
                                <td class=""text-md-center"" data-title=""學歷"">高中</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">胡連電子(越南)董事長、連盈電子(深圳)董事長、胡連電子(南京)執行董事、胡連電子(深圳)董事、東莞胡連電子科技董事、東莞胡連普光貿易執行董事、常益投資董事、得福投資董事、多達胡連太平洋董事</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">總經理</td>
                                <td class=""text-md-center"" data-title=""姓名"">胡盛清</td>
                                <td class=""text-md-center"" data-title=""學歷"">中學</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">胡連電子(深圳)董事長、東莞胡連電子科技董事長、連盈電子(深圳)董事、福茂發展有限公司董事、多達胡連太平洋董事</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">執行長兼台灣區、亞太區、集團營運中心(含資訊處)主管</td>
                                <td class=""text-md-center"" data-title=""姓名"">徐鳳財</td>
                                <td class=""text-md-center"" data-title=""學歷"">聯合工專機械科</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">福特六和經理、副總經理兼大陸區執行長、副總經理兼執行長</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">副執行長兼大陸區總經理、歐美區、集團研發中心(含企劃處)、越南胡連主管</td>
                                <td class=""text-md-center"" data-title=""姓名"">張子傑</td>
                                <td class=""text-md-center"" data-title=""學歷"">逢甲大學機械工程系</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">胡連精密研發部經理、連盈電子(深圳)經理、胡連香港分公司協理、胡連電子(南京)副總經理、大陸區總經理兼集團研發中心副總經理</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">台北胡連副總經理</td>
                                <td class=""text-md-center"" data-title=""姓名"">方凱平</td>
                                <td class=""text-md-center"" data-title=""學歷"">台北科技大學工業工程與管理碩士</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">胡連精密研究發展處協理、胡連電子(深圳)協理、胡連精密台北銷售事業處副總經理</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">胡連電子(東莞)副總經理</td>
                                <td class=""text-md-center"" data-title=""姓名"">林明淼</td>
                                <td class=""text-md-center"" data-title=""學歷"">新竹高工</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">中國端子協理、南通大地(江蘇)副總</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">胡連電子(南京)副總經理</td>
                                <td class=""text-md-center"" data-title=""姓名"">陳克宙</td>
                                <td class=""text-md-center"" data-title=""學歷"">台北科技大學工業工程與管理碩士</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">胡連精密品質管理處代理經理、胡連電子(南京)品保部經理、胡連電子(深圳)協理</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">印尼胡連董事長兼總經理、亞太區銷售主管</td>
                                <td class=""text-md-center"" data-title=""姓名"">張秉鈞</td>
                                <td class=""text-md-center"" data-title=""學歷"">Georgia Institute of Technology Master of Science in Industrial Engineering</td>
                                <td data-title= ""經歷"" >
                                    <p class=""mb-0"">瀚宇博德工程師、胡連精密品質管理處副理、胡連精密台北生產事業處生產部副理、集團研發中心副理</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">集團研發中心實驗處兼集團營運中心財務處副總經理</td>
                                <td class=""text-md-center"" data-title=""姓名"">趙菁山</td>
                                <td class=""text-md-center"" data-title=""學歷"">台灣大學工業工程所碩士</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">胡連精密財務部兼經管部經理、胡連精密總管理處副總經理</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">集團研發中心研發處兼技術發展部副總工程師兼協理</td>
                                <td class=""text-md-center"" data-title=""姓名"">王志信</td>
                                <td class=""text-md-center"" data-title=""學歷"">永春中學</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">捷運工業模具工程師、順信精密負責人、胡連電子(南京)工程部兼研發部資深工程師兼經理、台北研發中心副總工程師兼協理兼大陸區研發中心協理</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">大陸銷售事業處總監</td>
                                <td class=""text-md-center"" data-title=""姓名"">游景富</td>
                                <td class=""text-md-center"" data-title=""學歷"">宜蘭農工</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">胡連精密國內部代理副理、大陸銷售事業處經理</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">董事長室兼集團營運中心法務暨投資處特助兼協理</td>
                                <td class=""text-md-center"" data-title=""姓名"">潘素秋</td>
                                <td class=""text-md-center"" data-title=""學歷"">大華大學經營管理研究所碩士</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">眾智聯合會計師事務所副理、胡連精密董事長特別助理兼財務部經理、胡連精密董事長室主任督導兼董事長特助</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">稽核室協理</td>
                                <td class=""text-md-center"" data-title=""姓名"">張秋對</td>
                                <td class=""text-md-center"" data-title=""學歷"">台灣大學事業經營碩士</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">胡連精密會計部副理、胡連精密監理室副理、胡連電子(南京)管理部經理、大陸生產事業群與銷售事業群群本部管理協理、胡連電子(東莞)管理單位協理</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">總經理室協理</td>
                                <td class=""text-md-center"" data-title=""姓名"">余宗吉</td>
                                <td class=""text-md-center"" data-title=""學歷"">南台工專</td>
                                <td data-title=""經歷"">
                                    <p class=""mb-0"">英鎂熱處理副廠長、胡連電子(深圳)協理、胡連精密生產事業處協理</p>
                                </td>
                            </tr>
                            <tr data-aos=""fade-up"">
                                <td class=""text-md-center"" data-title=""職稱"">集團營運中心人資處協理</td>
                                <td class=""text-md-center"" data-title=""姓名"">張仲貽</td>
                                <td class=""text-md-center"" data-title=""學歷"">University of Strathclyde Master of Science in international Human Resource Management</td>
                                <td data-title= ""經歷"" >
                                    <p class=""mb-0"">Next Animation Studio HR、星創在線人資管理師、集團營運中心人資處專案經理</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>" });
        }

        private void InsertStronghold() 
        {
            Insert
             .IntoTable("Article")
             .WithIdentityInsert()
             .Row(new { id = 3, name = "Stronghold", status = true, defaultLang = "tw", create_date = DateTime.Now });

            Insert
               .IntoTable("Section")
               .WithIdentityInsert()
               .Row(new { id = 3000, name = "Map", article_id = 3, status = true, sortIndex = 0, create_date = DateTime.Now })
               .Row(new { id = 3001, name = "Factory-1", article_id = 3, status = true, sortIndex = 1, create_date = DateTime.Now })
               .Row(new { id = 3002, name = "Factory-2", article_id = 3, status = true, sortIndex = 2, create_date = DateTime.Now })
               .Row(new { id = 3003, name = "Factory-3", article_id = 3, status = true, sortIndex = 3, create_date = DateTime.Now })
               .Row(new { id = 3004, name = "Factory-4", article_id = 3, status = true, sortIndex = 4, create_date = DateTime.Now })
               .Row(new { id = 3005, name = "Factory-5", article_id = 3, status = true, sortIndex = 5, create_date = DateTime.Now })
               .Row(new { id = 3006, name = "Factory-6", article_id = 3, status = true, sortIndex = 6, create_date = DateTime.Now })
               .Row(new { id = 3007, name = "Factory-7", article_id = 3, status = true, sortIndex = 7, create_date = DateTime.Now })
               .Row(new { id = 3008, name = "Factory-8", article_id = 3, status = true, sortIndex = 8, create_date = DateTime.Now })
               .Row(new { id = 3009, name = "Factory-9", article_id = 3, status = true, sortIndex = 9, create_date = DateTime.Now })
               .Row(new { id = 3010, name = "Factory-10", article_id = 3, status = true, sortIndex = 10, create_date = DateTime.Now })
               .Row(new { id = 3011, name = "Factory-11", article_id = 3, status = true, sortIndex = 11, create_date = DateTime.Now })
               .Row(new { id = 3012, name = "Factory-12", article_id = 3, status = true, sortIndex = 12, create_date = DateTime.Now })
               .Row(new { id = 3013, name = "Factory-13", article_id = 3, status = true, sortIndex = 13, create_date = DateTime.Now })
               .Row(new { id = 3014, name = "Factory-14", article_id = 3, status = true, sortIndex = 14, create_date = DateTime.Now })
               .Row(new { id = 3015, name = "Factory-15", article_id = 3, status = true, sortIndex = 15, create_date = DateTime.Now })
               .Row(new { id = 3016, name = "Factory-16", article_id = 3, status = true, sortIndex = 16, create_date = DateTime.Now })
               .Row(new { id = 3017, name = "Factory-17", article_id = 3, status = true, sortIndex = 17, create_date = DateTime.Now })
               .Row(new { id = 3018, name = "Factory-18", article_id = 3, status = true, sortIndex = 18, create_date = DateTime.Now })
               .Row(new { id = 3019, name = "Factory-19", article_id = 3, status = true, sortIndex = 19, create_date = DateTime.Now })
               .Row(new { id = 3020, name = "Factory-20", article_id = 3, status = true, sortIndex = 20, create_date = DateTime.Now });

            Insert
              .IntoTable("Section_lang")
              .Row(new
              {
                  section_id = 3000,
                  lang = "tw",
                  content = @"<img class=""mb-4 mb-md-5"" src=""/styles/images/about/stronghold/global.jpg"" alt=""胡連精密"" data-aos=""fade-up"">"
              })
              .Row(new
              {
                  section_id = 3001,
                  lang = "tw",
                  content = @"
                        <h3 class=""font-20 font-weight-bold pb-sm-2"">台北總公司</h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">資本額：</span>
                                                    <span>新台幣 97,158 萬元</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">廠房面積：</span>
                                                    <span>11,257 平方米</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">主要產品項目：</span>
                                                    <span>1. 端子 2. 連接器 3. 套管 4.防水栓 5. 汽車橡膠防塵套6. 繼電器盒 7.保險絲盒 8. 壓著模具</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>新北市汐止區環河街68號</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話：</span>
                                                    <span>
                                                        <a class=""text-hover-green2AB3AC"" href=""tel:+886-2-26940551"">+886-2-26940551</a>
                                                    </span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">傳真：</span>
                                                    <span>+886-2-2694-2526 (台灣業務服務)、<br />+886-2-2694-6248 (國外業務服務)</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.tw"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3002,
                  lang = "tw",
                  content = @"
                            <h3 class=""font-20 font-weight-bold pb-sm-2"">東莞胡連普光貿易有限公司</h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">資本額：</span>
                                                    <span>美金500 萬元</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">廠房面積：</span>
                                                    <span>22,233.27 平方米</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>廣東省東莞市大朗鎮犀牛陂村象山工業園公凹一路7號</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話：</span>
                                                    <span>
                                                        <a class=""text-hover-green2AB3AC"" href=""tel:+86-769-8331-3618"">+86-769-8331-3618</a>、<a class=""text-hover-green2AB3AC"" href=""tel:+86-769-8331-6178"">+86-769-8331-6178</a>
                                                    </span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">傳真：</span>
                                                    <span>+86-769-8331-7018</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3003,
                  lang = "tw",
                  content = @"
                        <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連電子(越南)責任有限公司 <span class=""d-block d-sm-inline"">【越南】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">資本額：</span>
                                                    <span>美金554 萬元</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">廠房面積：</span>
                                                    <span>6,900 平方米</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>Lot XN 28 and 32 Dai An Industrial Zone, Hai Duong Province,Vietnam</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話：</span>
                                                    <span>
                                                        <a class=""text-hover-green2AB3AC"" href=""tel:+84-220-3555-477"">+84-220-3555-477</a>
                                                    </span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">傳真：</span>
                                                    <span>+84-220-3555-476</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">Công ty TNHH Hulane Electronic (Việt Nam)</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">ADD：</span>
                                                    <span>Lô đất XN 28 và 32, KCN Đại An, TP Hải Dương, Tỉnh Hải Dương</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.tw"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3004,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">東莞胡連電子科技有限公司<span class=""d-block d-sm-inline"">【中國大陸】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">資本額：</span>
                                                    <span>美金808 萬元</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">廠房面積：</span>
                                                    <span>31,304 平方米</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>廣東省東莞市大朗鎮犀牛陂村象山工業園公凹一路5號</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3005,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連科技製造有限責任公司<span class=""d-block d-sm-inline"">【印尼】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>Jl. Jababeka II E Blok C No. 16 I dan J, Pasirgombong, Cikarang Utara, Kab. Bekasi, Jawa Barat, #17530 Indonesia</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話：</span>
                                                    <span>
                                                        <a class=""text-hover-green2AB3AC"" href=""tel:+62-21-8983-1816"">+62 21-8983-1816</a>
                                                    </span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.tw"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3006,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連公司【歐洲辦事處】Hu Lane Associate Incorporated</h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""mailto:marketing@hulane.com.tw"" class=""col-12 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3007,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連電子(南京)有限公司<span class=""d-block d-sm-inline"">【中國大陸】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">資本額：</span>
                                                    <span>美金 1,035萬 萬元</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">廠房面積：</span>
                                                    <span>19,108 平方米</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>江蘇省南京市江寧區祿口鎮空港工業園來鳳路 28 號</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3008,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【香港分公司】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話：</span>
                                                    <span>
                                                        <a class=""text-hover-green2AB3AC"" href=""tel:+852-26873228"">+852-26873228</a>
                                                    </span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">傳真：</span>
                                                    <span>+852-26871576</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>香港新界沙田火炭山尾街31號華東工業中心B座20樓28室</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3009,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【上海聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話：</span>
                                                    <span>
                                                        <a class=""text-hover-green2AB3AC"" href=""tel:+86-21-59561941,,2"">+86-21-59561941~2</a>
                                                    </span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">傳真：</span>
                                                    <span>+86-21-59561943</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>上海市嘉定區安智路155弄萬創坊4層403室</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3010,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【武漢聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-27-84799960</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>武漢市經濟技術開發區東風大道嘎纳印象4棟1904室</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3011,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【重慶聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-23-67141023</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>重慶市渝北區湖云路12號保利高爾夫豪園15棟1單元14-1</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3012,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【華北聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-312-3155173</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>河北省保定市蓮池區城苑路222號財苑小區2號樓3單元602室</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3013,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【蕪湖聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-553-5907127</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>安徽省蕪湖市經濟技術開發區波爾卡國際花園52幢2單元201室</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3014,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【福州聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-591-22778691</span>
                                                    <span class=""d-block font-weight-bold flex-shrink-0 ml-sm-3"">東南網：</span>
                                                    <span>1136
                                                    </span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>福州市閩侯縣青口鎮吉山北路6號</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3015,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【長春聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-431-84450299</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>長春市綠園區南陽路新奧藍城47棟1門1102室</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3016,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【西安聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-29-89024920</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>陝西省西安市戶縣草堂鎮水堡新村</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3017,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【柳州聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-772-3595356</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>廣西柳州市桂中大道陽光100城市廣場十棟12-4房</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3018,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【長沙聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-731-86189449</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>湖南省長沙市星沙開元東路恒基凱旋門二期23棟2404室</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3019,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【臺州聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-576-82356206
                                                    </span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>浙江省台州市路橋區螺洋街道雙廟村香樟源36幢2單元803</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              })
              .Row(new
              {
                  section_id = 3020,
                  lang = "tw",
                  content = @"
                    <h3 class=""font-20 font-weight-bold pb-sm-2"">胡連精密股份有限公司<span class=""d-block d-sm-inline"">【鄭州聯絡處】</span></h3>
                                            <ul class=""noneStyle mb-4 pb-sm-2"">
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">電話及傳真：</span>
                                                    <span>+86-371-55309707</span>
                                                </li>
                                                <li class=""d-sm-flex"">
                                                    <span class=""d-block font-weight-bold flex-shrink-0"">地址：</span>
                                                    <span>河南省郑州市鄭東新區東風南路与永平路交叉口鑫苑鑫城1棟1單元2607</span>
                                                </li>
                                            </ul>
                                            <div class=""col-12 flex-center bg-white box-shadow-style-1 rounded-5 px-xl-5 py-sm-3 mb-2 mb-sm-0"">
                                                <div class=""row flex-grow"">
                                                    <a href=""#"" target=""_blank"" class=""col-12 col-sm-6 flex-center border-right-sm-C8C8C8 border-bottom-C8C8C8 border-bottom-sm-0 text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""flex-center bg-black rounded-circle mr-2"" style=""width:30px;height:30px;"">
                                                            <i class=""icon-location text-green2AB3AC""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Google Map</span>
                                                    </a>
                                                    <a href=""mailto:marketing@hulane.com.cn"" class=""col-12 col-sm-6 flex-center text-hover-green2AB3AC py-2 py-sm-0"">
                                                        <span class=""d-block text-green2AB3AC mr-2"">
                                                            <i class=""icon-mail font-20""></i>
                                                        </span>
                                                        <span class=""font-weight-bold"">Email 聯絡我們</span>
                                                    </a>
                                                </div>
                                            </div>"
              });

        }
    }
}
