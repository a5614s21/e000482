﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.Main.Migration
{
    [Migration(2021072001)]
    public class CreateProduct : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Table("Product");
        }

        public override void Up()
        {
            Create.Table("Product")
               .WithColumn("id").AsInt32().PrimaryKey()
               .WithColumn("name").AsString(32).NotNullable().Unique()
               .WithColumn("showNew").AsBoolean().NotNullable().WithDefaultValue(true).WithColumnDescription("是否最新")
               .WithColumn("showMain").AsBoolean().NotNullable().WithDefaultValue(true).WithColumnDescription("是否顯示在首頁")
               .WithColumn("img").AsString(250).Nullable().WithColumnDescription("產品圖片")
               .WithColumn("img_url").AsString(250).Nullable().WithColumnDescription("產品圖片說明")
               .WithColumn("create_date").AsString(35).NotNullable().WithColumnDescription("創建時間");
        }
    }
}
