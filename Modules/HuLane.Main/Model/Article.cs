﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.Main.Model
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public string DefaultLang { get; set; }

        public List<Section> Sections { get; set; } = new List<Section>();
    }
}
