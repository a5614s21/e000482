﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.Main.Model
{
    public class Section
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public bool Status { get; set; }
        public int SortIndex { get; set; }

        public string Content { get; set; }

        public int? ParentId { get; set; }
    }
}
