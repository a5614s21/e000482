﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.Main.Model
{
    public class SectionLang
    {
        public int Id { get; set; }
        public string Lang { get; set; }
        public string Content { get; set; }
    }
}
