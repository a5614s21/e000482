﻿using HuLane.BuildBlock;
using HuLane.Main.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace HuLane.Main.Repositories
{
    public class ArticleRepository : RepositoryBase,IArticleRepository
    {
        public ArticleRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        { 
        
        }

        public Article GetArticle(string name)
        {
            Article result = null;
            using (var conn = ConnectionFactory.CreateBackendDBConnection())
            {
                string sql =
                    @"select a.id,a.name,a.status,a.defaultLang,b.id,b.name,b.status,b.sortIndex,b.parentId,c.content
                        from article a 
                        inner join section b on a.id = b.article_id 
                        left join section_lang c on b.id = c.section_id and c.lang = a.defaultLang
                        where a.name = @name and a.status = 1 and b.status = 1
                        order by b.id,b.sortIndex";

                var articleDictionary = new Dictionary<int, Article>();


                var list = conn.Query<Article, Section, Article>(
                    sql,
                    (article, section) =>
                    {
                        Article articleEntry;

                        if (!articleDictionary.TryGetValue(article.Id, out articleEntry))
                        {
                            articleEntry = article;
                            articleEntry.Sections = new List<Section>();
                            articleDictionary.Add(articleEntry.Id, articleEntry);
                        }

                        articleEntry.Sections.Add(section);
                        return articleEntry;
                    }, new { name = name },
                    splitOn: "id")
                .Distinct()
                .ToList();

                result = list.FirstOrDefault();
            }

            return result;
        }
    }
}
