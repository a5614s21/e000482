﻿using HuLane.Main.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.Main.Repositories
{
    public interface IArticleRepository
    {
        Article GetArticle(string name);
    }
}
