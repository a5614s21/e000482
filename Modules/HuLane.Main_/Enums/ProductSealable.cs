﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.Main.Enums
{
    public enum ProductSealable
    {
        UnDefined = -1,
        Yes = 0,
        No = 1,
        NA = 2
    }
}
