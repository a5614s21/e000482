﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.Main.Enums
{
    public enum ProductTable
    {
        Undefined = -1,
        All = 0, 
        Housing = 1, 
        Terminal = 2, 
        Rubber = 3, 
        Sleeve = 4,
        Grommet = 5
    }
}
