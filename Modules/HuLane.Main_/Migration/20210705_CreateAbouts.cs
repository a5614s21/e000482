﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuLane.Main.Migration
{
    [Migration(2021070501)]
    public class CreateAbouts : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Table("Abouts");
            Delete.Table("Abouts_lang");
        }

        public override void Up()
        {
            Create.Table("Abouts")
                .WithColumn("id").AsByte().PrimaryKey()
                .WithColumn("section").AsString().NotNullable()
                .WithColumn("status").AsBoolean().NotNullable().WithDefaultValue(true).WithColumnDescription("是否出現")
                .WithColumn("sortIndex").AsInt32().NotNullable().WithColumnDescription("頁面排序")
                .WithColumn("defaultLang").AsString(2).NotNullable().WithColumnDescription("預設語言，找不到會用預設的")
                .WithColumn("create_date").AsDateTime().NotNullable().WithColumnDescription("創建時間")
                .WithColumn("modify_date").AsDateTime().Nullable().WithColumnDescription("修改時間");

            Create.Table("Abouts_lang")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("abouts_id").AsByte().NotNullable()
                .WithColumn("lang").AsString(2).NotNullable()
                .WithColumn("content").AsString().WithColumnDescription("是否出現");

            //Insert
            //    .IntoTable("Abouts")
            //    .Row(new { id = 0, section = "About", status = true, sortIndex = 0, defaultLang = "tw", create_date = DateTime.Now });


            //Insert
            //    .IntoTable("Abouts_lang")
            //    .Row(new { abouts_id = 0, lang="tw" ,content = @"<div class=""max1200"">
            //            <h2 class=""font-30 font-weight-bold border-bottom-red pb-2 pb-sm-3 pb-lg-4 mb-2 mb-sm-3 mb-lg-4\"" data-aos=""fade-up"">關於胡連</h2>
            //            <div class=""pt-lg-2\"">
            //                <h3 class=""font-26 font-weight-bold"" data-aos=""fade-up"">Since 1977</h3> 
            //                <p data-aos=""fade-up"">胡連精密成立於1977年，創立時從事金屬沖壓端子零件產品，於2004年涉入塑膠連接器產品，如今已成為汽機車業界專業連接器廠；並跨足醫療、太陽能等各方面領域。胡連不斷努力精進提高模具、製程等相關技術開發能力，包括點焊端子、電動車連接器、保險絲盒、壓著機、橡膠件之自製開發產品，並拓展銷售於全球市場。</p>
            //                <div class=""background-no-repeat background-position-center border-bottom-C8C8C8 pt-lg-2 my-4 my-lg-5"" style=""background-image:url(styles/images/about/global.svg);background-size:80%"">
            //                    <ul class=""row noneStyle text-center justify-center"">
            //                        <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
            //                            <div class=""shadow-style-1 mb-2 pb-sm-1"">
            //                                <img src = ""styles/images/about/address-1.png"" alt=""胡連精密"">
            //                            </div>
            //                            <p class=""font-20 font-weight-bold lh14 mb-0"">台北總公司</p>
            //                            <p class=""mb-0"">TAIPEI Headquarters</p>
            //                        </li>
            //                        <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
            //                            <div class=""shadow-style-1 mb-2 pb-sm-1"">
            //                                <img src = ""styles/images/about/address-2.png"" alt=""胡連精密"">
            //                            </div>
            //                            <p class=""font-20 font-weight-bold lh14 mb-0"">台北新廠</p>
            //                            <p class=""mb-0"">TAIPEI New Plant</p>
            //                        </li>
            //                        <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
            //                            <div class=""shadow-style-1 mb-2 pb-sm-1"">
            //                                <img src = ""styles/images/about/address-3.png"" alt=""胡連精密"">
            //                            </div>
            //                            <p class=""font-20 font-weight-bold lh14 mb-0"">南京廠</p>
            //                            <p class=""mb-0"">Nanjing Plant</p>
            //                        </li>
            //                        <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
            //                            <div class=""shadow-style-1 mb-2 pb-sm-1"">
            //                                <img src = ""styles/images/about/address-4.png"" alt=""胡連精密"">
            //                            </div>
            //                            <p class=""font-20 font-weight-bold lh14 mb-0"">東莞廠</p>
            //                            <p class=""mb-0"">Dongguan Plant</p>
            //                        </li>
            //                        <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
            //                            <div class=""shadow-style-1 mb-2 pb-sm-1"">
            //                                <img src = ""styles/images/about/address-5.png"" alt=""胡連精密"">
            //                            </div>
            //                            <p class=""font-20 font-weight-bold lh14 mb-0"">越南廠</p>
            //                            <p class=""mb-0"">Vietnam Plant</p>
            //                        </li>
            //                        <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
            //                            <div class=""shadow-style-1 mb-2 pb-sm-1"">
            //                                <img src = ""styles/images/about/address-6.png"" alt=""胡連精密"">
            //                            </div>
            //                            <p class=""font-20 font-weight-bold lh14 mb-0"">印尼廠</p>
            //                            <p class=""mb-0"">Indonesia Plant</p>
            //                        </li>
            //                        <li class=""col-12 col-sm-6 col-md-4 col-xl-3 pb-4 pb-lg-5 mb-xl-4"" data-aos=""fade-up"">
            //                            <div class=""shadow-style-1 mb-2 pb-sm-1"">
            //                                <img src = ""styles/images/about/address-7.png"" alt=""胡連精密"">
            //                            </div>
            //                            <p class=""font-20 font-weight-bold lh14 mb-0"">歐洲辦事處</p>
            //                            <p class=""mb-0"">EU Sales Office</p>
            //                        </li>
            //                    </ul>
            //                </div>
            //            </div>
            //            <div class=""row"">
            //                <div class=""col-12 col-lg-7 pr-xl-0 mb-3 mb-sm-4 mb-lg-0"">
            //                    <h3 class=""font-26 font-weight-bold"" data-aos=""fade-up"">創新胡連，連接世界 Connecting Worldwide</h3>
            //                    <p class=""mb-2"" data-aos=""fade-up"">胡連擁有數十年技術經驗及高效率之銷售服務團隊，以台北為總公司，大陸於南京及東莞、東南亞於越南、印尼設立工廠及銷售團隊，形成堅強全球團隊，在業界建立良好商譽。為提供可靠及實用的產品，除了管理體系通過多項國際品質管理認證外，產品亦符合RoHS等相關標準，給予客戶品質信賴的產品。</p>
            //                    <p class=""mb-2"" data-aos=""fade-up"">胡連秉持「誠信務實、技術創新、客戶滿意、共榮共存、放眼天下」之經營理念；「同仁團隊合作、勇於挑戰、追求卓越」的企業精神、及四大發展方針『品質』、『專業』、『效率』、『服務』作為動力，持續向上成長。為滿足客戶的期望努力、求新求變實現 “Connecting Worldwide”，讓胡連連接器永續發展，連結全世界！</p>
            //                </div>
            //                <div class=""col-12 col-lg-5"">
            //                    <div class=""pl-xl-5"">
            //                        <div data-aos=""fade-up"">
            //                            <div class=""video-element"">
            //                                <iframe class=""border-0 w-100"" src=""https://www.youtube.com/embed/ykEbgOojWiU?loop=1&playlist=ykEbgOojWiU"" allowfullscreen></iframe>
            //                            </div>
            //                        </div>
            //                    </div>
            //                </div>
            //            </div>
            //        </div>" });
        }
    }
}
