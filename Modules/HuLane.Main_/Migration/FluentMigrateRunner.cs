﻿//using FluentMigrator.Runner;
//using FluentMigrator.Runner.Announcers;
//using FluentMigrator.Runner.Initialization;
//using FluentMigrator.Runner.Processors;
//using FluentMigrator.Runner.Processors.SqlServer;
//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Linq;
//using System.Runtime.InteropServices;
//using System.Text;
//using System.Threading.Tasks;

//namespace HuLane.Main.Migration
//{
//    public static class FluentMigrateRunner
//    {
//        public static void Up(int? version = null)
//        {
//            string connectionString = ConfigurationManager.ConnectionStrings["backendDB"].ConnectionString;
//            var announcer = new TextWriterAnnouncer(s => System.Diagnostics.Debug.WriteLine(s));
//            announcer.ShowSql = true;

//            var assembly = typeof(CreateAbouts).Assembly;
//            var options = new ProcessorOptions();

//            var factory = new SqlServer2012ProcessorFactory();
//            using (var processor = factory.Create(connectionString, announcer, options))
//            {
//                var migrationContext = new RunnerContext(announcer);

//                var runner = new MigrationRunner(assembly, migrationContext, processor);

//                if(version != null)
//                    runner.MigrateUp(version.Value);
//                else
//                    runner.MigrateUp();
//            }
//        }

//        public static void Down(int version)
//        {
//            string connectionString = ConfigurationManager.ConnectionStrings["backendDB"].ConnectionString;
//            var announcer = new ConsoleAnnouncer();
//            announcer.ShowSql = true;

//            var assembly = typeof(CreateAbouts).Assembly;
//            var options = new ProcessorOptions();

//            var factory = new SqlServerProcessorFactory();
//            using (var processor = factory.Create(connectionString, announcer, options))
//            {
//                var migrationContext = new RunnerContext(announcer);

//                var runner = new MigrationRunner(assembly, migrationContext, processor);
//                runner.MigrateDown(version);
//            }
//        }
//    }
//}
