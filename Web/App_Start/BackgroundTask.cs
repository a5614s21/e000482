﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Web.App_Start
{
    public static class BackgroundTask
    {
        public static void SyncProduct() 
        {
            using (var connPro = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
            using (var connBack = new SqlConnection(ConfigurationManager.ConnectionStrings["backendDB"].ConnectionString))
            {
                string sql = "select top 1 create_date from product order by create_date desc";

                var lastTime = connBack.QuerySingleOrDefault<string>(sql);

                sql = @"select p.id,p.name,p.create_date from product p
                    where p.create_date > @lastTime";

                if (lastTime == null)
                    lastTime = "1900年01月01日";

                var sourceProducts = connPro.Query(sql, new { lastTime = lastTime});

                sql = "Insert into Product(id,name,create_date) values(@id,@name,@createDate)";

                foreach (var product in sourceProducts)
                {
                    connBack.Execute(sql, new { id = product.id, name = product.name, createDate = product.create_date });
                }
            }
        }
    }
}