﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(null, "connector", new { controller = "Files", action = "Index" });
            routes.MapRoute(null, "Thumbnails/{tmb}", new { controller = "Files", action = "Thumbs", tmb = UrlParameter.Optional });

            routes.MapRoute(
              name: "analytics",
              url: "siteadmin/analytics/{key}",
              defaults: new { controller = "Siteadmin", action = "analytics", key = UrlParameter.Optional }
          );

            routes.MapRoute(
                     name: "SiteadminCkAccount",
                     url: "siteadmin/ckAccount/{tokens}",
                     defaults: new { controller = "Siteadmin", action = "CkAccount", tokens = UrlParameter.Optional }
                 );

            routes.MapRoute(
                  name: "SiteadminUploadToWeb",
                  url: "siteadmin/fileUpload",
                  defaults: new { controller = "Upload", action = "Upload", id = UrlParameter.Optional, FilePath = "" }
              );

            routes.MapRoute(
                name: "SiteadminMembersList",
                url: "siteadmin/member/{action}/{id}",
                defaults: new { controller = "AdminMember", id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "SiteadminSurveyList",
                url: "siteadmin/survey/{action}/{id}",
                defaults: new { controller = "Survey", id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "SiteadminInquireList",
                url: "siteadmin/inquire/{action}/{id}",
                defaults: new { controller = "Inquire", id = UrlParameter.Optional }
            );


            routes.MapRoute(
             name: "SiteadminContactList",
             url: "siteadmin/contact/{action}/{id}",
             defaults: new { controller = "Contacts", id = UrlParameter.Optional }
         );



            routes.MapRoute(
                name: "SiteadminList",
                url: "siteadmin/{tables}/{action}/{id}",
                defaults: new { controller = "Siteadmin", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Siteadmin",
                url: "siteadmin/{action}/{id}",
                defaults: new { controller = "Siteadmin", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "SiteadminIndex",
             url: "siteadmin",
             defaults: new { controller = "Siteadmin", action = "Index", id = UrlParameter.Optional }
         );

            routes.MapRoute(
                name: "HomeAjax",
                url: "Ajax",
                defaults: new { controller = "Home", action = "Ajax", id = UrlParameter.Optional }
            );

        routes.MapRoute(
             name: "HomeNoLang",
             url: "",
             defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
         );

            routes.MapRoute(
             name: "Home",
             url: "{lang}",
             defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
         );
            /*
            routes.MapRoute(
           name: "LangToController4",
           url: "{lang}/{controller}/{action}/{id}/{guid}/{subid}/{lastid}",
           defaults: new { id = UrlParameter.Optional, guid = UrlParameter.Optional, subid = UrlParameter.Optional, lastid = UrlParameter.Optional }
       );

            routes.MapRoute(
              name: "LangToController3",
              url: "{lang}/{controller}/{action}/{id}/{guid}/{subid}",
              defaults: new { id = UrlParameter.Optional, guid = UrlParameter.Optional, subid = UrlParameter.Optional }
          );

            routes.MapRoute(
               name: "LangToController2",
               url: "{lang}/{controller}/{action}/{id}/{guid}",
               defaults: new { id = UrlParameter.Optional, guid = UrlParameter.Optional }
           );
              */
            routes.MapRoute(
               name: "LangToController",
               url: "{lang}/{controller}/{action}/{id}",
               defaults: new { id = UrlParameter.Optional }
           );

         
        

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { id = UrlParameter.Optional }
            );


            routes.MapMvcAttributeRoutes();
        }
    }
}
