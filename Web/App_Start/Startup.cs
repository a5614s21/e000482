﻿using Autofac;
using Autofac.Integration.Mvc;
using Hangfire;
using HuLane.BuildBlock;
using HuLane.Main.Repositories;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;

[assembly: OwinStartup(typeof(Web.App_Start.Startup))]

namespace Web.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app) 
        {
           // GlobalConfiguration.Configuration.UseSqlServerStorage("backendDB");
           // app.UseHangfireDashboard();
           // app.UseHangfireServer();
        }
    }
}