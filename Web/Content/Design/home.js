﻿$(document).ready(function () {


    //重製驗證碼

    $('#captcha').click(function () {

        var src = $('#captcha').attr('src').split('?');

        $('#captcha').attr('src', src[0] + "?d=" + Date.now());

    });

    $("body").delegate(".changeCity", "change", function () {

        var valsTemp = new Array();

        var arr = new Object();
        arr.name = "city";
        arr.value = $(this).val();
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "category";
        arr.value = $(this).attr('data-category');
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "type";
        arr.value = $(this).attr('data-type');
        valsTemp.push(arr);

        var vals = JSON.stringify(valsTemp);
        useAjax('changeCity', vals);
        return false;

    });

    $("body").delegate(".openUrl", "click", function () {
        
        window.open($(this).attr('data-url'), '_blank');

    });


    $("body").delegate(".removeService", "click", function () {

        var valsTemp = new Array();

        var arr = new Object();
        arr.name = "services_guid";
        arr.value = $(this).attr('data-guid');
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "ad_id";
        arr.value = $(this).attr('data-adid');
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "type";
        arr.value = 'del';
        valsTemp.push(arr);

        var vals = JSON.stringify(valsTemp);

        useAjax('myFavorite', vals);



    });

    $("body").delegate(".myFavorite", "click", function () {

 


        var valsTemp = new Array();

        var arr = new Object();
        arr.name = "services_guid";
        arr.value = $(this).attr('data-guid');
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "ad_id";
        arr.value = $(this).attr('data-adid');
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "type";
        arr.value = $(this).attr('data-type');
        valsTemp.push(arr);


        if ($(this).attr('data-type') == "add") {
            $(this).attr('data-type', 'del')
        }
        else {
            $(this).attr('data-type', 'add');
        }

        var vals = JSON.stringify(valsTemp);
        useAjax('myFavorite', vals);


  
    });


    $("#contactForm").each(function () {


        $("#contactForm").validate({


            submitHandler: function (form) {


                $('.submitBut').attr("disabled", true);

                return true;
            },
            errorPlacement: function (error, element) {
                element.attr('style', 'border:#FF0000 1px solid;');

                element.next("._formErrorMsg").html('<div style="color: #FF0000; padding-bottom: 10px; padding-left: 10px;">' + error.text() + '</div>');


            },
            success: function (error) {

                var findID = $('#' + error[0].id.replace('-error', ''));

                //console.log(error[0].id);

                $(findID).attr('style', '');
                findID.next(".formNotice").html('');
                return false;

            }
        });

    });
});




/**
 * AJAX動作
 * @param ACT
 * @param needVal
 */
function useAjax(ACT, needVal) {



    $.ajax({
        type: 'POST',
        url: $('#ajaxRoot').val(),
        data: {
            Func: ACT,
            Val: encodeURI(needVal)
        },
        //cache: false, 
        dataType: 'json',
        beforeSend: function () {



        },
        success: function (json) {


            //console.log(json);

            //setLoadPlayer("none");


            switch (json.Func) {


                case "calculation":


                    if (json.re != "OK") {


                            swal({
                                title: json.msg,
                                text:'',
                                type: 'error',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'OK',
                            });
                        $('.submitBut').attr("disabled", false);
                    
                    }
                    else {


                       //console.log(json.YearPrice);

                        var appendStr = "";
                        $(".type" + json.type).show();
                        if (json.type == "1") {
                            $.each(json.data, function (i, item) {
                                $.each(item, function (s, subItem) {

                                    var rowspan = '';
                                    if (s == 0) {
                                        rowspan = '<td rowspan="12">' + i + '</td>';
                                    }

                                    appendStr = appendStr + '<tr>' + rowspan + '<td>' + subItem.month + '</td><td>' + subItem.principal + '</td><td>' + subItem.interest + '</td><td>' + subItem.month_price + '</td></tr>';

                                });
                            });

                            appendStr = appendStr + '<tr><td>' + json.total + '</td ><td>-</td><td>' + json.principal_all + '</td><td>' + json.interest_all + '</td><td>' + json.month_price_all + '</td></tr>';
                       
                        }
                   
                        if (json.type == "2") {
                            $.each(json.data, function (i, item) {
                                $.each(item, function (s, subItem) {

                                    var rowspan = '';
                                    if (s == 0) {
                                        rowspan = '<td rowspan="12">' + i + '</td>';
                                    }

                                    appendStr = appendStr + '<tr>' + rowspan + '<td>' + subItem.month + '</td><td>' + subItem.principal + '</td><td>' + subItem.tired + '</td><td>' + subItem.interest + '</td><td>' + subItem.month_price + '</td></tr>';

                                });

                            });

                            appendStr = appendStr + '<tr><td>' + json.total + '</td ><td>-</td><td>' + json.principal_all + '</td><td></td><td>' + json.interest_all + '</td><td>' + json.month_price_all + '</td></tr>';

                        }

                        if (json.type == "3") {
                            $.each(json.data, function (i, item) {
                                $.each(item, function (s, subItem) {
                                    var rowspan = '';
                                    if (s == 0) {
                                        rowspan = '<td rowspan="12">' + i + '</td>';
                                    }

                                    appendStr = appendStr + '<tr>' + rowspan + '<td>' + subItem.month + '</td><td>' + subItem.principal + '</td><td>' + subItem.interest + '</td><td>' + subItem.month_price + '</td></tr>';
                                });
                            });

                            appendStr = appendStr + '<tr><td>' + json.total + '</td ><td>-</td><td>' + json.principal_all + '</td><td>' + json.interest_all + '</td><td>' + json.month_price_all + '</td></tr>';
                        }

                        $('#type' + json.type + '_list').html(appendStr);
                        $('#type' + json.type + '_ever_month').text(json.every_month);
                        


                        //event.preventDefault();
                        $(".calculate_results").slideDown();

                        $('.submitBut').attr("disabled", false);
                    }


                    break;


                case "changeCity":

                    var district = $.parseJSON(json.district);
                    console.log(district);
                    $select = $("#district");
                    $select.html("");
                    $select.append('<option value="" selected>行政區</option>');
                    for (var i = 0; i < district.length; i++) {

                       /* var selected = "";
                        if (i == 0) {
                            selected = " selected";
                        }*/
                        $select.append('<option value="' + district[i] + '" >' + district[i] + '</option>');
                    }                
                       
                
                    return false;

                    break;




                case "myFavorite":

                    //myFavoriteList
                   
                    if (json.type == "add") {
                        if (json.reText != "") {
                            $('#myFavoriteList').append(json.reText);

                            $(".page-index ul>li .heart").on("click", function () {
                                $(this).toggleClass("red");
                              
                            });

                        }

                    }
                    else {
                        $('#service_' + json.services_guid).remove();

                        $('.myFavorite').each(function () {

                            if ($(this).attr('data-guid') == json.services_guid) {

                                $(this).removeClass('red');
                            }


                        });


                    }

                    if ($('#myFavoriteList').find('.addList').html() != null) {

                        $('#noAddFavorite').addClass('hidden');
                    }
                    else {
                        $('#noAddFavorite').removeClass('hidden');
                    }


                    
                    break;





            }


        },
        complete: function () { //生成分頁條

        },
        error: function () {
            //alert("讀取錯誤!");
        }
    });

}


function setLoadPlayer(view) {
    if (view == 'none') {
        $.unblockUI();
    }
    else {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}


var keyStr = "ABCDEFGHIJKLMNOP" +
    "QRSTUVWXYZabcdef" +
    "ghijklmnopqrstuv" +
    "wxyz0123456789+/" +
    "=";