﻿using HuLane.BuildBlock;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuLane.Main.Repositories;
using Web.Models;
using Scriban;
using Web.DB.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace Web.Controllers
{
    public class AboutController : BaseController
    {
        
        protected Dictionary<string, string> active = new Dictionary<string, string>();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            headerActive.Add("about", "active");
            ViewBag.headerActive = headerActive;//header active
            ViewBag.ResLang = langData;//取得語系檔案
        }

            // GET: About
        public ActionResult Index()
        {
            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
            //conn.Open();
            //SqlCommand cmd = new SqlCommand("select * from product", conn);
            //SqlDataReader reader = cmd.ExecuteReader();

            //while (reader.Read())
            //{
            //    var productid = Int32.Parse(reader[0].ToString());
            //    var productName = reader[6].ToString();

            //    var productNew = (from a in DB.product_info
            //                      where a.id == productid
            //                      select a).FirstOrDefault();
            //    if (productNew == null)
            //    {
            //        product_info c = new product_info();
            //        product_info c2 = new product_info();
            //        product_info c3 = new product_info();
            //        var newGid = Guid.NewGuid().ToString();
            //        c.guid = newGid;
            //        c.product_id = productid;
            //        c.create_date = DateTime.Now;
            //        c.lang = "tw";
            //        c.newName = productName;
            //        c2.newName = productName;
            //        c3.newName = productName;

            //        SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
            //        conn2.Open();
            //        string filecheck = "picture1";
            //        string productfilename;
            //        string img_sql = "select * from product_file where product_id= " + productid + " and filecheck= '" + filecheck + "'";
            //        SqlCommand cmd2 = new SqlCommand(img_sql, conn2);
            //        SqlDataReader reader2 = cmd2.ExecuteReader();

            //        if (reader2.Read())
            //        {
            //            productfilename = reader2[2].ToString();
            //            if (productfilename != "")
            //            {
            //                var temp = productfilename.Split('.');
            //                var productImg = "/Content/Upload/images/product/" + productid + "/" + temp[0] + "_WM." + temp[1];
            //                c.pic = productImg;
            //                c2.pic = productImg;
            //                c3.pic = productImg;
            //            }
            //        }

            //        conn2.Close();

            //        SqlConnection conn3 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
            //        conn3.Open();
            //        string filecheck2 = "picture2";
            //        string productspc;
            //        string img_sql2 = "select * from product_file where product_id= " + productid + " and filecheck= '" + filecheck2 + "'";
            //        SqlCommand cmd3 = new SqlCommand(img_sql2, conn3);
            //        SqlDataReader reader3 = cmd3.ExecuteReader();

            //        if (reader3.Read())
            //        {
            //            productspc = reader3[2].ToString();
            //            if (productspc != "")
            //            {
            //                var temp = productspc.Split('.');
            //                var productSpc = "/Content/Upload/images/product/" + productid + "/" + temp[0] + "_WM." + temp[1];
            //                c.spec_pic = productSpc;
            //                c2.spec_pic = productSpc;
            //                c3.spec_pic = productSpc;
            //            }
            //        }

            //        conn3.Close();

            //        SqlConnection conn5 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
            //        conn5.Open();
            //        string filecheck3 = "picture3";
            //        string productpaper;
            //        string img_sql3 = "select * from product_file where product_id= " + productid + " and filecheck= '" + filecheck3 + "'";
            //        SqlCommand cmd5 = new SqlCommand(img_sql3, conn5);
            //        SqlDataReader reader5 = cmd5.ExecuteReader();

            //        if (reader5.Read())
            //        {
            //            productpaper = reader5[2].ToString();
            //            if (productpaper != "")
            //            {
            //                var temp = productpaper.Substring(0, productpaper.Length - 4);
            //                var temp2 = productpaper.Substring(productpaper.Length - 4);
            //                var productPaper = "/Content/Upload/images/product/" + productid + "/" + temp + "_WM" + temp2;
            //                c.paper_pic = productPaper;
            //                c2.paper_pic = productPaper;
            //                c3.paper_pic = productPaper;
            //            }
            //        }

            //        conn5.Close();

            //        DB.product_info.Add(c);
            //        DB.SaveChanges();

                    
            //        c2.guid = newGid;
            //        c2.product_id = productid;
            //        c2.create_date = DateTime.Now;
            //        c2.lang = "cn";

            //        DB.product_info.Add(c2);
            //        DB.SaveChanges();

            //        c3.guid = newGid;
            //        c3.product_id = productid;
            //        c3.create_date = DateTime.Now;
            //        c3.lang = "en";

            //        DB.product_info.Add(c3);
            //        DB.SaveChanges();
            //    }
            //}
            active.Add("about", "class=\"active\"");
            ViewBag.active = active;
            var changeLang = langData["語系"].ToString();
            ViewBag.data = Repository.About.About.Single(changeLang ,"1" );
            ViewBag.title = ViewBag.data.title;

            var seoDescription = ViewBag.data.seo_description;
            ViewBag.description = seoDescription;
            var seoKeywords = ViewBag.data.seo_keywords;
            ViewBag.keywords = seoKeywords;

            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult History()
        {
            active.Add("history", "class=\"active\"");
            ViewBag.active = active;
            ViewBag.title = "胡連沿革";
            var changeLang = langData["語系"].ToString();
            List<history_category> categories = DB.history_category.Where(m => m.lang == changeLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            List<history> data = DB.history.Where(m => m.lang == changeLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();

            ViewBag.categories = categories;
            ViewBag.data = data;

            return View();
        }
        /// <summary>
        /// 企業願景
        /// </summary>
        /// <returns></returns>
        public ActionResult Overview() 
        {
            active.Add("overview", "class=\"active\"");
            ViewBag.active = active;
            var changeLang = langData["語系"].ToString();
            ViewBag.data = Repository.About.About.Single(changeLang, "2");
            ViewBag.title = ViewBag.data.title;

            var seoDescription = ViewBag.data.seo_description;
            ViewBag.description = seoDescription;
            var seoKeywords = ViewBag.data.seo_keywords;
            ViewBag.keywords = seoKeywords;

            return View();
        }
        /// <summary>
        /// 全球據點
        /// </summary>
        /// <returns></returns>
        public ActionResult Stronghold()
        {
            active.Add("stronghold", "class=\"active\"");
            ViewBag.active = active;
            var changeLang = langData["語系"].ToString();
            ViewBag.data = Repository.About.About.Single(changeLang, "3");
            ViewBag.title = ViewBag.data.title;

            ViewBag.location = DB.locations.Where(m => m.lang == changeLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();


            var seoDescription = ViewBag.data.seo_description;
            ViewBag.description = seoDescription;
            var seoKeywords = ViewBag.data.seo_keywords;
            ViewBag.keywords = seoKeywords;

            return View();
        }
        /// <summary>
        /// 品質榮耀
        /// </summary>
        /// <returns></returns>
        public ActionResult Glory()
        {
            active.Add("glory", "class=\"active\"");
            ViewBag.active = active;
            var changeLang = langData["語系"].ToString();
            ViewBag.data = Repository.About.About.Single(changeLang, "4");
            ViewBag.title = ViewBag.data.title;

            var seoDescription = ViewBag.data.seo_description;
            ViewBag.description = seoDescription;
            var seoKeywords = ViewBag.data.seo_keywords;
            ViewBag.keywords = seoKeywords;

            return View();
        }

        /// <summary>
        /// 技術發展
        /// </summary>
        /// <returns></returns>
        public ActionResult Technology()
        {
            active.Add("technology", "class=\"active\"");
            ViewBag.active = active;
            var changeLang = langData["語系"].ToString();
            ViewBag.data = Repository.About.About.Single(changeLang, "5");
            ViewBag.title = ViewBag.data.title;

            var seoDescription = ViewBag.data.seo_description;
            ViewBag.description = seoDescription;
            var seoKeywords = ViewBag.data.seo_keywords;
            ViewBag.keywords = seoKeywords;

            return View();
        }

        /// <summary>
        /// 胡連組織
        /// </summary>
        /// <returns></returns>
        public ActionResult Organization()
        {
            active.Add("organization", "class=\"active\"");
            ViewBag.active = active;
            var changeLang = langData["語系"].ToString();
            ViewBag.data = Repository.About.About.Single(changeLang, "6");
            ViewBag.title = ViewBag.data.title;

            var seoDescription = ViewBag.data.seo_description;
            ViewBag.description = seoDescription;
            var seoKeywords = ViewBag.data.seo_keywords;
            ViewBag.keywords = seoKeywords;

            return View();
        }

        /// <summary>
        /// 經營團隊
        /// </summary>
        /// <returns></returns>
        public ActionResult Team()
        {
            active.Add("team", "class=\"active\"");
            ViewBag.active = active;
            var changeLang = langData["語系"].ToString();
            ViewBag.data = Repository.About.About.Single(changeLang, "7");
            ViewBag.title = ViewBag.data.title;

            var seoDescription = ViewBag.data.seo_description;
            ViewBag.description = seoDescription;
            var seoKeywords = ViewBag.data.seo_keywords;
            ViewBag.keywords = seoKeywords;

            return View();
        }
    }
}