﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.DB.Models;
using Newtonsoft.Json;
using Web.Service;
using System.Collections.Specialized;
using System.Collections;
using System.Web.UI;
using Web.Repository;
using System.Configuration;
using Google.Apis.Auth.OAuth2;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Web.Script.Serialization;
using Web.ServiceModels;
using System.Web.Caching;
using Web.AdminService;
using System.Diagnostics;

namespace Web.Controllers
{
    public class AdminMemberController : Controller
    {
        public string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;

        //預設語系
        public string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString;

        //後台登入記錄方式 Session or Cookie
        public string projeftNo = ConfigurationManager.ConnectionStrings["projeftNo"].ConnectionString;

        public int cacheTime = 15;
        public string loginLastToUri = "";

        private Model DB = new Model();

        //更改較少資訊Cache使用
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //登入判斷
            ViewBag.projeftNo = projeftNo;

            ViewBag.username = "";
            ViewBag.role_guid = null;

            ViewBag.defLang = defLang;

            ViewBag.permissions = Session["permissions"];
            ViewBag.permissionsTop = Session["permissionsTop"];

            if (Request.Cookies["keepMeAccount"] != null || Session["sysUsername"] != null)
            {
                if (adminCathType == "Session" && Session["sysUsername"] != null && Session["sysUsername"].ToString() != "")
                {
                    ViewBag.username = Session["sysUsername"].ToString();
                }
                else
                {
                    if (Request.Cookies["sysLogin"] != null)
                    {
                        HttpCookie aCookie = Request.Cookies["sysLogin"];
                        if (aCookie != null && aCookie.Value != "" && aCookie.Value.IndexOf("&") != -1)
                        {
                            List<string> sysUser = aCookie.Value.Split('&').ToList();
                            string sysUsername = Server.HtmlEncode(sysUser[0].Replace("sysUsername=", ""));
                            ViewBag.username = sysUsername;
                        }
                    }
                }
            }

            if (ViewBag.username == "sysadmin")
            {
                ViewBag.system_menu = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.status == "Y").ToList();
            }
            else
            {
                ViewBag.system_menu = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.status == "Y").Where(m => m.area != "admin").ToList();
            }

            ViewBag.system_menu_data = null;
            ViewBag.tables = "member";

            string tables = "member";
            system_menu system_menu = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault();
            ViewBag.system_menu_data = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault();

            string topCategory = ViewBag.system_menu_data.category;
            ViewBag.system_menu_top_data = DB.system_menu.Where(m => m.guid == topCategory).FirstOrDefault();
            //  ViewBag.tables = RouteData.Values["tables"].ToString();

            if (tables == "notes_data")
            {
                if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
                {
                    string act_path = "edit/" + RouteData.Values["id"].ToString();

                    ViewBag.system_menu_data = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();
                }
            }

            if (tables == "finance_report" || tables == "share_download" || tables == "form_subject" || tables == "downloads" || tables == "advertise")
            {
                if (RouteData.Values["action"].ToString() == "list")
                {
                    if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
                    {
                        string act_path = "list/" + RouteData.Values["id"].ToString();

                        ViewBag.system_menu_data = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();


                    }
                }
                if (RouteData.Values["action"].ToString() == "edit")
                {
                    if (Request["c"] != null && Request["c"].ToString() != "")
                    {
                        string act_path = "list/" + Request["c"].ToString();

                        ViewBag.system_menu_data = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();
                    }
                }
            }

            //紀錄先進入的路徑
            loginLastToUri = tables;
            if (RouteData.Values["action"] != null && RouteData.Values["action"].ToString() != "")
            {
                loginLastToUri += "/" + RouteData.Values["action"].ToString();
            }
            if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
            {
                loginLastToUri += "/" + RouteData.Values["id"].ToString();
            }

            if (!string.IsNullOrEmpty(loginLastToUri))
            {
                Session.Remove("loginLastToUri");
                Session.Add("loginLastToUri", loginLastToUri);
            }


            ViewBag.language = null;
            ViewBag.viewLanguage = "語系";
            var language = DB.language.OrderBy(m => m.sortIndex).Where(m => m.status == "Y").ToList();
            if (language.Count > 0)
            {
                ViewBag.language = language;
                ViewBag.viewLanguage = language[0].title;
            }

            ViewBag.userData = FunctionService.ReUserData();//回傳使用者資訊

            //系統資訊
            Guid systemGuid = Guid.Parse("4795DABF-18DE-490E-9BB2-D57B4D99C127");
            ViewBag.systemData = DB.system_data.Where(m => m.guid == systemGuid).FirstOrDefault();
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        public ActionResult list()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }
            Session.Remove("dataTableCategory");//移除暫存細項

            ViewBag.RetuenToList = "N";
            ViewBag.statusData = null;//狀態篩選顯示
            ViewBag.statusVal = null;//狀態篩選值

            ViewBag.RetuenID = "";
            ViewBag.dataTableAddPostVal = "{}";

            ViewBag.systemTitle = "客戶聯絡表單";

            if (RouteData.Values["tables"] != null)
            {
                string tables = RouteData.Values["tables"].ToString();

                ViewBag.Tables = tables;

                ViewBag.addTitle = "";
                ViewBag.addId = "";
                Session.Remove("dataTableCategory");

             
            }
           
            return View("Siteadmin/list");
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        /// <summary>
        /// 新增/修改
        /// </summary>
        /// <returns></returns>
        public ActionResult edit()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            //權限
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }
            ViewBag.mainCol = null;
            ViewBag.mediaCol = null;
            ViewBag.otherCol = null;
            ViewBag.guid = "";
           
            ViewBag.addTitle = "";

            ViewBag.addId = "";
            ViewBag.data = null;

            if (RouteData.Values.Count() == 2)
            {

            }else
            {
                string id = RouteData.Values["id"].ToString();
                member data = DB.member.Where(m => m.guid == id).FirstOrDefault();
                ViewBag.data = data;
            }
            

            return View("Siteadmin/edit");
        }

        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult save(FormCollection form)
        {
            string guid = "";
            if(string.IsNullOrEmpty(form["guid"].ToString()))
            {
                guid = Guid.NewGuid().ToString();
                member data = new member();
                data.guid = guid;
                data.username = form["username"].ToString();
                data.password = FunctionService.md5(form["password"].ToString());
                data.name = form["name"].ToString();
                data.company = form["company"].ToString();
                data.phone = form["phone"].ToString();
                data.email = form["email"].ToString();
                string status = form["status"].ToString();
                data.status = status;
                data.create_date = DateTime.Now;

                DB.member.Add(data);
                DB.SaveChanges();
            }
            else
            {
                guid = form["guid"].ToString();

                member data = DB.member.Where(m => m.guid == guid).FirstOrDefault();

                data.username = form["username"].ToString();

                if(!string.IsNullOrEmpty(form["password"].ToString()))
                {
                    data.password = FunctionService.md5(form["password"].ToString());
                }
               

                data.name = form["name"].ToString();
                data.company = form["company"].ToString();
                data.phone = form["phone"].ToString();
                data.email = form["email"].ToString();
                string status = form["status"].ToString();
                data.status = status;
                data.modifydate = DateTime.Now;

                DB.SaveChanges();
            }
         


           //完成轉跳
            Response.Write("<script>window.location.href='" + Url.Content("~/siteadmin/member/edit/" + guid) + "';</script>");
           
            return null;
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        /// <summary>
        /// DataTable
        /// </summary>
        /// <returns></returns>
        public string dataTable()
        {
            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> listData = new Dictionary<string, object>();

            string json = "";

            dynamic data = null;

           
                NameValueCollection requests = Request.Params;
                string guid = "";

                try
                {
                    guid = !string.IsNullOrEmpty(RouteData.Values["id"].ToString()) ? RouteData.Values["id"].ToString() : "";
                }
                catch(Exception e)
                {

                }


                data = GetListData.getListData("member", requests, Url.Content("~/") , guid);

              

            listData = data;

            json = JsonConvert.SerializeObject(listData, Formatting.Indented);

            return json;
        }

        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public string Ajax(FormCollection form)
        {
            Dictionary<String, Object> dic = new Dictionary<string, object>();

            NameValueCollection getData = FunctionService.reSubmitFormDataJson(HttpUtility.UrlDecode(form["Val"].ToString()));//回傳JSON資料
            dic.Add("Func", form["Func"].ToString());

            switch (form["Func"].ToString())
            {


                #region 徹底刪除

                case "removeData":

                    if (getData != null)
                    {
                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            Guid guid = Guid.Parse(guidArr[i].ToString());
                            var ashcan = DB.ashcan.Where(m => m.guid == guid).FirstOrDefault();

                            switch (ashcan.tables)
                            {
                                case "forum":
                                    DB.Database.ExecuteSqlCommand("delete from forum where top_guid = '" + ashcan.from_guid + "'");
                                    DB.Database.ExecuteSqlCommand("delete from forum_message where forum_guid = '" + ashcan.from_guid + "'");
                                    break;
                            }
                            if (getData["type"].ToString() == "del")
                            {
                                DB.Database.ExecuteSqlCommand("delete from [" + ashcan.tables + "] where guid = '" + ashcan.from_guid + "'");
                            }
                            else
                            {
                                DB.Database.ExecuteSqlCommand("update [" + ashcan.tables + "] set status = 'N' where guid = '" + ashcan.from_guid + "'");
                            }

                            DB.Database.ExecuteSqlCommand("delete from [ashcan] where guid = '" + guid.ToString() + "'");
                        }
                    }
                    break;

                #endregion

                #region 修改狀態

                case "changeStatus":

                    if (getData != null)
                    {
                        string useLang = TablesService.useLang(getData["tables"].ToString());//是否使用語系

                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            if (useLang == "Y")
                            {
                                if (ViewBag.language != null)
                                {
                                    foreach (language item in ViewBag.language)
                                    {
                                        Dictionary<String, Object> temp = new Dictionary<string, object>();
                                        temp.Add("guid", guidArr[i].ToString());
                                        temp.Add("status", getData["status"].ToString());
                                        temp.Add("lang", item.lang);
                                        string jsonData = JsonConvert.SerializeObject(temp);
                                        //呼叫寫入或修改
                                        string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "status", guidArr[i].ToString(), "edit", null);
                                    }
                                }
                            }
                            else
                            {
                                Dictionary<String, Object> temp = new Dictionary<string, object>();
                                temp.Add("guid", guidArr[i].ToString());
                                temp.Add("status", getData["status"].ToString());
                                string jsonData = JsonConvert.SerializeObject(temp);
                                //呼叫寫入或修改
                                string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "status", guidArr[i].ToString(), "edit", null);
                            }
                        }

                        //資源回收桶
                        if (getData["status"].ToString() == "D")
                        {
                            ashcan ashcan = new ashcan();
                            for (int i = 0; i < guidArr.Length; i++)
                            {
                                ashcan.title = titleArr[i].ToString();
                                ashcan.tables = getData["tables"].ToString();
                                ashcan.from_guid = guidArr[i].ToString();
                                ashcan.create_date = DateTime.Now;
                                ashcan.modifydate = DateTime.Now;
                                DB.ashcan.Add(ashcan);

                                DB.SaveChanges();
                            }
                        }

                        dic["status"] = getData["status"].ToString();
                    }
                    break;

                    #endregion




            }

            string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
            //輸出json格式
            return json;
        }


    }
}