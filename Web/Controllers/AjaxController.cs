﻿using HuLane.BuildBlock;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuLane.Main.Repositories;
using Web.Models;
using Scriban;
using Web.DB.Models;
using Newtonsoft.Json;

namespace Web.Controllers
{
    public class AjaxController : BaseController
    {
        
      

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        
        [HttpPost]
        public string  AddToInquery(FormCollection form)
        {
            List<string> InqueryId = new List<string>();
            if (Session["Inquery"] != null)
            {
                InqueryId = (List<string>)Session["Inquery"];         
            }

            InqueryId.Add(form["id"].ToString());
            Session.Remove("Inquery");
            Session.Add("Inquery", InqueryId);

            return JsonConvert.SerializeObject(form);
        }

        [HttpPost]
        public string addToProductInquery(int data)
        {
            List<string> InqueryId = new List<string>();
            if (Session["Inquery"] != null)
            {
                InqueryId = (List<string>)Session["Inquery"];
            }

            InqueryId.Add(data.ToString());
            Session.Remove("Inquery");
            Session.Add("Inquery", InqueryId);

            return data.ToString();
        }

        [HttpPost]
        public string deleteToProductInquery(int productid)
        {
            List<string> InqueryId = new List<string>();
            List<string> resultInqueryId = new List<string>();
            if (Session["Inquery"] != null)
            {
                InqueryId = (List<string>)Session["Inquery"];
            }

            Session["Inquery"] = null;

            foreach (var item in InqueryId)
            {
                if (item == productid.ToString())
                {
                    //
                }
                else
                {
                    resultInqueryId.Add(item.ToString());
                }
            }

            Session.Add("Inquery", resultInqueryId);

            return productid.ToString();
        }
        [HttpPost]
        public List<carsysl4> getCarsys(string name,int value)
        {
            List<carsysl4> result = new List<carsysl4>();
            Model DB = new Model();
            dynamic re = null;
            result = DB.carsysl4.Where(m => m.lang == "tw").Where(m => m.guid == value).ToList();
            ViewBag.carsysl4Data = result;

            return result;
        }
    }
}