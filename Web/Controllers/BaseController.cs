﻿using HuLane.BuildBlock;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuLane.Main.Repositories;
using Web.Models;
using Scriban;
using Web.DB.Models;
using System.Collections.Specialized;
using System.Configuration;
using Web.Service;
using System.Web.Script.Serialization;

namespace Web.Controllers
{
    public class BaseController : Controller
    {
        //預設語系
        protected NameValueCollection langData = new NameValueCollection();
        protected string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;
        protected string webURL = "";
        protected web_data webData = new web_data();
        protected Model DB = new Model();
        protected Dictionary<string, string> headerActive = new Dictionary<string, string>();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {            

            webURL = FunctionService.getWebUrl();
            ViewBag.webURL = webURL;
            ViewBag.ID = "";
            ViewBag.langulange = "Language語言";


            //if (RouteData.Values["lang"] != null)
            //{
            //    if (RouteData.Values["controller"].ToString() == "products")
            //    {

            //    }
            //    else if (RouteData.Values["controller"].ToString() == "about")
            //    {

            //    }
            //    else if (RouteData.Values["controller"].ToString() == "business")
            //    {

            //    }
            //    else if (RouteData.Values["controller"].ToString() == "news")
            //    {

            //    }
            //    else if (RouteData.Values["controller"].ToString() == "investor")
            //    {

            //    }
            //    else if (RouteData.Values["controller"].ToString() == "welfare")
            //    {

            //    }
            //    else if (RouteData.Values["controller"].ToString() == "product")
            //    {

            //    }
            //    else if (RouteData.Values["controller"].ToString() == "contact")
            //    {

            //    }
            //    else
            //    {
            //        defLang = RouteData.Values["lang"].ToString();
            //        Session["changeLang"] = defLang;
            //        ViewBag.Lang = Session["changeLang"];
            //        if (ViewBag.Lang == "tw")
            //        {
            //            ViewBag.langulange = "繁體中文";
            //        }
            //        else if (ViewBag.Lang == "en")
            //        {
            //            ViewBag.langulange = "English";
            //        }
            //        else
            //        {
            //            ViewBag.langulange = "簡體中文";
            //        }
            //    } 
            //}


            if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
            {           
                ViewBag.ID = RouteData.Values["id"].ToString();
            }

            webData = DB.web_data.Where(model => model.lang == defLang).FirstOrDefault();//取得網站基本資訊
            if (webData != null)
            {
                ViewBag.webData = webData;
                ViewBag.SEO = FunctionService.SEO(webData, "", "", "");
                ViewBag.FileRoot = Url.Content("~/Content/");
            }
            var aaa = Session["changeLang"] as string;
            var bbb = Session["jLang"] as string;

            var trueUrl = Request.Url.AbsoluteUri;
            var trueUrlsplit = trueUrl.Split('/');
            if (trueUrlsplit[3] == "tw")
            {
                ViewBag.Lang = "tw";
                Session["changeLang"] = "tw";
                ViewBag.langulange = "繁體中文";
                langData = FunctionService.getLangData("tw");//取得語系檔案
            }
            else if (trueUrlsplit[3] == "cn")
            {
                ViewBag.Lang = "cn";
                Session["changeLang"] = "cn";
                ViewBag.langulange = "簡體中文";
                langData = FunctionService.getLangData("cn");//取得語系檔案
            }
            else if (trueUrlsplit[3] == "en")
            {
                ViewBag.Lang = "en";
                Session["changeLang"] = "en";
                ViewBag.langulange = "English";
                langData = FunctionService.getLangData("en");//取得語系檔案
            }
            else
            {
                ViewBag.Lang = "tw";
                langData = FunctionService.getLangData(ViewBag.Lang);//取得語系檔案
            }

            //if (Session["jLang"] as string == null)
            //{
            //    ViewBag.Lang = defLang;
            //    langData = FunctionService.getLangData(defLang);//取得語系檔案
            //}
            //else
            //{
            //    ViewBag.Lang = Session["jLang"] as string;
            //    langData = FunctionService.getLangData(Session["jLang"] as string);//取得語系檔案
            //    if (ViewBag.Lang == "tw")
            //    {
            //        ViewBag.langulange = "繁體中文";
            //    }
            //    else if (ViewBag.Lang == "en")
            //    {
            //        ViewBag.langulange = "English";
            //    }
            //    else
            //    {
            //        ViewBag.langulange = "簡體中文";
            //    }
            //}

            //langData = FunctionService.getLangData(ViewBag.Lang);//取得語系檔案

            ViewBag.title = "";
        }


        public string langjsonM(string lang)
        {
            ViewBag.Lang = lang;
            defLang = lang;
            langData = FunctionService.getLangData(lang);//取得語系檔案
            
            var result = new Dictionary<string, object>();
            foreach (string key in langData.Keys)
            {

                string[] values = langData.GetValues(key);
                if (values.Length == 1)
                {
                    if (key == "語系")
                    {
                        Session["jLang"] = values[0];
                    }
                    result.Add(key, values[0]);
                }
                else
                {
                    if (key == "語系")
                    {
                        Session["jLang"] = values;
                    }
                    result.Add(key, values);
                }

            }

            string json = new JavaScriptSerializer().Serialize(result);

            return json;
        }
        public string langjson()
        {
            var aaa = Session["changeLang"];
            if (RouteData.Values["lang"] != null)
            {
                if (Session["changeLang"] as string != null)
                {
                    defLang = Session["changeLang"] as string;
                }
                else
                {
                    defLang = RouteData.Values["lang"].ToString();
                }
            }
            NameValueCollection langData = FunctionService.getLangData(defLang);//取得語系檔案
            var result = new Dictionary<string, object>();
            foreach (string key in langData.Keys)
            {
              
                    string[] values = langData.GetValues(key);
                    if (values.Length == 1)
                    {
                        result.Add(key, values[0]);
                    }
                    else
                    {
                        result.Add(key, values);
                    }
               
            }

            string json = new JavaScriptSerializer().Serialize(result);

            return json;
        }
     }
}