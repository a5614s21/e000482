﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.DB.Models;
using PagedList;
using Web.Service;
using Newtonsoft.Json;
using System.Collections;
using Web.Models;
using Web.Repository;
using System.IO;
using System.Web.Security;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace Web.Controllers
{
    public class BusinessController : BaseController
    {
        public int FormJson { get; private set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            headerActive.Add("business", "active");
            ViewBag.headerActive = headerActive;
            ViewBag.ResLang = langData;//取得語系檔案
        }


        public ActionResult List()
        {
            var changeLang = langData["語系"].ToString();
            var temp = (from a in DB.problem
                        where a.lang == changeLang && a.category == "category7"
                        select a.title).FirstOrDefault();
            ViewBag.feedBackUrl = temp;
            ViewBag.title = langData["商務專區"];
            return View();
        }

        public ActionResult Faq()
        {
            var changeLang = langData["語系"].ToString();
            ViewBag.title = langData["FAQ問與答"];
            ViewBag.list = DB.business_faq.Where(m => m.lang == changeLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();


            return View();
        }



        public ActionResult Login()
        {
            ViewBag.title = langData["客戶教育訓練平台"];
            var changeLang = langData["語系"].ToString();
            if (this.User.Identity.IsAuthenticated)
            {
                return Redirect("~/" + changeLang + "/business/course");
            }


            return View();
        }

        public ActionResult CustomerForm()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddCustomerForm(FormCollection form)
        {
            //Dictionary<String, Object> dic = new Dictionary<string, object>();

            //for (int i = 0; i < 2; i++)
            //{
            //    string lang;
            //    if (i == 0)
            //    {
            //        lang = "tw";
            //    }
            //    else
            //    {
            //        lang = "cn";
            //    }
            //    Dictionary<String, Object> subForm = new Dictionary<string, object>();
            //    subForm.Add("lang", lang);

            //    foreach (var key in form.AllKeys)
            //    {
            //        subForm.Add(key.Replace("_" + lang, ""), form[key].ToString());
            //    }

            //    dic.Add(lang, subForm);
            //}

            //for (int i = 0; i < 2; i++)
            //{
            //    string lang;
            //    if (i == 0)
            //    {
            //        lang = "tw";
            //    }
            //    else
            //    {
            //        lang = "cn";
            //    }

            //    string json = JsonConvert.SerializeObject((Dictionary<string, object>)dic[lang]);
            //    var aaa = (Dictionary<string, object>)dic[lang];
            //}

            member data = new member();

            data.customer_general_information = JsonConvert.SerializeObject(
                       new customerGeneralData
                       {
                           companyName = form.AllKeys.Contains("companyName") ? form["companyName"].ToString() : "",
                           contry = form.AllKeys.Contains("Country") ? form["Country"].ToString() : "",
                           location = form.AllKeys.Contains("location") ? form["location"].ToString() : "",
                           invoiceAddress = form.AllKeys.Contains("invoiceAddress") ? form["invoiceAddress"].ToString() : "",
                           deliveryAddress = form.AllKeys.Contains("deliveryAddress") ? form["deliveryAddress"].ToString() : "",
                           tel = form.AllKeys.Contains("tel") ? form["tel"].ToString() : "",
                           fax = form.AllKeys.Contains("fax") ? form["fax"].ToString() : "",
                           chairman = form.AllKeys.Contains("chairman") ? form["chairman"].ToString() : "",
                           contactPerson = form.AllKeys.Contains("contactPerson") ? form["contactPerson"].ToString() : "",
                           website = form.AllKeys.Contains("website") ? form["website"].ToString() : "",
                           email = form.AllKeys.Contains("Email") ? form["Email"].ToString() : "",
                           establishment = form.AllKeys.Contains("establishment") ? form["establishment"].ToString() : "",
                           stock = form.AllKeys.Contains("stock") ? form["stock"].ToString() : "",
                           license = form.AllKeys.Contains("license") ? form["license"].ToString() : "",
                           employees = form.AllKeys.Contains("employees") ? form["employees"].ToString() : "",
                           Overseas = form.AllKeys.Contains("Overseas") ? form["Overseas"].ToString() : ""
                       });

            data.bank_affiliates = JsonConvert.SerializeObject(
                       new bankAffiliatesData
                       {
                           bankName1 = form.AllKeys.Contains("bankName-1") ? form["bankName-1"].ToString() : "",
                           branch1 = form.AllKeys.Contains("branch-1") ? form["branch-1"].ToString() : "",
                           account1 = form.AllKeys.Contains("account-1") ? form["account-1"].ToString() : "",
                           remark1 = form.AllKeys.Contains("remark-1") ? form["remark-1"].ToString() : "",
                           bankName2 = form.AllKeys.Contains("bankName-2") ? form["bankName-2"].ToString() : "",
                           branch2 = form.AllKeys.Contains("branch-2") ? form["branch-2"].ToString() : "",
                           account2 = form.AllKeys.Contains("account-2") ? form["account-2"].ToString() : "",
                           remark2 = form.AllKeys.Contains("remark-2") ? form["remark-2"].ToString() : "",
                           bankName3 = form.AllKeys.Contains("bankName-3") ? form["bankName-3"].ToString() : "",
                           branch3 = form.AllKeys.Contains("branch-3") ? form["branch-3"].ToString() : "",
                           account3 = form.AllKeys.Contains("account-3") ? form["account-3"].ToString() : "",
                           remark3 = form.AllKeys.Contains("remark-3") ? form["remark-3"].ToString() : ""
                       });

            data.businessType = JsonConvert.SerializeObject(
                       new businessTypeData
                       {
                           businessType = form.AllKeys.Contains("businessType") ? form["businessType"].ToString() : "",
                           otherbusinessTypeText = form.AllKeys.Contains("otherbusinessTypeText") ? form["otherbusinessTypeText"].ToString() : ""
                       });

            data.top3_businessContents = JsonConvert.SerializeObject(
                       new top3BusinessContentsData
                       {
                           productsItem1 = form.AllKeys.Contains("products-item-1") ? form["products-item-1"].ToString() : "",
                           productsPrecent1 = form.AllKeys.Contains("products-precent-1") ? form["products-precent-1"].ToString() : "",
                           productsItem2 = form.AllKeys.Contains("products-item-2") ? form["products-item-2"].ToString() : "",
                           productsPrecent2 = form.AllKeys.Contains("products-precent-2") ? form["products-precent-2"].ToString() : "",
                           productsItem3 = form.AllKeys.Contains("products-item-3") ? form["products-item-3"].ToString() : "",
                           productsPrecent3 = form.AllKeys.Contains("products-precent-3") ? form["products-precent-3"].ToString() : ""
                       });

            data.top3_customers = JsonConvert.SerializeObject(
                       new top3CustomersData
                       {
                           customerItem1 = form.AllKeys.Contains("customer-item-1") ? form["customer-item-1"].ToString() : "",
                           customerPrecent1 = form.AllKeys.Contains("customer-precent-1") ? form["customer-precent-1"].ToString() : "",
                           customerItem2 = form.AllKeys.Contains("customer-item-2") ? form["customer-item-2"].ToString() : "",
                           customerPrecent2 = form.AllKeys.Contains("customer-precent-2") ? form["customer-precent-2"].ToString() : "",
                           customerItem3 = form.AllKeys.Contains("customer-item-3") ? form["customer-item-3"].ToString() : "",
                           customerPrecent3 = form.AllKeys.Contains("customer-precent-3") ? form["customer-precent-3"].ToString() : ""
                       });

            data.top3_venders = JsonConvert.SerializeObject(
                       new top3VendersData
                       {
                           venderItem1 = form.AllKeys.Contains("vender-item-1") ? form["vender-item-1"].ToString() : "",
                           venderPrecent1 = form.AllKeys.Contains("vender-precent-1") ? form["vender-precent-1"].ToString() : "",
                           venderItem2 = form.AllKeys.Contains("vender-item-2") ? form["vender-item-2"].ToString() : "",
                           venderPrecent2 = form.AllKeys.Contains("vender-precent-2") ? form["vender-precent-2"].ToString() : "",
                           venderItem3 = form.AllKeys.Contains("vender-item-3") ? form["vender-item-3"].ToString() : "",
                           venderPrecent3 = form.AllKeys.Contains("vender-precent-3") ? form["vender-precent-3"].ToString() : ""
                       });

            data.major_equipmentList = JsonConvert.SerializeObject(
                       new majorEquipmentData
                       {
                           list1 = form.AllKeys.Contains("list-1") ? form["list-1"].ToString() : "",
                           list2 = form.AllKeys.Contains("list-2") ? form["list-2"].ToString() : "",
                           list3 = form.AllKeys.Contains("list-3") ? form["list-3"].ToString() : "",
                           list4 = form.AllKeys.Contains("list-4") ? form["list-4"].ToString() : "",
                           list5 = form.AllKeys.Contains("list-5") ? form["list-5"].ToString() : "",
                           list6 = form.AllKeys.Contains("list-6") ? form["list-6"].ToString() : ""
                       });

            data.floorArea = JsonConvert.SerializeObject(
                       new floorAreaData
                       {
                           headquarter = form.AllKeys.Contains("headquarter") ? form["headquarter"].ToString() : "",
                           branch = form.AllKeys.Contains("branch") ? form["branch"].ToString() : ""
                       });


            data.productModel = JsonConvert.SerializeObject(
                       new productModelData
                       {
                           CustomersText1 = form.AllKeys.Contains("CustomersText-1") ? form["CustomersText-1"].ToString() : "",
                           CustomersText2 = form.AllKeys.Contains("CustomersText-2") ? form["CustomersText-2"].ToString() : "",
                           CustomersText3 = form.AllKeys.Contains("CustomersText-3") ? form["CustomersText-3"].ToString() : "",
                           CustomersText4 = form.AllKeys.Contains("CustomersText-4") ? form["CustomersText-4"].ToString() : "",
                           CustomersText5 = form.AllKeys.Contains("CustomersText-5") ? form["CustomersText-5"].ToString() : "",
                           CustomersText6 = form.AllKeys.Contains("CustomersText-6") ? form["CustomersText-6"].ToString() : "",
                           CustomersText7 = form.AllKeys.Contains("CustomersText-7") ? form["CustomersText-7"].ToString() : "",
                           CustomersText8 = form.AllKeys.Contains("CustomersText-8") ? form["CustomersText-8"].ToString() : "",
                           CustomersText9 = form.AllKeys.Contains("CustomersText-9") ? form["CustomersText-9"].ToString() : ""
                       });

            data.companyType1 = JsonConvert.SerializeObject(
                       new companyType1Data
                       {
                           type1 = form.AllKeys.Contains("TypeI") ? form["TypeI"].ToString() : ""
                       });

            data.companyType2 = JsonConvert.SerializeObject(
                       new companyType2Data
                       {
                           type2 = form.AllKeys.Contains("TypeII") ? form["TypeII"].ToString() : ""
                       });

            data.cooperation = JsonConvert.SerializeObject(
                       new cooperationData
                       {
                           cooperationItem1 = form.AllKeys.Contains("Cooperation-item-1") ? form["Cooperation-item-1"].ToString() : "",
                           cooperationPrecent1 = form.AllKeys.Contains("Cooperation-precent-1") ? form["Cooperation-precent-1"].ToString() : "",
                           cooperationItem2 = form.AllKeys.Contains("Cooperation-item-2") ? form["Cooperation-item-2"].ToString() : "",
                           cooperationPrecent2 = form.AllKeys.Contains("Cooperation-precent-2") ? form["Cooperation-precent-2"].ToString() : "",
                           cooperationItem3 = form.AllKeys.Contains("Cooperation-item-3") ? form["Cooperation-item-3"].ToString() : "",
                           cooperationPrecent3 = form.AllKeys.Contains("Cooperation-precent-3") ? form["Cooperation-precent-3"].ToString() : ""
                       });

            data.whitchMethods = JsonConvert.SerializeObject(
                       new whitchMethodsData
                       {
                           methodsType = form.AllKeys.Contains("methodsType") ? form["methodsType"].ToString() : "",
                           othermethodsTypeText = form.AllKeys.Contains("othermethodsTypeText") ? form["othermethodsTypeText"].ToString() : ""
                       });

            data.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            data.status = "N";
            data.username = "";
            data.password = "";
            data.name = "";
            data.company = "";
            data.email = "";
            data.phone = "";
            data.mobile = "";
            data.address = "";
            data.note = "";

            data.guid = Guid.NewGuid().ToString();
            DB.member.Add(data);
            DB.SaveChanges();

            ////SMTP
            //var smtpdata = DB.smtp_data.FirstOrDefault();

            ////設定smtp主機
            //string smtpAddress = smtpdata.host;
            ////設定Port
            //int portNumber = Int32.Parse(smtpdata.port);
            //bool enableSSL = true;
            ////SSL是否啟用
            //enableSSL = (smtpdata.smtp_auth == "Y" ? true : false);
            ////填入寄送方email和password
            //string emailFrom = smtpdata.username;
            //string password = smtpdata.password;
            ////收信方email 可以用豆後區分多個收件人
            //string emailTo = form["Email"].ToString();
            //string subject = "【胡連精密股份有限公司】新會員資訊";

            //string sMessage = "test";
            ////sMessage = sMessage.Replace("{$content}", user.content);

            //using (MailMessage mail = new MailMessage())
            //{
            //    mail.From = new MailAddress(emailFrom);
            //    mail.To.Add(emailTo);
            //    mail.Subject = subject;
            //    mail.Body = sMessage;
            //    // 若你的內容是HTML格式，則為True
            //    mail.IsBodyHtml = true;

            //    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
            //    {
            //        smtp.Credentials = new NetworkCredential(emailFrom, password);
            //        smtp.EnableSsl = enableSSL;
            //        smtp.Send(mail);
            //    }
            //}
            //


            //string smtp_use = ConfigurationManager.ConnectionStrings["smtp_use"].ConnectionString;//SMTP 正式/測試

            //var smtpData = DB.smtp_data.Where(m => m.guid == smtp_use).FirstOrDefault();

            string metaFile = "";
            metaFile = "newmember.html";
            string sMessage = System.IO.File.ReadAllText(Server.MapPath("~/Content/Mail") + "\\" + metaFile);

            //var aaa = form["Email"].ToString();

            //sMessage = sMessage.Replace("{$companyName}", form["companyName"].ToString() == null ? "" : form["companyName"].ToString());
            //sMessage = sMessage.Replace("{$country}", form["Country"].ToString() == null ? "" : form["Country"].ToString());
            //sMessage = sMessage.Replace("{$location}", form["location"].ToString() == null ? "" : form["location"].ToString());
            //sMessage = sMessage.Replace("{$invoice}", form["invoiceAddress"].ToString() == null ? "" : form["invoiceAddress"].ToString());
            //sMessage = sMessage.Replace("{$delivery}", form["deliveryAddress"].ToString() == null ? "" : form["deliveryAddress"].ToString());
            //sMessage = sMessage.Replace("{$telphone}", form["tel"].ToString() == null ? "" : form["tel"].ToString());
            //sMessage = sMessage.Replace("{$faxNo}", form["fax"].ToString() == null ? "" : form["fax"].ToString());
            //sMessage = sMessage.Replace("{$chairman}", form["chairman"].ToString() == null ? "" : form["chairman"].ToString());
            //sMessage = sMessage.Replace("{$personContact}", form["contactPerson"].ToString() == null ? "" : form["contactPerson"].ToString());
            //sMessage = sMessage.Replace("{$website}", form["website"].ToString() == null ? "" : form["website"].ToString());
            //sMessage = sMessage.Replace("{$email}", form["Email"].ToString() == null ? "" : aaa);
            //sMessage = sMessage.Replace("{$esrablishment}", "168");
            //sMessage = sMessage.Replace("{$capitalStock}", form["stock"].ToString() == null ? "" : form["stock"].ToString());
            //sMessage = sMessage.Replace("{$licence}", form["license"].ToString() == null ? "" : form["license"].ToString());
            //sMessage = sMessage.Replace("{$employeeNum}", form["employees"].ToString() == null ? "" : form["employees"].ToString());
            //sMessage = sMessage.Replace("{$overseas}", form["Overseas"].ToString() == null ? "" : form["Overseas"].ToString());

            //string re = "";
            //MailMessage msg = new MailMessage();
            ////收件者，以逗號分隔不同收件者 ex "test@gmail.com,test2@gmail.com"
            //msg.To.Add(form["Email"].ToString());
            //msg.From = new MailAddress(smtpData.from_email.ToString(), "新會員資訊", System.Text.Encoding.UTF8);
            ////郵件標題
            //msg.Subject = "【胡連精密股份有限公司】新會員資訊";
            ////郵件標題編碼
            //msg.SubjectEncoding = System.Text.Encoding.UTF8;
            ////郵件內容
            //msg.Body = sMessage;
            //msg.IsBodyHtml = true;
            //msg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼
            //msg.Priority = MailPriority.Normal;//郵件優先級
            //                                   //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port


            //SmtpClient MySmtp = new SmtpClient(smtpData.host.ToString(), int.Parse(smtpData.port.ToString()));
            ////設定你的帳號密碼
            //MySmtp.Credentials = new System.Net.NetworkCredential(smtpData.username.ToString(), smtpData.password.ToString());
            ////Gmial 的 smtp 使用 SSL
            //if (smtpData.smtp_auth.ToString() == "Y")
            //{
            //    MySmtp.EnableSsl = true;
            //}
            //else
            //{
            //    MySmtp.EnableSsl = false;
            //}

            //try
            //{
            //    MySmtp.Send(msg);
            //}
            //catch (SmtpFailedRecipientsException ex)
            //{
            //    Console.WriteLine("Exception caught in RetryIfBusy(): {0}", ex.ToString());
            //}




            try
            {
                //var user_mail = "sales@hulane.com.tw";
                var user_mail = "m0246@minmax.biz";
                //var product = _context.inquiry_detail.Where(x => x.iguid == guid).ToList();

                //SMTP
                //var smtpdata = DB.smtp_data.SingleOrDefault();
                string smtp_use = ConfigurationManager.ConnectionStrings["smtp_use"].ConnectionString;//SMTP 正式/測試

                var smtpdata = DB.smtp_data.Where(m => m.guid == smtp_use).FirstOrDefault();


                //設定smtp主機
                string smtpAddress = smtpdata.host;
                //設定Port
                int portNumber = Int32.Parse(smtpdata.port);
                bool enableSSL = true;
                //SSL是否啟用
                enableSSL = (smtpdata.smtp_auth == "Y" ? true : false);
                //填入寄送方email和password
                string emailFrom = smtpdata.username;
                string password = smtpdata.password;
                //收信方email 可以用豆後區分多個收件人
                string emailTo = user_mail;
                string subject = "【胡連精密股份有限公司】新會員資訊";

                string webURL = ConfigurationManager.ConnectionStrings["APP_URL"].ConnectionString;

                web_data web_Data = DB.web_data.Where(model => model.lang == defLang).FirstOrDefault();//取得網站基本資訊

                sMessage = sMessage.Replace("{[websiteName]}", web_Data.title);
                sMessage = sMessage.Replace("{[websiteUrl]}", webURL);

                sMessage = sMessage.Replace("{[companyName]}", form["companyName"].ToString() == null ? "" : form["companyName"].ToString());
                sMessage = sMessage.Replace("{[country]}", form["Country"].ToString() == null ? "" : form["Country"].ToString());
                sMessage = sMessage.Replace("{[location]}", form["location"].ToString() == null ? "" : form["location"].ToString());
                sMessage = sMessage.Replace("{[invoice]}", form["invoiceAddress"].ToString() == null ? "" : form["invoiceAddress"].ToString());
                sMessage = sMessage.Replace("{[delivery]}", form["deliveryAddress"].ToString() == null ? "" : form["deliveryAddress"].ToString());
                sMessage = sMessage.Replace("{[telphone]}", form["tel"].ToString() == null ? "" : form["tel"].ToString());
                sMessage = sMessage.Replace("{[faxNo]}", form["fax"].ToString() == null ? "" : form["fax"].ToString());
                sMessage = sMessage.Replace("{[chairman]}", form["chairman"].ToString() == null ? "" : form["chairman"].ToString());
                sMessage = sMessage.Replace("{[personContact]}", form["contactPerson"].ToString() == null ? "" : form["contactPerson"].ToString());
                sMessage = sMessage.Replace("{[website]}", form["website"].ToString() == null ? "" : form["website"].ToString());
                sMessage = sMessage.Replace("{[email]}", user_mail);
                sMessage = sMessage.Replace("{[esrablishment]}", form["establishment"].ToString() == null ? "" : form["establishment"].ToString());
                sMessage = sMessage.Replace("{[capitalStock]}", form["stock"].ToString() == null ? "" : form["stock"].ToString());
                sMessage = sMessage.Replace("{[licence]}", form["license"].ToString() == null ? "" : form["license"].ToString());
                sMessage = sMessage.Replace("{[employeeNum]}", form["employees"].ToString() == null ? "" : form["employees"].ToString());
                sMessage = sMessage.Replace("{[overseas]}", form["Overseas"].ToString() == null ? "" : form["Overseas"].ToString());

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom, "新會員資訊", System.Text.Encoding.UTF8);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    mail.Body = sMessage;
                    // 若你的內容是HTML格式，則為True
                    mail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(emailFrom, password);
                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                var error = ex;
            }


            var changeLang = langData["語系"].ToString();

            return Redirect("~/" + changeLang + "/business/customerformsend");
        }

        public ActionResult CustomerFormSend()
        {
            return View();
        }


        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult CourseLogin(FormCollection form)
        {
            var changeLang = langData["語系"].ToString();
            if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/business/login"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                return RedirectPermanent(Url.Content("~/" + changeLang + "/Home/Alert"));
            }
            string username = form["username"].ToString();
            string password = FunctionService.md5(form["password"].ToString());
            member member = DB.member.Where(m => m.username == username && m.password == password).FirstOrDefault();

            if (member == null)
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["查無此帳號"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + changeLang + "/business/login"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                return RedirectPermanent(Url.Content("~/" + changeLang + "/Home/Alert"));

            }
            if (member.status == "N")
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["該帳號已停用"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + changeLang + "/business/login"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                return RedirectPermanent(Url.Content("~/" + changeLang + "/Home/Alert"));

            }


            var ticket = new FormsAuthenticationTicket(
            version: 1,
            name: form["username"].ToString(),
            issueDate: DateTime.UtcNow,
            expiration: DateTime.UtcNow.AddMinutes(60),
            isPersistent: true,
            userData: $"account={form["username"].ToString()}",
            cookiePath: FormsAuthentication.FormsCookiePath);

            var encryptedTicket = FormsAuthentication.Encrypt(ticket); //把驗證的表單加密
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            Response.Cookies.Add(cookie);

            var rootPath = Request.ApplicationPath;

            return Redirect("~/" + changeLang + "/business/course");

        }

        public ActionResult Course(string id, int? page)
        {
            var changeLang = langData["語系"].ToString();
            ViewBag.title = langData["客戶教育訓練平台"];
            List<course_category> categoriec = DB.course_category.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            ViewBag.category = categoriec;

            id = string.IsNullOrEmpty(id) ? categoriec.FirstOrDefault().guid : id;

            ViewBag.id = id;
            ViewBag.title = categoriec.Where(m => m.guid == id).FirstOrDefault().title + "-" + ViewBag.title;

            int pageWidth = 10;//預設每頁長度
            var pageNumeber = page ?? 1;

            var courses = DB.courses.Where(m => m.lang == changeLang).Where(m => m.status == "Y").Where(m => m.category == id).OrderBy(m => m.sortIndex);
            ViewBag.SearchCasesCount = courses.Count();//取總筆數

            ViewBag.list = courses.ToPagedList(pageNumeber, pageWidth);

            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)ViewBag.SearchCasesCount / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + changeLang + "/business/course/" + id, Allpage, pageNumeber, ViewBag.SearchCasesCount, "");//頁碼


            return View();
        }

        /// <summary>
        /// 客戶滿意度調查
        /// </summary>
        /// <returns></returns>
        public ActionResult Survey()
        {
            var changeLang = langData["語系"].ToString();
            ViewBag.title = langData["客戶滿意度調查"];

            notes_data data = DB.notes_data.Where(m => m.lang == changeLang).Where(m => m.guid == "1").FirstOrDefault();
            ViewBag.data = data;


            ViewBag.form = new FormCollection();
            if (Session["form"] != null)
            {
                ViewBag.form = (FormCollection)Session["form"];
            }

            //問題
            List<problem_category> category = DB.problem_category.Where(m => m.lang == changeLang && m.guid != "category6" && m.guid != "category8" && m.guid != "category9" && m.guid != "category10" && m.guid != "category11" && m.guid != "category12" && m.guid != "category13" && m.guid != "category14" && m.guid != "category17" && m.guid != "category18" && m.guid != "category1" && m.guid != "category2" && m.guid != "category3" && m.guid != "category4" && m.guid != "category5").Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            List<problem> problem = DB.problem.Where(m => m.lang == changeLang).Where(m => m.status == "Y").Where(m => m.guid != "60" && m.guid != "61" && m.guid != "62" && m.guid != "63" && m.guid != "64").OrderBy(m => m.sortIndex).ToList();
            ViewBag.category = category;
            ViewBag.problem = problem;
            var category6 = DB.problem.Where(m => m.lang == changeLang).Where(m => m.status == "Y").Where(m => m.guid == "58").OrderBy(m => m.sortIndex).FirstOrDefault();
            var category7 = DB.problem.Where(m => m.lang == changeLang).Where(m => m.status == "Y").Where(m => m.guid == "59").OrderBy(m => m.sortIndex).FirstOrDefault();
            ViewBag.category6Title = category6.title;
            ViewBag.category7Title = category7.title;

            var category1 = DB.problem.Where(m => m.lang == changeLang).Where(m => m.status == "Y").Where(m => m.guid == "60").OrderBy(m => m.sortIndex).FirstOrDefault();
            var category2 = DB.problem.Where(m => m.lang == changeLang).Where(m => m.status == "Y").Where(m => m.guid == "61").OrderBy(m => m.sortIndex).FirstOrDefault();
            var category3 = DB.problem.Where(m => m.lang == changeLang).Where(m => m.status == "Y").Where(m => m.guid == "62").OrderBy(m => m.sortIndex).FirstOrDefault();
            var category4 = DB.problem.Where(m => m.lang == changeLang).Where(m => m.status == "Y").Where(m => m.guid == "63").OrderBy(m => m.sortIndex).FirstOrDefault();
            var category5 = DB.problem.Where(m => m.lang == changeLang).Where(m => m.status == "Y").Where(m => m.guid == "64").OrderBy(m => m.sortIndex).FirstOrDefault();
            ViewBag.category1Title = category1.title;
            ViewBag.category2Title = category2.title;
            ViewBag.category3Title = category3.title;
            ViewBag.category4Title = category4.title;
            ViewBag.category5Title = category5.title;

            var seoDescription = ViewBag.data.seo_description;
            ViewBag.description = seoDescription;
            var seoKeywords = ViewBag.data.seo_keywords;
            ViewBag.keywords = seoKeywords;

            return View();
        }


        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult SurveySend(FormCollection form)
        {
            var changeLang = langData["語系"].ToString();

            Session.Remove("form");
            Session.Add("form", form);


            if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + changeLang + "/business/survey"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                //Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                return RedirectPermanent(Url.Content("~/" + changeLang + "/Home/Alert"));
            }
            else
            {
                survey data = new survey();


                data.guid = Guid.NewGuid().ToString();
                data.name = form["name"].ToString();
                data.no = form["no"].ToString();
                data.email = form["email"].ToString();
                data.company = JsonConvert.SerializeObject(
                    new companyData
                    {
                        title = form["company_title"].ToString(),
                        department = form["company_department"].ToString(),
                        jobTitle = form["company_jobTitle"].ToString(),
                        tel = form["company_tel"].ToString(),
                    });

                data.area = JsonConvert.SerializeObject(
                  new areaData
                  {
                      area = form["area_area"].ToString(),
                      other = form["area_other"].ToString(),
                  });

                data.industry = JsonConvert.SerializeObject(
                       new industryData
                       {
                           industry = form.AllKeys.Contains("industry_industry") ? form["industry_industry"].ToString() : "",
                           industry_other = form.AllKeys.Contains("industindustry_otherry_industry") ? form["industry_other"].ToString() : "",
                           product = form.AllKeys.Contains("industry_product") ? form["industry_product"].ToString() : "",
                           product_other = form.AllKeys.Contains("industry_product_other") ? form["industry_product_other"].ToString() : "",
                           market = form.AllKeys.Contains("industry_market") ? form["industry_market"].ToString() : "",
                           market_other = form.AllKeys.Contains("industry_market_other") ? form["industry_market_other"].ToString() : "",
                       });

                data.satisfaction = JsonConvert.SerializeObject(
                 new satisfactionData
                 {
                     A = form.AllKeys.Contains("satisfaction_A") ? form["satisfaction_A"].ToString() : "",
                     B = form.AllKeys.Contains("satisfaction_B") ? form["satisfaction_B"].ToString() : "",
                     C = form.AllKeys.Contains("satisfaction_C") ? form["satisfaction_C"].ToString() : "",
                     D = form.AllKeys.Contains("satisfaction_D") ? form["satisfaction_D"].ToString() : "",
                     E = form.AllKeys.Contains("satisfaction_E") ? form["satisfaction_E"].ToString() : "",
                     F = form.AllKeys.Contains("satisfaction_F") ? form["satisfaction_F"].ToString() : "",
                     G = form.AllKeys.Contains("satisfaction_G") ? form["satisfaction_G"].ToString() : "",
                     H = form.AllKeys.Contains("satisfaction_H") ? form["satisfaction_H"].ToString() : "",
                     I = form.AllKeys.Contains("satisfaction_I") ? form["satisfaction_I"].ToString() : "",
                     J = form.AllKeys.Contains("satisfaction_J") ? form["satisfaction_J"].ToString() : "",
                     K = form.AllKeys.Contains("satisfaction_K") ? form["satisfaction_K"].ToString() : "",
                     L = form.AllKeys.Contains("satisfaction_L") ? form["satisfaction_L"].ToString() : "",
                     M = form.AllKeys.Contains("satisfaction_M") ? form["satisfaction_M"].ToString() : "",
                     N = form.AllKeys.Contains("satisfaction_N") ? form["satisfaction_N"].ToString() : "",
                     O = form.AllKeys.Contains("satisfaction_O") ? form["satisfaction_O"].ToString() : "",
                     P = form.AllKeys.Contains("satisfaction_P") ? form["satisfaction_P"].ToString() : "",
                 });

                //問題
                string problems = "";
                List<problem_category> category = DB.problem_category.Where(m => m.lang == changeLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
                List<problem> problem = DB.problem.Where(m => m.lang == changeLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
                int code = 65;
                int a = 1;
                foreach (problem_category item in category)
                {
                    problems += " <tr><td style=\"padding: 5px; border: 1px solid #dedcdc;\" align=\"left\" colspan=\"2\">" + item.title + "</td></tr>";

                    int b = 1;
                    foreach (problem subItem in problem.Where(m => m.category == item.guid).ToList())
                    {
                        string val = form.AllKeys.Contains("satisfaction_" + changeASCII(code)) ? form["satisfaction_" + changeASCII(code)].ToString() : "";
                        problems += " <tr><td style=\"padding: 5px; border: 1px solid #dedcdc;\" align=\"left\" colspan=\"2\">" + subItem.title + "：+" + val + "</td></tr>";
                        code++;
                        b++;
                    }

                }



                form.Add("problems", problems);

                List<string> comprehensive = new List<string>();

                for (int i = 1; i <= 11; i++)
                {
                    if (form.AllKeys.Contains("problem_comprehensive_" + i.ToString()))
                    {
                        comprehensive.Add(form["problem_comprehensive_" + i.ToString()].ToString() ?? "");
                    }

                }


                List<string> env = new List<string>();

                for (int i = 1; i <= 6; i++)
                {
                    if (form.AllKeys.Contains("problem_env_" + i.ToString() + i.ToString()))
                    {
                        env.Add(form["problem_env_" + i.ToString() + i.ToString()].ToString() ?? "");
                    }
                }

                try
                {
                    data.problem = JsonConvert.SerializeObject(
                    new problemData
                    {
                        comprehensive = JsonConvert.SerializeObject(comprehensive),
                        comprehensive_other = form["problem_comprehensive_other"].ToString(),

                        env = JsonConvert.SerializeObject(env),
                        env_other = form["problem_env_other"].ToString(),

                        company = form["problem_company"].ToString(),
                        company_other = form["problem_company_other"].ToString(),
                    });
                }
                catch { }


                data.message = form["message"].ToString();

                data.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                data.lang = changeLang;
                data.status = "Y";

                Session.Remove("form");
                DB.survey.Add(data);
                DB.SaveChanges();
                //return Redirect("~/business/survey");

                List<string> MailList = new List<string>();
                MailList.Add(webData.UrbanContactEmail);

                FunctionService.sendMailIndex(MailList, "客戶滿意度調查", changeLang, "/Content/Mail/survey.html", form);




                return RedirectPermanent(Url.Content("~/" + changeLang + "/business/SurveySendEnd"));
            }





        }

        public ActionResult SurveySendEnd()
        {
            ViewBag.title = langData["客戶滿意度調查"];

            return View();
        }


        public static string changeASCII(int code)
        {

            byte[] array = new byte[1];
            array[0] = (byte)(Convert.ToInt32(code)); //ASCII碼強制轉換二進制
            string str = Convert.ToString(System.Text.Encoding.ASCII.GetString(array));

            return str;
        }
    }
}