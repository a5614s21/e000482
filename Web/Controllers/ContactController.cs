﻿using HuLane.BuildBlock;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuLane.Main.Repositories;
using Web.Models;
using Scriban;
using Web.DB.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Web.Service;
using Newtonsoft.Json;

namespace Web.Controllers
{
    public class ContactController : BaseController
    {
        
        protected Dictionary<string, string> active = new Dictionary<string, string>();


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            headerActive.Add("contact", "active");
            ViewBag.headerActive = headerActive;//header active
            ViewBag.ResLang = langData;//取得語系檔案
        }

            // GET: About
        public ActionResult Index()
        {
            ViewBag.title = langData["聯絡我們"];

            List<SearchResultDetailVM> searchResult = new List<SearchResultDetailVM>();

            //有諮詢商品
            if (Session["Inquery"] != null)
            {              
                List<string> InqueryId = (List<string>)Session["Inquery"];
                var productService = new ProductService();
                searchResult = productService.productAllData(InqueryId , new SearchBySpecVM());
            }

            ViewBag.form = new FormCollection();
            if (Session["form"] != null)
            {
                ViewBag.form = (FormCollection)Session["form"];
            }

            string filecheck = "picture1";
            string productfilename;

            foreach (var item in searchResult)
            {
                using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    conn2.Open();
                    string img_sql = "select * from product_file where product_id= " + item.id + " and filecheck= '" + filecheck + "'";
                    using (SqlCommand cmd = conn2.CreateCommand())
                    {
                        cmd.CommandText = img_sql;
                        cmd.CommandType = CommandType.Text;
                        //SqlDataAdapter dAda = new SqlDataAdapter(cmd);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            productfilename = reader[2].ToString();
                            if (productfilename != "")
                            {
                                var temp = productfilename.Split('.');
                                var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + item.id + "/" + temp[0] + "_WM." + temp[1];
                                item.productImg = productImg;
                            }
                            else
                            {
                                item.productImg = "";
                            }
                        }
                    }
                }
            }

            ViewBag.products = searchResult;

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Send(FormCollection form)
        {

            Session.Remove("form");
            Session.Add("form", form);


            if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/contact/index"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                //Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                return RedirectPermanent(Url.Content("~/" + defLang + "/Home/Alert"));
            }
            else
            {
                contact data = new contact();


                data.guid = Guid.NewGuid().ToString();
                data.name = form["name"].ToString();            
                data.email = form["email"].ToString();
                data.company = form["company"].ToString();
                data.department = form["department"].ToString();
                data.jobTitle = form["jobTitle"].ToString();
                data.country = form["country"].ToString();
                data.website = form["website"].ToString();
                data.tel = form["tel"].ToString();
                data.message = form["message"].ToString();
                data.status = "N";
                data.lang = defLang;

                data.products = "";

                //有諮詢商品
                if (Session["Inquery"] != null)
                {
                    List<SearchResultDetailVM> searchResult = new List<SearchResultDetailVM>();

                    List<string> InqueryId = (List<string>)Session["Inquery"];
                    var productService = new ProductService();
                    searchResult = productService.productAllData(InqueryId, new SearchBySpecVM());
                    foreach (var item in searchResult)
                    {
                        var tempImg = (from a in DB.product_info
                                       where a.product_id == item.id
                                       select a.pic).FirstOrDefault();
                        item.productImg = tempImg;
                    }
                    data.products = JsonConvert.SerializeObject(searchResult);
                }

                data.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                Session.Remove("form");
                DB.contact.Add(data);
                DB.SaveChanges();
                //return Redirect("~/business/survey");

                Session.Remove("Inquery");

                List<string> MailList = new List<string>();
                MailList.Add(webData.ContactEmail);

                FunctionService.sendMailIndex(MailList, "客戶聯絡表單", defLang, "/Content/Mail/contact.html", form);

                return RedirectPermanent(Url.Content("~/" + defLang + "/contact/ContactSend"));
            }


        }

        public ActionResult ContactSend()
        {
            ViewBag.title = langData["聯絡我們"];

            return View();
        }


    }
}