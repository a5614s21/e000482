﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.DB.Models;
using Web.Repository.Notes;

namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);         
            ViewBag.headerActive = headerActive;
            ViewBag.ResLang = langData;//取得語系檔案
        }

        public ActionResult Index()
        {
            var changeLang = langData["語系"].ToString();

            ViewBag.banner = DB.advertise.Where(m => m.status == "Y")
                                         .Where(m => m.lang == changeLang)
                                         .OrderBy(m=>m.sortIndex)
                                         .Where(m => m.category == "banner")
                                         .ToList();

            ViewBag.footer = DB.advertise.Where(m => m.status == "Y")
                                    .Where(m => m.lang == changeLang)
                                    .OrderBy(m => m.sortIndex)
                                    .Where(m => m.category == "footer")
                                    .ToList();

            ViewBag.indexContent = DB.index_content
                        .Where(m => m.lang == changeLang)
                        .ToList();

            ViewBag.news = DB.news.Where(m => m.status == "Y")
                                     .Where(m => m.lang == changeLang)
                                     .Where(m => m.tops == "Y")
                                     .OrderByDescending(m => m.startdate)
                                     .ToList();

            return View();
        }

        public ActionResult Sitemap()
        {
            return View();
        }

        public ActionResult privacy()
        {
            ViewBag.title = ViewBag.ResLang["隱私權政策"];

            ViewBag.data = NotesData.Single(defLang , "2");

            var seoDescription = ViewBag.data.seo_description;
            ViewBag.description = seoDescription;
            var seoKeywords = ViewBag.data.seo_keywords;
            ViewBag.keywords = seoKeywords;

            return View();
        }

        public ActionResult terms()
        {
            ViewBag.title = ViewBag.ResLang["網站使用條款"];

            ViewBag.data = NotesData.Single(defLang, "3");

            var seoDescription = ViewBag.data.seo_description;
            ViewBag.description = seoDescription;
            var seoKeywords = ViewBag.data.seo_keywords;
            ViewBag.keywords = seoKeywords;

            return View();
        }

        public ActionResult SitemapSpec(string spec)
        {
            TempData["HomeSearch"] = new SearchBySpecVM() { ProductTable = spec };

            return RedirectToAction("spec","products");
        }

        public ActionResult SitemapApply(int activeCategory)
        {
            TempData["HomeSearch"] = new SearchByApplyVM() { Id = activeCategory };

            return RedirectToAction("apply", "products");
        }


        public ActionResult Alert()
        {
            if (Session["alertData"] != null)
            {
                ViewBag.alertData = Session["alertData"];
                Session.Remove("alertData");

                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }
    }
}