﻿using HuLane.BuildBlock;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuLane.Main.Repositories;
using Web.Models;
using Scriban;
using Web.DB.Models;
using PagedList;
using Web.Service;
using System.Globalization;

namespace Web.Controllers
{
    public class InvestorController : BaseController
    {

        protected Dictionary<string, string> topActive = new Dictionary<string, string>();
        protected Dictionary<string, string> active = new Dictionary<string, string>();
        protected List<news_category> categoriec = new List<news_category>();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            headerActive.Add("investor", "active");
            ViewBag.headerActive = headerActive;//header active
            ViewBag.ResLang = langData;//取得語系檔案

        }

        public ActionResult List()
        {
            active.Add("index", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("index", "class=\"active\"");
            ViewBag.topActive = topActive;


            ViewBag.title = langData["投資人專區"];
            return View();
        }

        public ActionResult Stakeholder()
        {
            active.Add("stakeholder", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("stakeholder", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["利害關係人"];
            ViewBag.data = getData("4");

            if (ViewBag.data != null)
            {
                var seoDescription = ViewBag.data.seo_description;
                ViewBag.description = seoDescription;
                var seoKeywords = ViewBag.data.seo_keywords;
                ViewBag.keywords = seoKeywords;

                ViewBag.isStakeholder = "true";
            }
            else
            {
                ViewBag.isStakeholder = "false";
            }

            return View();
        }

        public ActionResult Csr(int? page)
        {
            active.Add("csr", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("csr", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["企業社會責任"];

            string year = !string.IsNullOrEmpty(Request["year"]) ? Request["year"].ToString() : "";
            ViewBag.year = year;

            Dictionary<string, object> data = getDownloadsPage("csr", "/investor/csr?", 8, page ?? 1, year);
            ViewBag.data = data;




            return View();
        }


        public ActionResult Meeting(int? page)
        {
            active.Add("meeting", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("corporate", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["股東會"];

            string year = !string.IsNullOrEmpty(Request["year"]) ? Request["year"].ToString() : "";
            ViewBag.year = year;

            Dictionary<string, object> data = getDownloadsPage("meeting", "/investor/csr?", 20, page ?? 1, year);
            ViewBag.data = data;

            //取得年分(分頁)

            List<int> pageYear = formatYear((List<Year>)data["years"]);
            ViewBag.pageYear = pageYear;

            return View();
        }

        public ActionResult Diretors(int? page)
        {
            active.Add("diretors", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("corporate", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["董事會"];

            string year = !string.IsNullOrEmpty(Request["year"]) ? Request["year"].ToString() : "";
            ViewBag.year = year;


            Dictionary<string, object> diretors_member = getDownloads("diretors_member", year);
            List<int> pageYear = formatYear((List<Year>)diretors_member["years"]);
            ViewBag.diretors_member = diretors_member;



            Dictionary<string, object> diretors_info = getDownloads("diretors_info", year);
            List<int> pageYear2 = formatYear((List<Year>)diretors_info["years"]);
            ViewBag.diretors_info = diretors_info;


            Dictionary<string, object> diretors_resolution = getDownloads("diretors_resolution", year);
            List<int> pageYear3 = formatYear((List<Year>)diretors_resolution["years"]);
            ViewBag.diretors_resolution = diretors_resolution;


            pageYear = pageYear.Union(pageYear2).ToList<int>();
            pageYear = pageYear.Union(pageYear3).ToList<int>();
            ViewBag.pageYear = pageYear;



            return View();
        }

        public ActionResult Regulations(int? page)
        {
            active.Add("regulations", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("corporate", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["公司規章"];


            string year = !string.IsNullOrEmpty(Request["year"]) ? Request["year"].ToString() : "";
            ViewBag.year = year;

            Dictionary<string, object> data = getDownloadsPage("regulations", "/investor/regulations?", 20, page ?? 1, year);
            ViewBag.data = data;

            //取得年分(分頁)

            List<int> pageYear = formatYear((List<Year>)data["years"]);
            ViewBag.pageYear = pageYear;


            return View();
        }
        public ActionResult Audit()
        {
            var changeLang = langData["語系"].ToString();
            active.Add("audit", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("corporate", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["內部稽核組織及運作"];

            Dictionary<string, object> audit_master = getDownloads("audit", "");
            ViewBag.audit_master = audit_master;

            Dictionary<string, object> audit_implement = getDownloads("audit_implement", "");
            ViewBag.audit_implement = audit_implement;

            Dictionary<string, object> audit_comm = getDownloads("audit_comm", "");
            ViewBag.audit_comm = audit_comm;

            Dictionary<string, object> audit_member = getDownloads("audit_member", "");
            ViewBag.audit_member = audit_member;

            var addCategory = DB.download_category
.Where(m => m.lang == changeLang)
.Where(m => m.guid != "audit")
.Where(m => m.guid != "audit_comm")
.Where(m => m.guid != "audit_implement")
.Where(m => m.guid != "audit_master")
.Where(m => m.guid != "audit_member")
.Where(m => m.guid != "corporate")
.Where(m => m.guid != "csr")
.Where(m => m.guid != "diretors")
.Where(m => m.guid != "diretors_info")
.Where(m => m.guid != "diretors_member")
.Where(m => m.guid != "diretors_resolution")
.Where(m => m.guid != "financial_report")
.Where(m => m.guid != "investor")
.Where(m => m.guid != "meeting")
.Where(m => m.guid != "monthly_report")
.Where(m => m.guid != "organization")
.Where(m => m.guid != "organization_master")
.Where(m => m.guid != "organization_service")
.Where(m => m.guid != "regulations")
.Where(m => m.status == "Y")
.OrderBy(m => m.sortIndex)
.ToList();
            ViewBag.addCategory = addCategory;

            Dictionary<string, object> addTitle = getDownloads("addTitle", "");
            ViewBag.addTitle = addTitle;

            return View();
        }

        public ActionResult OrganizationDownload()
        {
            active.Add("organizationDownload", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("corporate", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["關係企業組織圖"];


            string year = !string.IsNullOrEmpty(Request["year"]) ? Request["year"].ToString() : "";
            ViewBag.year = year;


            Dictionary<string, object> organization = getDownloads("organization", year);
            List<int> pageYear = formatYear((List<Year>)organization["years"]);
            ViewBag.organization = organization;

            Dictionary<string, object> organization_service = getDownloads("organization_service", year);
            List<int> pageYear2 = formatYear((List<Year>)organization_service["years"]);
            ViewBag.organization_service = organization_service;


            pageYear = pageYear.Union(pageYear2).ToList<int>();

            ViewBag.pageYear = pageYear;


            return View();
        }

        public ActionResult MonthlyReport()
        {
            active.Add("monthlyReport", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("monthlyReport", "class=\"active\"");
            ViewBag.topActive = topActive;


            ViewBag.title = langData["月營業額報告"];

            string year = !string.IsNullOrEmpty(Request["year"]) ? Request["year"].ToString() : "";
            ViewBag.year = year;

            Dictionary<string, object> monthly_report = getDownloads("monthly_report", year);
            ViewBag.monthly_report = monthly_report;
            //取得年分(分頁)

            List<int> pageYear = formatYear((List<Year>)monthly_report["years"]);
            ViewBag.pageYear = pageYear;


            return View();
        }


        public ActionResult FinancialReport(int? page)
        {
            active.Add("financialReport", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("monthlyReport", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["財務報告"];



            string year = !string.IsNullOrEmpty(Request["year"]) ? Request["year"].ToString() : "";
            ViewBag.year = year;

            Dictionary<string, object> data = getDownloadsPage("financial_report", "/investor/financialReport?", 20, page ?? 1, year);
            ViewBag.data = data;

            //取得年分(分頁)

            List<int> pageYear = formatYear((List<Year>)data["years"]);
            ViewBag.pageYear = pageYear;




            return View();
        }

        public ActionResult CorporateBriefing(int? page)
        {
            active.Add("corporateBriefing", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("corporateBriefing", "class=\"active\"");
            ViewBag.topActive = topActive;


            ViewBag.title = langData["法人說明會"];

            //投資人資訊

            Dictionary<string, object> investor = getDownloads("investor", "");
            ViewBag.investor = investor;



            string year = !string.IsNullOrEmpty(Request["year"]) ? Request["year"].ToString() : "";
            ViewBag.year = year;

            Dictionary<string, object> data = getDownloadsPage("corporate", "/investor/corporateBriefing?", 20, page ?? 1, year);
            ViewBag.data = data;

            //取得年分(分頁)

            List<int> pageYear = formatYear((List<Year>)data["years"]);
            ViewBag.pageYear = pageYear;



            return View();
        }



        public ActionResult ShareholderFaq(int? page)
        {
            var changeLang = langData["語系"].ToString();

            active.Add("shareholderFaq", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("shareholderFaq", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["投資人問答集"];

            int pageWidth = 10;//預設每頁長度
            var pageNumeber = page ?? 1;

            string keyword = !string.IsNullOrEmpty(Request["keyword"]) ? Request["keyword"].ToString() : "";
            ViewBag.keyword = keyword;

            //資訊列表
            var news = DB.shareholder_faq.Where(m => m.lang == changeLang)
                .Where(m => m.status == "Y")
                .Where(m => m.title.ToString().Contains(keyword) || m.notes.ToString().Contains(keyword))
                .OrderBy(m => m.sortIndex);


            ViewBag.SearchCasesCount = news.Count();//取總筆數

            ViewBag.list = news.ToPagedList(pageNumeber, pageWidth);

            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)ViewBag.SearchCasesCount / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + changeLang + "/investor/shareholderFaq?R", Allpage, pageNumeber, ViewBag.SearchCasesCount, "&keyword=" + keyword);//頁碼



            return View();
        }

        public ActionResult Inquire()
        {
            active.Add("inquire", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("shareholderFaq", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["投資人意見留言板"];

            ViewBag.form = new FormCollection();
            if (Session["form"] != null)
            {
                ViewBag.form = (FormCollection)Session["form"];
            }

            return View();
        }




        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult InquireSend(FormCollection form)
        {
            var changeLang = langData["語系"].ToString();

            Session.Remove("form");
            Session.Add("form", form);


            if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + changeLang + "/investor/inquire"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                //Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                return RedirectPermanent(Url.Content("~/" + changeLang + "/Home/Alert"));
            }
            else
            {
                inquire data = new inquire();


                data.guid = Guid.NewGuid().ToString();
                data.name = form["name"].ToString();
                data.email = form["email"].ToString();
                data.company = form["company"].ToString();
                data.department = form["department"].ToString();
                data.jobTitle = form["jobTitle"].ToString();
                data.country = form["country"].ToString();
                data.website = form["website"].ToString();
                data.tel = form["tel"].ToString();
                data.message = form["message"].ToString();
                data.fax = form["fax"].ToString();
                data.address = form["address"].ToString();

                data.status = "N";
                data.lang = changeLang;


                data.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                Session.Remove("form");
                DB.inquire.Add(data);
                DB.SaveChanges();
                //return Redirect("~/business/survey");

                List<string> MailList = new List<string>();
                MailList.Add(webData.CaseEmail);


                FunctionService.sendMailIndex(MailList, "投資人意見留言板", changeLang, "/Content/Mail/inquire.html", form);


                return RedirectPermanent(Url.Content("~/" + changeLang + "/investor/inquireSendEend"));
            }


        }

        public ActionResult inquireSendEend()
        {
            active.Add("inquire", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("shareholderFaq", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["投資人意見留言板"];

            return View();
        }

        public ActionResult Service()
        {
            active.Add("service", "class=\"text-red\"");
            ViewBag.active = active;

            topActive.Add("service", "class=\"active\"");
            ViewBag.topActive = topActive;

            ViewBag.title = langData["投資人服務"];
            ViewBag.data = getData("5");
            ViewBag.description = ViewBag.data.seo_description;
            ViewBag.keywords = ViewBag.data.seo_keywords;


            return View();
        }


        public notes_data getData(string guid)
        {
            var changeLang = langData["語系"].ToString();

            notes_data data = DB.notes_data.Where(m => m.lang == changeLang).Where(m => m.guid == guid).Where(m => m.status == "Y").FirstOrDefault();
            return data;
        }

        /// <summary>
        /// 下載含頁碼
        /// </summary>
        /// <param name="category"></param>
        /// <param name="path"></param>
        /// <param name="pageWidth"></param>
        /// <param name="page"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public Dictionary<string, object> getDownloadsPage(string category, string path, int pageWidth = 8, int page = 1, string year = "")
        {
            var changeLang = langData["語系"].ToString();

            Dictionary<string, object> re = new Dictionary<string, object>();

            var data = DB.downloads
                .Where(m => m.lang == changeLang)
                .Where(m => m.category == category)
                .Where(m => m.status == "Y")
                .OrderBy(m => m.sortIndex)
                .Where(m => m.startdate.ToString().Contains(year)); ;

            int SearchCasesCount = data.Count();//取總筆數

            var list = data.ToPagedList(page, pageWidth);

            re.Add("list", list);

            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)SearchCasesCount / pageWidth));//總頁數

            string pageView = FunctionService.getPageNum(Url.Content("~/") + changeLang + path, Allpage, page, SearchCasesCount, "&year=" + year);//頁碼

            re.Add("pageView", pageView);


            //取得年分
            var years = DB.downloads.Where(m => m.lang == changeLang)
                 .Where(m => m.status == "Y")
                 .Where(m => m.category == category)
                 .OrderByDescending(m => m.startdate)
                 .Select(x => new Year() { year = x.startdate.ToString().Substring(6, 4) })
                 .GroupBy(m => m.year)
                 .OrderByDescending(m => m.Key)
                 .Select(x => new Year() { year = x.Key })
                 .ToList();

            re.Add("years", years);

            return re;
        }



        public Dictionary<string, object> getDownloads(string category, string year = "")
        {
            var changeLang = langData["語系"].ToString();

            Dictionary<string, object> re = new Dictionary<string, object>();

            if (category == "addTitle")
            {
                var data = DB.downloads
.Where(m => m.lang == changeLang)
.Where(m => m.category != "audit_master,audit")
.Where(m => m.category != "audit")
.Where(m => m.category != "audit_comm")
.Where(m => m.category != "audit_implement")
.Where(m => m.category != "audit_master")
.Where(m => m.category != "audit_member")
.Where(m => m.category != "corporate")
.Where(m => m.category != "csr")
.Where(m => m.category != "diretors")
.Where(m => m.category != "diretors_info")
.Where(m => m.category != "diretors_member")
.Where(m => m.category != "diretors_resolution")
.Where(m => m.category != "financial_report")
.Where(m => m.category != "organization")
.Where(m => m.category != "organization_master")
.Where(m => m.category != "organization_service")
.Where(m => m.category != "regulations")
.Where(m => m.status == "Y")
.OrderBy(m => m.sortIndex)
.Where(m => m.startdate.ToString().Contains(year))
.ToList();
                re.Add("list", data);
            }
            else
            {
                var data = DB.downloads
    .Where(m => m.lang == changeLang)
    .Where(m => m.category == category)
    .Where(m => m.status == "Y")
    .OrderBy(m => m.sortIndex)
    .Where(m => m.startdate.ToString().Contains(year))
    .ToList();

                re.Add("list", data);
            }

            //取得年分
            var years = DB.downloads.Where(m => m.lang == changeLang)
                 .Where(m => m.status == "Y")
                 .Where(m => m.category == category)
                 .OrderByDescending(m => m.startdate)
                 .Select(x => new Year() { year = x.startdate.ToString().Substring(6, 4) })
                 .GroupBy(m => m.year)
                 .OrderByDescending(m => m.Key)
                 .Select(x => new Year() { year = x.Key })
                 .ToList();

            re.Add("years", years);

            return re;
        }



        public List<int> formatYear(dynamic data)
        {
            //取得年分(分頁)
            List<int> pageYear = new List<int>();
            foreach (Year item in data)
            {
                if (!string.IsNullOrEmpty(item.year.ToString()))
                {

                    if (pageYear.IndexOf(int.Parse(item.year.ToString())) == -1)
                    {
                        pageYear.Add(int.Parse(item.year.ToString()));
                    }

                }
            }
            pageYear = pageYear.OrderByDescending(i => i).ToList();

            return pageYear;
        }
    }
}