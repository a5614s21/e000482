﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Web.Models;

namespace Web.Controllers
{
    public class MemberController : Controller
    {
        // GET: Member
        public ActionResult Index()
        {
            if(!this.User.Identity.IsAuthenticated)
                return View();
            else
                return Redirect("/Member/MemberProfile");
        }

        [HttpPost]
        public ActionResult Login(LoginVM loginVM) 
        {
            if (loginVM.Account == "demo" && loginVM.Password == "123456" && loginVM.VerificationCode == TempData["verificationCode"].ToString())
            {
                var ticket = new FormsAuthenticationTicket(
                version: 1,
                name: loginVM.Account,
                issueDate: DateTime.UtcNow,
                expiration: DateTime.UtcNow.AddMinutes(60),
                isPersistent: true,
                userData: $"account={loginVM.Account}",
                cookiePath: FormsAuthentication.FormsCookiePath);

                var encryptedTicket = FormsAuthentication.Encrypt(ticket); //把驗證的表單加密
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                Response.Cookies.Add(cookie);

                var rootPath = Request.ApplicationPath;
                if (rootPath == "/")
                    return Redirect("/Member/MemberProfile");
                else
                    return Redirect(rootPath + "/Member/MemberProfile");

            }
            else 
            {
                var rootPath = Request.ApplicationPath;
                if(rootPath == "/")
                    return Redirect("/Member");
                else
                    return Redirect(rootPath + "/Member");
            }
        }

        public ActionResult MemberProfile() 
        {
            return View();
        }

        public ActionResult GetValidateCode()
        {
            byte[] data = null;
            string code = RandomCode(6);
            TempData["verificationCode"] = code;
            //定義一個畫板
            MemoryStream ms = new MemoryStream();
            using (Bitmap map = new Bitmap(132, 50))
            {
                //畫筆,在指定畫板畫板上畫圖
                //g.Dispose();
                using (Graphics g = Graphics.FromImage(map))
                {
                    g.Clear(Color.White);
                    SolidBrush solidBrush = new SolidBrush(Color.FromArgb(55, 121, 241));
                    g.DrawString(code, new Font("Courier New", 24.0F, FontStyle.Bold), solidBrush, new Point(10, 8));
                    //繪製干擾線(數字代表幾條)
                    PaintInterLine(g, 10, map.Width, map.Height);
                }
                map.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            data = ms.GetBuffer();
            return File(data, "image/jpeg");
        }

        private string RandomCode(int length)
        {
            string s = "0123456789zxcvbnmasdfghjklqwertyuiop";
            StringBuilder sb = new StringBuilder();
            Random rand = new Random();
            int index;
            for (int i = 0; i < length; i++)
            {
                index = rand.Next(0, s.Length);
                sb.Append(s[index]);
            }
            return sb.ToString();
        }

        private void PaintInterLine(Graphics g, int num, int width, int height)
        {
            Random r = new Random();
            int startX, startY, endX, endY;
            for (int i = 0; i < num; i++)
            {
                startX = r.Next(0, width);
                startY = r.Next(0, height);
                endX = r.Next(0, width);
                endY = r.Next(0, height);
                g.DrawLine(new Pen(Brushes.LightGray), startX, startY, endX, endY);
            }
        }
    }
}