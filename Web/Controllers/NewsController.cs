﻿using HuLane.BuildBlock;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuLane.Main.Repositories;
using Web.Models;
using Scriban;
using Web.DB.Models;
using PagedList;
using Web.Service;
using System.Globalization;

namespace Web.Controllers
{
    public class NewsController : BaseController
    {
        
        protected Dictionary<string, string> active = new Dictionary<string, string>();
        protected List<news_category> categoriec = new List<news_category>();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            headerActive.Add("news", "active");
            ViewBag.headerActive = headerActive;//header active
            ViewBag.ResLang = langData;//取得語系檔案
            var changeLang = langData["語系"].ToString();
            categoriec = DB.news_category.Where(m => m.lang == changeLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            ViewBag.category = categoriec;
        }

            // GET: About
        public ActionResult Index(string id, int? page)
        {
            var changeLang = langData["語系"].ToString();
            ViewBag.title = langData["最新消息"];

            id = string.IsNullOrEmpty(id) ? categoriec.FirstOrDefault().guid : id;

            ViewBag.id = id;
            ViewBag.title = categoriec.Where(m => m.guid == id).FirstOrDefault().title;

            int pageWidth = 9;//預設每頁長度
            var pageNumeber = page ?? 1;

            string year = !string.IsNullOrEmpty(Request["year"]) ? Request["year"].ToString() : "";
            ViewBag.year = year;
            //資訊列表
            var news = DB.news.Where(m => m.lang == changeLang)
                .Where(m => m.status == "Y")
                .Where(m => m.category == id)
                .OrderByDescending(m => m.startdate)
                .Where(m => m.startdate.ToString().Contains(year));


            ViewBag.SearchCasesCount = news.Count();//取總筆數

            ViewBag.list = news.ToPagedList(pageNumeber, pageWidth);

            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)ViewBag.SearchCasesCount / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + changeLang + "/news/index/" + id + "?", Allpage, pageNumeber, ViewBag.SearchCasesCount, "&year="+year);//頁碼

            //取得新聞年分
           var years = DB.news.Where(m => m.lang == changeLang)
                .Where(m => m.status == "Y")
                .Where(m => m.category == id)
                .OrderByDescending(m => m.startdate)
                .Select(x => new Year(){ year = x.startdate.ToString().Substring(6,4) })
                .GroupBy(m=>m.year)
                .OrderByDescending(m => m.Key)
                .Select(x => new Year(){year = x.Key})
                .ToList();

            ViewBag.years = years;

            return View();
        }

        /// <summary>
        /// 新聞內容
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Detail(string id)
        {
            var changeLang = langData["語系"].ToString();
            news data = DB.news.Where(m => m.lang == changeLang).Where(m => m.guid == id).FirstOrDefault();
            ViewBag.data = data;
            ViewBag.categoryData = categoriec.Where(m => m.guid == data.category).FirstOrDefault();
            ViewBag.title = data.title;

            //取得新聞年分
            var years = DB.news.Where(m => m.lang == changeLang)
                 .Where(m => m.status == "Y")
                 .Where(m => m.category == data.category)
                 .OrderByDescending(m => m.startdate)
                 .Select(x => new Year() { year = x.startdate.ToString().Substring(6, 4) })
                 .GroupBy(m => m.year)
                 .OrderByDescending(m => m.Key)
                 .Select(x => new Year() { year = x.Key })
                 .ToList();

            ViewBag.years = years;



            List<news> newsList = DB.news.Where(m => m.status == "Y")
                                         .Where(m => m.lang == changeLang)
                                                  .OrderByDescending(m => m.startdate)
                                                  // .ThenByDescending(m => m.create_date)
                                                  .Where(m => m.category == data.category)
                                                  .ToList();

            List<string> ids = new List<string>();


            foreach (news item in newsList)
            {
                ids.Add(item.guid);
            }

            int arrayIndex = ids.IndexOf(id);

            news prevData = null;

            try
            {
                if (ids[arrayIndex - 1] != null)
                {
                    string prevId = ids[arrayIndex - 1].ToString();
                    prevData = DB.news.Where(m => m.guid == prevId).FirstOrDefault();
                }
            }
            catch { }

            ViewBag.prevData = prevData;

            news nextData = null;
            try
            {
                if (ids[arrayIndex + 1] != null)
                {
                    string nextId = ids[arrayIndex + 1].ToString();
                    nextData = DB.news.Where(m => m.guid == nextId).FirstOrDefault();
                }
            }
            catch { }


            ViewBag.nextData = nextData;

            return View();
        }


    }
}