﻿using Dapper;
using Hangfire;
using HuLane.BuildBlock;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.App_Start;
using Web.DB.Models;
using Web.Models;

namespace Web.Controllers
{
    public class ProductController : BaseController
    {
        private IConnectionFactory _connectionFactory = null;
        public ProductController(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            headerActive.Add("product", "active");
            ViewBag.headerActive = headerActive;//header active
            ViewBag.ResLang = langData;//取得語系檔案
        }
        // GET: Product
        public ActionResult Index(decimal id)
        {
            var seoDescription = (from a in DB.product_info
                                  where a.product_id == id
                                  select a.seo_description).FirstOrDefault();
            ViewBag.description = seoDescription;
            var seoKeywords = (from a in DB.product_info
                               where a.product_id == id
                               select a.seo_keywords).FirstOrDefault();
            ViewBag.keywords = seoKeywords;

            return View();
        }

        [HttpPost]
        public JsonResult Get(decimal id)
        {
            var changeLang = langData["語系"].ToString();
            ProductVerboseVM productVerboseVM = new ProductVerboseVM();
            using (var conn = _connectionFactory.CreateProductDBConnection())
            {
                dynamic productInfo = null;
                string sql = "select id,name,new_name,[type] from product where id=@id";

                productInfo = conn.QuerySingle(sql, new { id = id });

                string judgeType = productInfo.type;

                switch (productInfo.type)
                {
                    case "Housing":
                        sql = "select '膠盒' as type,'Housing' as o_type,material,item_class,positions,packaging as packaging_o,seal_capability,quantity,temper_operating,flamm_rating,color as color_o,sealable as sealable_o,sex as sex_o,plate_thickness,plate_form,rows,width,length,height,series,leaking from housing where product_id=@id";
                        productVerboseVM = conn.QuerySingleOrDefault<HousingVM>(sql, new { id = id });
                        fillProductIdName(productVerboseVM, productInfo);

                        break;
                    case "Terminal":
                        sql = "select '端子' as type,'Terminal' as o_type,material,e_init,e_allow,feed_type,depth,plating,tab,item_class,packaging as packaging_o,quantity,sealable as sealable_o,sex as sex_o,length,width,height,diameter,wire_size,elongation from terminal where product_id=@id";
                        productVerboseVM = conn.QuerySingleOrDefault<TerminalVM>(sql, new { id = id });
                        fillProductIdName(productVerboseVM, productInfo);
                        break;
                    case "Rubber":
                        sql = "select '防水栓' as type,'Rubber' as o_type,item_class,quantity,material,diameter_1,diameter_2,temper_operating,diameter,color as color_o,length,width from rubber where product_id=@id";
                        productVerboseVM = conn.QuerySingleOrDefault<RubberVM>(sql, new { id = id });
                        fillProductIdName(productVerboseVM, productInfo);
                        break;
                    case "Sleeve":
                        sql = "select 'PVC絕緣護套' as type,'Sleeve' as o_type,material,diameter_1,diameter_2,elongation,width_1,width_2,length_1,length_2,item_class,quantity,temper_operating,color as color_o,applicable_wire as diameter,length,width,height from sleeve where product_id=@id";
                        productVerboseVM = conn.QuerySingleOrDefault<SleeveVM>(sql, new { id = id });
                        fillProductIdName(productVerboseVM, productInfo);
                        break;
                    case "Grommet":
                        sql = "select '索環護套' as type,'Grommet' as o_type,material,item_class,quantity,temper_operating,color as color_o,length,width,height from grommet where product_id=@id";
                        productVerboseVM = conn.QuerySingleOrDefault<GrommetVM>(sql, new { id = id });
                        fillProductIdName(productVerboseVM, productInfo);
                        break;
                }

                SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                conn2.Open();
                string filecheck = "picture1";
                string productfilename;
                string img_sql = "select * from product_file where product_id= " + id + " and filecheck= '" + filecheck + "'";
                SqlCommand cmd = new SqlCommand(img_sql, conn2);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    productfilename = reader[2].ToString();
                    var temp = productfilename.Split('.');
                    if (productfilename != "" && productfilename != null)
                    {
                        var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + id + "/" + temp[0] + "_WM." + temp[1];
                        fillProductImg(productVerboseVM, productImg);
                    }
                }
                conn2.Close();

                SqlConnection conn3 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                conn3.Open();
                string filecheck2 = "picture2";
                string productspc;
                string img_sql2 = "select * from product_file where product_id= " + id + " and filecheck= '" + filecheck2 + "'";
                SqlCommand cmd2 = new SqlCommand(img_sql2, conn3);
                SqlDataReader reader2 = cmd2.ExecuteReader();
                while (reader2.Read())
                {
                    productspc = reader2[2].ToString();
                    var temp = productspc.Split('.');
                    if (productspc != "" && productspc != null)
                    {
                        var productSpc = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + id + "/" + temp[0] + "_WM." + temp[1];
                        fillProductSpc(productVerboseVM, productSpc);
                    }
                }
                conn3.Close();

                SqlConnection conn5 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                conn5.Open();
                string filecheck3 = "picture3";
                string productpaper;
                string img_sql3 = "select * from product_file where product_id= " + id + " and filecheck= '" + filecheck3 + "'";
                SqlCommand cmd3 = new SqlCommand(img_sql3, conn5);
                SqlDataReader reader3 = cmd3.ExecuteReader();
                while (reader3.Read())
                {
                    productpaper = reader3[2].ToString();
                    if (productpaper != "")
                    {
                        var temp = productpaper.Substring(0, productpaper.Length - 4);
                        var temp2 = productpaper.Substring(productpaper.Length - 4);
                        if (productpaper != "" && productpaper != null)
                        {
                            var productPaper = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + id + "/" + temp + "_WM" + temp2;
                            fillProductPaper(productVerboseVM, productPaper);
                        }
                    }
                }
                conn5.Close();

                SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                conn6.Open();

                string productnewname;

                string img_sql6 = "select * from product where id= " + id ;
                SqlCommand cmd6 = new SqlCommand(img_sql6, conn6);
                SqlDataReader reader6 = cmd6.ExecuteReader();
                while (reader6.Read())
                {
                    productnewname = reader6[6].ToString();
                    if (productnewname != "")
                    {
                        var productNewName = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/files/SeriesSpec_Latest/" + productnewname + ".html";
                        fillProduct3D(productVerboseVM, productNewName);
                    }
                }
                conn6.Close();

                if (productInfo.type == "Terminal")
                {
                    string sizeT = "L:" + productVerboseVM.length + " W:" + productVerboseVM.width + " H:" + productVerboseVM.height + " D:" + productVerboseVM.wire_size + " E:" + productVerboseVM.elongation;
                    fillProductSizeT(productVerboseVM, sizeT);
                    fillProductShowColor(productVerboseVM, false);
                    fillProductShowFrontColor(productVerboseVM, false);
                }
                else if (productInfo.type == "Rubber")
                {
                    string sizeR = "L:" + productVerboseVM.length + " D1:" + productVerboseVM.diameter_1 + " D2:" + productVerboseVM.diameter_2;
                    fillProductSizeR(productVerboseVM, sizeR);
                    fillProductShowColor(productVerboseVM, false);
                    fillProductShowFrontColor(productVerboseVM, true);
                }
                else if (productInfo.type == "Sleeve")
                {
                    string sizeS = "D1:" + productVerboseVM.diameter_1 + " D2:" + productVerboseVM.diameter_2 + " E:" + productVerboseVM.elongation + " W1:" + productVerboseVM.width_1 + " W2:" + productVerboseVM.width_2 + " L1:" + productVerboseVM.length_1 + " L2:" + productVerboseVM.length_2 + " L:" + productVerboseVM.length;
                    fillProductSize(productVerboseVM, sizeS);
                    fillProductShowColor(productVerboseVM, false);
                    fillProductShowFrontColor(productVerboseVM, true);
                }
                else if (productInfo.type == "Housing")
                {
                    SqlConnection conn7 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                    conn7.Open();

                    string productbook;

                    string img_sql7 = "select * from housing where product_id= " + id;
                    SqlCommand cmd7 = new SqlCommand(img_sql7, conn7);
                    SqlDataReader reader7 = cmd7.ExecuteReader();
                    while (reader7.Read())
                    {
                        productbook = reader7[39].ToString();
                        if (productbook != "")
                        {
                            //var productBook = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/files/SeriesSpec_Latest/" + productbook + ".pdf";
                            fillProductBook(productVerboseVM, productbook);
                        }
                    }
                    conn7.Close();

                    fillProductShowColor(productVerboseVM, true);
                    fillProductShowFrontColor(productVerboseVM, false);
                }
                else
                {
                    fillProductShowColor(productVerboseVM, true);
                    fillProductShowFrontColor(productVerboseVM, false);
                }

                //productVerboseVM.SetShowable();
                productVerboseVM.SetShowable_j(judgeType);

                string productData1 = changeLang == "tw" ? "排數" : changeLang == "cn" ? "排数" : "Number of Rows";
                string productData2 = changeLang == "tw" ? "極數" : changeLang == "cn" ? "极数" : "Number of Positions";
                string productData3 = changeLang == "tw" ? "材質" : changeLang == "cn" ? "材质" : "Material";
                string productData4 = changeLang == "tw" ? "表面處理" : changeLang == "cn" ? "表面处理" : "Treatment";
                string productData5 = changeLang == "tw" ? "料厚(mm)" : changeLang == "cn" ? "料厚(mm)" : "Thickness";
                string productData6 = changeLang == "tw" ? "公/母" : changeLang == "cn" ? "公/母" : "Male/Female";
                string productData7 = changeLang == "tw" ? "防水/非防水" : changeLang == "cn" ? "防水/非防水" : "Sealed/Unsealed";
                string productData8 = changeLang == "tw" ? "顏色" : changeLang == "cn" ? "顏色" : "Color";
                string productData9 = changeLang == "tw" ? "適用線徑(mm²)" : changeLang == "cn" ? "适用线径(mm²)" : "Applicable Wire Size";
                string productData10 = changeLang == "tw" ? "適用電線外徑(mm²)" : changeLang == "cn" ? "适用电线外径(mm²)" : "Insulatir O.D.";
                string productData11 = changeLang == "tw" ? "適用溫度(ﾟC)" : changeLang == "cn" ? "适用温度(ﾟC)" : "Operating Temperature Range(ﾟC)";
                string productData12 = changeLang == "tw" ? "單位包裝量" : changeLang == "cn" ? "单位包装量" : "Quantity(QUP)";
                string productData13 = changeLang == "tw" ? "包裝方式" : changeLang == "cn" ? "包装方式" : "Packing Type";
                string productData14 = changeLang == "tw" ? "料帶型式" : changeLang == "cn" ? "料带型式" : "Feeding Type";
                string productData15 = changeLang == "tw" ? "耐熱等級" : changeLang == "cn" ? "耐热等级" : "Flammability Rating";
                string productData16 = changeLang == "tw" ? "防水等級" : changeLang == "cn" ? "防水等級" : "Ingress Protection(IP)";
                string productData17 = changeLang == "tw" ? "氣密性(kPa)" : changeLang == "cn" ? "气密性(kPa)" : "Air Tightness(kPa)";
                string productData18 = changeLang == "tw" ? "CLIP孔徑(mm)" : changeLang == "cn" ? "CLIP孔径(mm)" : "Clip Bore Diameter(mm)";
                string productData19 = changeLang == "tw" ? "CLIP鈑厚(mm)" : changeLang == "cn" ? "CLIP钣厚(mm)" : "CLIP Stab Thickness(mm)";
                string productData20 = changeLang == "tw" ? "適用額定電流(A)" : changeLang == "cn" ? "适用额定电流(A)" : "Applicable Current(A)";
                string productData21 = changeLang == "tw" ? "接觸電阻(初始)(mΩ)" : changeLang == "cn" ? "接触电阻(初始)(mΩ)" : "Contact Resistance(Initial)(mΩ)";
                fillProductData1(productVerboseVM, productData1);
                fillProductData2(productVerboseVM, productData2);
                fillProductData3(productVerboseVM, productData3);
                fillProductData4(productVerboseVM, productData4);
                fillProductData5(productVerboseVM, productData5);
                fillProductData6(productVerboseVM, productData6);
                fillProductData7(productVerboseVM, productData7);
                fillProductData8(productVerboseVM, productData8);
                fillProductData9(productVerboseVM, productData9);
                fillProductData10(productVerboseVM, productData10);
                fillProductData11(productVerboseVM, productData11);
                fillProductData12(productVerboseVM, productData12);
                fillProductData13(productVerboseVM, productData13);
                fillProductData14(productVerboseVM, productData14);
                fillProductData15(productVerboseVM, productData15);
                fillProductData16(productVerboseVM, productData16);
                fillProductData17(productVerboseVM, productData17);
                fillProductData18(productVerboseVM, productData18);
                fillProductData19(productVerboseVM, productData19);
                fillProductData20(productVerboseVM, productData20);
                fillProductData21(productVerboseVM, productData21);
            }

            var reType = productVerboseVM.type;
            if (reType == null || reType == "")
            {

            }
            else
            {
                if (reType == "膠盒")
                {
                    if (changeLang == "tw")
                    {
                        fillReType(productVerboseVM, "膠盒");
                    }
                    else if (changeLang == "cn")
                    {
                        fillReType(productVerboseVM, "胶盒");
                    }
                    else if (changeLang == "en")
                    {
                        fillReType(productVerboseVM, "Housing");
                    }
                    else
                    {
                        
                    }
                }
                else if (reType == "端子")
                {
                    if (changeLang == "tw")
                    {
                        fillReType(productVerboseVM, "端子");
                    }
                    else if (changeLang == "cn")
                    {
                        fillReType(productVerboseVM, "端子");
                    }
                    else if (changeLang == "en")
                    {
                        fillReType(productVerboseVM, "Terminal");
                    }
                    else
                    {

                    }
                }
                else if (reType == "防水栓")
                {
                    if (changeLang == "tw")
                    {
                        fillReType(productVerboseVM, "防水栓");
                    }
                    else if (changeLang == "cn")
                    {
                        fillReType(productVerboseVM, "防水栓");
                    }
                    else if (changeLang == "en")
                    {
                        fillReType(productVerboseVM, "Rubber Seal");
                    }
                    else
                    {

                    }
                }
                else if (reType == "PVC絕緣護套")
                {
                    if (changeLang == "tw")
                    {
                        fillReType(productVerboseVM, "PVC絕緣護套");
                    }
                    else if (changeLang == "cn")
                    {
                        fillReType(productVerboseVM, "PVC绝缘护套");
                    }
                    else if (changeLang == "en")
                    {
                        fillReType(productVerboseVM, "PVC Insulated Sleeve");
                    }
                    else
                    {

                    }
                }
                else if (reType == "索環護套")
                {
                    if (changeLang == "tw")
                    {
                        fillReType(productVerboseVM, "索環護套");
                    }
                    else if (changeLang == "cn")
                    {
                        fillReType(productVerboseVM, "索环护套");
                    }
                    else if (changeLang == "en")
                    {
                        fillReType(productVerboseVM, "Grommet");
                    }
                    else
                    {

                    }
                }
                else
                {

                }
            }

            var reSex = productVerboseVM.sex;
            if (reSex == null || reSex == "")
            {

            }
            else
            {
                if (changeLang == "en")
                {
                    fillReSex(productVerboseVM, reSex.Split('(')[0]);
                }
                else
                {
                    fillReSex(productVerboseVM, reSex);
                }
            }

            var reColor = productVerboseVM.color;
            if (reColor == null || reColor == "")
            {

            }
            else
            {
                if (changeLang == "en")
                {
                    var temp = reColor.Split('(');
                    if (reColor.Split('(').Length > 1)
                    {
                        fillReColor(productVerboseVM, reColor.Split('(')[0]);
                    }
                    else
                    {
                        fillReColor(productVerboseVM, reColor);
                    }
                }
                else
                {
                    fillReColor(productVerboseVM, reColor);
                }
            }

            var reBag = productVerboseVM.packaging;
            if (reBag == null || reBag == "")
            {

            }
            else
            {
                if (changeLang == "en")
                {
                    var temp = reBag.Split('(');
                    if (reBag.Split('(').Length > 1)
                    {
                        fillReBag(productVerboseVM, reBag.Split('(')[0]);
                    }
                    else
                    {
                        fillReBag(productVerboseVM, reBag);
                    }
                }
                else
                {
                    fillReBag(productVerboseVM, reBag);
                }
            }

            var reMaterial = productVerboseVM.material;
            if (reMaterial == null || reMaterial == "")
            {

            }
            else
            {
                if (changeLang == "en")
                {
                    var temp = reMaterial.Split('(');
                    if (reMaterial.Split('(').Length > 1)
                    {
                        fillReMaterial(productVerboseVM, reMaterial.Split('(')[0]);
                    }
                    else
                    {
                        fillReMaterial(productVerboseVM, reMaterial);
                    }
                }
                else
                {
                    fillReMaterial(productVerboseVM, reMaterial);
                }
            }

            var rePlating = productVerboseVM.plating;
            if (rePlating == null || rePlating == "")
            {

            }
            else
            {
                if (changeLang == "en")
                {
                    var temp = rePlating.Split('(');
                    if (rePlating.Split('(').Length > 1)
                    {
                        fillRePlating(productVerboseVM, rePlating.Split('(')[0]);
                    }
                    else
                    {
                        fillRePlating(productVerboseVM, rePlating);
                    }
                }
                else
                {
                    fillRePlating(productVerboseVM, rePlating);
                }
            }

            var reSealable = productVerboseVM.sealable;
            if (reSealable == null || reSealable == "")
            {

            }
            else
            {
                if (changeLang == "en")
                {
                    var temp = reSealable.Split('(');
                    if (reSealable.Split('(').Length > 1)
                    {
                        fillReSealable(productVerboseVM, reSealable.Split('(')[0]);
                    }
                    else
                    {
                        fillReSealable(productVerboseVM, reSealable);
                    }
                }
                else
                {
                    fillReSealable(productVerboseVM, reSealable);
                }
            }

            var reFeedType = productVerboseVM.feed_type;
            if (reFeedType == null || reFeedType == "")
            {

            }
            else
            {
                if (changeLang == "en")
                {
                    var temp = reFeedType.Split('(');
                    if (reFeedType.Split('(').Length > 1)
                    {
                        fillReFeedType(productVerboseVM, reFeedType.Split('(')[0]);
                    }
                    else
                    {
                        fillReFeedType(productVerboseVM, reFeedType);
                    }
                }
                else
                {
                    fillReFeedType(productVerboseVM, reFeedType);
                }
            }

            var reItemClass = productVerboseVM.item_class;
            if (reItemClass == null || reItemClass == "")
            {

            }
            else
            {
                if (changeLang == "en")
                {
                    var temp = reItemClass.Split('(');
                    if (reItemClass.Split('(').Length > 1)
                    {
                        fillReItemClass(productVerboseVM, reItemClass.Split('(')[0]);
                    }
                    else
                    {
                        fillReItemClass(productVerboseVM, reItemClass);
                    }
                }
                else
                {
                    fillReItemClass(productVerboseVM, reItemClass);
                }
            }

            return Json(productVerboseVM, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getRelations(decimal id)
        {
            var changeLang = langData["語系"].ToString();
            List<ProductionRelationVM> productionRelationVMs = new List<ProductionRelationVM>();
            using (var conn = _connectionFactory.CreateProductDBConnection())
            {
                string sqlProduct =
                    @"SELECT type FROM product WHERE id = @id";

                string productType = conn.QuerySingle<string>(sqlProduct, new { id = id });

                string sql =
                    @"SELECT MTable.mapping_type,P.type,P.type,P.group_id,P.id,P.new_name 
                    FROM Product P JOIN
                    (
                        SELECT id, target_id, mapping_type, source_id
                        FROM mapgroup
                        WHERE source_id = (SELECT group_id FROM product WHERE id = @id)
                    ) MTable ON P.group_id = MTable.target_id
                    ORDER BY MTable.mapping_type,P.type";

                var relatedproducts = conn.Query(sql, new { id = id });

                if (relatedproducts.Any(s => s.mapping_type == "Family"))
                {
                    if (relatedproducts.Any(s => s.type == "Terminal" && s.mapping_type == "Family"))
                    {
                        //ProductionRelationVM productionRelations = new ProductionRelationVM() { name = "家族端子" };
                        ProductionRelationVM productionRelations = new ProductionRelationVM();
                        productionRelations.name = changeLang == "tw" ? "家族端子" : changeLang == "cn" ? "家族端子" : "Terminal Family";

                        foreach (var product in relatedproducts.Where(s => s.mapping_type == "Family").OrderBy(s => s.id))
                        {
                            using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                            {
                                conn2.Open();
                                string filecheck = "picture1";
                                string productfilename;
                                string img_sql = "select * from product_file where product_id= " + product.id + " and filecheck= '" + filecheck + "'";
                                using (SqlCommand cmd = conn2.CreateCommand())
                                {
                                    cmd.CommandText = img_sql;
                                    cmd.CommandType = CommandType.Text;
                                    SqlDataReader reader = cmd.ExecuteReader();
                                    while (reader.Read())
                                    {
                                        productfilename = reader[2].ToString();
                                        if (productfilename != "")
                                        {
                                            var temp = productfilename.Split('.');
                                            var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + product.id + "/" + temp[0] + "_WM." + temp[1];
                                            productionRelations.AddProduct(product.id, product.new_name, productImg);
                                        }
                                        else
                                        {
                                            productionRelations.AddProduct(product.id, product.new_name, "");
                                        }
                                    }
                                }
                            }
                        }

                        productionRelationVMs.Add(productionRelations);
                    }
                    else
                    {
                        //ProductionRelationVM productionRelations = new ProductionRelationVM() { name = "家族膠盒" };
                        ProductionRelationVM productionRelations = new ProductionRelationVM();
                        productionRelations.name = changeLang == "tw" ? "家族膠盒" : changeLang == "cn" ? "家族胶盒" : "Housing Family";

                        foreach (var product in relatedproducts.Where(s => s.mapping_type == "Family").OrderBy(s => s.id))
                        {
                            using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                            {
                                conn2.Open();
                                string filecheck = "picture1";
                                string productfilename;
                                string img_sql = "select * from product_file where product_id= " + product.id + " and filecheck= '" + filecheck + "'";
                                using (SqlCommand cmd = conn2.CreateCommand())
                                {
                                    cmd.CommandText = img_sql;
                                    cmd.CommandType = CommandType.Text;
                                    SqlDataReader reader = cmd.ExecuteReader();
                                    while (reader.Read())
                                    {
                                        productfilename = reader[2].ToString();
                                        if (productfilename != "")
                                        {
                                            var temp = productfilename.Split('.');
                                            var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + product.id + "/" + temp[0] + "_WM." + temp[1];
                                            productionRelations.AddProduct(product.id, product.new_name,productImg);
                                        }
                                        else
                                        {
                                            productionRelations.AddProduct(product.id, product.new_name,"");
                                        }
                                    }
                                }
                            }
                            
                        }

                        productionRelationVMs.Add(productionRelations);
                    }

                }

                if (relatedproducts.Any(s => s.mapping_type == "Housing"))
                {
                    ProductionRelationVM productionRelations = new ProductionRelationVM();

                    if (productType.ToLower() == "housing")
                        //productionRelations.name = "對手膠盒";
                    productionRelations.name = changeLang == "tw" ? "對手膠盒" : changeLang == "cn" ? "对手胶盒" : "Mating Housing";
                    else if (productType.ToLower() == "terminal")
                        //productionRelations.name = "配對端子";
                        //productionRelations.name = "配對膠盒";
                    productionRelations.name = changeLang == "tw" ? "配對膠盒" : changeLang == "cn" ? "配对胶盒" : "Applicable Housing";
                    else if (productType.ToLower() == "rubber")
                        //productionRelations.name = "配對膠盒";
                    productionRelations.name = changeLang == "tw" ? "配對膠盒" : changeLang == "cn" ? "配对胶盒" : "Applicable Housing";

                    foreach (var product in relatedproducts.Where(s => s.mapping_type == "Housing").OrderBy(s => s.id))
                    {
                        using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                        {
                            conn2.Open();
                            string filecheck = "picture1";
                            string productfilename;
                            string img_sql = "select * from product_file where product_id= " + product.id + " and filecheck= '" + filecheck + "'";
                            using (SqlCommand cmd = conn2.CreateCommand())
                            {
                                cmd.CommandText = img_sql;
                                cmd.CommandType = CommandType.Text;
                                SqlDataReader reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    productfilename = reader[2].ToString();
                                    if (productfilename != "")
                                    {
                                        var temp = productfilename.Split('.');
                                        var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + product.id + "/" + temp[0] + "_WM." + temp[1];
                                        productionRelations.AddProduct(product.id, product.new_name, productImg);
                                    }
                                    else
                                    {
                                        productionRelations.AddProduct(product.id, product.new_name, "");
                                    }
                                }
                            }
                        }
                    }

                    productionRelationVMs.Add(productionRelations);
                }

                if (relatedproducts.Any(s => s.mapping_type == "Terminal"))
                {
                    ProductionRelationVM productionRelations = new ProductionRelationVM() {
                        //name = "配對端子"
                        name = changeLang == "tw" ? "配對端子" : changeLang == "cn" ? "配对端子" : "Applicable Terminal"
                    };

                    if (productType.ToLower() == "housing")
                        //productionRelations.name = "配對膠盒";
                        //productionRelations.name = "配對端子";
                        productionRelations.name = changeLang == "tw" ? "配對端子" : changeLang == "cn" ? "配对端子" : "Applicable Terminal";
                    else if (productType.ToLower() == "terminal")
                        //productionRelations.name = "對手端子";
                        productionRelations.name = changeLang == "tw" ? "對手端子" : changeLang == "cn" ? "对手端子" : "Mating Terminal";

                    foreach (var product in relatedproducts.Where(s => s.mapping_type == "Terminal").OrderBy(s => s.id))
                    {
                        using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                        {
                            conn2.Open();
                            string filecheck = "picture1";
                            string productfilename;
                            string img_sql = "select * from product_file where product_id= " + product.id + " and filecheck= '" + filecheck + "'";
                            using (SqlCommand cmd = conn2.CreateCommand())
                            {
                                cmd.CommandText = img_sql;
                                cmd.CommandType = CommandType.Text;
                                SqlDataReader reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    productfilename = reader[2].ToString();
                                    if (productfilename != "")
                                    {
                                        var temp = productfilename.Split('.');
                                        var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + product.id + "/" + temp[0] + "_WM." + temp[1];
                                        productionRelations.AddProduct(product.id, product.new_name, productImg);
                                    }
                                    else
                                    {
                                        productionRelations.AddProduct(product.id, product.new_name, "");
                                    }
                                }
                            }
                        }
                    }

                    productionRelationVMs.Add(productionRelations);
                }

                if (relatedproducts.Any(s => s.mapping_type == "Rubber"))
                {
                    ProductionRelationVM productionRelations = new ProductionRelationVM() {
                        //name = "適用防水栓"
                        name = changeLang == "tw" ? "適用防水栓" : changeLang == "cn" ? "适用防水栓" : "Applicable Seal"
                    };

                    foreach (var product in relatedproducts.Where(s => s.mapping_type == "Rubber").OrderBy(s => s.id))
                    {

                        using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                        {
                            conn2.Open();
                            string filecheck = "picture1";
                            string productfilename;
                            string img_sql = "select * from product_file where product_id= " + product.id + " and filecheck= '" + filecheck + "'";
                            using (SqlCommand cmd = conn2.CreateCommand())
                            {
                                cmd.CommandText = img_sql;
                                cmd.CommandType = CommandType.Text;
                                SqlDataReader reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    productfilename = reader[2].ToString();
                                    if (productfilename != "")
                                    {
                                        var temp = productfilename.Split('.');
                                        var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + product.id + "/" + temp[0] + "_WM." + temp[1];
                                        productionRelations.AddProduct(product.id, product.new_name, productImg);
                                    }
                                    else
                                    {
                                        productionRelations.AddProduct(product.id, product.new_name, "");
                                    }
                                }
                            }
                        }
                    }

                    productionRelationVMs.Add(productionRelations);
                }

                if (relatedproducts.Any(s => s.mapping_type == "Assembling"))
                {
                    ProductionRelationVM productionRelations = new ProductionRelationVM() {
                        //name = "產品配件"
                        name = changeLang == "tw" ? "產品配件" : changeLang == "cn" ? "产品配件" : "Assembling Parts"
                    };

                    foreach (var product in relatedproducts.Where(s => s.mapping_type == "Assembling").OrderBy(s => s.id))
                    {
                        using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                        {
                            conn2.Open();
                            string filecheck = "picture1";
                            string productfilename;
                            string img_sql = "select * from product_file where product_id= " + product.id + " and filecheck= '" + filecheck + "'";
                            using (SqlCommand cmd = conn2.CreateCommand())
                            {
                                cmd.CommandText = img_sql;
                                cmd.CommandType = CommandType.Text;
                                SqlDataReader reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    productfilename = reader[2].ToString();
                                    if (productfilename != "")
                                    {
                                        var temp = productfilename.Split('.');
                                        var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + product.id + "/" + temp[0] + "_WM." + temp[1];
                                        productionRelations.AddProduct(product.id, product.new_name, productImg);
                                    }
                                    else
                                    {
                                        productionRelations.AddProduct(product.id, product.new_name, "");
                                    }
                                }
                            }
                        }
                    }

                    productionRelationVMs.Add(productionRelations);
                }

            }

            return Json(productionRelationVMs, JsonRequestBehavior.AllowGet);
        }
        private void fillProductIdName(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.id = origin.id;
                target.name = origin.name;
                target.new_name = origin.new_name;
                target.id = origin.id;
            }
        }

        private void fillProductImg(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productImg = origin;
            }
        }

        private void fillProductSpc(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productSpc = origin;
            }
        }

        private void fillProductPaper(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productPaper = origin;
            }
        }

        private void fillProduct3D(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.product3D = origin;
            }
        }

        private void fillProductBook(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productBook = origin;
            }
        }

        private void fillProductSize(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.sizeS = origin;
            }
        }
        private void fillProductSizeR(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.sizeR = origin;
            }
        }
        private void fillProductSizeT(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.sizeT = origin;
            }
        }

        private void fillProductShowColor(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.showSColor = origin;
            }
        }

        private void fillProductShowFrontColor(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.showFrontColor = origin;
            }
        }

        private void fillProductData1(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData1 = origin;
            }
        }
        private void fillProductData2(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData2 = origin;
            }
        }
        private void fillProductData3(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData3 = origin;
            }
        }
        private void fillProductData4(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData4 = origin;
            }
        }
        private void fillProductData5(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData5 = origin;
            }
        }
        private void fillProductData6(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData6 = origin;
            }
        }
        private void fillProductData7(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData7 = origin;
            }
        }
        private void fillProductData8(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData8 = origin;
            }
        }
        private void fillProductData9(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData9 = origin;
            }
        }
        private void fillProductData10(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData10 = origin;
            }
        }
        private void fillProductData11(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData11 = origin;
            }
        }
        private void fillProductData12(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData12 = origin;
            }
        }
        private void fillProductData13(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData13 = origin;
            }
        }
        private void fillProductData14(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData14 = origin;
            }
        }
        private void fillProductData15(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData15 = origin;
            }
        }
        private void fillProductData16(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData16 = origin;
            }
        }
        private void fillProductData17(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData17 = origin;
            }
        }
        private void fillProductData18(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData18 = origin;
            }
        }
        private void fillProductData19(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData19 = origin;
            }
        }
        private void fillProductData20(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData20 = origin;
            }
        }

        private void fillProductData21(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productData21 = origin;
            }
        }
        private void fillReType(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productReType = origin;
            }
        }

        private void fillReSex(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productReSex = origin;
            }
        }
        private void fillReColor(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productReColor = origin;
            }
        }
        private void fillReBag(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productReBag = origin;
            }
        }
        private void fillReMaterial(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productReMaterial = origin;
            }
        }
        private void fillRePlating(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productRePlating = origin;
            }
        }
        private void fillReSealable(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productReSealable = origin;
            }
        }
        private void fillReFeedType(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productReFeedType = origin;
            }
        }
        private void fillReItemClass(ProductVerboseVM target, dynamic origin)
        {
            if (target != null)
            {
                target.productReItemClass = origin;
            }
        }
        public ActionResult Sync()
        {
            RecurringJob.AddOrUpdate("SyncProduct",
                () => BackgroundTask.SyncProduct(),
                "10 * * * *"
                );

            return Content("Task Start");
        }
        [HttpPost]
        public JsonResult getCarsysLevel(string name)
        {
            List<ProductionRelationVM> productionRelationVMs = new List<ProductionRelationVM>();
            return Json(productionRelationVMs, JsonRequestBehavior.AllowGet);
        }
    }
}