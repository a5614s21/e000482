﻿using HuLane.BuildBlock;
using HuLane.Main.Enums;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using PagedList;
using Web.Service;

namespace Web.Controllers
{
    public class ProductsController : BaseController
    {
        private IConnectionFactory _connectionFactory = null;
        private const int bySpec = 0;
        private const int byApply = 1;
        private const int byKeyword = 2;
        private const int byLast = 3;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            headerActive.Add("product", "active");
            ViewBag.headerActive = headerActive;//header active
            ViewBag.ResLang = langData;//取得語系檔案
        }

        public ProductsController(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public ActionResult Index()
        {
            if (Session["homeBySpec"] as string == "1")
            {
                ViewBag.TabIndex = 1;
            }
            else
            {
                ViewBag.TabIndex = bySpec;
            }
            ViewBag.isauth = this.User.Identity.IsAuthenticated;
            ViewBag.HomeSearch = "{}";
            return View(); ;
        }
        public ActionResult BannerIndex(int productType, int page)
        {
            var changeLang = langData["語系"].ToString();
            ViewBag.data = Repository.About.About.Single(changeLang, "1");
            ViewBag.title = ViewBag.data.title;

            string productTypeString = productType.ToString();
            var typeName = (from a in DB.advertise
                            where a.link == productTypeString && a.lang == changeLang
                            select a.title).FirstOrDefault();
            ViewBag.typeName = typeName;
            List<SearchResultDetailVM> searchResult = new List<SearchResultDetailVM>();
            SearchResultDetailVM addResultDetailVM = new SearchResultDetailVM();
            List<int> typeList = new List<int>();
            List<int> productList = new List<int>();
            string type = "";
            string newName = "";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
            {
                string sql1 = "select * from usefor where type= " + productType;
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = sql1;
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        typeList.Add(Int32.Parse(reader[0].ToString()));
                    }
                }
            }

            foreach (var item in typeList)
            {
                using (SqlConnection conn5 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    string sql5 = "select * from map_use where use_id= " + item;
                    using (SqlCommand cmd6 = conn5.CreateCommand())
                    {
                        conn5.Open();
                        cmd6.CommandText = sql5;
                        cmd6.CommandType = CommandType.Text;
                        SqlDataReader reader6 = cmd6.ExecuteReader();
                        while (reader6.Read())
                        {
                            productList.Add(Int32.Parse(reader6[1].ToString()));
                        }
                    }
                }
            }
            var distinctProduct = productList.Distinct();
            foreach (var allData in distinctProduct)
            {
                using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    string sql6 = "select * from product where id= " + allData;
                    using (SqlCommand cmd2 = conn2.CreateCommand())
                    {
                        conn2.Open();
                        cmd2.CommandText = sql6;
                        cmd2.CommandType = CommandType.Text;
                        SqlDataReader reader2 = cmd2.ExecuteReader();
                        while (reader2.Read())
                        {
                            type = reader2[2].ToString();
                            newName = reader2[6].ToString();
                        }
                    }
                }

                if (type == "Housing")
                {
                    using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    {
                        string sql2 = "select * from housing where product_id= " + allData;
                        using (SqlCommand cmd7 = conn6.CreateCommand())
                        {
                            conn6.Open();
                            cmd7.CommandText = sql2;
                            cmd7.CommandType = CommandType.Text;
                            SqlDataReader reader7 = cmd7.ExecuteReader();
                            while (reader7.Read())
                            {
                                //addResultDetailVM.item_class = reader6[2].ToString();
                                addResultDetailVM.type = "膠盒";
                                addResultDetailVM.sex_o = reader7[2].ToString();
                                addResultDetailVM.sealable_o = reader7[6].ToString();
                                addResultDetailVM.length = reader7[26].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[26].ToString());
                                addResultDetailVM.width = reader7[27].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[27].ToString());
                                addResultDetailVM.height = reader7[28].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[28].ToString());
                                addResultDetailVM.positions = reader7[11].ToString();
                                addResultDetailVM.series = reader7[39].ToString();
                                addResultDetailVM.item_class = reader7[30].ToString();
                                addResultDetailVM.temper_operating = reader7[10].ToString();
                                addResultDetailVM.id = Int32.Parse(reader7[1].ToString());
                                addResultDetailVM.new_name = newName;
                                addResultDetailVM.material = reader7[5].ToString();
                                searchResult.Add(addResultDetailVM);
                            }
                        }
                    }
                }
                else if (type == "Terminal")
                {
                    using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    {
                        string sql2 = "select * from terminal where product_id= " + allData;
                        using (SqlCommand cmd7 = conn6.CreateCommand())
                        {
                            conn6.Open();
                            cmd7.CommandText = sql2;
                            cmd7.CommandType = CommandType.Text;
                            SqlDataReader reader7 = cmd7.ExecuteReader();
                            while (reader7.Read())
                            {
                                addResultDetailVM.type = "端子";
                                addResultDetailVM.sex_o = reader7[3].ToString();
                                addResultDetailVM.sealable_o = reader7[21].ToString();
                                addResultDetailVM.length = reader7[31].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[31].ToString());
                                addResultDetailVM.width = reader7[32].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[32].ToString());
                                addResultDetailVM.height = reader7[33].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[33].ToString());
                                addResultDetailVM.item_class = reader7[2].ToString();
                                addResultDetailVM.temper_operating = "N/A";
                                addResultDetailVM.id = Int32.Parse(reader7[1].ToString());
                                addResultDetailVM.new_name = newName;
                                addResultDetailVM.material = reader7[4].ToString();

                                searchResult.Add(addResultDetailVM);
                            }
                        }
                    }
                }
                else if (type == "Rubber")
                {
                    using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    {
                        string sql2 = "select * from rubber where product_id= " + allData;
                        using (SqlCommand cmd7 = conn6.CreateCommand())
                        {
                            conn6.Open();
                            cmd7.CommandText = sql2;
                            cmd7.CommandType = CommandType.Text;
                            SqlDataReader reader7 = cmd7.ExecuteReader();
                            while (reader7.Read())
                            {
                                addResultDetailVM.type = "防水栓";
                                addResultDetailVM.sex_o = "N";
                                addResultDetailVM.sealable_o = "N";
                                addResultDetailVM.length = reader7[6].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[6].ToString());
                                addResultDetailVM.width = reader7[15].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[15].ToString());
                                addResultDetailVM.height = Single.Parse("0");
                                addResultDetailVM.item_class = reader7[14].ToString();
                                addResultDetailVM.temper_operating = reader7[7].ToString();
                                addResultDetailVM.id = Int32.Parse(reader7[1].ToString());
                                addResultDetailVM.new_name = newName;
                                addResultDetailVM.material = reader7[4].ToString();

                                searchResult.Add(addResultDetailVM);
                            }
                        }
                    }
                }
                else if (type == "Sleeve")
                {
                    using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    {
                        string sql2 = "select * from sleeve where product_id= " + allData;
                        using (SqlCommand cmd7 = conn6.CreateCommand())
                        {
                            conn6.Open();
                            cmd7.CommandText = sql2;
                            cmd7.CommandType = CommandType.Text;
                            SqlDataReader reader7 = cmd7.ExecuteReader();
                            while (reader7.Read())
                            {
                                addResultDetailVM.type = "PVC絕緣護套";
                                addResultDetailVM.sex_o = "N";
                                addResultDetailVM.sealable_o = "N";
                                addResultDetailVM.length = reader7[19].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[19].ToString());
                                addResultDetailVM.width = reader7[20].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[20].ToString());
                                addResultDetailVM.height = reader7[21].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[21].ToString());
                                addResultDetailVM.item_class = reader7[2].ToString();
                                addResultDetailVM.temper_operating = reader7[7].ToString();
                                addResultDetailVM.id = Int32.Parse(reader7[1].ToString());
                                addResultDetailVM.new_name = newName;
                                addResultDetailVM.material = reader7[4].ToString();

                                searchResult.Add(addResultDetailVM);
                            }
                        }
                    }
                }
                else if (type == "Grommet")
                {
                    using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    {
                        string sql2 = "select * from grommet where product_id= " + allData;
                        using (SqlCommand cmd7 = conn6.CreateCommand())
                        {
                            conn6.Open();
                            cmd7.CommandText = sql2;
                            cmd7.CommandType = CommandType.Text;
                            SqlDataReader reader7 = cmd7.ExecuteReader();
                            while (reader7.Read())
                            {
                                addResultDetailVM.type = "索環護套";
                                addResultDetailVM.sex_o = "N";
                                addResultDetailVM.sealable_o = "N";
                                addResultDetailVM.length = reader7[19].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[19].ToString());
                                addResultDetailVM.width = reader7[20].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[20].ToString());
                                addResultDetailVM.height = reader7[21].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[21].ToString());
                                addResultDetailVM.item_class = reader7[2].ToString();
                                addResultDetailVM.temper_operating = reader7[7].ToString();
                                addResultDetailVM.id = Int32.Parse(reader7[1].ToString());
                                addResultDetailVM.new_name = newName;
                                addResultDetailVM.material = reader7[4].ToString();

                                searchResult.Add(addResultDetailVM);
                            }
                        }
                    }
                }
                else
                {

                }
                addResultDetailVM = new SearchResultDetailVM();
            }

            string filecheck = "picture1";
            string productfilename;
            string productnewname;

            foreach (var tempResult in searchResult)
            {
                using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    conn2.Open();
                    string img_sql = "select * from product_file where product_id= " + tempResult.id + " and filecheck= '" + filecheck + "'";
                    using (SqlCommand cmd = conn2.CreateCommand())
                    {
                        cmd.CommandText = img_sql;
                        cmd.CommandType = CommandType.Text;
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            productfilename = reader[2].ToString();
                            if (productfilename != "")
                            {
                                var temp = productfilename.Split('.');
                                var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + tempResult.id + "/" + temp[0] + "_WM." + temp[1];
                                tempResult.productImg = productImg;
                            }
                            else
                            {
                                tempResult.productImg = "";
                            }
                        }
                    }
                }

                using (SqlConnection conn3 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    string newName_sql = "select * from product where id= " + tempResult.id;
                    using (SqlCommand cmd2 = conn3.CreateCommand())
                    {
                        conn3.Open();
                        cmd2.CommandText = newName_sql;
                        cmd2.CommandType = CommandType.Text;
                        SqlDataReader reader = cmd2.ExecuteReader();
                        while (reader.Read())
                        {
                            productnewname = reader[6].ToString();
                            if (productnewname != "")
                            {
                                tempResult.new_name = productnewname;
                            }
                            else
                            {
                                tempResult.new_name = "";
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < searchResult.Count(); i++)
            {
                //if (i == 0)
                //{
                //    searchResult[i].th = 1;
                //    searchResult[i].type = "All";
                //}
                //else
                //{
                //    searchResult[i].th = 0;
                //    searchResult[i].type = "All";
                //}

                if (searchResult[i].color != null)
                {
                    if (searchResult[i].color == "N/A")
                    {

                    }
                    else if (searchResult[i].color == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = searchResult[i].color.Split('(');
                            searchResult[i].color = temp[0];
                        }
                        else
                        {
                            var temp = searchResult[i].color.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                searchResult[i].color = resultData;
                            }
                            else
                            {
                                searchResult[i].color = temp[0];
                            }

                        }
                    }
                }

                if (searchResult[i].sex_o != null)
                {
                    if (searchResult[i].sex_o == "N/A")
                    {

                    }
                    else if (searchResult[i].sex_o == "")
                    {

                    }
                    else if (searchResult[i].sex_o == "N")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = searchResult[i].sex_o.Split('(');
                            searchResult[i].reSex = temp[0];
                        }
                        else
                        {
                            var temp = searchResult[i].sex_o.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                searchResult[i].reSex = resultData;
                            }
                            else
                            {
                                searchResult[i].reSex = temp[0];
                            }
                        }
                    }
                }

                if (searchResult[i].type == "端子" || searchResult[i].type == "All")
                {
                    searchResult[i].reSealable = changeLang == "tw" ? searchResult[i].sealable : changeLang == "cn" ? searchResult[i].sealable : searchResult[i].sealable_o.Split('(').Length > 1 ? searchResult[i].sealable_o.Split('(')[0] : searchResult[i].sealable_o;
                }
                else
                {
                    searchResult[i].reSealable = changeLang == "tw" ? searchResult[i].sealable : changeLang == "cn" ? searchResult[i].sealable : searchResult[i].sealable_o;
                }


                if (searchResult[i].material != null)
                {
                    if (searchResult[i].material == "N/A")
                    {

                    }
                    else if (searchResult[i].material == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = searchResult[i].material.Split('(');
                            searchResult[i].reMaterial = temp[0];
                        }
                        else
                        {
                            var temp = searchResult[i].material.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                searchResult[i].reMaterial = resultData;
                            }
                            else
                            {
                                searchResult[i].reMaterial = temp[0];
                            }
                        }
                    }
                }

                if (searchResult[i].plating != null)
                {
                    if (searchResult[i].plating == "N/A")
                    {

                    }
                    else if (searchResult[i].plating == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = searchResult[i].plating.Split('(');
                            searchResult[i].rePlating = temp[0];
                        }
                        else
                        {
                            var temp = searchResult[i].plating.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                searchResult[i].rePlating = resultData;
                            }
                            else
                            {
                                searchResult[i].rePlating = temp[0];
                            }
                        }
                    }
                }

                if (searchResult[i].item_class != null)
                {
                    if (searchResult[i].item_class == "N/A")
                    {

                    }
                    else if (searchResult[i].item_class == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = searchResult[i].item_class.Split('(');
                            searchResult[i].item_class = temp[0];
                        }
                        else
                        {
                            var temp = searchResult[i].item_class.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                searchResult[i].item_class = resultData;
                            }
                            else
                            {
                                searchResult[i].item_class = temp[0];
                            }
                        }
                    }
                }
            }

            ViewBag.productTypePhoto = changeLang == "tw" ? "產品圖片" : changeLang == "cn" ? "產品圖片" : "Product Photo";
            ViewBag.productTypeNo = changeLang == "tw" ? "ID品號/品號" : changeLang == "cn" ? "ID品號/品號" : "ID Part No. /Part No.";
            ViewBag.productTypeCategory = changeLang == "tw" ? "類別" : changeLang == "cn" ? "类别" : "Products Type";
            ViewBag.typeMaterial = changeLang == "tw" ? "材質" : changeLang == "cn" ? "材質" : "Material";
            ViewBag.typeaMleFemale = changeLang == "tw" ? "公/母" : changeLang == "cn" ? "公/母" : "Male/Female";
            ViewBag.typeSizeType = changeLang == "tw" ? "尺寸 (mm)" : changeLang == "cn" ? "尺寸(mm)" : "Dimension (mm)";

            ViewBag.productTypeKeyWord = changeLang == "tw" ? "關鍵字" : changeLang == "cn" ? "關鍵字" : "Key Words";
            ViewBag.productTypeTotalSearch = changeLang == "tw" ? "共篩選到" : changeLang == "cn" ? "共篩選到" : "Results";
            ViewBag.productTypeFit = changeLang == "tw" ? "個符合條件的產品" : changeLang == "cn" ? "個符合條件的產品" : "hits";

            ViewBag.typeDataCount = searchResult.Count();
            ViewBag.location = DB.locations.Where(m => m.lang == changeLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            ViewBag.productByType = DB.ashcan;
            IPagedList<SearchResultDetailVM> searchResult_PagedList;

            if (searchResult != null && searchResult.Count != 0)
            {
                var pageNumber = page;
                var pageSize = 8;
                searchResult_PagedList = searchResult.ToPagedList(pageNumber, pageSize);
                int Allpage = Convert.ToInt16(Math.Ceiling((double)searchResult.Count / pageSize));
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + changeLang + "/Products/BannerIndex?productType=" + productType + "&", Allpage, pageNumber, searchResult.Count, "");
                ViewBag.typeList = searchResult_PagedList;
            }

            return View();
        }

        public ActionResult HomeIndex()
        {
            Session["homeBySpec"] = "1";
            return RedirectToAction("Index", "products");
        }

        public ActionResult Spec()
        {
            ViewBag.TabIndex = bySpec;
            SearchBySpecVM searchBySpecVM = TempData["HomeSearch"] as SearchBySpecVM;

            if (searchBySpecVM != null)
            {
                ViewBag.HomeSearch = Newtonsoft.Json.JsonConvert.SerializeObject(searchBySpecVM);
            }
            else
            {
                ViewBag.HomeSearch = "{}";
            }

            return View("Index"); ;
        }

        public ActionResult Apply()
        {
            ViewBag.TabIndex = byApply;

            SearchByApplyVM searchByApplyVM = TempData["HomeSearch"] as SearchByApplyVM;

            if (searchByApplyVM != null)
            {
                ViewBag.HomeSearch = Newtonsoft.Json.JsonConvert.SerializeObject(searchByApplyVM);
            }
            else
            {
                ViewBag.HomeSearch = "{}";
            }

            return View("Index"); ;
        }

        [HttpPost]
        public ActionResult Keyword(string keyword)
        {
            ViewBag.TabIndex = byKeyword;
            if (string.IsNullOrEmpty(keyword))
            {
                ViewBag.HomeSearchKeyword = string.Empty;
            }
            else
            {
                ViewBag.HomeSearchKeyword = keyword;
            }

            ViewBag.HomeSearch = "{}";

            return View("Index");
        }

        public ActionResult Keyword()
        {
            ViewBag.TabIndex = byKeyword;
            string keyword = TempData["HomeSearchKeyword"]?.ToString();

            if (string.IsNullOrEmpty(keyword))
            {
                ViewBag.HomeSearchKeyword = string.Empty;
            }
            else
            {
                ViewBag.HomeSearchKeyword = keyword;
            }

            SearchBySpecVM searchBySpecVM = TempData["HomeSearch"] as SearchBySpecVM;

            if (searchBySpecVM != null)
            {
                ViewBag.HomeSearch = Newtonsoft.Json.JsonConvert.SerializeObject(searchBySpecVM);
            }
            else
            {
                ViewBag.HomeSearch = "{}";
            }
            return View("Index");
        }

        public ActionResult Lastest()
        {
            ViewBag.TabIndex = byLast;
            return View("Index"); ;
        }
        #region SearchBySpec
        public JsonResult SearchBySpec(SearchBySpecVM searchBySpecVM)
        {
            var changeLang = langData["語系"].ToString();
            ViewBag.nowLang = changeLang;
            var searchResult = new List<SearchResultDetailVM>();
            //var keywordSearchResult = new List<SearchResultDetailVM>();

            using (var conn = _connectionFactory.CreateProductDBConnection())
            {
                //不在規格搜尋查關鍵字了
                //keywordSearchResult = getByKeyworkResult(conn, searchBySpecVM.Keyword);

                switch (searchBySpecVM.ProductTable)
                {
                    case "Undefined":
                    case "A":
                        searchResult.AddRange(getHousingResult(conn, searchBySpecVM));
                        if (!string.IsNullOrEmpty(searchBySpecVM.ProductSerial))
                        {

                        }
                        else
                        {
                            searchResult.AddRange(getTerminalResult(conn, searchBySpecVM));
                            searchResult.AddRange(getRubberResult(conn, searchBySpecVM));
                            searchResult.AddRange(getSleeveResult(conn, searchBySpecVM));
                            searchResult.AddRange(getGrommetResult(conn, searchBySpecVM));
                        }
                        break;
                    case "H":
                        searchResult = getHousingResult(conn, searchBySpecVM);
                        break;
                    case "T":
                        searchResult = getTerminalResult(conn, searchBySpecVM);
                        break;
                    case "R":
                        searchResult = getRubberResult(conn, searchBySpecVM);
                        break;
                    case "S":
                        searchResult = getSleeveResult(conn, searchBySpecVM);
                        break;
                    case "G":
                        searchResult = getGrommetResult(conn, searchBySpecVM);
                        break;
                }

            }

            //if (!string.IsNullOrEmpty(searchBySpecVM.Keyword))
            //{
            //    searchResult = searchResult.Where(s => keywordSearchResult.Any(a => a.id == s.id)).ToList();
            //}

            //searchResult = searchResult.OrderBy(s => s.id).ToList();

            if (searchBySpecVM.ProductTable == "A")
            {
                foreach (var item in searchResult)
                {
                    item.type = "All";
                }
            }

            string filecheck = "picture1";
            string productfilename;

            foreach (var item in searchResult)
            {
                using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    conn2.Open();
                    string img_sql = "select * from product_file where product_id= " + item.id + " and filecheck= '" + filecheck + "'";
                    using (SqlCommand cmd = conn2.CreateCommand())
                    {
                        cmd.CommandText = img_sql;
                        cmd.CommandType = CommandType.Text;
                        //SqlDataAdapter dAda = new SqlDataAdapter(cmd);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            productfilename = reader[2].ToString();
                            if (productfilename != "")
                            {
                                var temp = productfilename.Split('.');
                                var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + item.id + "/" + temp[0] + "_WM." + temp[1];
                                item.productImg = productImg;
                            }
                            else
                            {
                                item.productImg = "";
                            }
                        }
                    }
                }
            }

            var pageDatas = searchResult.Skip((searchBySpecVM.PageIndex - 1) * searchBySpecVM.PageSize).Take(searchBySpecVM.PageSize).ToList();

            for (int i = 0; i < pageDatas.Count(); i++)
            //foreach (var data in pageDatas)
            {
                if (i == 0)
                {
                    pageDatas[i].th = 1;
                }
                else
                {
                    pageDatas[i].th = 0;
                }

                if (pageDatas[i].color != null)
                {
                    //if (pageDatas[i].color == "N/A")
                    //{

                    //}
                    if (pageDatas[i].color == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].color.Split('(');
                            pageDatas[i].color = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].color.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].color = resultData;
                            }
                            else
                            {
                                pageDatas[i].color = temp[0];
                            }

                        }
                    }
                }

                if (pageDatas[i].sex_o != null)
                {
                    //if (pageDatas[i].sex_o == "N/A")
                    //{

                    //}
                    if (pageDatas[i].sex_o == "")
                    {

                    }
                    else if (pageDatas[i].sex_o == "N")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].sex_o.Split('(');
                            pageDatas[i].reSex = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].sex_o.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].reSex = resultData;
                            }
                            else
                            {
                                pageDatas[i].reSex = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].type == "端子" || pageDatas[i].type == "All")
                {
                    pageDatas[i].reSealable = changeLang == "tw" ? pageDatas[i].sealable : changeLang == "cn" ? pageDatas[i].sealable : pageDatas[i].sealable_o.Split('(').Length > 1 ? pageDatas[i].sealable_o.Split('(')[0] : pageDatas[i].sealable_o;
                }
                else
                {
                    pageDatas[i].reSealable = changeLang == "tw" ? pageDatas[i].sealable : changeLang == "cn" ? pageDatas[i].sealable : pageDatas[i].sealable_o;
                }


                if (pageDatas[i].material != null)
                {
                    //if (pageDatas[i].material == "N/A")
                    //{

                    //}
                    if (pageDatas[i].material == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].material.Split('(');
                            pageDatas[i].reMaterial = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].material.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].reMaterial = resultData;
                            }
                            else
                            {
                                pageDatas[i].reMaterial = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].plating != null)
                {
                    //if (pageDatas[i].plating == "N/A")
                    //{

                    //}
                    if (pageDatas[i].plating == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].plating.Split('(');
                            pageDatas[i].rePlating = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].plating.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].rePlating = resultData;
                            }
                            else
                            {
                                pageDatas[i].rePlating = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].item_class != null)
                {
                    //if (pageDatas[i].item_class == "N/A")
                    //{

                    //}
                    if (pageDatas[i].item_class == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].item_class.Split('(');
                            pageDatas[i].item_class = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].item_class.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].item_class = resultData;
                            }
                            else
                            {
                                pageDatas[i].item_class = temp[0];
                            }
                        }
                    }
                }

            }

            return Json(new SearchResultVM() { TotalCount = searchResult.Count, PageDatas = pageDatas });
        }

        [HttpPost]
        public JsonResult SearchByHomeSpec(SearchByHomeSpecVM searchByHomSpecVM)
        {
            var searchResult = new List<SearchResultDetailVM>();
            var keywordSearchResult = new List<SearchResultDetailVM>();
            var searchBySpecVM = searchByHomSpecVM.Build();

            TempData["HomeSearch"] = searchBySpecVM;
            TempData["HomeSearchKeyword"] = searchByHomSpecVM.Keyword;

            using (var conn = _connectionFactory.CreateProductDBConnection())
            {
                //先查關鍵字
                keywordSearchResult = getByKeyworkResult(conn, searchByHomSpecVM.Keyword);

                switch (searchByHomSpecVM.ProductTable)
                {
                    case "Undefined":
                    case "A":
                        searchResult.AddRange(getHousingResult(conn, searchBySpecVM));
                        searchResult.AddRange(getTerminalResult(conn, searchBySpecVM));
                        searchResult.AddRange(getRubberResult(conn, searchBySpecVM));
                        searchResult.AddRange(getSleeveResult(conn, searchBySpecVM));
                        searchResult.AddRange(getGrommetResult(conn, searchBySpecVM));

                        break;
                    case "H":
                        searchResult = getHousingResult(conn, searchBySpecVM);
                        break;
                    case "T":
                        searchResult = getTerminalResult(conn, searchBySpecVM);
                        break;
                    case "R":
                        searchResult = getRubberResult(conn, searchBySpecVM);
                        break;
                    case "S":
                        searchResult = getSleeveResult(conn, searchBySpecVM);
                        break;
                    case "G":
                        searchResult = getGrommetResult(conn, searchBySpecVM);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            if (!string.IsNullOrEmpty(searchByHomSpecVM.Keyword))
            {
                searchResult = searchResult.Where(s => keywordSearchResult.Any(a => a.id == s.id)).ToList();
            }

            TempData["SearchByHomeSpec"] = searchResult;

            return Json(searchResult.Count);
        }

        private List<SearchResultDetailVM> getHousingResult(IDbConnection conn, SearchBySpecVM searchBySpecVM)
        {
            var builder = new SqlBuilder();

            builder.Where("p.[type] = 'Housing'");

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSizeType))
            {
                builder.Where("h.item_class = @productSizeType", new { productSizeType = searchBySpecVM.ProductSizeType });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductClass))
            {
                builder.Where("h.item_class = @productClass", new { productClass = searchBySpecVM.ProductClass });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSerial))
            {
                builder.Where("h.series = @productSerial", new { productSerial = searchBySpecVM.ProductSerial });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSex))
            {
                builder.Where("h.sex = @productSex", new { productSex = searchBySpecVM.ProductSex });
            }
            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSealable))
            {
                builder.Where("h.sealable = @productSealable", new { productSealable = searchBySpecVM.ProductSealable });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductPosition))
            {
                if (searchBySpecVM.ProductPosition.Contains("~") == false)
                {
                    int positions = 0;
                    if (int.TryParse(searchBySpecVM.ProductPosition, out positions))
                    {
                        //1,2,3,4,5,6,7,8,9,10
                        builder.Where("((DATALENGTH(h.positions) > 0  and TRY_CAST(h.positions AS INT) = @positions) or (DATALENGTH(h.positions) = 0 and TRY_CAST(SUBSTRING(p.name, 5, 2) AS INT) = @positions))", new { positions = positions });
                    }
                    else
                    {
                        //N/A
                        builder.Where("h.positions = 'N/A'");
                    }
                }
                else
                {
                    int start = 0, end = 0;
                    string[] positionStr = searchBySpecVM.ProductPosition.Split('~');
                    if (!int.TryParse(positionStr[0], out start))
                        throw new ArgumentOutOfRangeException($"孔數起始區間錯誤{positionStr[0]}");
                    if (int.TryParse(positionStr[1], out end))
                    {
                        //2個都數字
                        builder.Where("((DATALENGTH(h.positions) > 0  and (TRY_CAST(h.positions AS INT) >= @start and TRY_CAST(h.positions AS INT) <= @end)) or (DATALENGTH(h.positions) = 0 and (TRY_CAST(SUBSTRING(p.name, 5, 2) AS INT) >= @start and TRY_CAST(SUBSTRING(p.name, 5, 2) AS INT) <= @end)))", new { start = start, end = end });
                    }
                    else
                    {
                        //結尾是∞
                        builder.Where("((DATALENGTH(h.positions) > 0  and (TRY_CAST(h.positions AS INT) >= @start)) or (DATALENGTH(h.positions) = 0 and (TRY_CAST(SUBSTRING(p.name, 5, 2) AS INT) >= @start)))", new { start = start });
                    }
                }

            }


            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTemper))
            {
                builder.Where("h.temper_operating Like '%' +  @productTemper + '%'", new { productTemper = searchBySpecVM.ProductTemper });
            }

            if (searchBySpecVM.Size != null)
            {
                //Single dimension = searchBySpecVM.Size.Width ?? 0 * searchBySpecVM.Size.Length ?? 0 * searchBySpecVM.Size.Height ?? 0;
                //builder.Where("(h.width * h.length * h.height) = @dimension", new { dimension = dimension });
                if (searchBySpecVM.Size.Width.HasValue)
                {
                    builder.Where("TRY_CAST(h.width AS float) >= @width2 and TRY_CAST(h.width AS float) <= @width", new { width = searchBySpecVM.Size.Width + 0.2, width2 = searchBySpecVM.Size.Width - 0.2 });
                }
                if (searchBySpecVM.Size.Length.HasValue)
                {
                    builder.Where("TRY_CAST(h.length AS float) >= @length2 and TRY_CAST(h.length AS float) <= @length", new { length = searchBySpecVM.Size.Length + 0.3, length2 = searchBySpecVM.Size.Length - 0.3 });
                }
                if (searchBySpecVM.Size.Height.HasValue)
                {
                    builder.Where("TRY_CAST(h.height AS float) >= @height2 and TRY_CAST(h.height AS float) <= @height", new { height = searchBySpecVM.Size.Height + 0.2, height2 = searchBySpecVM.Size.Height - 0.2 });
                }
            }

            var builderTemplate =
                builder.AddTemplate("select p.id,p.name,p.new_name,'膠盒' as [type],h.material,h.color,h.series,h.positions,h.sex as sex_o,h.sealable as sealable_o,h.temper_operating,h.item_class,h.length,h.width,h.height from product p inner join housing h on p.id = h.product_id /**where**/");

            return
                conn.Query<SearchResultDetailVM>(builderTemplate.RawSql, builderTemplate.Parameters).ToList();
        }
        private List<SearchResultDetailVM> getTerminalResult(IDbConnection conn, SearchBySpecVM searchBySpecVM)
        {
            var builder = new SqlBuilder();

            builder.Where("p.[type] = 'Terminal'");

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSizeType))
            {
                builder.Where("h.item_class = @productSizeType", new { productSizeType = searchBySpecVM.ProductSizeType });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductClass))
            {
                builder.Where("h.item_class = @productClass", new { productClass = searchBySpecVM.ProductClass });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSex))
            {
                builder.Where("h.sex = @productSex", new { productSex = searchBySpecVM.ProductSex });
            }
            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSealable))
            {
                builder.Where("h.sealable = @productSealable", new { productSealable = searchBySpecVM.ProductSealable });
            }
            if (!string.IsNullOrEmpty(searchBySpecVM.ProductDepth))
            {
                float start = 0, end = 0;
                string[] depthStr = searchBySpecVM.ProductDepth.Split('~');
                if (!float.TryParse(depthStr[0], out start))
                    throw new ArgumentOutOfRangeException($"料厚起始值錯誤{depthStr[0]}");
                if (!float.TryParse(depthStr[1], out end))
                    throw new ArgumentOutOfRangeException($"料厚結束值錯誤{depthStr[1]}");

                if (!float.IsInfinity(end))
                {
                    builder.Where("TRY_CAST(h.depth AS float) >= @start and TRY_CAST(h.depth AS float) < @end", new { start = start, end = end });
                }
                else
                {
                    builder.Where("TRY_CAST(h.depth AS float) >= @start", new { start = start });
                }

            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductDiameter))
            {
                if (searchBySpecVM.ProductDiameter.Contains("~") == false)
                {
                    builder.Where("h.diameter = 'N/A'");
                }
                else
                {
                    float start = 0, end = 0;
                    string[] diameterStr = searchBySpecVM.ProductDiameter.Split('~');
                    if (!float.TryParse(diameterStr[0], out start))
                        throw new ArgumentOutOfRangeException($"適用線徑起始值錯誤{diameterStr[0]}");
                    if (!float.TryParse(diameterStr[1], out end))
                        throw new ArgumentOutOfRangeException($"適用線徑結束值錯誤{diameterStr[1]}");

                    builder.Where("TRY_CAST(h.diameter AS float) >= @start and TRY_CAST(h.diameter AS float) < @end", new { start = start, end = end });

                }
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTab))
            {
                float start = 0, end = 0;
                string[] tabStr = searchBySpecVM.ProductTab.Split('~');
                if (!float.TryParse(tabStr[0], out start))
                    throw new ArgumentOutOfRangeException($"新增TAB起始值錯誤{tabStr[0]}");
                if (!float.TryParse(tabStr[1], out end))
                    throw new ArgumentOutOfRangeException($"新增TAB結束值錯誤{tabStr[1]}");

                if (float.IsInfinity(end))
                {
                    builder.Where("TRY_CAST(h.tab AS float) >= @start", new { start = start });
                }
                else
                {
                    builder.Where("TRY_CAST(h.tab AS float) >= @start and TRY_CAST(h.tab AS float) < @end", new { start = start, end = end });
                }
            }

            if (searchBySpecVM.Size != null)
            {
                if (searchBySpecVM.Size.Width.HasValue)
                {
                    builder.Where("TRY_CAST(h.width AS float) >= @width2 and TRY_CAST(h.width AS float) <= @width", new { width = searchBySpecVM.Size.Width + 0.2, width2 = searchBySpecVM.Size.Width - 0.2 });
                }
                if (searchBySpecVM.Size.Length.HasValue)
                {
                    builder.Where("TRY_CAST(h.length AS float) >= @length2 and TRY_CAST(h.length AS float) <= @length", new { length = searchBySpecVM.Size.Length + 0.3, length2 = searchBySpecVM.Size.Length - 0.3 });
                }
                if (searchBySpecVM.Size.Height.HasValue)
                {
                    builder.Where("TRY_CAST(h.height AS float) >= @height2 and TRY_CAST(h.height AS float) <= @height", new { height = searchBySpecVM.Size.Height + 0.1, height2 = searchBySpecVM.Size.Height - 0.1 });
                }
                if (searchBySpecVM.Size.Wire_Size.HasValue)
                {
                    builder.Where("TRY_CAST(h.wire_size AS float) >= @wire_size2 and TRY_CAST(h.wire_size AS float) <= @wire_size", new { wire_size = searchBySpecVM.Size.Wire_Size + 0.2, wire_size2 = searchBySpecVM.Size.Wire_Size - 0.2 });
                }
            }

            var builderTemplate =
                builder.AddTemplate("select p.id,p.name,p.new_name,'端子' as [type],h.wire_size,h.depth,h.diameter,h.plating,h.material,h.sex as sex_o,h.sealable as sealable_o,'N/A' as temper_operating,h.item_class,h.length,h.width,h.height from product p inner join terminal h on p.id = h.product_id /**where**/ ");

            return
                conn.Query<SearchResultDetailVM>(builderTemplate.RawSql, builderTemplate.Parameters).ToList();
        }
        private List<SearchResultDetailVM> getRubberResult(IDbConnection conn, SearchBySpecVM searchBySpecVM)
        {
            var builder = new SqlBuilder();

            builder.Where("p.[type] = 'Rubber'");

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSizeType))
            {
                builder.Where("h.item_class = @productSizeType", new { productSizeType = searchBySpecVM.ProductSizeType });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductClass))
            {
                builder.Where("h.item_class = @productClass", new { productClass = searchBySpecVM.ProductClass });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTemper))
            {
                builder.Where("h.temper_operating = @productTemper", new { productTemper = searchBySpecVM.ProductTemper });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductDiameter))
            {
                if (searchBySpecVM.ProductDiameter.Contains("~") == false)
                {
                    builder.Where("h.diameter = 'N/A'");
                }
                else
                {
                    float start = 0, end = 0;
                    string[] diameterStr = searchBySpecVM.ProductDiameter.Split('~');
                    if (!float.TryParse(diameterStr[0], out start))
                        throw new ArgumentOutOfRangeException($"適用線徑起始值錯誤{diameterStr[0]}");
                    if (!float.TryParse(diameterStr[1], out end))
                        throw new ArgumentOutOfRangeException($"適用線徑結束值錯誤{diameterStr[1]}");

                    builder.Where("TRY_CAST(h.diameter AS float) >= @start and TRY_CAST(h.diameter AS float) < @end", new { start = start, end = end });

                }
            }

            if (searchBySpecVM.Size != null)
            {
                if (searchBySpecVM.Size.Length.HasValue)
                {
                    builder.Where("TRY_CAST(h.length AS float) >= @length2 and TRY_CAST(h.length AS float) <= @length", new { length = searchBySpecVM.Size.Length + 0.4, length2 = searchBySpecVM.Size.Length - 0.4 });
                }
                if (searchBySpecVM.Size.Diameter_1.HasValue)
                {
                    builder.Where("TRY_CAST(h.diameter_1 AS float) >= @diameter_12 and TRY_CAST(h.diameter_1 AS float) <= @diameter_1", new { diameter_1 = searchBySpecVM.Size.Diameter_1 + 0.2, diameter_12 = searchBySpecVM.Size.Diameter_1 - 0.2 });
                }
                if (searchBySpecVM.Size.Diameter_2.HasValue)
                {
                    builder.Where("TRY_CAST(h.diameter_2 AS float) >= @diameter_22 and TRY_CAST(h.diameter_2 AS float) <= @diameter_2", new { diameter_2 = searchBySpecVM.Size.Diameter_2 + 0.2, diameter_22 = searchBySpecVM.Size.Diameter_2 - 0.2 });
                }
            }

            var builderTemplate =
                builder.AddTemplate("select p.id,p.name,p.new_name,'防水栓' as [type],'N' as sex_o,'N' as sealable_o,h.material,h.color,h.temper_operating,h.item_class,h.diameter,h.diameter_1,h.diameter_2,h.length,h.width,0 as height from product p inner join rubber h on p.id = h.product_id /**where**/ ");

            return
                conn.Query<SearchResultDetailVM>(builderTemplate.RawSql, builderTemplate.Parameters).ToList();
        }
        private List<SearchResultDetailVM> getSleeveResult(IDbConnection conn, SearchBySpecVM searchBySpecVM)
        {
            var builder = new SqlBuilder();

            builder.Where("p.[type] = 'Sleeve'");

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSizeType))
            {
                builder.Where("h.item_class = @productSizeType", new { productSizeType = searchBySpecVM.ProductSizeType });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductClass))
            {
                builder.Where("h.item_class = @productClass", new { productClass = searchBySpecVM.ProductClass });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTemper))
            {
                builder.Where("h.temper_operating = @productTemper", new { productTemper = searchBySpecVM.ProductTemper });
            }

            if (searchBySpecVM.Size != null)
            {
                if (searchBySpecVM.Size.Length.HasValue)
                {
                    builder.Where("TRY_CAST(h.length AS float) >= @length2 and TRY_CAST(h.length AS float) <= @length", new { length = searchBySpecVM.Size.Length + 0.3, length2 = searchBySpecVM.Size.Length - 0.3 });
                }
                if (searchBySpecVM.Size.Diameter_1.HasValue)
                {
                    builder.Where("TRY_CAST(h.diameter_1 AS float) >= @diameter_12 and TRY_CAST(h.diameter_1 AS float) <= @diameter_1", new { diameter_1 = searchBySpecVM.Size.Diameter_1 + 0.2, diameter_12 = searchBySpecVM.Size.Diameter_1 - 0.2 });
                }
                if (searchBySpecVM.Size.Diameter_2.HasValue)
                {
                    builder.Where("TRY_CAST(h.diameter_2 AS float) >= @diameter_22 and TRY_CAST(h.diameter_2 AS float) <= @diameter_2", new { diameter_2 = searchBySpecVM.Size.Diameter_2 + 0.2, diameter_22 = searchBySpecVM.Size.Diameter_2 - 0.2 });
                }
            }

            var builderTemplate =
                builder.AddTemplate("select p.id,p.name,p.new_name,'PVC絕緣護套' as [type],'N' as sex_o,'N' as sealable_o,h.applicable_wire,h.diameter_1,h.diameter_2,h.material,h.color,h.temper_operating,h.item_class,h.length,h.width,h.height as height from product p inner join sleeve h on p.id = h.product_id /**where**/ ");

            return
                conn.Query<SearchResultDetailVM>(builderTemplate.RawSql, builderTemplate.Parameters).ToList();
        }
        private List<SearchResultDetailVM> getGrommetResult(IDbConnection conn, SearchBySpecVM searchBySpecVM)
        {
            var builder = new SqlBuilder();

            builder.Where("p.[type] = 'Grommet'");

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSizeType))
            {
                builder.Where("h.item_class = @productSizeType", new { productSizeType = searchBySpecVM.ProductSizeType });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductClass))
            {
                builder.Where("h.item_class = @productClass", new { productClass = searchBySpecVM.ProductClass });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTemper))
            {
                builder.Where("h.temper_operating Like '%' + @productTemper + '%'", new { productTemper = searchBySpecVM.ProductTemper });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTemper))
            {
                builder.Where("h.temper_operating = @productTemper", new { productTemper = searchBySpecVM.ProductTemper });
            }

            if (searchBySpecVM.Size != null)
            {
                if (searchBySpecVM.Size.Length.HasValue)
                {
                    builder.Where("TRY_CAST(h.length AS float) >= @length2 and TRY_CAST(h.length AS float) <= @length", new { length = searchBySpecVM.Size.Length + 0.5, length2 = searchBySpecVM.Size.Length - 0.5 });
                }
                if (searchBySpecVM.Size.Diameter_1.HasValue)
                {
                    builder.Where("TRY_CAST(h.diameter_1 AS float) >= @diameter_12 and TRY_CAST(h.diameter_1 AS float) <= @diameter_1", new { diameter_1 = searchBySpecVM.Size.Diameter_1 + 0.5, diameter_12 = searchBySpecVM.Size.Diameter_1 - 0.5 });
                }
                if (searchBySpecVM.Size.Diameter_2.HasValue)
                {
                    builder.Where("TRY_CAST(h.diameter_2 AS float) >= @diameter_22 and TRY_CAST(h.diameter_2 AS float) <= @diameter_2", new { diameter_2 = searchBySpecVM.Size.Diameter_2 + 0.5, diameter_22 = searchBySpecVM.Size.Diameter_2 - 0.5 });
                }
            }

            var builderTemplate =
                builder.AddTemplate("select p.id,p.name,p.new_name,'索環護套' as [type],'N' as sex_o,'N' as sealable_o,h.applicable_wire,h.diameter_1,h.diameter_2,h.temper_operating,h.material,h.color,h.item_class,h.length,h.width,h.height as height from product p inner join grommet h on p.id = h.product_id /**where**/ ");

            return
                conn.Query<SearchResultDetailVM>(builderTemplate.RawSql, builderTemplate.Parameters).ToList();
        }
        #endregion

        public JsonResult SearchByApply(SearchByApplyVM searchByApplyVM)
        {
            var changeLang = langData["語系"].ToString();
            var searchResult = new List<SearchResultDetailVM>();
            var searchResult2 = new List<SearchResultDetailVM>();
            SearchResultDetailVM addResultDetailVM = new SearchResultDetailVM();
            using (var conn = _connectionFactory.CreateProductDBConnection())
            {
                //string findProduct =
                //    @"select distinct p.id, p.name,p.new_name, p.type from [dbo].[carsys_l3] l3
                //        inner join[dbo].[carsys_l4] l4 on l3.id = l4.l3
                //        inner join[dbo].[carsys_l5] l5 on l4.id = l5.l4
                //        inner join[dbo].[map_carsys] map on map.l5 = l5.id
                //        inner join[dbo].[product] p on map.product_id = p.id
                //        where l3.id = @id";
                string findProduct =
                     @"select distinct p.id, p.name,p.new_name, p.type from [dbo].[carsys_l4] l4
                        inner join[dbo].[carsys_l5] l5 on l4.id = l5.l4
                        inner join[dbo].[map_carsys] map on map.l5 = l5.id
                        inner join[dbo].[product] p on map.product_id = p.id
                        where l4.id = @id";

                var products = conn.Query(findProduct, new { id = searchByApplyVM.Id });
                foreach (var product in products)
                    searchResult.Add(new SearchResultDetailVM() { id = (int)product.id, name = product.name });

                string searchHosing = @"select h.product_id,h.sex as sex_o,h.series,h.positions,h.sealable as sealable_o,h.temper_operating,h.item_class,h.length,h.width,h.height,h.material from housing h where product_id in @hosuings";
                var housings = conn.Query(searchHosing, new { hosuings = products.Where(w => w.type == "Housing").Select(s => s.id) });
                foreach (var housing in housings)
                {
                    var result = searchResult.SingleOrDefault(s => s.id == housing.product_id);
                    if (result != null)
                    {
                        result.type = "膠盒";
                        result.sex_o = housing.sex_o;
                        result.sealable_o = housing.sealable_o;
                        result.length = housing.length;
                        result.width = housing.width;
                        result.height = housing.height;
                        result.positions = housing.positions;
                        result.series = housing.series;
                        result.item_class = housing.item_class;
                        result.temper_operating = housing.temper_operating;
                        result.material = housing.material;
                    }
                }

                string searchTerminal = @"select h.product_id,h.sex as sex_o,h.sealable as sealable_o,'N/A' as temper_operating,h.item_class,h.length,h.width,h.height,h.material from terminal h where product_id in @terminals";
                var terminals = conn.Query(searchTerminal, new { terminals = products.Where(w => w.type == "Terminal").Select(s => s.id) });
                foreach (var terminal in terminals)
                {
                    var result = searchResult.SingleOrDefault(s => s.id == terminal.product_id);
                    if (result != null)
                    {
                        result.type = "端子";
                        result.sex_o = terminal.sex_o;
                        result.sealable_o = terminal.sealable_o;
                        result.length = terminal.length;
                        result.width = terminal.width;
                        result.height = terminal.height;
                        result.item_class = terminal.item_class;
                        result.temper_operating = terminal.temper_operating;
                        result.material = terminal.material;
                    }
                }

                string searchRubber = @"select h.product_id,'N' as sex_o,'N' as sealable_o,h.temper_operating,h.item_class,h.length,h.width,0 as height,h.material from rubber h where product_id in @rubbers";
                var rubbers = conn.Query(searchRubber, new { rubbers = products.Where(w => w.type == "Rubber").Select(s => s.id) });
                foreach (var rubber in rubbers)
                {
                    var result = searchResult.SingleOrDefault(s => s.id == rubber.product_id);
                    if (result != null)
                    {
                        result.type = "防水栓";
                        result.sex_o = rubber.sex_o;
                        result.sealable_o = rubber.sealable_o;
                        result.length = rubber.length;
                        result.width = rubber.width;
                        result.height = rubber.height;
                        result.item_class = rubber.item_class;
                        result.temper_operating = rubber.temper_operating;
                        result.material = rubber.material;
                    }
                }

                string searchSleeve = @"select h.product_id,'N' as sex_o,'N' as sealable_o,h.temper_operating,h.item_class,h.length,h.width,h.height,h.material from sleeve h where product_id in @sleeves";
                var sleeves = conn.Query(searchSleeve, new { sleeves = products.Where(w => w.type == "Rubber").Select(s => s.id) });
                foreach (var sleeve in sleeves)
                {
                    var result = searchResult.SingleOrDefault(s => s.id == sleeve.product_id);
                    if (result != null)
                    {
                        result.type = "PVC絕緣護套";
                        result.sex_o = sleeve.sex_o;
                        result.sealable_o = sleeve.sealable_o;
                        result.length = sleeve.length;
                        result.width = sleeve.width;
                        result.height = sleeve.height;
                        result.item_class = sleeve.item_class;
                        result.temper_operating = sleeve.temper_operating;
                        result.material = sleeve.material;
                    }
                }

                string searchGrommet = @"select h.product_id,'N' as sex_o,'N' as sealable_o,h.temper_operating,h.item_class,h.length,h.width,h.height,h.material as height from grommet h where product_id in @grommets";
                var grommets = conn.Query(searchGrommet, new { grommets = products.Where(w => w.type == "Grommet").Select(s => s.id) });
                foreach (var grommet in grommets)
                {
                    var result = searchResult.SingleOrDefault(s => s.id == grommet.product_id);
                    if (result != null)
                    {
                        result.type = "索環護套";
                        result.sex_o = grommet.sex_o;
                        result.sealable_o = grommet.sealable_o;
                        result.length = grommet.length;
                        result.width = grommet.width;
                        result.height = grommet.height;
                        result.item_class = grommet.item_class;
                        result.temper_operating = grommet.temper_operating;
                        result.material = grommet.material;
                    }
                }
            }

            var tempL4Id = Int32.Parse(searchByApplyVM.Id.ToString());
            var productCarsysl5 = (from a in DB.carsysl5
                                   where a.l4 == tempL4Id
                                   select a).FirstOrDefault();
            var productSelf = (from a in DB.mapcarsys
                               where a.l5 == productCarsysl5.id
                               select a).ToList();

            //var productSelf = (from a in DB.product_info
            //                   where a.carsysl4name == searchByApplyVM.Id.ToString()
            //                   select a).ToList();
            string type = "";
            foreach (var item in productSelf)
            {
                using (SqlConnection conn5 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    string sql1 = "select * from product where id= " + item.product_id;
                    using (SqlCommand cmd6 = conn5.CreateCommand())
                    {
                        conn5.Open();
                        cmd6.CommandText = sql1;
                        cmd6.CommandType = CommandType.Text;
                        SqlDataReader reader6 = cmd6.ExecuteReader();
                        while (reader6.Read())
                        {
                            //addResultDetailVM.item_class = reader6[2].ToString();
                            type = reader6[2].ToString();
                        }
                    }
                }

                if (type == "Housing")
                {
                    using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    {
                        string sql2 = "select * from housing where product_id= " + item.product_id;
                        using (SqlCommand cmd7 = conn6.CreateCommand())
                        {
                            conn6.Open();
                            cmd7.CommandText = sql2;
                            cmd7.CommandType = CommandType.Text;
                            SqlDataReader reader7 = cmd7.ExecuteReader();
                            while (reader7.Read())
                            {
                                //addResultDetailVM.item_class = reader6[2].ToString();
                                addResultDetailVM.type = "膠盒";
                                addResultDetailVM.material = reader7[5].ToString();
                                addResultDetailVM.sex_o = reader7[2].ToString();
                                addResultDetailVM.sealable_o = reader7[6].ToString();
                                addResultDetailVM.length = reader7[26].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[26].ToString());
                                addResultDetailVM.width = reader7[27].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[27].ToString());
                                addResultDetailVM.height = reader7[28].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[28].ToString());
                                addResultDetailVM.positions = reader7[11].ToString();
                                addResultDetailVM.series = reader7[39].ToString();
                                addResultDetailVM.item_class = reader7[30].ToString();
                                addResultDetailVM.temper_operating = reader7[10].ToString();
                                addResultDetailVM.id = Int32.Parse(reader7[1].ToString());
                                searchResult.Add(addResultDetailVM);
                            }
                        }
                    }
                }
                else if (type == "Terminal")
                {
                    using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    {
                        string sql2 = "select * from terminal where product_id= " + item.product_id;
                        using (SqlCommand cmd7 = conn6.CreateCommand())
                        {
                            conn6.Open();
                            cmd7.CommandText = sql2;
                            cmd7.CommandType = CommandType.Text;
                            SqlDataReader reader7 = cmd7.ExecuteReader();
                            while (reader7.Read())
                            {
                                addResultDetailVM.type = "端子";
                                addResultDetailVM.material = reader7[4].ToString();
                                addResultDetailVM.sex_o = reader7[3].ToString();
                                addResultDetailVM.sealable_o = reader7[21].ToString();
                                addResultDetailVM.length = reader7[31].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[31].ToString());
                                addResultDetailVM.width = reader7[32].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[32].ToString());
                                addResultDetailVM.height = reader7[33].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[33].ToString());
                                addResultDetailVM.item_class = reader7[2].ToString();
                                addResultDetailVM.temper_operating = "N/A";
                                addResultDetailVM.id = Int32.Parse(reader7[1].ToString());

                                searchResult.Add(addResultDetailVM);
                            }
                        }
                    }
                }
                else if (type == "Rubber")
                {
                    using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    {
                        string sql2 = "select * from rubber where product_id= " + item.product_id;
                        using (SqlCommand cmd7 = conn6.CreateCommand())
                        {
                            conn6.Open();
                            cmd7.CommandText = sql2;
                            cmd7.CommandType = CommandType.Text;
                            SqlDataReader reader7 = cmd7.ExecuteReader();
                            while (reader7.Read())
                            {
                                addResultDetailVM.type = "防水栓";
                                addResultDetailVM.material = reader7[4].ToString();
                                addResultDetailVM.sex_o = "N";
                                addResultDetailVM.sealable_o = "N";
                                addResultDetailVM.length = reader7[6].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[6].ToString());
                                addResultDetailVM.width = reader7[15].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[15].ToString());
                                addResultDetailVM.height = Single.Parse("0");
                                addResultDetailVM.item_class = reader7[14].ToString();
                                addResultDetailVM.temper_operating = reader7[7].ToString();
                                addResultDetailVM.id = Int32.Parse(reader7[1].ToString());

                                searchResult.Add(addResultDetailVM);
                            }
                        }
                    }
                }
                else if (type == "Sleeve")
                {
                    using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    {
                        string sql2 = "select * from sleeve where product_id= " + item.product_id;
                        using (SqlCommand cmd7 = conn6.CreateCommand())
                        {
                            conn6.Open();
                            cmd7.CommandText = sql2;
                            cmd7.CommandType = CommandType.Text;
                            SqlDataReader reader7 = cmd7.ExecuteReader();
                            while (reader7.Read())
                            {
                                addResultDetailVM.type = "PVC絕緣護套";
                                addResultDetailVM.material = reader7[4].ToString();
                                addResultDetailVM.sex_o = "N";
                                addResultDetailVM.sealable_o = "N";
                                addResultDetailVM.length = reader7[19].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[19].ToString());
                                addResultDetailVM.width = reader7[20].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[20].ToString());
                                addResultDetailVM.height = reader7[21].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[21].ToString());
                                addResultDetailVM.item_class = reader7[2].ToString();
                                addResultDetailVM.temper_operating = reader7[7].ToString();
                                addResultDetailVM.id = Int32.Parse(reader7[1].ToString());

                                searchResult.Add(addResultDetailVM);
                            }
                        }
                    }
                }
                else if (type == "Grommet")
                {
                    using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    {
                        string sql2 = "select * from grommet where product_id= " + item.product_id;
                        using (SqlCommand cmd7 = conn6.CreateCommand())
                        {
                            conn6.Open();
                            cmd7.CommandText = sql2;
                            cmd7.CommandType = CommandType.Text;
                            SqlDataReader reader7 = cmd7.ExecuteReader();
                            while (reader7.Read())
                            {
                                addResultDetailVM.type = "索環護套";
                                addResultDetailVM.material = reader7[4].ToString();
                                addResultDetailVM.sex_o = "N";
                                addResultDetailVM.sealable_o = "N";
                                addResultDetailVM.length = reader7[19].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[19].ToString());
                                addResultDetailVM.width = reader7[20].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[20].ToString());
                                addResultDetailVM.height = reader7[21].ToString() == "" ? Single.Parse("0") : Single.Parse(reader7[21].ToString());
                                addResultDetailVM.item_class = reader7[2].ToString();
                                addResultDetailVM.temper_operating = reader7[7].ToString();
                                addResultDetailVM.id = Int32.Parse(reader7[1].ToString());

                                searchResult.Add(addResultDetailVM);
                            }
                        }
                    }
                }
                else
                {

                }
                addResultDetailVM = new SearchResultDetailVM();
            }

            string filecheck = "picture1";
            string productfilename;
            string productnewname;

            foreach (var tempResult in searchResult)
            {
                using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    conn2.Open();
                    string img_sql = "select * from product_file where product_id= " + tempResult.id + " and filecheck= '" + filecheck + "'";
                    using (SqlCommand cmd = conn2.CreateCommand())
                    {
                        cmd.CommandText = img_sql;
                        cmd.CommandType = CommandType.Text;
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            productfilename = reader[2].ToString();
                            if (productfilename != "")
                            {
                                var temp = productfilename.Split('.');
                                var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + tempResult.id + "/" + temp[0] + "_WM." + temp[1];
                                tempResult.productImg = productImg;
                            }
                            else
                            {
                                tempResult.productImg = "";
                            }
                        }
                    }
                }

                using (SqlConnection conn3 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    string newName_sql = "select * from product where id= " + tempResult.id;
                    using (SqlCommand cmd2 = conn3.CreateCommand())
                    {
                        conn3.Open();
                        cmd2.CommandText = newName_sql;
                        cmd2.CommandType = CommandType.Text;
                        SqlDataReader reader = cmd2.ExecuteReader();
                        while (reader.Read())
                        {
                            productnewname = reader[6].ToString();
                            if (productnewname != "")
                            {
                                tempResult.new_name = productnewname;
                            }
                            else
                            {
                                tempResult.new_name = "";
                            }
                        }
                    }
                }
            }

            var pageDatas = searchResult.Skip(searchByApplyVM.PageIndex - 1).Take(searchByApplyVM.PageSize).ToList();

            for (int i = 0; i < pageDatas.Count(); i++)
            {
                if (i == 0)
                {
                    pageDatas[i].th = 1;
                    pageDatas[i].type = "All";
                }
                else
                {
                    pageDatas[i].th = 0;
                    pageDatas[i].type = "All";
                }

                if (pageDatas[i].color != null)
                {
                    //if (pageDatas[i].color == "N/A")
                    //{

                    //}
                    if (pageDatas[i].color == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].color.Split('(');
                            pageDatas[i].color = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].color.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].color = resultData;
                            }
                            else
                            {
                                pageDatas[i].color = temp[0];
                            }

                        }
                    }
                }

                if (pageDatas[i].sex_o != null)
                {
                    //if (pageDatas[i].sex_o == "N/A")
                    //{

                    //}
                    if (pageDatas[i].sex_o == "")
                    {

                    }
                    else if (pageDatas[i].sex_o == "N")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].sex_o.Split('(');
                            pageDatas[i].reSex = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].sex_o.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].reSex = resultData;
                            }
                            else
                            {
                                pageDatas[i].reSex = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].type == "端子" || pageDatas[i].type == "All")
                {
                    pageDatas[i].reSealable = changeLang == "tw" ? pageDatas[i].sealable : changeLang == "cn" ? pageDatas[i].sealable : pageDatas[i].sealable_o.Split('(').Length > 1 ? pageDatas[i].sealable_o.Split('(')[0] : pageDatas[i].sealable_o;
                }
                else
                {
                    pageDatas[i].reSealable = changeLang == "tw" ? pageDatas[i].sealable : changeLang == "cn" ? pageDatas[i].sealable : pageDatas[i].sealable_o;
                }


                if (pageDatas[i].material != null)
                {
                    //if (pageDatas[i].material == "N/A")
                    //{

                    //}
                    if (pageDatas[i].material == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].material.Split('(');
                            pageDatas[i].reMaterial = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].material.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].reMaterial = resultData;
                            }
                            else
                            {
                                pageDatas[i].reMaterial = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].plating != null)
                {
                    //if (pageDatas[i].plating == "N/A")
                    //{

                    //}
                    if (pageDatas[i].plating == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].plating.Split('(');
                            pageDatas[i].rePlating = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].plating.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].rePlating = resultData;
                            }
                            else
                            {
                                pageDatas[i].rePlating = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].item_class != null)
                {
                    //if (pageDatas[i].item_class == "N/A")
                    //{

                    //}
                    if (pageDatas[i].item_class == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].item_class.Split('(');
                            pageDatas[i].item_class = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].item_class.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].item_class = resultData;
                            }
                            else
                            {
                                pageDatas[i].item_class = temp[0];
                            }
                        }
                    }
                }
            }

            return Json(new SearchResultVM() { TotalCount = searchResult.Count, PageDatas = pageDatas });
        }

        public JsonResult SearchByKeyword(SearchByKeyWordVM searchByKeyWordVM)
        {
            var changeLang = langData["語系"].ToString();
            var searchResult = new List<SearchResultDetailVM>();

            using (var conn = _connectionFactory.CreateProductDBConnection())
            {
                if (searchByKeyWordVM.FindWay == 0)
                {
                    searchResult = getByKeyworkResult(conn, searchByKeyWordVM.Keyword, searchByKeyWordVM.TableKey);
                }
                else if (searchByKeyWordVM.FindWay == 1)
                {
                    searchResult = getByPartNoResult(conn, searchByKeyWordVM.PartNo, searchByKeyWordVM.TableKey);
                }
            }

            if (searchByKeyWordVM.TableKey == null || searchByKeyWordVM.TableKey == "A")
            {
                foreach (var item in searchResult)
                {
                    switch (item.type)
                    {
                        case "Housing":
                            //item.type = "膠盒";
                            item.type = "端子";
                            break;
                        case "Terminal":
                            item.type = "端子";
                            break;
                        case "Rubber":
                            item.type = "防水栓";
                            break;
                        case "Sleeve":
                            item.type = "PVC絕緣護套";
                            break;
                        case "Grommet":
                            item.type = "索環護套";
                            break;
                        default:
                            item.type = string.Empty;
                            break;
                    }
                }
            }
            else
            {
                foreach (var item in searchResult)
                {
                    switch (item.type)
                    {
                        case "Housing":
                            item.type = "膠盒";
                            //item.type = "端子";
                            break;
                        case "Terminal":
                            item.type = "端子";
                            break;
                        case "Rubber":
                            item.type = "防水栓";
                            break;
                        case "Sleeve":
                            item.type = "PVC絕緣護套";
                            break;
                        case "Grommet":
                            item.type = "索環護套";
                            break;
                        default:
                            item.type = string.Empty;
                            break;
                    }
                }
            }


            //var pageDatas = searchResult.Skip(searchByKeyWordVM.PageIndex - 1).Take(searchByKeyWordVM.PageSize).ToList();
            var pageDatas = searchResult.Skip((searchByKeyWordVM.PageIndex - 1) * searchByKeyWordVM.PageSize).Take(searchByKeyWordVM.PageSize).ToList();

            string filecheck = "picture1";
            string productfilename;
            string productnewname;
            string productname;

            for (int i = 0; i < pageDatas.Count(); i++)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    string newName_sql = "select * from product where id= " + pageDatas[i].id;
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        conn.Open();
                        cmd.CommandText = newName_sql;
                        cmd.CommandType = CommandType.Text;
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            productnewname = reader[6].ToString();
                            if (productnewname != "")
                            {
                                pageDatas[i].new_name = productnewname;
                            }
                            else
                            {
                                pageDatas[i].new_name = "";
                            }

                            productname = reader[1].ToString();
                            if (productname != "")
                            {
                                pageDatas[i].name = productname;
                            }
                            else
                            {
                                pageDatas[i].name = "";
                            }
                        }
                    }
                }

                using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    conn2.Open();
                    string img_sql = "select * from product_file where product_id= " + pageDatas[i].id + " and filecheck= '" + filecheck + "'";
                    using (SqlCommand cmd = conn2.CreateCommand())
                    {
                        cmd.CommandText = img_sql;
                        cmd.CommandType = CommandType.Text;
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            productfilename = reader[2].ToString();
                            if (productfilename != "")
                            {
                                var temp = productfilename.Split('.');
                                var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + pageDatas[i].id + "/" + temp[0] + "_WM." + temp[1];
                                pageDatas[i].productImg = productImg;
                            }
                            else
                            {
                                pageDatas[i].productImg = "";
                            }
                        }
                    }
                }

                if (i == 0)
                {
                    pageDatas[i].th = 1;
                }
                else
                {
                    pageDatas[i].th = 0;
                }

                if (pageDatas[i].color != null)
                {
                    //if (pageDatas[i].color == "N/A")
                    //{

                    //}
                    if (pageDatas[i].color == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].color.Split('(');
                            pageDatas[i].color = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].color.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].color = resultData;
                            }
                            else
                            {
                                pageDatas[i].color = temp[0];
                            }

                        }
                    }
                }

                if (pageDatas[i].sex_o != null)
                {
                    //if (pageDatas[i].sex_o == "N/A")
                    //{

                    //}
                    if (pageDatas[i].sex_o == "")
                    {

                    }
                    else if (pageDatas[i].sex_o == "N")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].sex_o.Split('(');
                            pageDatas[i].reSex = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].sex_o.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].reSex = resultData;
                            }
                            else
                            {
                                pageDatas[i].reSex = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].type == "端子")
                {
                    pageDatas[i].reSealable = changeLang == "tw" ? pageDatas[i].sealable : changeLang == "cn" ? pageDatas[i].sealable : pageDatas[i].sealable_o.Split('(').Length > 1 ? pageDatas[i].sealable_o.Split('(')[0] : pageDatas[i].sealable_o;
                }
                else
                {
                    pageDatas[i].reSealable = changeLang == "tw" ? pageDatas[i].sealable : changeLang == "cn" ? pageDatas[i].sealable : pageDatas[i].sealable_o;
                }


                if (pageDatas[i].material != null)
                {
                    //if (pageDatas[i].material == "N/A")
                    //{

                    //}
                    if (pageDatas[i].material == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].material.Split('(');
                            pageDatas[i].reMaterial = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].material.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].reMaterial = resultData;
                            }
                            else
                            {
                                pageDatas[i].reMaterial = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].plating != null)
                {
                    //if (pageDatas[i].plating == "N/A")
                    //{

                    //}
                    if (pageDatas[i].plating == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].plating.Split('(');
                            pageDatas[i].rePlating = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].plating.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].rePlating = resultData;
                            }
                            else
                            {
                                pageDatas[i].rePlating = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].item_class != null)
                {
                    //if (pageDatas[i].item_class == "N/A")
                    //{

                    //}
                    if (pageDatas[i].item_class == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].item_class.Split('(');
                            pageDatas[i].item_class = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].item_class.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].item_class = resultData;
                            }
                            else
                            {
                                pageDatas[i].item_class = temp[0];
                            }
                        }
                    }
                }
            }

            return Json(new SearchResultVM() { TotalCount = searchResult.Count, PageDatas = pageDatas }, JsonRequestBehavior.AllowGet);
        }

        private List<SearchResultDetailVM> getByKeyworkResult(IDbConnection conn, string keyword, string tableKey = null)
        {
            if (string.IsNullOrEmpty(keyword))
                return new List<SearchResultDetailVM>();

            if (tableKey == null || tableKey == "A")
            {
                string sql =
                       @"select p.id,p.name,p.new_name,a.series,a.sex_o,a.positions,a.sealable_o,a.temper_operating,a.item_class,a.depth,a.diameter,a.plating,a.material,a.wire_size,a.color,a.width,a.length,a.height,a.type from product p
                        left join 
                        (
	                        select h.product_id,h.sex as sex_o,h.series,h.positions,h.sealable as sealable_o,h.temper_operating,h.item_class,'' as depth,'' as diameter,'' as plating,h.material,'' as wire_size,h.color,h.length,h.width,h.height,'Housing' as [type] from housing h
	                        union
	                        select t.product_id,t.sex as sex_o,'' as series,'' as positions,t.sealable as sealable_o,'N/A' as temper_operating,t.item_class,t.depth,t.diameter,t.plating,t.material,t.wire_size,'' as color,t.length,t.width,t.height,'Terminal' as [type] from terminal t
	                        union
	                        select r.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,r.temper_operating,r.item_class,'' as depth,'' as diameter,'' as plating,r.material,'' as wire_size,r.color,r.length,r.width,0 as height,'Rubber' as [type] from rubber r
	                        union
	                        select s.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,s.temper_operating,s.item_class,'' as depth,'' as diameter,'' as plating,'' as material,'' as wire_size,s.color,s.length,s.width,s.height,'Sleeve' as [type] from sleeve s
	                        union
	                        select g.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,g.temper_operating,g.item_class,'' as depth,'' as diameter,'' as plating,'' as material,'' as wire_size,g.color,g.length,g.width,g.height as height,'Grommet' as [type] from grommet g
                        ) a on p.id = a.product_id and p.type = a.type
                        where (p.name like '%' + @keyword + '%') or (p.new_name like '%' + @keyword + '%') ";

                string tableSql = @"
                    union
                    select p.id,p.name,p.new_name,a.series,a.sex as sex_o,a.positions,a.sealable as sealable_o,a.temper_operating,a.item_class,'' as depth,'' as diameter,'' as plating,a.material,'' as wire_size,a.color,a.width,a.length,a.height,'Housing' as [type]
                    from housing a inner join product p on a.product_id = p.id and p.type = 'Housing'
                    where a.item_class like '%' + @keyword + '%'
	                union
	                select p.id,p.name,p.new_name,'' as series,a.sex as sex_o,'' as positions,a.sealable as sealable_o,'N/A' as temper_operating,a.item_class,a.depth,a.diameter,a.plating,a.material,a.wire_size,NULL as color,a.width,a.length,a.height,'Terminal' as [type]
                    from terminal a inner join product p on a.product_id = p.id and p.type = 'Terminal'
                    where a.item_class like '%' + @keyword + '%'
	                union
	                select p.id,p.name,p.new_name,'' as series,'N' as sex_o,'' as positions,'N' as sealable_o,a.temper_operating,a.item_class,'' as depth,'' as diameter,'' as plating,a.material,'' as wire_size,a.color,a.width,a.length,0 as height,'Rubber' as [type]
                    from rubber a inner join product p on a.product_id = p.id and p.type = 'Rubber'
                    where a.item_class like '%' + @keyword + '%'
	                union
	                select p.id,p.name,p.new_name,'' as series,'N' as sex_o,'' as positions,'N' as sealable_o,a.temper_operating,a.item_class,'' as depth,'' as diameter,'' as plating,'' as material,'' as wire_size,a.color,a.width,a.length,0 as height,'Sleeve' as [type]
                    from sleeve a inner join product p on a.product_id = p.id and p.type = 'Sleeve'
                    where a.item_class like '%' + @keyword + '%'
	                union
	                select p.id,p.name,p.new_name,'' as series,'N' as sex_o,'' as positions,'N' as sealable_o,a.temper_operating,a.item_class,'' as depth,'' as diameter,'' as plating,'' as material,'' as wire_size,a.color,a.width,a.length,0 as height,'Grommet' as [type]
                    from grommet a inner join product p on a.product_id = p.id and p.type = 'Grommet'
                    where a.item_class like '%' + @keyword + '%'";


                return conn.Query<SearchResultDetailVM>(sql + tableSql, new { keyword = keyword }).ToList();

            }
            else
            {
                string sql = string.Empty;
                switch (tableKey)
                {
                    case "H":
                        sql =
                            @"select p.id,p.name,p.new_name,a.series,a.sex as sex_o,a.positions,a.sealable as sealable_o,a.temper_operating,a.item_class,a.material,a.color,a.width,a.length,a.height,p.type from product p
                            left join housing a on p.id = a.product_id
                            where ((p.name like '%' + @keyword + '%') or (p.new_name like '%' + @keyword + '%')) and p.type='Housing'
                            union
                            select p.id,p.name,p.new_name,a.series,a.sex as sex_o,a.positions,a.sealable as sealable_o,a.temper_operating,a.item_class,a.material,a.color,a.width,a.length,a.height,'Housing' as [type]
                            from housing a inner join product p on a.product_id = p.id and p.type = 'Housing'
                            where a.item_class like '%' + @keyword + '%'";
                        break;
                    case "T":
                        sql =
                            @"select p.id,p.name,p.new_name,'' as series,a.sex as sex_o,'' as positions,a.sealable as sealable_o,'N/A' as temper_operating,a.item_class,a.depth,a.diameter,a.plating,a.material,a.wire_size,a.width,a.length,a.height,p.type from product p
                            left join terminal a on p.id = a.product_id
                            where ((p.name like '%' + @keyword + '%') or (p.new_name like '%' + @keyword + '%')) and p.type='Terminal'
                            union
                            select p.id,p.name,p.new_name,'' as series,a.sex as sex_o,'' as positions,a.sealable as sealable_o,'N/A' as temper_operating,a.item_class,a.depth,a.diameter,a.plating,a.material,a.wire_size,a.width,a.length,a.height,'Terminal' as [type]
                            from terminal a inner join product p on a.product_id = p.id and p.type = 'Terminal'
                            where a.item_class like '%' + @keyword + '%'";
                        break;
                    case "R":
                        sql =
                            @"select p.id,p.name,p.new_name,'' as series,'N' as sex_o,'' as positions,'N' as sealable_o,a.temper_operating,a.item_class,a.width,a.length,0 as height,p.type from product p
                            left join rubber a on p.id = a.product_id
                            where ((p.name like '%' + @keyword + '%') or (p.new_name like '%' + @keyword + '%')) and p.type='Rubber'
                            union
                            select p.id,p.name,p.new_name,'' as series,'N' as sex_o,'' as positions,'N' as sealable_o,a.temper_operating,a.item_class,a.width,a.length,0 as height,'Rubber' as [type]
                            from rubber a inner join product p on a.product_id = p.id and p.type = 'Rubber'
                            where a.item_class like '%' + @keyword + '%'";
                        break;
                    case "S":
                        sql =
                            @"select p.id,p.name,p.new_name,'' as series,'N' as sex_o,'' as positions,'N' as sealable_o,a.temper_operating,a.item_class,a.width,a.length,0 as height,p.type from product p
                            left join sleeve a on p.id = a.product_id
                            where ((p.name like '%' + @keyword + '%') or (p.new_name like '%' + @keyword + '%')) and p.type='Sleeve'
                            union
                            select p.id,p.name,p.new_name,'' as series,'N' as sex_o,'' as positions,'N' as sealable_o,a.temper_operating,a.item_class,a.width,a.length,0 as height,'Sleeve' as [type]
                            from sleeve a inner join product p on a.product_id = p.id and p.type = 'Sleeve'
                            where a.item_class like '%' + @keyword + '%'";
                        break;
                    case "G":
                        sql =
                            @"select p.id,p.name,p.new_name,'' as series,'N' as sex_o,'' as positions,'N' as sealable_o,a.temper_operating,a.item_class,a.width,a.length,0 as height,p.type from product p
                            left join grommet a on p.id = a.product_id
                            where ((p.name like '%' + @keyword + '%') or (p.new_name like '%' + @keyword + '%')) and p.type='Grommet'
                            union
                            select p.id,p.name,p.new_name,'' as series,'N' as sex_o,'' as positions,'N' as sealable_o,a.temper_operating,a.item_class,a.width,a.length,0 as height,'Grommet' as [type]
                            from grommet a inner join product p on a.product_id = p.id and p.type = 'Grommet'
                            where a.item_class like '%' + @keyword + '%'";
                        break;
                }

                return conn.Query<SearchResultDetailVM>(sql, new { keyword = keyword }).ToList();
            }
        }

        private List<SearchResultDetailVM> getByPartNoResult(IDbConnection conn, string partNo, string tableKey)
        {
            if (string.IsNullOrEmpty(partNo))
                return new List<SearchResultDetailVM>();

            string sql2 =
                       @"select p.id,p.name,p.new_name,a.sex_o,a.positions,a.sealable_o,a.width,a.length,a.height,a.type from product p
                        left join 
                        (
	                        select h.product_id,h.sex as sex_o,h.series,h.positions,h.sealable as sealable_o,h.temper_operating,h.item_class,h.length,h.width,h.height,'Housing' as [type],h.reference_pn from housing h
	                        union
	                        select t.product_id,t.sex as sex_o,'' as series,'' as positions,t.sealable as sealable_o,'N/A' as temper_operating,t.item_class,t.length,t.width,t.height,'Terminal' as [type],t.reference_pn from terminal t
	                        union
	                        select r.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,r.temper_operating,r.item_class,r.length,r.width,0 as height,'Rubber' as [type],r.reference_pn from rubber r
	                        union
	                        select s.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,s.temper_operating,s.item_class,s.length,s.width,s.height,'Sleeve' as [type],s.reference_pn from sleeve s
	                        union
	                        select g.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,g.temper_operating,g.item_class,g.length,g.width,g.height as height,'Grommet' as [type],g.reference_pn from grommet g

                        ) a on p.id = a.product_id and p.type = a.type
                        where a.reference_pn Like '%' + @reference + '%'";

            return conn.Query<SearchResultDetailVM>(sql2, new { reference = partNo }).ToList();
        }

        public JsonResult SearchByLast(SearchByLastVM searchByLastVM)
        {
            var changeLang = langData["語系"].ToString();
            var searchResult = new List<SearchResultDetailVM>();

            string allSql = @"select p.id,p.name,p.new_name,a.series,a.sex_o,a.positions,a.sealable_o,a.temper_operating,a.width,a.length,a.height,a.type,a.item_class,a.material from product p
                        inner join 
                        (
	                        select h.product_id,h.sex as sex_o,h.series,h.positions,h.sealable as sealable_o,h.temper_operating,h.item_class,h.length,h.width,h.height,'Housing' as [type],h.reference_pn,h.material from housing h
	                        union
	                        select t.product_id,t.sex as sex_o,'' as series,'' as positions,t.sealable as sealable_o,'N/A' as temper_operating,t.item_class,t.length,t.width,t.height,'Terminal' as [type],t.reference_pn,t.material from terminal t
	                        union
	                        select r.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,r.temper_operating,r.item_class,r.length,r.width,0 as height,'Rubber' as [type],r.reference_pn,r.material from rubber r
	                        union
	                        select s.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,s.temper_operating,s.item_class,s.length,s.width,s.height,'Sleeve' as [type],s.reference_pn,s.material from sleeve s
	                        union
	                        select g.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,g.temper_operating,g.item_class,g.length,g.width,g.height as height,'Grommet' as [type],g.reference_pn,g.material from grommet g

                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string housingSql = @"select p.id,p.name,p.new_name,a.series,a.sex_o,a.positions,a.sealable_o,a.temper_operating,a.width,a.length,a.height,a.type,a.item_class,a.material,a.color from product p
                        inner join 
                        (
	                        select h.product_id,h.sex as sex_o,h.series,h.positions,h.sealable as sealable_o,h.temper_operating,h.item_class,h.length,h.width,h.height,h.material,h.color,'Housing' as [type],h.reference_pn from housing h

                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string terminalSql = @"select p.id,p.name,p.new_name,a.series,a.sex_o,a.positions,a.sealable_o,a.temper_operating,a.width,a.length,a.height,a.type,a.item_class,a.depth,a.diameter,a.material,a.plating,a.wire_size from product p
                        inner join 
                        (
	                        select t.product_id,t.sex as sex_o,'' as series,'' as positions,t.sealable as sealable_o,'N/A' as temper_operating,t.item_class,t.depth,t.diameter,t.material,t.plating,t.wire_size,t.length,t.width,t.height,'Terminal' as [type],t.reference_pn from terminal t

                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string rubberSql = @"select p.id,p.name,p.new_name,a.series,a.sex_o,a.positions,a.sealable_o,a.temper_operating,a.width,a.length,a.height,a.type,a.item_class,a.diameter,a.diameter_1,a.diameter_2,a.material,a.color from product p
                        inner join 
                        (
	                        select r.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,r.temper_operating,r.item_class,r.diameter,r.diameter_1,r.diameter_2,r.material,r.color,r.length,r.width,0 as height,'Rubber' as [type],r.reference_pn from rubber r
                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string sleeveSql = @"select p.id,p.name,p.new_name,a.series,a.sex_o,a.positions,a.sealable_o,a.temper_operating,a.width,a.length,a.height,a.type,a.material from product p
                        inner join 
                        (
	                        select s.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,s.temper_operating,s.item_class,s.length,s.width,s.height,'Sleeve' as [type],s.reference_pn,s.material from sleeve s
                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string grommetSql = @"select p.id,p.name,p.new_name,a.series,a.sex_o,a.positions,a.sealable_o,a.temper_operating,a.width,a.length,a.height,a.type,a.material from product p
                        inner join 
                        (
	                        select g.product_id,'N' as sex_o,'' as series,'' as positions,'N' as sealable_o,g.temper_operating,g.item_class,g.length,g.width,g.height as height,'Grommet' as [type],g.reference_pn,g.material from grommet g
                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string finalSql = string.Empty;

            switch (searchByLastVM.Table)
            {
                case ProductTable.All:
                    finalSql = allSql;
                    break;
                case ProductTable.Housing:
                    finalSql = housingSql;
                    break;
                case ProductTable.Terminal:
                    finalSql = terminalSql;
                    break;
                case ProductTable.Rubber:
                    finalSql = rubberSql;
                    break;
                case ProductTable.Sleeve:
                    finalSql = sleeveSql;
                    break;
                case ProductTable.Grommet:
                    finalSql = grommetSql;
                    break;
            }

            using (var conn = _connectionFactory.CreateProductDBConnection())
            {
                searchResult = conn.Query<SearchResultDetailVM>(finalSql, new { lastDate = DateTime.Now.AddYears(-3).ToString("yyyy年MM月dd日 HH:mm:ss") }).ToList();
            }

            var pageDatas = searchResult.Skip(searchByLastVM.PageIndex - 1).Take(10).ToList();

            string filecheck = "picture1";
            string productfilename;

            for (int i = 0; i < pageDatas.Count(); i++)
            {
                using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    conn2.Open();
                    string img_sql = "select * from product_file where product_id= " + pageDatas[i].id + " and filecheck= '" + filecheck + "'";
                    using (SqlCommand cmd = conn2.CreateCommand())
                    {
                        cmd.CommandText = img_sql;
                        cmd.CommandType = CommandType.Text;
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            productfilename = reader[2].ToString();
                            if (productfilename != "")
                            {
                                var temp = productfilename.Split('.');
                                var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + pageDatas[i].id + "/" + temp[0] + "_WM." + temp[1];
                                pageDatas[i].productImg = productImg;
                            }
                            else
                            {
                                pageDatas[i].productImg = "";
                            }
                        }
                    }
                }

                switch (searchByLastVM.Table)
                {
                    case ProductTable.All:
                        pageDatas[i].type = "All";
                        break;
                    case ProductTable.Housing:
                        pageDatas[i].type = "膠盒";
                        break;
                    case ProductTable.Terminal:
                        pageDatas[i].type = "端子";
                        break;
                    case ProductTable.Rubber:
                        pageDatas[i].type = "防水栓";
                        break;
                    case ProductTable.Sleeve:
                        pageDatas[i].type = "PVC絕緣護套";
                        break;
                    case ProductTable.Grommet:
                        pageDatas[i].type = "索環護套";
                        break;
                }

                if (i == 0)
                {
                    pageDatas[i].th = 1;
                }
                else
                {
                    pageDatas[i].th = 0;
                }

                if (pageDatas[i].color != null)
                {
                    //if (pageDatas[i].color == "N/A")
                    //{

                    //}
                    if (pageDatas[i].color == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].color.Split('(');
                            pageDatas[i].color = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].color.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].color = resultData;
                            }
                            else
                            {
                                pageDatas[i].color = temp[0];
                            }

                        }
                    }
                }

                if (pageDatas[i].sex_o != null)
                {
                    //if (pageDatas[i].sex_o == "N/A")
                    //{

                    //}
                    if (pageDatas[i].sex_o == "")
                    {

                    }
                    else if (pageDatas[i].sex_o == "N")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].sex_o.Split('(');
                            pageDatas[i].reSex = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].sex_o.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].reSex = resultData;
                            }
                            else
                            {
                                pageDatas[i].reSex = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].type == "端子" || pageDatas[i].type == "All")
                {
                    pageDatas[i].reSealable = changeLang == "tw" ? pageDatas[i].sealable : changeLang == "cn" ? pageDatas[i].sealable : pageDatas[i].sealable_o.Split('(').Length > 1 ? pageDatas[i].sealable_o.Split('(')[0] : pageDatas[i].sealable_o;
                }
                else
                {
                    pageDatas[i].reSealable = changeLang == "tw" ? pageDatas[i].sealable : changeLang == "cn" ? pageDatas[i].sealable : pageDatas[i].sealable_o;
                }


                if (pageDatas[i].material != null)
                {
                    //if (pageDatas[i].material == "N/A")
                    //{

                    //}
                    if (pageDatas[i].material == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].material.Split('(');
                            pageDatas[i].reMaterial = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].material.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].reMaterial = resultData;
                            }
                            else
                            {
                                pageDatas[i].reMaterial = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].plating != null)
                {
                    //if (pageDatas[i].plating == "N/A")
                    //{

                    //}
                    if (pageDatas[i].plating == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].plating.Split('(');
                            pageDatas[i].rePlating = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].plating.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].rePlating = resultData;
                            }
                            else
                            {
                                pageDatas[i].rePlating = temp[0];
                            }
                        }
                    }
                }

                if (pageDatas[i].item_class != null)
                {
                    //if (pageDatas[i].item_class == "N/A")
                    //{

                    //}
                    if (pageDatas[i].item_class == "")
                    {

                    }
                    else
                    {
                        if (changeLang == "en")
                        {
                            var temp = pageDatas[i].item_class.Split('(');
                            pageDatas[i].item_class = temp[0];
                        }
                        else
                        {
                            var temp = pageDatas[i].item_class.Split('(');
                            if (temp.Length > 1)
                            {
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];
                                pageDatas[i].item_class = resultData;
                            }
                            else
                            {
                                pageDatas[i].item_class = temp[0];
                            }
                        }
                    }
                }
            }

            return Json(new SearchResultVM() { TotalCount = searchResult.Count, PageDatas = pageDatas });

        }

        public JsonResult SearchByLastHome(SearchByLastVM searchByLastVM)
        {
            var changeLang = langData["語系"].ToString();
            var searchResult = new List<ProductVerboseVM>();

            string allSql = @"select p.id,p.name,p.new_name,a.item_class,a.width,a.length,a.height,a.type from product p
                        inner join 
                        (
	                        select h.product_id,h.item_class,h.length,h.width,h.height,'Housing' as [type],h.reference_pn from housing h
	                        union
	                        select t.product_id,t.item_class,t.length,t.width,t.height,'Terminal' as [type],t.reference_pn from terminal t
	                        union
	                        select r.product_id,r.item_class,r.length,r.width,0 as height,'Rubber' as [type],r.reference_pn from rubber r
	                        union
	                        select s.product_id,s.item_class,s.length,s.width,s.height,'Sleeve' as [type],s.reference_pn from sleeve s
	                        union
	                        select g.product_id,g.item_class,g.length,g.width,g.height as height,'Grommet' as [type],g.reference_pn from grommet g

                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string housingSql = @"select p.id,p.name,p.new_name,a.item_class,a.width,a.length,a.height,a.type from product p
                        inner join 
                        (
	                        select h.product_id,h.item_class,h.length,h.width,h.height,'Housing' as [type],h.reference_pn from housing h

                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string terminalSql = @"select p.id,p.name,p.new_name,a.item_class,a.width,a.length,a.height,a.type from product p
                        inner join 
                        (
	                        select t.product_id,t.item_class,t.length,t.width,t.height,'Terminal' as [type],t.reference_pn from terminal t

                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string rubberSql = @"select p.id,p.name,p.new_name,a.item_class,a.width,a.length,a.height,a.type from product p
                        inner join 
                        (
	                        select r.product_id,r.item_class,r.length,r.width,0 as height,'Rubber' as [type],r.reference_pn from rubber r
                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string sleeveSql = @"select p.id,p.name,p.new_name,a.item_class,a.width,a.length,a.height,a.type from product p
                        inner join 
                        (
	                        select s.product_id,s.item_class,s.length,s.width,s.height,'Sleeve' as [type],s.reference_pn from sleeve s
                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string grommetSql = @"select p.id,p.name,p.new_name,a.item_class,a.width,a.length,a.height,a.type from product p
                        inner join 
                        (
	                        select g.product_id,g.item_class,g.length,g.width,g.height as height,'Grommet' as [type],g.reference_pn from grommet g
                        ) a on p.id = a.product_id and p.type = a.type
                        where p.create_date >= @lastDate";
            string finalSql = string.Empty;

            switch (searchByLastVM.Table)
            {
                case ProductTable.All:
                    finalSql = allSql;
                    break;
                case ProductTable.Housing:
                    finalSql = housingSql;
                    break;
                case ProductTable.Terminal:
                    finalSql = terminalSql;
                    break;
                case ProductTable.Rubber:
                    finalSql = rubberSql;
                    break;
                case ProductTable.Sleeve:
                    finalSql = sleeveSql;
                    break;
                case ProductTable.Grommet:
                    finalSql = grommetSql;
                    break;
            }

            using (var conn = _connectionFactory.CreateProductDBConnection())
            {
                searchResult = conn.Query<ProductVerboseVM>(finalSql, new { lastDate = DateTime.Now.AddYears(-3).ToString("yyyy年MM月dd日 HH:mm:ss") }).ToList();
            }

            var pageDatas = searchResult.Skip(searchByLastVM.PageIndex - 1).Take(10).ToList();

            string filecheck = "picture1";
            string productfilename;

            for (int i = 0; i < pageDatas.Count(); i++)
            {
                using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                {
                    conn2.Open();
                    string img_sql = "select * from product_file where product_id= " + pageDatas[i].id + " and filecheck= '" + filecheck + "'";
                    using (SqlCommand cmd = conn2.CreateCommand())
                    {
                        cmd.CommandText = img_sql;
                        cmd.CommandType = CommandType.Text;
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            productfilename = reader[2].ToString();
                            if (productfilename != "")
                            {
                                var temp = productfilename.Split('.');
                                var productImg = "http://iis.24241872.tw/projects/public/e000482/test/Content/Upload/images/product/" + pageDatas[i].id + "/" + temp[0] + "_WM." + temp[1];
                                pageDatas[i].productImg = productImg;
                            }
                            else
                            {
                                pageDatas[i].productImg = "";
                            }
                        }
                    }
                }
            }

            return Json(pageDatas);

        }
        [HttpGet]
        public JsonResult getProductsCategories()
        {
            var changeLang = langData["語系"].ToString();
            var result = new
            {
                categoryList = new List<dynamic>()
            {
                 new { id= 1, name=changeLang == "tw" ? "膠盒" : changeLang == "cn" ? "胶盒" : "Housing"},
                 new  { id= 2, name= changeLang == "tw" ? "端子" : changeLang == "cn" ? "端子" : "Terminal" },
                 new   { id= 3, name= changeLang == "tw" ? "防水栓" : changeLang == "cn" ? "防水栓" : "Rubber Seal" },
                 new   { id= 4, name= changeLang == "tw" ? "PVC絕緣護套" : changeLang == "cn" ? "PVC绝缘护套" : "PVC Insulated Sleeve" },
                 new   { id= 5, name= changeLang == "tw" ? "索環護套" : changeLang == "cn" ? "索环护套" : "Grommet" } ,
            },
                newCategoryAll = changeLang == "tw" ? "新品專區ALL" : changeLang == "cn" ? "新品专区ALL" : "New Products Area(ALL)",
                moreName = changeLang == "tw" ? "深入了解" : changeLang == "cn" ? "深入了解" : "More",
                carConnector1 = changeLang == "tw" ? "發動機線束" : changeLang == "cn" ? "发动机线束" : "Engine Harness",
                carConnector2 = changeLang == "tw" ? "地板線束" : changeLang == "cn" ? "地板线束" : "Floor Harness",
                carConnector3 = changeLang == "tw" ? "車門線束" : changeLang == "cn" ? "车门线束" : "Car Door Harness",
                carConnector4 = changeLang == "tw" ? "尾門線束" : changeLang == "cn" ? "尾门线束" : "Trailer Harness",
                carConnector5 = changeLang == "tw" ? "座椅線束" : changeLang == "cn" ? "座椅线束" : "Seat Harness",
                carConnector6 = changeLang == "tw" ? "頂棚線束" : changeLang == "cn" ? "顶棚线束" : "Car Roof Harness",
                carConnector7 = changeLang == "tw" ? "前保險桿線束" : changeLang == "cn" ? "前保险杆线束" : "Front Bumper Harness",
                carConnector8 = changeLang == "tw" ? "後保險桿線束" : changeLang == "cn" ? "后保险杆线束" : "Rear bumper Harness",
                carConnector9 = changeLang == "tw" ? "動力線束" : changeLang == "cn" ? "动力线束" : "Power Harness",
                carConnector10 = changeLang == "tw" ? "倒車雷達線束" : changeLang == "cn" ? "倒车雷达线束" : "Parking Sensor Harness",
                carConnector11 = changeLang == "tw" ? "儀錶板線束" : changeLang == "cn" ? "仪表板线束" : "Instrment Panel(IP) Harness",
                carConnector12 = changeLang == "tw" ? "前艙線束" : changeLang == "cn" ? "前舱线束" : "Front Cabin Harness",
                carConnector13 = changeLang == "tw" ? "汽車連接器&端子應用" : changeLang == "cn" ? "汽車連接器&端子應用" : "",
                tab1 = changeLang == "tw" ? "依規格搜尋" : changeLang == "cn" ? "依规格搜寻" : "SPEC Search",
                tab2 = changeLang == "tw" ? "產品應用功能篩選" : changeLang == "cn" ? "产品应用功能筛选" : "Application Search",
                tab3 = changeLang == "tw" ? "產品關鍵字搜尋" : changeLang == "cn" ? "产品关键字搜寻" : "Key Words Search",
                tab4 = changeLang == "tw" ? "新產品訊息" : changeLang == "cn" ? "新产品讯息" : "Products Area",

                productSize = changeLang == "tw" ? "尺寸" : changeLang == "cn" ? "尺寸" : "Dimension",
                productSerial = changeLang == "tw" ? "家族名稱 / 系列" : changeLang == "cn" ? "家族名称 / 系列" : "Family / Series",
                productNewName = changeLang == "tw" ? "產品品號" : changeLang == "cn" ? "产品品号" : "Part No.",
                productBoxData1 = changeLang == "tw" ? "產品" : changeLang == "cn" ? "产品" : "Category",
                productBoxData2 = changeLang == "tw" ? "產品類別" : changeLang == "cn" ? "产品类别" : "Products Type",
                productBoxData3 = changeLang == "tw" ? "產品ID品號" : changeLang == "cn" ? "产品ID品号" : "ID Part No.",
                productBoxData4 = changeLang == "tw" ? "產品品號" : changeLang == "cn" ? "产品品号" : "Part No.",
                productBoxData5 = changeLang == "tw" ? "尺寸(mm)" : changeLang == "cn" ? "尺寸(mm)" : "Dimension(mm)",
                productBoxData6 = changeLang == "tw" ? "系列碼" : changeLang == "cn" ? "系列码" : "Series",
                productBoxData7 = changeLang == "tw" ? "參考規格圖" : changeLang == "cn" ? "参考规格图" : "Reference Drawing",
                productBoxData8 = changeLang == "tw" ? "產品圖紙" : changeLang == "cn" ? "产品图纸" : "Product Drawing",
                productBoxData9 = changeLang == "tw" ? "產品規格書" : changeLang == "cn" ? "产品规格书" : "Product Drawing",
                productBoxData10 = changeLang == "tw" ? "放大鏡" : changeLang == "cn" ? "放大镜" : "Magnifier",
                productBoxData11 = changeLang == "tw" ? "規格比較" : changeLang == "cn" ? "规格比较" : "Compare SPEC",
                productBoxData12 = changeLang == "tw" ? "產品詢問" : changeLang == "cn" ? "产品询问" : "Inquire Product",
            };


            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetProductTables()
        {

            var changeLang = langData["語系"].ToString();

            var result = new
            {
                categoryItems = new List<dynamic>() {
                    new { key="A" , value= changeLang == "tw" ? "所有(All)" : changeLang == "cn" ? "所有(All)" : "All"},
                    new { key="H", value= changeLang == "tw" ? "膠盒 H" : changeLang == "cn" ? "胶盒 H" : "Housing"},
                    new { key="T", value= changeLang == "tw" ? "端子 T" : changeLang == "cn" ? "端子 T" : "Terminal"},
                    new { key="R", value= changeLang == "tw" ? "防水栓 R" : changeLang == "cn" ? "防水栓 R" : "Rubber"},
                    new { key="S", value= changeLang == "tw" ? "PVC絕緣護套 S" : changeLang == "cn" ? "PVC绝缘护套 S" : "Sleeve"},
                    new { key="G", value= changeLang == "tw" ? "索環護套 G" : changeLang == "cn" ? "索环护套 G" : "Grommet"}
                },
                productCategory = changeLang == "tw" ? "產品類別" : changeLang == "cn" ? "产品类别" : "Products Category",
                //productCategory = changeLang == "tw" ? "1" : changeLang == "cn" ? "2" : "3",
                sizeType = changeLang == "tw" ? "尺寸類型(H/T/R/S/G)" : changeLang == "cn" ? "尺寸类型(H/T/R/S/G)" : "Dimension Type (H/T/R/S/G)",
                functionCategory = changeLang == "tw" ? "功能類別(H/T/R/S/G)" : changeLang == "cn" ? "功能类别(H/T/R/S/G)" : "Function Type (H/T/R/S/G)",
                seriesCode = changeLang == "tw" ? "系列碼(H)" : changeLang == "cn" ? "系列码(H)" : "Series (H)",
                maleFemale = changeLang == "tw" ? "公/母(H/T)" : changeLang == "cn" ? "公/母(H/T)" : "Male/Female(H/T)",
                sealedUnsealed = changeLang == "tw" ? "防水/非防水 (H/T)" : changeLang == "cn" ? "防水/非防水 (H/T)" : "Sealed/Unsealed(H/T)",
                numofPositions = changeLang == "tw" ? "孔數(H)" : changeLang == "cn" ? "孔数(H)" : "Number of Positions(H)",
                temperatureR = changeLang == "tw" ? "適用溫度 (ﾟC)(H)" : changeLang == "cn" ? "适用温度 (ﾟC)(H)" : "Temperature Range (ﾟC)(H)",
                thickness = changeLang == "tw" ? "料厚 (T)" : changeLang == "cn" ? "料厚 (T)" : "Thickness (mm)(T)",
                appWireSize = changeLang == "tw" ? "適用線徑 (mm²) (T/R)" : changeLang == "cn" ? "适用线径 (mm²) (T/R)" : "Wire Size (mm²)(T/R)",
                tabThickness = changeLang == "tw" ? "TAB 厚度(mm)(T)" : changeLang == "cn" ? "TAB 厚度(mm)(T)" : "TAB Thickness(mm)(T)",
                searchBtn = changeLang == "tw" ? "瀏覽搜尋結果" : changeLang == "cn" ? "浏览搜索结果" : "Search",
                totalRecord = changeLang == "tw" ? "共篩選到" : changeLang == "cn" ? "共筛选到" : "Results",
                conformHits = changeLang == "tw" ? "個符合條件的產品" : changeLang == "cn" ? "个符合条件的产品" : "hits",
                sizeAll = changeLang == "tw" ? "尺寸(W/L/H/D/D1/D2)" : changeLang == "cn" ? "尺寸(W/L/H/D/D1/D2)" : "Dimension (mm)",
                keywordNo = changeLang == "tw" ? "關鍵字/產品ID料號" : changeLang == "cn" ? "关键字/产品ID料号" : "Search by Part no. or Key words",
                keywordText = changeLang == "tw" ? "關鍵字" : changeLang == "cn" ? "关键字" : "Key Words",
                hosingText = changeLang == "tw" ? "膠盒" : changeLang == "cn" ? "胶盒" : "Housing",
                terminalText = changeLang == "tw" ? "端子" : changeLang == "cn" ? "端子" : "Terminal",
                rubberText = changeLang == "tw" ? "防水栓" : changeLang == "cn" ? "防水栓" : "Rubber Seal",
                pvcText = changeLang == "tw" ? "PVC 絕緣護套" : changeLang == "cn" ? "PVC 绝缘护套" : "PVC Sleeve",
                grommetText = changeLang == "tw" ? "索環護套" : changeLang == "cn" ? "索环护套" : "Grommet",

            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetProductTable(string key)
        {
            var changeLang = langData["語系"].ToString();
            var result = new SearchCategoryTableVM();
            using (var conn = _connectionFactory.CreateProductDBConnection())
            {
                switch (key)
                {
                    case "A":
                        result.sizeType = SearchCategoryVM.CreateSizeType(conn, key, changeLang);
                        result.productClass = SearchCategoryVM.CreateProductClass(conn, key, changeLang);
                        result.serial = SearchCategoryVM.CreateSerial(conn, key, changeLang);
                        result.sex = SearchCategoryVM.CreateSex(key, changeLang);
                        result.sealable = SearchCategoryVM.CreateSealable(key, changeLang);
                        result.position = SearchCategoryVM.CreatePosition();
                        result.temper = SearchCategoryVM.CreateTemper(conn, key, changeLang);
                        result.depth = SearchCategoryVM.CreateDepth();
                        result.diameter = SearchCategoryVM.CreateDiameter();
                        result.size = SearchCategoryVM.CreateSize(key, changeLang);
                        result.tab = SearchCategoryVM.CreateTab();
                        break;
                    case "H":
                        result.sizeType = SearchCategoryVM.CreateSizeType(conn, key, changeLang);
                        result.productClass = SearchCategoryVM.CreateProductClass(conn, key, changeLang);
                        result.serial = SearchCategoryVM.CreateSerial(conn, key, changeLang);
                        result.sex = SearchCategoryVM.CreateSex(key, changeLang);
                        result.sealable = SearchCategoryVM.CreateSealable(key, changeLang);
                        result.position = SearchCategoryVM.CreatePosition();
                        result.temper = SearchCategoryVM.CreateTemper(conn, key, changeLang);
                        result.size = SearchCategoryVM.CreateSize(key, changeLang);
                        break;
                    case "T":
                        result.sizeType = SearchCategoryVM.CreateSizeType(conn, key, changeLang);
                        result.productClass = SearchCategoryVM.CreateProductClass(conn, key, changeLang);
                        result.sex = SearchCategoryVM.CreateSex(key, changeLang);
                        result.sealable = SearchCategoryVM.CreateSealable(key, changeLang);
                        result.depth = SearchCategoryVM.CreateDepth();
                        result.diameter = SearchCategoryVM.CreateDiameter();
                        result.size = SearchCategoryVM.CreateSize(key, changeLang);
                        result.tab = SearchCategoryVM.CreateTab();
                        break;
                    case "R":
                        result.sizeType = SearchCategoryVM.CreateSizeType(conn, key, changeLang);
                        result.productClass = SearchCategoryVM.CreateProductClass(conn, key, changeLang);
                        result.diameter = SearchCategoryVM.CreateDiameter();
                        result.size = SearchCategoryVM.CreateSize(key, changeLang);
                        break;
                    case "S":
                        result.sizeType = SearchCategoryVM.CreateSizeType(conn, key, changeLang);
                        result.productClass = SearchCategoryVM.CreateProductClass(conn, key, changeLang);
                        result.size = SearchCategoryVM.CreateSize(key, changeLang);
                        break;
                    case "G":
                        result.sizeType = SearchCategoryVM.CreateSizeType(conn, key, changeLang);
                        result.productClass = SearchCategoryVM.CreateProductClass(conn, key, changeLang);
                        result.size = SearchCategoryVM.CreateSize(key, changeLang);
                        break;
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetL2CarSysIncludeL3()
        {
            var changeLang = langData["語系"].ToString();
            List<CarSysVM> result = new List<CarSysVM>();

            var carSysDictionary = new Dictionary<int, CarSysVM>();

            using (var conn = _connectionFactory.CreateProductDBConnection())
            {
                result =
                    conn.Query<CarSysVM, CarSysVM, CarSysVM>(
                        @"select l3.l2,l2.id,l2.caption,l3.id,l3.caption from [dbo].[carsys_l2] l2
                    Inner Join[dbo].[carsys_l3] l3 on l3.l2 = l2.id
                    order by l2.id",
                        (carSysVM, childCarSysVM) =>
                        {
                            CarSysVM carSysVMEntry;

                            if (!carSysDictionary.TryGetValue(carSysVM.Id, out carSysVMEntry))
                            {
                                carSysVMEntry = carSysVM;
                                carSysVMEntry.Childs = new List<CarSysVM>();
                                carSysDictionary.Add(carSysVMEntry.Id, carSysVMEntry);
                            }

                            carSysVMEntry.Childs.Add(childCarSysVM);
                            return carSysVMEntry;


                        }, splitOn: "id").Distinct().ToList();
            }

            foreach (var item in result)
            {
                var carSysDictionaryl4 = new List<CarSysVM>();

                if (changeLang == "en")
                {
                    var temp = item.Caption.Split('(');
                    if (temp.Length > 1)
                    {
                        var temp2 = temp[1].Split(')');
                        item.Caption = temp2[0];
                    }
                    else
                    {
                        item.Caption = temp[0];
                    }

                    foreach (var l3 in item.Childs)
                    {
                        using (var conn = _connectionFactory.CreateProductDBConnection())
                        {
                            SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                            conn2.Open();
                            string l4name;
                            string l4id;
                            string l3id;
                            string img_sql = "select * from carsys_l4 where l3= " + l3.Id;

                            SqlCommand cmd = new SqlCommand(img_sql, conn2);
                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                CarSysVM carSysVMEntryl4 = new CarSysVM();
                                l4id = reader[0].ToString();
                                l3id = reader[1].ToString();
                                l4name = reader[2].ToString();

                                carSysVMEntryl4.Id = Int32.Parse(l4id);
                                carSysVMEntryl4.l3 = Int32.Parse(l3id);
                                carSysVMEntryl4.Caption = l4name;

                                carSysDictionaryl4.Add(carSysVMEntryl4);
                            }
                            conn2.Close();
                        }
                    }

                    foreach (var resultChildName in carSysDictionaryl4)
                    {
                        var tempChildName = resultChildName.Caption.Split('(');
                        if (tempChildName.Length > 1)
                        {
                            var tempChildName2 = tempChildName[1].Split(')');
                            resultChildName.Caption = tempChildName2[0];
                        }
                        else
                        {
                            resultChildName.Caption = tempChildName[0];
                        }

                    }
                }
                else
                {
                    item.Caption = item.Caption.Split('(')[0];
                    foreach (var l3 in item.Childs)
                    {
                        using (var conn = _connectionFactory.CreateProductDBConnection())
                        {
                            SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                            conn2.Open();
                            string l4name;
                            string l4id;
                            string l3id;
                            string img_sql = "select * from carsys_l4 where l3= " + l3.Id;

                            SqlCommand cmd = new SqlCommand(img_sql, conn2);
                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                CarSysVM carSysVMEntryl4 = new CarSysVM();
                                l4id = reader[0].ToString();
                                l3id = reader[1].ToString();
                                l4name = reader[2].ToString();

                                carSysVMEntryl4.Id = Int32.Parse(l4id);
                                carSysVMEntryl4.l3 = Int32.Parse(l3id);
                                carSysVMEntryl4.Caption = l4name;

                                carSysDictionaryl4.Add(carSysVMEntryl4);
                            }
                            conn2.Close();
                        }
                    }

                    foreach (var resultChildName in carSysDictionaryl4)
                    {
                        resultChildName.Caption = resultChildName.Caption.Split('(')[0];
                    }
                }
                item.Childs = carSysDictionaryl4;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}