﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.DB.Models;
using Newtonsoft.Json;
using Web.Service;
using System.Collections.Specialized;
using System.Collections;
using System.Web.UI;
using Web.Repository;
using System.Configuration;
using Google.Apis.Auth.OAuth2;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Web.Script.Serialization;
using Web.ServiceModels;
using System.Web.Caching;
using Web.AdminService;
using System.Diagnostics;
using System.Data.SqlClient;

namespace Web.Controllers
{
    public class SiteadminController : Controller
    {
        public string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;

        //預設語系
        public string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString;

        //後台登入記錄方式 Session or Cookie
        public string projeftNo = ConfigurationManager.ConnectionStrings["projeftNo"].ConnectionString;

        public int cacheTime = 15;
        public string loginLastToUri = "";

        //案件編號
        protected System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;

        private Model DB = new Model();
        
        public static string FormatVerifyInfo(string tables, string verifyInfo, string verifyInfoJson, string actType)
        {
            List<string> verifyUseTab = verifyTables();//取得有審核的資料表

            List<verifyInfoModel> verifyInfoModel = new List<verifyInfoModel>();

            if (verifyUseTab.IndexOf(tables) != -1)
            {
                if (!string.IsNullOrEmpty(verifyInfoJson))
                {
                    verifyInfoModel = JsonConvert.DeserializeObject<List<verifyInfoModel>>(verifyInfoJson);
                }
                if (!string.IsNullOrEmpty(verifyInfo))
                {
                    verifyInfoModel verifyInfoData = new verifyInfoModel();
                    verifyInfoData.date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    verifyInfoData.info = verifyInfo;
                    verifyInfoModel.Add(verifyInfoData);
                }
                return JsonConvert.SerializeObject(verifyInfoModel);
            }
            else
            {
                return verifyInfo;
            }
        }

        /// <summary>
        /// 取得是否有審核功能
        /// </summary>
        /// <returns></returns>
        public static List<string> verifyTables()
        {
            List<string> tables = new List<string>();

            tables.Add("news");
            tables.Add("about_houses");
            tables.Add("cases");
            tables.Add("finance_report");
            tables.Add("urban_column");

            return tables;
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult login()
        {


            //判斷IP
            int firewallsCount = DB.firewalls.Where(m => m.status != "D").Count();


            //有IP判斷是否可通行


            if(firewallsCount > 0)
            {
                string nowIP = FunctionService.GetIP();
        
                firewalls firewalls = DB.firewalls.Where(m => m.ip == nowIP).FirstOrDefault();

                if (firewalls == null)
                {
                    return RedirectPermanent(Url.Content("~/"));
                }
                else
                {
                    if (firewalls.status == "N")
                    {
                        return RedirectPermanent(Url.Content("~/"));
                    }
                }
            }
               
          
            ViewBag.username = "";
            if (Request.Cookies["keepMeAccount"] != null)
            {
                HttpCookie aCookie = Request.Cookies["keepMeAccount"];

                string keepAdminID = Server.HtmlEncode(aCookie.Value.Replace("keepMeAccount=", ""));

                if (keepAdminID != null && keepAdminID != "")
                {
                    ViewBag.account = keepAdminID;
                }
            }

            bool isLogin = FunctionService.systemUserCheck();
            if (isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin"));
            }


            return View();
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        // GET: Siteadmin
        public ActionResult Index()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }

            ViewBag.IsGA = "Y";

            GoogleAnalyticsService GaData = new GoogleAnalyticsService();
            GaParameterModel GaParameter = new GaParameterModel();//宣告參數使用

            try
            {
                ////即時人數
                Google.Apis.Analytics.v3.Data.RealtimeData RealtimeData = GaData.nowUser();
                ViewBag.RealtimeData = RealtimeData.TotalsForAllResults["rt:pageviews"].ToString();

                GaParameter.endDate = DateTime.Now.ToString("yyyy") + "-12-31";//預設結束日
            }
            catch (Exception ex)
            {
                ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }
            ////總瀏覽數

            #region 總瀏覽數

            try
            {
                GaParameter.startDate = "2015-01-01";
                GaParameter.metrics = "ga:visits";
                GaParameter.Dimensions = "ga:year";
                var TotalWebView = GaData.ReGaContent(GaParameter);
                int reNum = 0;
                if (TotalWebView.Rows.Count > 0)
                {
                    foreach (List<string> item in TotalWebView.Rows)
                    {
                        reNum = reNum + int.Parse(item[1].ToString());
                    }
                }
                ViewBag.TotalWebViewNum = reNum;
            }
            catch (Exception ex)
            {
                ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }

            #endregion

            GaParameter.startDate = DateTime.Now.ToString("yyyy") + "-01-01";//預設起始日

            ////網站關鍵字

            #region 網站關鍵字

            try
            {
                GaParameter.metrics = "ga:organicSearches";
                GaParameter.Dimensions = "ga:keyword";
                var KeyWords = GaData.ReGaContent(GaParameter);
                ViewBag.KeyWords = KeyWords.Rows.ToList().Skip(0).Take(4);

                #endregion

                ////搜尋引勤來源數量

                #region 搜尋引勤來源數量

                GaParameter.metrics = "ga:organicSearches";
                GaParameter.Dimensions = "ga:fullReferrer";
                var FullReferrerTemp = GaData.ReGaContent(GaParameter);

                List<FullReferrerModel> FullReferrer = new List<FullReferrerModel>();
                int s = 0;
                if (FullReferrerTemp.Rows != null)
                {
                    foreach (List<string> item in FullReferrerTemp.Rows)
                    {
                        FullReferrerModel m = new FullReferrerModel();
                        m.name = item[0];
                        m.num = int.Parse(item[1].ToString());
                        FullReferrer.Add(m);
                        s++;
                    }
                    ViewBag.FullReferrer = FullReferrer;
                }
                else
                {
                    ViewBag.IsGA = "N";
                }
            }
            catch (Exception ex)
            {
                ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }

            #endregion

            ////本月流量次數
            try
            {
                string thisMonth = int.Parse(DateTime.Now.ToString("MM")).ToString();
                ViewBag.ViewYearUseMonth = JsonConvert.DeserializeObject(GaData.ViewYearUseMonth(thisMonth));
            }
            catch (Exception ex)
            {
                ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }

            // 新/舊訪問者

            #region 新舊訪問者

            try
            {
                GaParameter.metrics = "ga:users";
                GaParameter.Dimensions = "ga:userType";
                ViewBag.UserType = GaData.ReGaContent(GaParameter);
            }
            catch (Exception ex)
            {
                ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }

            #endregion

            //社交網站來源

            #region 社交網站來源

            try
            {
                GaParameter.metrics = "ga:organicSearches";
                GaParameter.Dimensions = "ga:socialNetwork";
                ViewBag.SocialNetwork = GaData.ReGaContent(GaParameter);

                #endregion

                //瀏覽頁面與頁面標題

                #region 瀏覽頁面與頁面標題

                GaParameter.metrics = "ga:pageviews";
                GaParameter.Dimensions = "ga:pagePath,ga:pageTitle";
                var test = GaData.ReGaContent(GaParameter);
            }
            catch (Exception ex)
            {
                // ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }

            #endregion

            //聯絡我們

           // ViewBag.contactData = DB.contacts.OrderByDescending(m => m.create_date).ToList().Skip(0).Take(4);

            //網址
            ViewBag.WebUrl = FunctionService.getWebUrl();

            //最新公告
           // ViewBag.News = DB.news.Where(m => m.lang == defLang).OrderByDescending(m => m.startdate).ToList().Skip(0).Take(3);

            //聯絡我們
          //  ViewBag.contactData = DB.contacts.OrderByDescending(m => m.create_date).ToList().Skip(0).Take(4);

            return View();
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        public ActionResult list()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }
            Session.Remove("dataTableCategory");//移除暫存細項

            ViewBag.RetuenToList = "N";
            ViewBag.statusData = null;//狀態篩選顯示
            ViewBag.statusVal = null;//狀態篩選值

            ViewBag.RetuenID = "";
            ViewBag.dataTableAddPostVal = "{}";

            if (RouteData.Values["tables"] != null)
            {
                string tables = RouteData.Values["tables"].ToString();

                ViewBag.Tables = tables;

                ViewBag.addTitle = "";
                ViewBag.addId = "";
                Session.Remove("dataTableCategory");

                //DataTable用額外參數

                if (RouteData.Values["id"] != null)
                {
                    ViewBag.RetuenToList = "Y";

                    if (tables == "finance_report" || tables == "share_download")
                    {
                        ViewBag.dataTableAddPostVal = "types=" + RouteData.Values["id"].ToString();
                        ViewBag.RetuenToList = "N";
                    }
                    if (tables == "urban_column_files")
                    {
                        ViewBag.dataTableAddPostVal = "category=" + RouteData.Values["id"].ToString();
                        // ViewBag.RetuenToList = "N";
                    }
                    if (tables == "downloads")
                    {
                        ViewBag.dataTableAddPostVal = "category=" + RouteData.Values["id"].ToString();
                        // ViewBag.RetuenToList = "N";
                    }
                    if (tables == "form_subject")
                    {
                        ViewBag.dataTableAddPostVal = "category=" + RouteData.Values["id"].ToString();
                        ViewBag.RetuenToList = "N";
                    }
                    if (tables == "downloads")
                    {                      
                        ViewBag.RetuenToList = "N";
                    }

                    if (tables == "advertise")
                    {                      
                        ViewBag.RetuenToList = "N";
                    }
                    Session.Add("dataTableCategory", RouteData.Values["id"].ToString());//紀錄傳值

                    //取得標題

                    string reTables = ViewBag.Tables;

                    ViewBag.addTitle = TablesService.getDataTitle(reTables, RouteData.Values["id"].ToString());
                    ViewBag.addId = "?c=" + RouteData.Values["id"].ToString();

                    ViewBag.RetuenID = RouteData.Values["id"].ToString();
                }
                /*if(ViewBag.forum_status != null)
                {
                    ViewBag.dataTableAddPostVal += ",{'name': 'forum_status', 'value': '" + ViewBag.forum_status.ToString() + "'}";
                }*/

                ViewBag.listMessage = TablesService.listMessage(tables);//取得列表說明

                //取得表頭
                ViewBag.dataTableTitle = TablesService.dataTableTitle(tables , ViewBag.RetuenID);
                ViewBag.columns = "";
                ViewBag.columnsLength = 0;//移除動作排序

                Dictionary<String, Object> defaultOrderBy = TablesService.defaultOrderBy(tables);//取得預設排續欄位名稱
                if (!defaultOrderBy.ContainsKey("orderByKey"))
                {
                    defaultOrderBy.Add("orderByKey", "create_date");
                    defaultOrderBy.Add("orderByType", "desc");
                }

                ViewBag.defaultOrderBy = 1;//預設排續欄位
                ViewBag.defaultOrderType = defaultOrderBy["orderByType"].ToString();//預設排續(asc或desc)

                foreach (KeyValuePair<string, object> item in ViewBag.dataTableTitle)
                {
                    ViewBag.columns += "{\"data\": \"" + item.Key + "\" },";
                    if (item.Key == defaultOrderBy["orderByKey"].ToString())
                    {
                        ViewBag.defaultOrderBy = ViewBag.columnsLength;
                    }
                    ViewBag.columnsLength++;
                }

                ViewBag.columnsLength = ViewBag.columnsLength - 1;//移除動作排序

                #region 取得檢視篩選

                //if (!string.IsNullOrEmpty(requests["search[value]"]))
                //{
                //    sSearch = Newtonsoft.Json.Linq.JArray.Parse(requests["search[value]"].ToString());
                //    sSearch = sSearch[0];
                //}

                Dictionary<String, Object> tempData = new Dictionary<string, object>();
                tempData.Add("nums", 1);

                Dictionary<String, Object> colData = TablesService.getColData(RouteData.Values["tables"].ToString(), tempData);

                if (colData.ContainsKey("other") && ((Dictionary<String, Object>)colData["other"]).Count > 0)
                {
                    Dictionary<String, Object> otherCol = (Dictionary<String, Object>)colData["other"];

                    if (otherCol["status"] != null && otherCol["status"].ToString() != "")
                    {
                        var mJObj = Newtonsoft.Json.Linq.JArray.Parse(otherCol["status"].ToString());
                        string[] statusData = mJObj[0]["data"].ToString().Split('/');//狀態篩選顯示
                        string[] statusVal = mJObj[0]["Val"].ToString().Split('/');//狀態篩選值

                        ViewBag.statusData = statusData;//狀態篩選顯示
                        ViewBag.statusVal = statusVal;//狀態篩選值
                    }
                }

                #endregion
             
            }
           
            return View();
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        /// <summary>
        /// 新增/修改
        /// </summary>
        /// <returns></returns>
        public ActionResult edit()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            //權限
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }
            ViewBag.mainCol = null;
            ViewBag.mediaCol = null;
            ViewBag.otherCol = null;
            ViewBag.guid = "";
            ViewBag.data = null;
            ViewBag.addTitle = "";

            ViewBag.addId = "";
            //dataTableCategory
            if (Session["dataTableCategory"] != null)
            {
                ViewBag.addId = "/" + Session["dataTableCategory"].ToString();
            }

            if (RouteData.Values["tables"] != null)
            {
                ViewBag.useLang = TablesService.useLang(RouteData.Values["tables"].ToString());//是否使用語系

                ViewBag.RetuenID = "";
                //修改資料
                if (RouteData.Values["id"] != null)
                {
                    ViewBag.guid = RouteData.Values["id"].ToString();
                    string Data = TablesService.getPresetData(RouteData.Values["tables"].ToString(), ViewBag.guid);

                    var obj = Newtonsoft.Json.Linq.JArray.Parse(Data.ToString());

                    if (ViewBag.useLang == "Y")
                    {
                        Dictionary<String, Object> langValData = new Dictionary<string, object>();
                        foreach (var item in obj)
                        {
                            langValData.Add(item["lang"].ToString(), item);
                        }

                        ViewBag.data = langValData;
                    }
                    else
                    {
                        ViewBag.data = obj[0];
                    }

                    ViewBag.RetuenID = RouteData.Values["id"].ToString();

                    //特別指定標題
                    ViewBag.addTitle = TablesService.getDataTitle(RouteData.Values["tables"].ToString(), RouteData.Values["id"].ToString());
                }

                //子層
                if (Request["c"] != null)
                {
                    ViewBag.addTitle = TablesService.getDataTitle(RouteData.Values["tables"].ToString(), Request["c"].ToString());
                }

                ViewBag.Tables = RouteData.Values["tables"].ToString();

                //針對共用Table處理
                var colUseData = ViewBag.data;

                if (ViewBag.Tables == "finance_report" || ViewBag.Tables == "share_download" || ViewBag.Tables == "downloads" || ViewBag.Tables == "advertise")
                {
                    colUseData = Request["c"].ToString();
                }

                Dictionary<String, Object> colData = TablesService.getColData(RouteData.Values["tables"].ToString(), colUseData);

                #region 基本欄位設定

                if (((Dictionary<String, Object>)colData["main"]).Count > 0)
                {
                    ViewBag.mainCol = colData["main"];
                }
                if (((Dictionary<String, Object>)colData["media"]).Count > 0)
                {
                    ViewBag.mediaCol = colData["media"];
                }
                if (((Dictionary<String, Object>)colData["other"]).Count > 0)
                {
                    ViewBag.otherCol = colData["other"];
                }

                #endregion

                #region 延伸欄位設定

                if (colData.ContainsKey("user") && ((Dictionary<String, Object>)colData["user"]).Count > 0)
                {
                    ViewBag.userCol = colData["user"];
                }

                if (colData.ContainsKey("review") && ((Dictionary<String, Object>)colData["review"]).Count > 0)
                {
                    ViewBag.reviewCol = colData["review"];
                }
                if (colData.ContainsKey("verify") && ((Dictionary<String, Object>)colData["verify"]).Count > 0)
                {
                    ViewBag.verifyCol = colData["verify"];
                }
                if (colData.ContainsKey("verifyForum") && ((Dictionary<String, Object>)colData["verifyForum"]).Count > 0)
                {
                    ViewBag.verifyColForum = colData["verifyForum"];
                }

                if (colData.ContainsKey("reInfo") && ((Dictionary<String, Object>)colData["reInfo"]).Count > 0)
                {
                    ViewBag.reInfoCol = colData["reInfo"];
                }
                if (colData.ContainsKey("reUser") && ((Dictionary<String, Object>)colData["reUser"]).Count > 0)
                {
                    ViewBag.reUserCol = colData["reUser"];
                }
                if (colData.ContainsKey("modifyer") && ((Dictionary<String, Object>)colData["modifyer"]).Count > 0)
                {
                    ViewBag.modifyerCol = colData["modifyer"];
                }

                if (colData.ContainsKey("delay") && ((Dictionary<String, Object>)colData["delay"]).Count > 0)
                {
                    ViewBag.delayCol = colData["delay"];
                }

                if (colData.ContainsKey("delay2") && ((Dictionary<String, Object>)colData["delay2"]).Count > 0)
                {
                    ViewBag.delay2Col = colData["delay2"];
                }

                if (colData.ContainsKey("viewSet") && ((Dictionary<String, Object>)colData["viewSet"]).Count > 0)
                {
                    ViewBag.viewSetCol = colData["viewSet"];
                }

                if (colData.ContainsKey("hidden") && ((Dictionary<String, Object>)colData["hidden"]).Count > 0)
                {
                    ViewBag.hiddenCol = colData["hidden"];
                }
                if (colData.ContainsKey("indexUse") && ((Dictionary<String, Object>)colData["indexUse"]).Count > 0)
                {
                    ViewBag.reIndexUse = colData["indexUse"];
                }

                #endregion
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult save(FormCollection form)
        {
            if (RouteData.Values["tables"] != null)
            {
                string useLang = TablesService.useLang(RouteData.Values["tables"].ToString());//是否使用語系
                                                                                              // string colFrom = TablesService.getColData(RouteData.Values["tables"].ToString() , null);//欄位

                //呼叫寫入或修改
                string guid = FunctionService.getGuid();
                string actType = "add";
                if (form["guid"].ToString() != "")
                {
                    guid = form["guid"].ToString();
                    actType = "edit";
                }

                Dictionary<String, Object> dic = new Dictionary<string, object>();
                if (useLang == "Y")
                {
                    if (ViewBag.language != null)
                    {
                        int i = 0;
                        foreach (language item in ViewBag.language)
                        {
                            Dictionary<String, Object> subForm = new Dictionary<string, object>();
                            subForm.Add("lang", item.lang);
                            //subForm.Add("status", form["status"].ToString());

                            foreach (var key in form.AllKeys)
                            {
                                if (key.IndexOf("verify_info") == -1)
                                {
                                    if (RouteData.Values["tables"].ToString() == "locations")
                                    {
                                        if (key != "status")
                                        {
                                            //if (key == "status_" + item.lang)
                                            //{
                                            //    subForm.Add(key.Replace("_" + item.lang, ""), form["status"].ToString());
                                            //    //subForm.Add(key, form["status"].ToString());
                                            //}
                                            //else
                                            //{
                                            //    subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                            //}
                                            subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                        }
                                        //else if (key == "status_" + item.lang)
                                        //{
                                        //    subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                        //}
                                    }
                                    else
                                    {
                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                    }
                                }
                                else
                                {
                                    //審核訊息
                                    subForm.Add(
                                        key.Replace("_" + item.lang, ""),
                                        FormatVerifyInfo(RouteData.Values["tables"].ToString(),
                                        form[key].ToString(),
                                        form["verify_json"].ToString(),
                                        actType));
                                }
                            }

                            if (actType == "add")
                            {
                                subForm.Remove("id");
                            }

                            dic.Add(item.lang, subForm);
                        }
                    }
                }
                else
                {
                    foreach (var key in form.AllKeys)
                    {
                        //

                        if (key.IndexOf("verify_info") == -1)
                        {
                            dic.Add(key, form[key].ToString());
                        }
                        else
                        {
                            //dic.Add(key, form[key].ToString());
                            //審核訊息
                            dic.Add(
                                key,
                                FormatVerifyInfo(RouteData.Values["tables"].ToString(),
                                form[key].ToString(),
                                form["verify_json"].ToString(),
                                actType));
                        }
                    }

                    if (actType == "add")
                    {
                        dic.Remove("id");
                    }
                }

                string Field = "";
                if (RouteData.Values["tables"].ToString() == "forum_message")
                {
                    Field = "forum_message";
                }

                if (RouteData.Values["tables"].ToString() == "product_info")
                {
                    string json = JsonConvert.SerializeObject((Dictionary<string, object>)dic);
                    TablesService.saveData(RouteData.Values["tables"].ToString(), json, Field, guid, actType, (Dictionary<string, object>)dic);
                }
                else
                {
                    if (useLang == "Y")
                    {
                        if (ViewBag.language != null)
                        {
                            int i = 0;
                            foreach (language item in ViewBag.language)
                            {
                                string json = JsonConvert.SerializeObject((Dictionary<string, object>)dic[item.lang]);
                                TablesService.saveData(RouteData.Values["tables"].ToString(), json, Field, guid, actType, (Dictionary<string, object>)dic[item.lang]);
                            }
                        }
                    }
                    else
                    {
                        string json = JsonConvert.SerializeObject((Dictionary<string, object>)dic);
                        TablesService.saveData(RouteData.Values["tables"].ToString(), json, Field, guid, actType, (Dictionary<string, object>)dic);
                    }
                }

                string addId = "";
                //dataTableCategory
                if (Session["dataTableCategory"] != null)
                {
                    addId = "?c=" + Session["dataTableCategory"].ToString();
                }
                //完成轉跳
                Response.Write("<script>window.location.href='" + Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/edit/" + guid + addId) + "';</script>");
            }
            return null;
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        /// <summary>
        /// DataTable
        /// </summary>
        /// <returns></returns>
        public string dataTable()
        {
            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> listData = new Dictionary<string, object>();

            string json = "";

            dynamic data = null;

            if (RouteData.Values["tables"] != null)
            {
                NameValueCollection requests = Request.Params;
                string guid = "";

                try
                {
                    guid = !string.IsNullOrEmpty(RouteData.Values["id"].ToString()) ? RouteData.Values["id"].ToString() : "";
                }
                catch(Exception e)
                {

                }


                data = GetListData.getListData(RouteData.Values["tables"].ToString(), requests, Url.Content("~/") , guid);
            }

            listData = data;

            json = JsonConvert.SerializeObject(listData, Formatting.Indented);

            return json;
        }

        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public string Ajax(FormCollection form)
        {
            Dictionary<String, Object> dic = new Dictionary<string, object>();

            NameValueCollection getData = FunctionService.reSubmitFormDataJson(HttpUtility.UrlDecode(form["Val"].ToString()));//回傳JSON資料
            dic.Add("Func", form["Func"].ToString());

            switch (form["Func"].ToString())
            {
                #region 切換縣市區域

                case "changeCity":
                    dic["district"] = "";
                    if (getData != null)
                    {
                       /* List<taiwan_city> district = FunctionService.TaiwanCitys(getData["city"].ToString());
                        var jsonSerialiser = new JavaScriptSerializer();
                        var districtJson = jsonSerialiser.Serialize(district);
                        dic["district"] = districtJson;*/
                    }
                    break;

                #endregion

                #region 徹底刪除

                case "removeData":

                    if (getData != null)
                    {
                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            Guid guid = Guid.Parse(guidArr[i].ToString());
                            var ashcan = DB.ashcan.Where(m => m.guid == guid).FirstOrDefault();

                            switch (ashcan.tables)
                            {
                                case "forum":
                                    DB.Database.ExecuteSqlCommand("delete from forum where top_guid = '" + ashcan.from_guid + "'");
                                    DB.Database.ExecuteSqlCommand("delete from forum_message where forum_guid = '" + ashcan.from_guid + "'");
                                    break;
                            }
                            if (getData["type"].ToString() == "del")
                            {
                                DB.Database.ExecuteSqlCommand("delete from [" + ashcan.tables + "] where guid = '" + ashcan.from_guid + "'");
                            }
                            else
                            {
                                DB.Database.ExecuteSqlCommand("update [" + ashcan.tables + "] set status = 'N' where guid = '" + ashcan.from_guid + "'");
                            }

                            DB.Database.ExecuteSqlCommand("delete from [ashcan] where guid = '" + guid.ToString() + "'");
                        }
                    }
                    break;

                #endregion

                #region 修改狀態

                case "changeStatus":

                    if (getData != null)
                    {
                        string useLang = TablesService.useLang(getData["tables"].ToString());//是否使用語系

                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            if (useLang == "Y")
                            {
                                if (ViewBag.language != null)
                                {
                                    foreach (language item in ViewBag.language)
                                    {
                                        Dictionary<String, Object> temp = new Dictionary<string, object>();
                                        temp.Add("guid", guidArr[i].ToString());
                                        temp.Add("status", getData["status"].ToString());
                                        temp.Add("lang", item.lang);
                                        string jsonData = JsonConvert.SerializeObject(temp);
                                        //呼叫寫入或修改
                                        string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "status", guidArr[i].ToString(), "edit", null);
                                    }
                                }
                            }
                            else
                            {
                                Dictionary<String, Object> temp = new Dictionary<string, object>();
                                temp.Add("guid", guidArr[i].ToString());
                                temp.Add("status", getData["status"].ToString());
                                string jsonData = JsonConvert.SerializeObject(temp);
                                //呼叫寫入或修改
                                string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "status", guidArr[i].ToString(), "edit", null);
                            }
                        }

                        //資源回收桶
                        if (getData["status"].ToString() == "D")
                        {
                            ashcan ashcan = new ashcan();
                            for (int i = 0; i < guidArr.Length; i++)
                            {
                                ashcan.title = titleArr[i].ToString();
                                ashcan.tables = getData["tables"].ToString();
                                ashcan.from_guid = guidArr[i].ToString();
                                ashcan.create_date = DateTime.Now;
                                ashcan.modifydate = DateTime.Now;
                                DB.ashcan.Add(ashcan);

                                DB.SaveChanges();
                            }
                        }

                        dic["status"] = getData["status"].ToString();
                    }
                    break;

                #endregion

                #region 修改排序

                case "cahngeSortIndex":

                    if (getData != null)
                    {
                        string useLang = TablesService.useLang(getData["tables"].ToString());//是否使用語系
                        string[] guidArr = getData["guid"].ToString().Split(',');

                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            /*Dictionary<String, Object> temp = new Dictionary<string, object>();
                            temp.Add("guid", guidArr[i].ToString());
                            temp.Add("sortIndex", getData["sortIndex"].ToString());
                            string jsonData = JsonConvert.SerializeObject(temp);
                            //呼叫寫入或修改
                            string guid = TablesService.saveData(getData["tables"].ToString(), guidArr[i].ToString(), "edit", jsonData, "sortIndex");
                            */
                            if (useLang == "Y")
                            {
                                if (ViewBag.language != null)
                                {
                                    foreach (language item in ViewBag.language)
                                    {
                                        Dictionary<String, Object> temp = new Dictionary<string, object>();
                                        temp.Add("guid", guidArr[i].ToString());
                                        temp.Add("sortIndex", getData["sortIndex"].ToString());
                                        temp.Add("lang", item.lang);
                                        string jsonData = JsonConvert.SerializeObject(temp);
                                        //呼叫寫入或修改
                                        string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "sortIndex", guidArr[i].ToString(), "edit", null);
                                    }
                                }
                            }
                            else
                            {
                                Dictionary<String, Object> temp = new Dictionary<string, object>();
                                temp.Add("guid", guidArr[i].ToString());
                                temp.Add("sortIndex", getData["sortIndex"].ToString());
                                string jsonData = JsonConvert.SerializeObject(temp);
                                //呼叫寫入或修改
                                string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "sortIndex", guidArr[i].ToString(), "edit", null);
                            }
                        }
                    }
                    break;

                #endregion

                #region 登入

                case "sysLogin":

                    if (getData != null)
                    {
                        system_log system_Log = new system_log();

                        dic["re"] = "OK";

                      

                        if (getData["verification"].ToString() != Session["_ValCheckCode"].ToString())
                        {
                            dic["re"] = "codeError";
                            system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，驗證碼輸入錯誤!</code>";
                            system_Log.status = "N";
                            Session["_ValCheckCode"] = null;
                        }
                        else
                        {
                            Session["_ValCheckCode"] = null;

                            string username = getData["username"].ToString();

                            var users = DB.user.Where(m => m.username == username);
                            if (users.Count() <= 0)
                            {
                                dic["re"] = "usernameError";
                                system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，查無此帳號!</code>";
                                system_Log.status = "N";
                            }
                            else
                            {
                                string password = FunctionService.md5(getData["password"].ToString());

                                users = users.Where(m => m.password == password);
                                if (users.Count() <= 0)
                                {
                                    dic["re"] = "passwordError";
                                    system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，密碼比對錯誤!</code>";
                                    system_Log.status = "N";
                                }
                                else
                                {
                                    users = users.Where(m => m.status == "Y");
                                    if (users.Count() <= 0)
                                    {
                                        dic["re"] = "statusError";
                                        system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，帳號為停用狀態!</code>";
                                        system_Log.status = "N";
                                    }
                                    else
                                    {
                                        //判斷IP
                                        int firewallsCount = DB.firewalls.Where(m => m.status != "D").Count();

                                        bool ipCheck = true;

                                        //有IP判斷是否可通行
                                        if (firewallsCount > 0 && getData["username"].ToString() != "sysadmin")
                                        {
                                            string nowIP = FunctionService.GetIP();
                                            firewalls firewalls = DB.firewalls.Where(m => m.ip == nowIP).FirstOrDefault();

                                            if (firewalls == null)
                                            {
                                                dic["re"] = "ipError";
                                                ipCheck = false;
                                                system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，IP比對錯誤!</code>";
                                                system_Log.status = "N";
                                            }
                                            else
                                            {
                                                if (firewalls.status == "N")
                                                {
                                                    dic["re"] = "ipError";
                                                    ipCheck = false;
                                                    system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，IP比對錯誤!</code>";
                                                    system_Log.status = "N";
                                                }
                                            }
                                        }

                                        if (ipCheck)
                                        {
                                            system_Log.notes = "登入成功";
                                            system_Log.status = "Y";

                                            if (adminCathType == "Session")
                                            {
                                                Session.Add("sysUsername", username);
                                                Session.Add("sysUserGuid", users.FirstOrDefault().guid);
                                            }
                                            else
                                            {
                                                //產生一個Cookie
                                                HttpCookie cookie = new HttpCookie("sysLogin");
                                                //設定過期日
                                                cookie.Expires = DateTime.Now.AddDays(365);
                                                cookie.Values.Add("sysUsername", username);//增加属性
                                                cookie.Values.Add("sysUserGuid", users.FirstOrDefault().guid);
                                                Response.AppendCookie(cookie);//确定写入cookie中
                                            }

                                            FunctionService.getAdminPermissions(username);//權限
                                                                                          //更新登入日期
                                            var saveData = DB.user.Where(m => m.username == username).Where(m => m.status == "Y").FirstOrDefault();
                                            saveData.logindate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                            DB.SaveChanges();
                                        }
                                    }

                                    if (getData["keepMe"] != null)
                                    {
                                        if (getData["keepMe"] == "Y")
                                        {
                                            //產生一個Cookie
                                            HttpCookie cookie2 = new HttpCookie("keepMeAccount");
                                            //設定單值
                                            cookie2.Value = getData["username"].ToString();
                                            //設定過期日
                                            cookie2.Expires = DateTime.Now.AddDays(365);
                                            //寫到用戶端
                                            Response.Cookies.Add(cookie2);
                                        }
                                    }
                                }
                            }

                            try
                            {
                                dic["url"] = Url.Content("~/siteadmin/" + Session["loginLastToUri"].ToString());
                                Session.Remove("loginLastToUri");
                            }
                            catch
                            {
                                dic["url"] = Url.Content("~/siteadmin");
                            }
                        }

                        system_Log.title = "後台登入帳號：" + getData["username"].ToString();
                        system_Log.ip = FunctionService.GetIP();
                        system_Log.types = "sysLogin";
                        system_Log.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        system_Log.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        system_Log.guid = Guid.NewGuid().ToString();
                        system_Log.username = getData["username"].ToString();
                        if (getData["username"].ToString() != "sysadmin")
                        {
                            DB.system_log.Add(system_Log);
                            DB.SaveChanges();
                        }
                    }
                    break;

                    #endregion
            }

            string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
            //輸出json格式
            return json;
        }

        /// <summary>
        /// 帳號判斷
        /// </summary>
        [HttpGet]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public void CkAccount()
        {
            dynamic data = null;
            string username = Request["username"].ToString();

            if (RouteData.Values["tokens"] != null)
            {
                string[] inputAccount = RouteData.Values["tokens"].ToString().Split('@');

                switch (inputAccount[0])
                {
                    case "user":
                        data = DB.user.Where(m => m.username == username).Count();
                        break;
                    case "member":
                        data = DB.member.Where(m => m.username == username).Count();
                        break;
                }
            }
            else
            {
                data = DB.user.Where(m => m.username == username).Count();
            }

            if (data != null && data > 0)
            {
                Response.Write("false");
            }
            else
            {
                Response.Write("true");
            }
        }

        public ActionResult Logout()
        {
            Response.Cookies.Clear();

            //FormsAuthentication.SignOut();

            HttpCookie c = new HttpCookie("sysLogin");
            c.Values.Remove("adminDataID");
            c.Values.Remove("adminCategory");
            c.Values.Remove("adminUsername");
            c.Values.Remove("sysUsername");
            c.Values.Remove("sysUserGuid");

            c.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(c);

            Session.Clear();

            return RedirectPermanent(Url.Content("~/siteadmin/login"));
        }

        public string analytics()
        {
            var GaData = new GoogleAnalyticsService();

            GaParameterModel GaParameter = new GaParameterModel();//宣告參數使用

            string sDate = DateTime.Now.ToString("yyyy") + "-01-01";
            string eDate = DateTime.Now.ToString("yyyy") + "-12-31";

            if (Session["sDate"] != null && Session["eDate"] != null)
            {
                sDate = Session["sDate"].ToString();
                eDate = Session["eDate"].ToString();
            }

            GaParameter.startDate = sDate;//預設起始日
            GaParameter.endDate = eDate;//預設結束日

            string json = "";
            if (RouteData.Values["key"] != null)
            {
                switch (RouteData.Values["key"].ToString())
                {
                    case "year":
                        json = GaData.ViewYear();
                        break;
                    //年齡層
                    case "age":

                        try
                        {
                            string CacheName = "age" + sDate + eDate;
                            dynamic tmp = null;
                            GaParameter.metrics = "ga:pageviews";
                            GaParameter.Dimensions = "ga:visitorAgeBracket";
                            tmp = GaData.ReGaContent(GaParameter);

                            if (cacheContainer.Get(CacheName) == null)
                            {
                                tmp = GaData.ReGaContent(GaParameter);
                                cacheContainer.Insert(CacheName, tmp, null, DateTime.Now.AddMinutes(cacheTime), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            }
                            else
                            {
                                tmp = cacheContainer.Get(CacheName);//自cache取出
                            }

                            //List<AgeViewModel> age = tmp.Rows;
                            List<AgeViewModel> AgeViewModel = new List<AgeViewModel>();
                            int s = 0;
                            foreach (List<string> item in tmp.Rows)
                            {
                                AgeViewModel m = new AgeViewModel();
                                m.age = item[0];//網頁
                                m.visits = int.Parse(item[1].ToString());
                                AgeViewModel.Add(m);
                                s++;
                            }

                            json = JsonConvert.SerializeObject(AgeViewModel);
                        }
                        catch { }

                        break;

                    ////流量來源
                    case "medium":

                        #region 流量來源

                        try
                        {
                            GaParameter.metrics = "ga:organicSearches";
                            GaParameter.Dimensions = "ga:medium";

                            string CacheName = "GaMedium";
                            dynamic tmp = null;
                            if (cacheContainer.Get(CacheName) == null)
                            {
                                tmp = GaData.ReGaContent(GaParameter);
                                cacheContainer.Insert(CacheName, tmp, null, DateTime.Now.AddMinutes(cacheTime), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            }
                            else
                            {
                                tmp = cacheContainer.Get(CacheName);//自cache取出
                            }

                            List<MediumViewModel> MediumViewModel = new List<MediumViewModel>();
                            int s = 0;
                            foreach (List<string> item in tmp.Rows)
                            {
                                MediumViewModel m = new MediumViewModel();

                                if (item[0].ToString() != "(none)" && item[0].ToString() != "(not set)")
                                {
                                    switch (item[0].ToString())
                                    {
                                        case "organic":
                                            m.source = item[0].ToString() + "(搜索)";//名稱
                                            break;

                                        case "WebPush":
                                            m.source = item[0].ToString() + "(直接)";//名稱

                                            break;

                                        case "referral":
                                            m.source = item[0].ToString() + "(推薦)";//名稱

                                            break;
                                    }

                                    m.litres = int.Parse(item[1].ToString());
                                    MediumViewModel.Add(m);
                                    s++;
                                }
                            }
                            json = JsonConvert.SerializeObject(MediumViewModel);
                        }
                        catch { }

                        #endregion

                        break;

                    ////流量來源(線性圖)
                    case "traffic":

                        string subkey = RouteData.Values["subkey"].ToString();

                        #region 流量來源(線性圖)

                        try
                        {
                            GaParameter.metrics = "ga:organicSearches";
                            GaParameter.Dimensions = "ga:medium,ga:date";

                            string CacheName = "GaMediumDate" + sDate + eDate;
                            dynamic tmp = GaData.ReGaContent(GaParameter); ;
                            if (cacheContainer.Get(CacheName) == null)
                            {
                                tmp = GaData.ReGaContent(GaParameter);
                                cacheContainer.Insert(CacheName, tmp, null, DateTime.Now.AddMinutes(cacheTime), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            }
                            else
                            {
                                tmp = cacheContainer.Get(CacheName);//自cache取出
                            }
                            IFormatProvider ifp = new System.Globalization.CultureInfo("zh-TW", true);

                            List<TrafficViewModel> TrafficViewModel = new List<TrafficViewModel>();
                            int s = 0;
                            foreach (List<string> item in tmp.Rows)
                            {
                                TrafficViewModel m = new TrafficViewModel();

                                if (item[0].ToString() != "(none)" && item[0].ToString() != "(not set)" && item[0].ToString().ToLower() == subkey.ToLower())
                                {
                                    DateTime tempDate = DateTime.ParseExact(item[1].ToString(), "yyyyMMdd", ifp, System.Globalization.DateTimeStyles.None);
                                    m.date = tempDate.ToString("yyyy-MM-dd") + "T16:00:00.000Z";
                                    m.value = int.Parse(item[2].ToString());
                                    TrafficViewModel.Add(m);
                                    s++;
                                }
                            }
                            json = JsonConvert.SerializeObject(TrafficViewModel);
                        }
                        catch { }

                        #endregion

                        break;
                }
            }

            return json;
        }

        //更改較少資訊Cache使用
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //登入判斷
            ViewBag.projeftNo = projeftNo;

            ViewBag.username = "";
            ViewBag.role_guid = null;

            ViewBag.defLang = defLang;

            ViewBag.permissions = Session["permissions"];
            ViewBag.permissionsTop = Session["permissionsTop"];

            if (Request.Cookies["keepMeAccount"] != null || Session["sysUsername"] != null)
            {
                if (adminCathType == "Session" && Session["sysUsername"] != null && Session["sysUsername"].ToString() != "")
                {
                    ViewBag.username = Session["sysUsername"].ToString();
                }
                else
                {
                    if (Request.Cookies["sysLogin"] != null)
                    {
                        HttpCookie aCookie = Request.Cookies["sysLogin"];
                        if (aCookie != null && aCookie.Value != "" && aCookie.Value.IndexOf("&") != -1)
                        {
                            List<string> sysUser = aCookie.Value.Split('&').ToList();
                            string sysUsername = Server.HtmlEncode(sysUser[0].Replace("sysUsername=", ""));
                            ViewBag.username = sysUsername;
                        }
                    }
                }
            }

            if (ViewBag.username == "sysadmin")
            {
                ViewBag.system_menu = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.status == "Y").ToList();
            }
            else
            {
                ViewBag.system_menu = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.status == "Y").Where(m => m.area != "admin").ToList();
            }

            ViewBag.system_menu_data = null;
            ViewBag.tables = "";
            if (RouteData.Values["tables"] != null)
            {
                string tables = RouteData.Values["tables"].ToString();
                ViewBag.system_menu_data = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault();

                string topCategory = ViewBag.system_menu_data.category;
                ViewBag.system_menu_top_data = DB.system_menu.Where(m => m.guid == topCategory).FirstOrDefault();
                ViewBag.tables = RouteData.Values["tables"].ToString();

                if (tables == "notes_data")
                {
                    if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
                    {
                        string act_path = "edit/" + RouteData.Values["id"].ToString();

                        ViewBag.system_menu_data = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();
                    }
                }

                if (tables == "finance_report" || tables == "share_download" || tables == "form_subject" || tables == "downloads"  || tables == "advertise")
                {
                    if (RouteData.Values["action"].ToString() == "list")
                    {
                        if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
                        {
                            string act_path = "list/" + RouteData.Values["id"].ToString();

                            ViewBag.system_menu_data = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();
                   
                            
                        }
                    }
                    if (RouteData.Values["action"].ToString() == "edit")
                    {
                        if (Request["c"] != null && Request["c"].ToString() != "")
                        {
                            string act_path = "list/" + Request["c"].ToString();

                            ViewBag.system_menu_data = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();
                        }
                    }
                }

                //紀錄先進入的路徑
                loginLastToUri = RouteData.Values["tables"].ToString();
                if (RouteData.Values["action"] != null && RouteData.Values["action"].ToString() != "")
                {
                    loginLastToUri += "/" + RouteData.Values["action"].ToString();
                }
                if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
                {
                    loginLastToUri += "/" + RouteData.Values["id"].ToString();
                }

                if (!string.IsNullOrEmpty(loginLastToUri))
                {
                    Session.Remove("loginLastToUri");
                    Session.Add("loginLastToUri", loginLastToUri);
                }
            }

            ViewBag.language = null;
            ViewBag.viewLanguage = "語系";
            var language = DB.language.OrderBy(m => m.sortIndex).Where(m => m.status == "Y").ToList();
            if (language.Count > 0)
            {
                ViewBag.language = language;
                ViewBag.viewLanguage = language[0].title;
            }

            ViewBag.userData = FunctionService.ReUserData();//回傳使用者資訊

            //系統資訊
            Guid systemGuid = Guid.Parse("4795DABF-18DE-490E-9BB2-D57B4D99C127");
            ViewBag.systemData = DB.system_data.Where(m => m.guid == systemGuid).FirstOrDefault();
        }

        /// <summary>
        /// 無限欄位用
        /// </summary>
        public partial class infinityLayoutModel
        {
            public string key { get; set; }
            public List<string> val { get; set; }
        }

        public partial class verifyInfoModel
        {
            public string date { get; set; }
            public string info { get; set; }
        }
    }
}