﻿using HuLane.BuildBlock;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuLane.Main.Repositories;
using Web.Models;
using Scriban;
using Web.DB.Models;
using PagedList;
using Web.Service;
using System.Globalization;

namespace Web.Controllers
{
    public class WelfareController : BaseController
    {

        protected Dictionary<string, string> topActive = new Dictionary<string, string>();
        protected Dictionary<string, string> active = new Dictionary<string, string>();
        protected List<news_category> categoriec = new List<news_category>();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            headerActive.Add("welfare", "active");
            ViewBag.headerActive = headerActive;//header active
            ViewBag.ResLang = langData;//取得語系檔案
      
        }

        public ActionResult Index()
        {
            active.Add("welfare", "class=\"active\"");
            ViewBag.active = active;
            var changeLang = langData["語系"].ToString();
            ViewBag.title = langData["選擇胡連"];
 
            
            List<welfare> data = DB.welfare.Where(m => m.lang == changeLang)
                                .Where(m => m.category == "1")
                                .Where(m => m.status == "Y")
                                .OrderBy(m => m.sortIndex)
                                .ToList();


            ViewBag.data = data;

            return View();
        }

        public ActionResult Recruit()
        {
            active.Add("recruit", "class=\"active\"");
            ViewBag.active = active;
            var changeLang = langData["語系"].ToString();
            ViewBag.title = langData["人才招募"];


            List<welfare> data = DB.welfare.Where(m => m.lang == changeLang)
                          .Where(m => m.category == "2")
                          .Where(m => m.status == "Y")
                          .OrderBy(m => m.sortIndex)
                          .ToList();


            ViewBag.data = data;

            return View();
        }
    }
}