using Autofac;
using Autofac.Integration.Mvc;
using HuLane.BuildBlock;
using HuLane.Main.Migration;
using HuLane.Main.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AutofacRegistration();
        }

        public static void AutofacRegistration()
        {
            var builder = new ContainerBuilder();
            // Controller
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<ConnectionFactory>().As<IConnectionFactory>().SingleInstance();

            builder.RegisterType<ArticleRepository>().As<IArticleRepository>().InstancePerRequest();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        
    }
}
