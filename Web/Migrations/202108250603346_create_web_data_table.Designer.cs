﻿// <auto-generated />
namespace Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class create_web_data_table : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(create_web_data_table));
        
        string IMigrationMetadata.Id
        {
            get { return "202108250603346_create_web_data_table"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
