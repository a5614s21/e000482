﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_web_data_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.web_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 200),
                        url = c.String(unicode: false, storeType: "text"),
                        phone = c.String(maxLength: 255),
                        fax = c.String(maxLength: 255),
                        servicemail = c.String(maxLength: 255),
                        ContactEmail = c.String(),
                        UrbanContactEmail = c.String(),
                        CaseEmail = c.String(),
                        ext_num = c.String(maxLength: 255),
                        address = c.String(maxLength: 255),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                        contact_info = c.String(),
                        global_url = c.String(),
                        mail_app_url = c.String(),
                        facebook_url = c.String(),
                        token_timeout = c.Int(),
                        token_url = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });



            Sql("execute sp_addextendedproperty 'MS_Description', N'網站名稱' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'網址' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'電話' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'phone'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'傳真' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'fax'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'服務信箱' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'servicemail'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'分機' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'ext_num'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'地址' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'address'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'lang'");

        }

        public override void Down()
        {
            DropTable("dbo.web_data");
        }
    }
}
