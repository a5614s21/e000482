﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_system_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.language",
                c => new
                    {
                        guid = c.String(nullable: false, maxLength: 64),
                        id = c.Int(nullable: false, identity: true),
                        title = c.String(maxLength: 50),
                        codes = c.String(maxLength: 10),
                        status = c.String(maxLength: 1),
                        sortIndex = c.Int(),
                        date = c.DateTime(precision: 7, storeType: "datetime2"),
                        lang = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => new { t.guid, t.id });

            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'language', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'language', 'COLUMN', N'codes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'language', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'language', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'日期' ,'SCHEMA', N'dbo','TABLE', N'language', 'COLUMN', N'date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'language', 'COLUMN', N'lang'");

            CreateTable(
                "dbo.role_permissions",
                c => new
                    {
                        guid = c.Guid(nullable: false),
                        role_guid = c.String(maxLength: 64),
                        permissions_guid = c.String(maxLength: 64),
                        permissions_status = c.String(maxLength: 1),
                    })
                .PrimaryKey(t => t.guid);

            Sql("execute sp_addextendedproperty 'MS_Description', N'RolesGuid' ,'SCHEMA', N'dbo','TABLE', N'role_permissions', 'COLUMN', N'role_guid'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'權限Guid' ,'SCHEMA', N'dbo','TABLE', N'role_permissions', 'COLUMN', N'permissions_guid'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'權限狀態' ,'SCHEMA', N'dbo','TABLE', N'role_permissions', 'COLUMN', N'permissions_status'");



            CreateTable(
                "dbo.roles",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 255),
                        site = c.String(maxLength: 255),
                        rolenote = c.String(),
                        status = c.String(maxLength: 1),
                        verify = c.String(maxLength: 1),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'權限名稱' ,'SCHEMA', N'dbo','TABLE', N'roles', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'備註' ,'SCHEMA', N'dbo','TABLE', N'roles', 'COLUMN', N'rolenote'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'roles', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'roles', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'roles', 'COLUMN', N'modifydate'");


            CreateTable(
                "dbo.smtp_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        host = c.String(maxLength: 255),
                        port = c.String(maxLength: 20),
                        smtp_auth = c.String(maxLength: 1),
                        username = c.String(maxLength: 255),
                        password = c.String(maxLength: 255),
                        from_email = c.String(maxLength: 255),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'HOST' ,'SCHEMA', N'dbo','TABLE', N'smtp_data', 'COLUMN', N'host'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'PORT' ,'SCHEMA', N'dbo','TABLE', N'smtp_data', 'COLUMN', N'port'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'是否啟用SSL' ,'SCHEMA', N'dbo','TABLE', N'smtp_data', 'COLUMN', N'smtp_auth'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'帳號' ,'SCHEMA', N'dbo','TABLE', N'smtp_data', 'COLUMN', N'username'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'密碼' ,'SCHEMA', N'dbo','TABLE', N'smtp_data', 'COLUMN', N'password'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'來源信箱' ,'SCHEMA', N'dbo','TABLE', N'smtp_data', 'COLUMN', N'from_email'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'smtp_data', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'smtp_data', 'COLUMN', N'modifydate'");



            CreateTable(
                "dbo.system_data",
                c => new
                    {
                        guid = c.Guid(nullable: false),
                        title = c.String(maxLength: 255),
                        login_title = c.String(maxLength: 255),
                        logo = c.String(),
                        logo_alt = c.String(),
                        design_by = c.String(maxLength: 50),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        background = c.String(),
                        background_alt = c.String(),
                    })
                .PrimaryKey(t => t.guid);


            Sql("execute sp_addextendedproperty 'MS_Description', N'後台網站標題' ,'SCHEMA', N'dbo','TABLE', N'system_data', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'登入頁網站標題' ,'SCHEMA', N'dbo','TABLE', N'system_data', 'COLUMN', N'login_title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'網站LOGO' ,'SCHEMA', N'dbo','TABLE', N'system_data', 'COLUMN', N'logo'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'登入頁背景' ,'SCHEMA', N'dbo','TABLE', N'system_data', 'COLUMN', N'background'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'Design by' ,'SCHEMA', N'dbo','TABLE', N'system_data', 'COLUMN', N'design_by'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'system_data', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'system_data', 'COLUMN', N'modifydate'");


            CreateTable(
                "dbo.system_log",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 255),
                        ip = c.String(maxLength: 255),
                        types = c.String(maxLength: 255),
                        tables = c.String(maxLength: 255),
                        tables_guid = c.String(maxLength: 255),
                        notes = c.String(),
                        status = c.String(maxLength: 1),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        username = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.system_menu",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 255),
                        category = c.String(maxLength: 255),
                        tables = c.String(maxLength: 255),
                        sortindex = c.Int(),
                        status = c.String(maxLength: 1),
                        act_path = c.String(maxLength: 255),
                        icon = c.String(maxLength: 255),
                        area = c.String(maxLength: 255),
                        category_table = c.String(maxLength: 255),
                        index_view_url = c.String(maxLength: 255),
                        can_add = c.String(maxLength: 1),
                        can_edit = c.String(maxLength: 1),
                        can_del = c.String(maxLength: 1),
                        prev_table = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'選單名稱' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'上層選單GUID' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'資料表' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'tables'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'sortindex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'直接網址' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'act_path'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'圖示' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'icon'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'顯示區域' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'area'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所屬分類Table' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'category_table'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'前台網址' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'index_view_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'可否新增' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'can_add'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'可否修改' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'can_edit'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'可否刪除' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'can_del'");


            CreateTable(
                "dbo.user",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        username = c.String(maxLength: 255),
                        password = c.String(maxLength: 255),
                        rolename = c.String(maxLength: 255),
                        role_guid = c.String(maxLength: 64),
                        name = c.String(maxLength: 255),
                        email = c.String(maxLength: 255),
                        phone = c.String(maxLength: 50),
                        mobile = c.String(maxLength: 50),
                        address = c.String(maxLength: 255),
                        note = c.String(),
                        status = c.String(maxLength: 1),
                        logindate = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'帳號' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'username'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'密碼' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'password'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'群組名稱' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'rolename'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'群組GUID' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'role_guid'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'姓名' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'name'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'Email' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'email'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'電話' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'phone'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'手機' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'mobile'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'地址' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'address'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'登入日期' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'logindate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'modifydate'");



            CreateTable(
                "dbo.user_role",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        user_guid = c.String(maxLength: 64),
                        role_guid = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.user_role");
            DropTable("dbo.user");
            DropTable("dbo.system_menu");
            DropTable("dbo.system_log");
            DropTable("dbo.system_data");
            DropTable("dbo.smtp_data");
            DropTable("dbo.roles");
            DropTable("dbo.role_permissions");
            DropTable("dbo.language");
        }
    }
}
