﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_system2_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ashcan",
                c => new
                    {
                        guid = c.Guid(nullable: false, identity: true),
                        title = c.String(maxLength: 255),
                        tables = c.String(maxLength: 255),
                        from_guid = c.String(maxLength: 64),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                    })
                .PrimaryKey(t => t.guid);

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'ashcan', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'來源資料表' ,'SCHEMA', N'dbo','TABLE', N'ashcan', 'COLUMN', N'tables'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'來源資料表鍵值' ,'SCHEMA', N'dbo','TABLE', N'ashcan', 'COLUMN', N'from_guid'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'ashcan', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'ashcan', 'COLUMN', N'modifydate'");



            CreateTable(
                "dbo.firewalls",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 255),
                        ip = c.String(maxLength: 255),
                        notes = c.String(),
                        status = c.String(maxLength: 1),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'來源' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'IP位置' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'ip'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'屬性' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'types'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'來源資料表' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'tables'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'來源資料表鍵值' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'tables_guid'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'備註' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'modifydate'");


            Sql("execute sp_addextendedproperty 'MS_Description', N'名稱' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'IP位置' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'ip'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'其他敘述' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'modifydate'");

            CreateTable(
                "dbo.google_analytics",
                c => new
                    {
                        guid = c.Guid(nullable: false, identity: true),
                        area = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                    })
                .PrimaryKey(t => t.guid);

            Sql("execute sp_addextendedproperty 'MS_Description', N'區域值' ,'SCHEMA', N'dbo','TABLE', N'google_analytics', 'COLUMN', N'area'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'google_analytics', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'google_analytics', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建檔日期' ,'SCHEMA', N'dbo','TABLE', N'google_analytics', 'COLUMN', N'create_date'");


            CreateTable(
                "dbo.mail_contents",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建檔日期' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'lang'");


        }

        public override void Down()
        {
            DropTable("dbo.mail_contents");
            DropTable("dbo.google_analytics");
            DropTable("dbo.firewalls");
            DropTable("dbo.ashcan");
        }
    }
}
