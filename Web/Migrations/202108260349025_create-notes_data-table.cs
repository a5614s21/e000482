﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createnotes_datatable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.notes_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'說明' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'lang'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO關鍵字' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'seo_keywords'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO敘述' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'seo_description'");


        }

        public override void Down()
        {
            DropTable("dbo.notes_data");
        }
    }
}
