﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addsortIndextocoursetable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.courses", "sortIndex", c => c.Int());
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'courses', 'COLUMN', N'sortIndex'");

        }

        public override void Down()
        {
            DropColumn("dbo.courses", "sortIndex");
        }
    }
}
