﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createlocalhoststable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.locations",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        map = c.String(),
                        email = c.String(),
                        modifydate = c.DateTime(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'據點名稱' ,'SCHEMA', N'dbo','TABLE', N'locations', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'據點內容' ,'SCHEMA', N'dbo','TABLE', N'locations', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'Google Map' ,'SCHEMA', N'dbo','TABLE', N'locations', 'COLUMN', N'map'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'Email' ,'SCHEMA', N'dbo','TABLE', N'locations', 'COLUMN', N'email'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'locations', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'locations', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'locations', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'locations', 'COLUMN', N'sortIndex'");

        }

        public override void Down()
        {
            DropTable("dbo.locations");
        }
    }
}
