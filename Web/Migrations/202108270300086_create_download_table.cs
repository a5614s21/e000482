﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_download_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.download_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        uri = c.String(),
                        category = c.String(maxLength: 64),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'download_category', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'標籤' ,'SCHEMA', N'dbo','TABLE', N'download_category', 'COLUMN', N'uri'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'分類' ,'SCHEMA', N'dbo','TABLE', N'download_category', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'download_category', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'download_category', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'download_category', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'download_category', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'download_category', 'COLUMN', N'lang'");

            CreateTable(
                "dbo.downloads",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        category = c.String(maxLength: 64),
                        file = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        modifydate = c.DateTime(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        startdate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'downloads', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'分類' ,'SCHEMA', N'dbo','TABLE', N'downloads', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'檔案' ,'SCHEMA', N'dbo','TABLE', N'downloads', 'COLUMN', N'file'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'downloads', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'downloads', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'downloads', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'downloads', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'downloads', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'downloads', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'發布日期' ,'SCHEMA', N'dbo','TABLE', N'downloads', 'COLUMN', N'startdate'");

        }
        
        public override void Down()
        {
            DropTable("dbo.downloads");
            DropTable("dbo.download_category");
        }
    }
}
