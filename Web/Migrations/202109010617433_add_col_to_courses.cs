﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_col_to_courses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.courses", "file_title", c => c.String());
            AddColumn("dbo.courses", "video_title", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'PDF名稱' ,'SCHEMA', N'dbo','TABLE', N'courses', 'COLUMN', N'file_title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'影片名稱' ,'SCHEMA', N'dbo','TABLE', N'courses', 'COLUMN', N'video_title'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.courses", "video_title");
            DropColumn("dbo.courses", "file_title");
        }
    }
}
