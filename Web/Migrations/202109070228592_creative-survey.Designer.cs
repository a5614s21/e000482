﻿// <auto-generated />
namespace Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class creativesurvey : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(creativesurvey));
        
        string IMigrationMetadata.Id
        {
            get { return "202109070228592_creative-survey"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
