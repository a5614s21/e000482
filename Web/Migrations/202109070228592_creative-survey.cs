﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creativesurvey : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.surveys",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        no = c.String(),
                        name = c.String(),
                        company = c.String(),
                        email = c.String(),
                        area = c.String(),
                        industry = c.String(),
                        satisfaction = c.String(),
                        problem = c.String(),
                        message = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'客戶編號' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'no'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'姓名' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'name'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'公司資訊' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'company'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'E-Mail' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'email'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'客戶區域' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'area'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'公司的產業分類' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'industry'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'滿意度調查' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'satisfaction'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'綜合問題' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'problem'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'其他意見' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'message'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動時間' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立時間' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'surveys', 'COLUMN', N'lang'");
        }
        
        public override void Down()
        {
            DropTable("dbo.survey");
        }
    }
}
