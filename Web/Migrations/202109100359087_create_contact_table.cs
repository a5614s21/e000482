﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_contact_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.contacts",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        name = c.String(),
                        department = c.String(),
                        email = c.String(),
                        jobTitle = c.String(),
                        eNews = c.String(),
                        company = c.String(),
                        country = c.String(),
                        website = c.String(),
                        products = c.String(),
                        tel = c.String(),
                        message = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'姓名' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'name'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'公司' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'company'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'E-Mail' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'email'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'部門' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'department'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'職稱' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'jobTitle'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'國家' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'country'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'網址' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'website'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'電話' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'tel'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'其他意見' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'message'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'eNews' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'eNews'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'諮詢產品' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'products'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'異動時間' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立時間' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'lang'");

        }
        
        public override void Down()
        {
            DropTable("dbo.contacts");
        }
    }
}
