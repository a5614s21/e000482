﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_pic_to_location : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.locations", "pic", c => c.String());
            AddColumn("dbo.locations", "pic_alt", c => c.String());

            Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'locations', 'COLUMN', N'pic'");
        }
      

        public override void Down()
        {
            DropColumn("dbo.locations", "pic_alt");
            DropColumn("dbo.locations", "pic");
        }
    }
}
