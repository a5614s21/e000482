﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createadvertisetable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.advertises",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        notes = c.String(),
                        content = c.String(),
                        link = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        mobile_pic = c.String(),
                        mobile_pic_alt = c.String(),
                        modifydate = c.DateTime(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        startdate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'title'");
        
            Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'手機用圖片' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'mobile_pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'content'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'發布日期' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'startdate'");



        }
        
        public override void Down()
        {
            DropTable("dbo.advertises");
        }
    }
}
