﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcategoryadvertisetable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.advertises", "category", c => c.String(maxLength: 64));
            Sql("execute sp_addextendedproperty 'MS_Description', N'分類' ,'SCHEMA', N'dbo','TABLE', N'advertises', 'COLUMN', N'category'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.advertises", "category");
        }
    }
}
