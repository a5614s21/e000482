﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createmembertable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.members",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        username = c.String(),
                        password = c.String(),
                        name = c.String(),
                        company = c.String(),
                        email = c.String(),
                        phone = c.String(),
                        mobile = c.String(),
                        address = c.String(),
                        note = c.String(),
                        status = c.String(maxLength: 1),
                        logindate = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'帳號' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'username'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'密碼' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'password'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'公司' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'company'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'姓名' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'name'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'Email' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'email'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'電話' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'phone'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'手機' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'mobile'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'地址' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'address'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'登入日期' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'logindate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'members', 'COLUMN', N'modifydate'");

        }
        
        public override void Down()
        {
            DropTable("dbo.members");
        }
    }
}
