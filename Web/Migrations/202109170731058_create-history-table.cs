﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createhistorytable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.histories",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        notes = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'histories', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'histories', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'histories', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'histories', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建檔日期' ,'SCHEMA', N'dbo','TABLE', N'histories', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所屬分類' ,'SCHEMA', N'dbo','TABLE', N'histories', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'histories', 'COLUMN', N'lang'");
      

            CreateTable(
                "dbo.history_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        en_title = c.String(maxLength: 255),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'history_category', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'history_category', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'history_category', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'history_category', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'history_category', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'history_category', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'history_category', 'COLUMN', N'modifydate'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'history_category', 'COLUMN', N'lang'");


        }

        public override void Down()
        {
            DropTable("dbo.history_category");
            DropTable("dbo.histories");
        }
    }
}
