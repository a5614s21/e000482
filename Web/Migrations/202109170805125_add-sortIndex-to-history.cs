﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addsortIndextohistory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.histories", "sortIndex", c => c.Int());
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'histories', 'COLUMN', N'sortIndex'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.histories", "sortIndex");
        }
    }
}
