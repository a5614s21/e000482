﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createsystemitemtable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.system_item",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        notes = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        sortIndex = c.Int(),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'system_item', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'system_item', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'system_item', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'system_item', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建檔日期' ,'SCHEMA', N'dbo','TABLE', N'system_item', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所屬分類' ,'SCHEMA', N'dbo','TABLE', N'system_item', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'system_item', 'COLUMN', N'lang'");

            CreateTable(
                "dbo.system_item_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'system_item_category', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'system_item_category', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'system_item_category', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'system_item_category', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'system_item_category', 'COLUMN', N'modifydate'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'system_item_category', 'COLUMN', N'lang'");

        }
        
        public override void Down()
        {
            DropTable("dbo.system_item_category");
            DropTable("dbo.system_item");
        }
    }
}
