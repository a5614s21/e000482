﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_seo_product1 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.index_content");
            DropTable("dbo.product_info");
        }
        
        public override void Down()
        {
            DropTable("dbo.index_content");
            DropTable("dbo.product_info");
        }
    }
}
