﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_seo_product11 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
               "dbo.index_content",
               c => new
               {
                   id = c.Int(nullable: false, identity: true),
                   guid = c.String(nullable: false, maxLength: 64),
                   title = c.Int(nullable: false),
                   content1 = c.String(),
                   content2 = c.String(),
                   create_date = c.DateTime(),
                   modifydate = c.DateTime(),
                   lang = c.String(maxLength: 30),
               })
               .PrimaryKey(t => new { t.id, t.guid });

            CreateTable(
                "dbo.product_info",
                c => new
                {
                    id = c.Int(nullable: false, identity: true),
                    guid = c.String(nullable: false, maxLength: 64),
                    product_id = c.Int(nullable: false),
                    content = c.String(),
                    pic = c.String(),
                    pic_alt = c.String(),
                    create_date = c.DateTime(),
                    modifydate = c.DateTime(),
                    lang = c.String(maxLength: 30),
                    seo_keywords = c.String(),
                    seo_description = c.String(),
                })
                .PrimaryKey(t => new { t.id, t.guid });
        }
        
        public override void Down()
        {
            DropTable("dbo.product_info");
            DropTable("dbo.index_content");
        }
    }
}
