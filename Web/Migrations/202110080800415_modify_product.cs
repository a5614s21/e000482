﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify_product : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_info", "spec_pic", c => c.String());
            AddColumn("dbo.product_info", "paper_pic", c => c.String());
            AddColumn("dbo.product_info", "spec_paper", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_info", "spec_paper");
            DropColumn("dbo.product_info", "paper_pic");
            DropColumn("dbo.product_info", "spec_pic");
        }
    }
}
