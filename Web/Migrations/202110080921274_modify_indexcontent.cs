﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify_indexcontent : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.index_content", "title", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.index_content", "title", c => c.String());
        }
    }
}
