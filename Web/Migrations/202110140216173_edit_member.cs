﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class edit_member : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.members", "customer_general_information", c => c.String());
            AddColumn("dbo.members", "bank_affiliates", c => c.String());
            AddColumn("dbo.members", "businessType", c => c.String());
            AddColumn("dbo.members", "top3_businessContents", c => c.String());
            AddColumn("dbo.members", "top3_customers", c => c.String());
            AddColumn("dbo.members", "top3_venders", c => c.String());
            AddColumn("dbo.members", "major_equipmentList", c => c.String());
            AddColumn("dbo.members", "floorArea", c => c.String());
            AddColumn("dbo.members", "productModel", c => c.String());
            AddColumn("dbo.members", "companyType1", c => c.String());
            AddColumn("dbo.members", "companyType2", c => c.String());
            AddColumn("dbo.members", "cooperation", c => c.String());
            AddColumn("dbo.members", "whitchMethods", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.members", "whitchMethods");
            DropColumn("dbo.members", "cooperation");
            DropColumn("dbo.members", "companyType2");
            DropColumn("dbo.members", "companyType1");
            DropColumn("dbo.members", "productModel");
            DropColumn("dbo.members", "floorArea");
            DropColumn("dbo.members", "major_equipmentList");
            DropColumn("dbo.members", "top3_venders");
            DropColumn("dbo.members", "top3_customers");
            DropColumn("dbo.members", "top3_businessContents");
            DropColumn("dbo.members", "businessType");
            DropColumn("dbo.members", "bank_affiliates");
            DropColumn("dbo.members", "customer_general_information");
        }
    }
}
