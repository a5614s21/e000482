﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editProductInfo1103 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_info", "newName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_info", "newName");
        }
    }
}
