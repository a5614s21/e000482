﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editNews1115 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "file", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "file");
        }
    }
}
