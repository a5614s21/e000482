﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCarSysLevel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.carsysl2",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.Int(nullable: false),
                        l1 = c.Int(nullable: false),
                        caption = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.carsysl3",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.Int(nullable: false),
                        l2 = c.Int(nullable: false),
                        caption = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.carsysl4",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.Int(nullable: false),
                        l3 = c.Int(nullable: false),
                        caption = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.carsysl5",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.Int(nullable: false),
                        l4 = c.Int(nullable: false),
                        caption = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.carsysl5");
            DropTable("dbo.carsysl4");
            DropTable("dbo.carsysl3");
            DropTable("dbo.carsysl2");
        }
    }
}
