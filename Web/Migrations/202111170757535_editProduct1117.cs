﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editProduct1117 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_info", "carsysl4id", c => c.String());
            AddColumn("dbo.product_info", "carsysl4name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_info", "carsysl4name");
            DropColumn("dbo.product_info", "carsysl4id");
        }
    }
}
