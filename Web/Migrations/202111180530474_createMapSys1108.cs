﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createMapSys1108 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.mapcarsys",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        l5 = c.Int(nullable: false),
                        product_id = c.Int(nullable: false),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            DropTable("dbo.map_carsys");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.map_carsys",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        product_id = c.Int(nullable: false),
                        l5 = c.Int(nullable: false),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.id);
            
            DropTable("dbo.mapcarsys");
        }
    }
}
