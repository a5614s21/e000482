﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Web.Migrations.Seed;

    internal sealed class Configuration : DbMigrationsConfiguration<Web.Models.Model>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Web.Models.Model context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            //系統基本資訊(選單權限等)
            SystemSeeder systemSeeder = new SystemSeeder();
            systemSeeder.run();

            NewsSeeder newsSeeder = new NewsSeeder();
            newsSeeder.run();


            AboutsSeeder aboutSeeder = new AboutsSeeder();
            aboutSeeder.run();

            NotesSeeder notesSeeder = new NotesSeeder();
            notesSeeder.run();


            BusinessFaqSeeder businessFaqSeeder = new BusinessFaqSeeder();
            businessFaqSeeder.run();


            CourseSeeder courseSeeder = new CourseSeeder();
            courseSeeder.run();


            LocationSeeder locationSeeder = new LocationSeeder();
            locationSeeder.run();

            DownloadSeeder downloadSeeder = new DownloadSeeder();
            downloadSeeder.run();

            WelfareSeeder welfareSeeder = new WelfareSeeder();
            welfareSeeder.run();

            ShareholderFaqSeeder shareholderFaqSeeder = new ShareholderFaqSeeder();
            shareholderFaqSeeder.run();

            AdvSeeder advSeeder = new AdvSeeder();
            advSeeder.run();


            MemberSeeder memberSeeder = new MemberSeeder();
            memberSeeder.run();

            ProblemSeeder problemSeeder = new ProblemSeeder();
            problemSeeder.run();

            SystemItemSeeder systemItemSeeder = new SystemItemSeeder();
            systemItemSeeder.run();


        }
    
    }
}
