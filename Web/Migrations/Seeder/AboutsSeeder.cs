﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.DB.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class AboutsSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            about();
            history();
            historyData();
        }

        /// <summary>
        /// 公司簡介
        /// </summary>
        public void about()
        {
            var abouts = new List<abouts>
            {
                 new abouts {
                    guid = "1",
                    title = "關於胡連",
                    content = MapPath("/Content/Templates/About/about.html"),                
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "關於胡連",
                    seo_keywords = "關於胡連",
                  },
                 new abouts {
                    guid = "1",
                    title = "关于胡连",
                    content = MapPath("/Content/Templates/About/about.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "关于胡连",
                    seo_keywords = "关于胡连",
                  },
                 new abouts {
                    guid = "2",
                    title = "企業願景",
                    content = MapPath("/Content/Templates/About/overview.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "企業願景",
                    seo_keywords = "企業願景",
                  },
                 new abouts {
                 guid = "2",
                    title = "企业愿景",
                    content = MapPath("/Content/Templates/About/overview.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "企业愿景",
                    seo_keywords = "企业愿景",
                  },
                 new abouts {
                    guid = "3",
                    title = "全球據點",
                    content = MapPath("/Content/Templates/About/stronghold.html"),
                    pic = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "全球據點",
                    seo_keywords = "全球據點",
                  },
                 new abouts {
                   guid = "3",
                    title = "全球据点",
                    content = MapPath("/Content/Templates/About/stronghold.html"),
                    pic = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "全球据点",
                    seo_keywords = "全球据点",
                  },


                 new abouts {
                    guid = "4",
                    title = "品質榮耀",
                    content = MapPath("/Content/Templates/About/glory.html"),
                    pic = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "品質榮耀",
                    seo_keywords = "品質榮耀",
                  },
                 new abouts {
                 guid = "4",
                    title = "品质荣耀",
                    content = MapPath("/Content/Templates/About/glory.html"),
                    pic = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "品质荣耀",
                    seo_keywords = "品质荣耀",
                  },


                 new abouts {
                    guid = "5",
                    title = "技術發展",
                    content = MapPath("/Content/Templates/About/technology.html"),
                    pic = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "技術發展",
                    seo_keywords = "技術發展",
                  },
                 new abouts {
                 guid = "5",
                    title = "技术发展",
                    content = MapPath("/Content/Templates/About/technology.html"),
                    pic = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "技术发展",
                    seo_keywords = "技术发展",
                  },
                 new abouts {
                    guid = "6",
                    title = "胡連組織",
                    content = MapPath("/Content/Templates/About/organization.html"),
                    pic = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "胡連組織",
                    seo_keywords = "胡連組織",
                  },
                 new abouts {
                 guid = "6",
                    title = "胡连组织",
                    content = MapPath("/Content/Templates/About/organization.html"),
                    pic = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "胡连组织",
                    seo_keywords = "胡连组织",
                  },
                 new abouts {
                    guid = "7",
                    title = "經營團隊",
                    content = MapPath("/Content/Templates/About/team.html"),
                    pic = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "經營團隊",
                    seo_keywords = "經營團隊",
                  },
                 new abouts {
                  guid = "7",
                    title = "经营团队",
                    content = MapPath("/Content/Templates/About/team.html"),
                    pic = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "经营团队",
                    seo_keywords = "经营团队",
                  },
            };
            if (context.abouts.ToList().Count == 0)
            {
                abouts.ForEach(s => context.abouts.Add(s));
                context.SaveChanges();
            }
        }

        public void history() {


            //最新消息分類
            var history_category = new List<history_category>
            {
                 new history_category {
                    guid = "1",
                    title = "2011 - Now",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                    seo_description = "2011 - Now",
                    seo_keywords = "2011 - Now",
                  },
                 new history_category {
                    guid = "1",
                    title = "2011 - Now",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 1,
                    seo_description = "2011 - Now",
                    seo_keywords = "2011 - Now",
                  },

                 new history_category {
                    guid = "2",
                    title = "2001 - 2010",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                    seo_description = "2001 - 2010",
                    seo_keywords = "2001 - 2010",
                  },
                 new history_category {
                      guid = "2",
                    title = "2001 - 2010",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 2,
                    seo_description = "2001 - 2010",
                    seo_keywords = "2001 - 2010",
                  },
                  new history_category {
                    guid = "3",
                    title = "1991 - 2000",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 3,
                    seo_description = "1991 - 2000",
                    seo_keywords = "1991 - 2000",
                  },
                    new history_category {
                   guid = "3",
                    title = "1991 - 2000",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 3,
                    seo_description = "1991 - 2000",
                    seo_keywords = "1991 - 2000",
                     },
                      new history_category {
                        guid = "4",
                        title = "1977 - 1990",
                        modifydate = DateTime.Now,
                        status = "Y",
                        create_date = DateTime.Now,
                        lang = "tw",
                        sortIndex = 3,
                        seo_description = "1991 - 2000",
                        seo_keywords = "1991 - 2000",
                    },
                      new history_category {
                        guid = "4",
                        title = "1977 - 1990",
                        modifydate = DateTime.Now,
                        status = "Y",
                        create_date = DateTime.Now,
                        lang = "cn",
                        sortIndex = 3,
                        seo_description = "1991 - 2000",
                        seo_keywords = "1991 - 2000",
                    },

            };
            if (context.history_category.ToList().Count == 0)
            {
                history_category.ForEach(s => context.history_category.Add(s));
                context.SaveChanges();
            }

        }

        public void historyData()
        {

            List<string> temp = new List<string>
            {
                "2021",
                "2019",
                "2018",
                "2017",
                "2016",
                "2015-2017",
                "2015",
                "2014",
                "2013",
                "2012",
                "2011",
                "2010",
                "2009",
                "2008",
                "2007",
                "2006",
                "2004",
                "2003",
                "2002",
                "2001",
                "2000",
                "1997",
                "1996",
                "1995",
                "1994",
                "1990",
                "1989",
                "1982",
                "1981",
                "1979",
                "1977",
                
            };

            List<string> category = new List<string>
            {
                "1",
                "1",
                "1",
                "1",
                "1",
                "1",
                "1",
                "1",
                "1",
                "1",
                "1",
                "2",
                "2",
                "2",
                "2",
                "2",
                "2",
                "2",
                "2",
                "2",
                "3",
                "3",
                "3",
                "3",
                "3",
                "4",
                "4",
                "4",
                "4",
                "4",
                "4",

            };


            List<history> data = new List<history>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new history
                {
                    guid = (i + 1).ToString(),
                    title = title,
                    content = MapPath("/Content/Templates/History/" + title + ".html"),
                    modifydate = DateTime.Now,      
                    category = category[i].ToString(),
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                }); ;
                data.Add(new history
                {
                    guid = (i + 1).ToString(),
                    title = title,
                    content = MapPath("/Content/Templates/History/" + title + ".html"),
                    modifydate = DateTime.Now,
                    category = category[i].ToString(),
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "cn",
                });

                i++;
            }


            if (context.history.ToList().Count == 0)
            {
                data.ForEach(s => context.history.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}