﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.DB.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class AdvSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
         

            List<advertise> bannerList = banner();
            List<advertise> footerList = footer();
            List<advertise> data = bannerList.Concat(footerList).ToList<advertise>();
                      


            if (context.advertise.ToList().Count == 0)
            {
                data.ForEach(s => context.advertise.Add(s));
                context.SaveChanges();
            }
        }


        public List<advertise> banner()
        {
            List<string> temp = new List<string>
            {
                "全球專業汽車業連接器和端子供應商",
                "汽車橡膠",
                "汽車端子",

            };

            List<string> temp_cn = new List<string>
            {
             "全球专业汽车业连接器和端子供应商",
                "汽车橡胶",
                "汽车端子",
            };

            List<string> pic = new List<string>
            {
              "Styles/images/index/banner-1.jpg",
              "Styles/images/index/banner-2.jpg",
              "Styles/images/index/banner-3.jpg",
            };

            List<string> mobile_pic = new List<string>
            {
              "Styles/images/index/mobile-banner1.png",
              "Styles/images/index/mobile-banner2.png",
              "Styles/images/index/mobile-banner3.png",
            };



            List<string> content = new List<string>
            {
                MapPath("/Content/Templates/Adv/1.html"),
                "",
                "",

            };

            List<advertise> data = new List<advertise>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new advertise
                {
                    guid = "banner"+(i + 1).ToString(),
                    title = title,
                    content = content[i].ToString(),
                    modifydate = DateTime.Now,                  
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                    pic = pic[i].ToString(),
                    category = "banner",
                    mobile_pic = mobile_pic[i].ToString(),
                });
                data.Add(new advertise
                {
                    guid = "banner" + (i + 1).ToString(),
                    title = title,
                    content = content[i].ToString(),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "cn",
                    category = "banner",
                    pic = pic[i].ToString(),
                    mobile_pic = mobile_pic[i].ToString(),
                });

                i++;
            }

            return data;

            /*
            if (context.downloads.ToList().Count == 0)
            {
                data.ForEach(s => context.downloads.Add(s));
                context.SaveChanges();
            }
            */
        }


        public List<advertise> footer()
        {
            List<string> temp = new List<string>
            {
                "機車產業應用",
                "新能源汽車產業應用",
                "新能源汽車產業應用",
                "壓著模具",

            };

            List<string> temp_cn = new List<string>
            {
                "机车产业应用",
                "新能源汽车产业应用",
                "新能源汽车产业应用",
                "压着模具",
            };

            List<string> pic = new List<string>
            {
              "Styles/images/index/applications-1.jpg",
              "Styles/images/index/applications-2.jpg",
              "Styles/images/index/applications-3.jpg",
              "Styles/images/index/applications-4.jpg",
            };

            List<string> mobile_pic = new List<string>
            {
            
            };

            List<advertise> data = new List<advertise>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new advertise
                {
                    guid = "footer" + (i + 1).ToString(),
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                    category = "footer",
                    pic = pic[i].ToString(),
                });
                data.Add(new advertise
                {
                    guid = "banner" + (i + 1).ToString(),
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "cn",
                    category = "footer",
                    pic = pic[i].ToString(),
                });

                i++;
            }

            return data;

        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}