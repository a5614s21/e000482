﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;
using Web.DB.Models;

namespace Web.Migrations.Seed
{
    public class CourseSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            category();
            data();
           
        }

        public void category()
        {
            //最新消息分類
            var course_category = new List<course_category>
            {
                 new course_category {
                    guid = "1",
                    title = "壓著模具使用教學",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                    seo_description = "壓著模具使用教學",
                    seo_keywords = "壓著模具使用教學",
                  },
                 new course_category {
                    guid = "1",
                    title = "压着模具使用教学",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 1,
                    seo_description = "压着模具使用教学",
                    seo_keywords = "压着模具使用教学",
                  },

                 new course_category {
                    guid = "2",
                    title = "項目2",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                    seo_description = "項目2",
                    seo_keywords = "項目2",
                  },
                 new course_category {
                    guid = "2",
                    title = "项目2",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 2,
                    seo_description = "项目2",
                    seo_keywords = "项目2",
                  },
                  new course_category {
                    guid = "3",
                    title = "項目3",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 3,
                    seo_description = "項目3",
                    seo_keywords = "項目3",
                  },
                    new course_category {
                    guid = "3",
                    title = "项目3",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 3,
                    seo_description = "项目3",
                    seo_keywords = "项目3",
                  },
            };
            if (context.course_category.ToList().Count == 0)
            {
                course_category.ForEach(s => context.course_category.Add(s));
                context.SaveChanges();
            }





        }


        public void data()
        {

            List<string> temp = new List<string>
            {
                "壓著模具機啟動機器注意事項",
                "如何架模於壓著模具機上",
                "如何在機台上調整壓著模",
                "如何裝端子盤及壓著線材",
                "壓著模具機啟動機器注意事項",
                "如何架模於壓著模具機上",
                "如何在機台上調整壓著模",
                "如何裝端子盤及壓著線材",
                "如何架模於壓著模具機上",
                "壓著模具機啟動機器注意事項",
            };

            List<string> temp_cn = new List<string>
            {
               "压着模具机启动机器注意事项",
                "如何架模于压着模具机上",
                "如何在机台上调整压着模",
                "如何装端子盘及压着线材",
                "压着模具机启动机器注意事项",
                "如何架模于压着模具机上",
                "如何在机台上调整压着模",
                "如何装端子盘及压着线材",
                "如何架模于压着模具机上",
                "压着模具机启动机器注意事项",
            };


            List<string> pic = new List<string>
            {
                "/Styles/images/business/course/1.png,/Styles/images/business/course/1.png",
                "/Styles/images/business/course/2.png,/Styles/images/business/course/2.png",
                "/Styles/images/business/course/3.png,/Styles/images/business/course/3.png",
                "",
                "/Styles/images/business/course/3.png,/Styles/images/business/course/3.png",
                   "/Styles/images/business/course/1.png,/Styles/images/business/course/1.png",
                  "/Styles/images/business/course/1.png,/Styles/images/business/course/1.png",
                "",
                "",
                "",
            };

            

            List<courses> data = new List<courses>();
            int i = 0;

            foreach(string title in temp)
            {
                
                data.Add(new courses
                {
                    guid = (i+1).ToString(),
                    title = title,
                    category = "1",
                    notes = "說明如何使用壓著模具機及注意啟動機器前，需注意機器之狀態操作說明。",
                    pic = pic[i].ToString(),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                    file = "/Content/Upload/files/test.pdf",
                    file_title = "壓著模具機啟動機器注意事項.pdf",
                    video_title = "壓著模具機啟動機器注意事項(00:50)",
                    video = "https://www.youtube.com/embed/ykEbgOojWiU",
                });
                data.Add(new courses
                {
                    guid = (i + 1).ToString(),
                    title = temp_cn[i].ToString(),
                    category = "1",
                    notes = "说明如何使用压着模具机及注意启动机器前，需注意机器之状态操作说明。",
                    pic = pic[i].ToString(),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = (i + 1),
                    file = "/Content/Upload/files/test.pdf",
                    file_title = "压着模具机启动机器注意事项.pdf",
                    video_title = "压着模具机启动机器注意事项(00:50)",
                    video = "https://www.youtube.com/embed/ykEbgOojWiU",
                });

                i++;
            }


            if (context.courses.ToList().Count == 0)
            {
                data.ForEach(s => context.courses.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}