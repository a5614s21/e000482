﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.DB.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class DownloadSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            category();

            List<downloads> csrList = csr();


            List<downloads> meetingList = meeting();

            List<downloads> data = csrList.Concat(meetingList).ToList<downloads>();

            List<downloads> diretors_memberList = diretors_member();
            data = data.Concat(diretors_memberList).ToList<downloads>();

            List<downloads> diretors_infoList = diretors_info();
            data = data.Concat(diretors_infoList).ToList<downloads>();


            List<downloads> diretors_resolutionList = diretors_resolution();
            data = data.Concat(diretors_resolutionList).ToList<downloads>();

            List<downloads> regulationsList = regulations();
            data = data.Concat(regulationsList).ToList<downloads>();

            List<downloads> audit_masterList = audit_master();
            data = data.Concat(audit_masterList).ToList<downloads>();

            List<downloads> audit_implementList = audit_implement();
            data = data.Concat(audit_implementList).ToList<downloads>();

            List<downloads> audit_commList = audit_comm();
            data = data.Concat(audit_commList).ToList<downloads>();

            List<downloads> audit_memberList = audit_member();
            data = data.Concat(audit_memberList).ToList<downloads>();

            List<downloads> organizationList = organization();
            data = data.Concat(organizationList).ToList<downloads>();

            List<downloads> organization_serviceList = organization_service();
            data = data.Concat(organization_serviceList).ToList<downloads>();

            List<downloads> investorList = investor();
            data = data.Concat(investorList).ToList<downloads>();

            List<downloads> corporateList = corporate();
            data = data.Concat(corporateList).ToList<downloads>();

            List<downloads> monthly_reportList = monthly_report();
            data = data.Concat(monthly_reportList).ToList<downloads>();

            List<downloads> financial_reportList = financial_report();
            data = data.Concat(financial_reportList).ToList<downloads>();


            if (context.downloads.ToList().Count == 0)
            {
                data.ForEach(s => context.downloads.Add(s));
                context.SaveChanges();
            }
        }


        public void category()
        {
            //最新消息分類
            var download_category = new List<download_category>
            {
                 new download_category {
                    guid = "csr",
                    title = "企業社會責任",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                    seo_description = "企業社會責任",
                    seo_keywords = "企業社會責任",
                    uri = "csr",
                    category = "0",
                  },
                 new download_category {
                    guid = "csr",
                    title = "企业社会责任",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 1,
                    seo_description = "企业社会责任",
                    seo_keywords = "企业社会责任",
                    uri = "csr",
                     category = "0",
                  },


                 new download_category {
                    guid = "meeting",
                    title = "股東會",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                    seo_description = "股東會",
                    seo_keywords = "股東會",
                      uri = "meeting",
                       category = "0",
                  },
                 new download_category {
                    guid = "meeting",
                    title = "股东会",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                  lang = "cn",
                    sortIndex = 2,
                    seo_description = "股东会",
                    seo_keywords = "股东会",
                      uri = "meeting",
                       category = "0",
                  },

                  new download_category {
                    guid = "diretors",
                    title = "董事會",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 3,
                    seo_description = "董事會",
                    seo_keywords = "董事會",
                     uri = "diretors",
                      category = "0",
                  },
                    new download_category {
                    guid = "diretors",
                    title = "董事会",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 3,
                    seo_description = "董事会",
                    seo_keywords = "董事会",
                     uri = "diretors",
                      category = "0",
                  },

                #region 董事會細項
                new download_category {
                            guid = "diretors_member",
                            title = "董事會成員",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "tw",
                            sortIndex = 1,
                            seo_description = "董事會成員",
                            seo_keywords = "董事會成員",
                             uri = "diretors_member",
                              category = "diretors",
                          },
                            new download_category {
                            guid = "diretors_member",
                            title = "董事会成员",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "cn",
                            sortIndex = 1,
                            seo_description = "董事会成员",
                            seo_keywords = "董事会成员",
                             uri = "diretors_member",
                              category = "diretors",
                          },

                              new download_category {
                            guid = "diretors_info",
                            title = "獨立董事選任資訊",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "tw",
                            sortIndex = 2,
                            seo_description = "獨立董事選任資訊",
                            seo_keywords = "獨立董事選任資訊",
                             uri = "diretors_info",
                              category = "diretors",
                          },
                            new download_category {
                            guid = "diretors_info",
                            title = "独立董事选任资讯",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "cn",
                            sortIndex = 2,
                            seo_description = "独立董事选任资讯",
                            seo_keywords = "独立董事选任资讯",
                             uri = "diretors_info",
                              category = "diretors",
                          },

                              new download_category {
                            guid = "diretors_resolution",
                            title = "董事會決議事項",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "tw",
                            sortIndex = 3,
                            seo_description = "董事會決議事項",
                            seo_keywords = "董事會決議事項",
                             uri = "diretors_resolution",
                              category = "diretors",
                          },
                            new download_category {
                            guid = "diretors_resolution",
                            title = "董事会决议事项",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "cn",
                            sortIndex = 3,
                            seo_description = "董事会决议事项",
                            seo_keywords = "董事会决议事项",
                             uri = "diretors_resolution",
                              category = "diretors",
                          },
                    #endregion




                new download_category {
                    guid = "regulations",
                    title = "公司規章",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 4,
                    seo_description = "公司規章",
                    seo_keywords = "公司規章",
                     uri = "regulations",
                      category = "0",
                  },
                    new download_category {
                    guid = "regulations",
                    title = "公司规章",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 4,
                    seo_description = "公司规章",
                    seo_keywords = "公司规章",
                     uri = "regulations",
                      category = "0",
                  },


                  new download_category {
                    guid = "audit_master",
                    title = "內部稽核組織及運作",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 5,
                    seo_description = "內部稽核組織及運作",
                    seo_keywords = "內部稽核組織及運作",
                     uri = "audit_master",
                      category = "0",
                  },
                    new download_category {
                    guid = "audit_master",
                    title = "内部稽核组织及运作",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 5,
                    seo_description = "内部稽核组织及运作",
                    seo_keywords = "内部稽核组织及运作",
                     uri = "audit_master",
                      category = "0",
                  },


                    #region 內部稽核組織及運作
                new download_category {
                            guid = "audit",
                            title = "內部稽核組織及運作",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "tw",
                            sortIndex = 1,
                            seo_description = "內部稽核組織及運作",
                            seo_keywords = "內部稽核組織及運作",
                             uri = "audit",
                              category = "audit_master",
                          },
                            new download_category {
                            guid = "audit",
                            title = "内部稽核组织及运作",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "cn",
                            sortIndex = 1,
                            seo_description = "内部稽核组织及运作",
                            seo_keywords = "内部稽核组织及运作",
                             uri = "audit",
                             category = "audit_master",
                          },


                              new download_category {
                            guid = "audit_implement",
                            title = "各兼職單位執行情形",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "tw",
                            sortIndex = 2,
                            seo_description = "各兼職單位執行情形",
                            seo_keywords = "各兼職單位執行情形",
                             uri = "audit_implement",
                             category = "audit_master",
                          },
                            new download_category {
                            guid = "audit_implement",
                            title = "各兼职单位执行情形",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "cn",
                            sortIndex = 2,
                            seo_description = "各兼职单位执行情形",
                            seo_keywords = "各兼职单位执行情形",
                             uri = "audit_implement",
                             category = "audit_master",
                          },

                              new download_category {
                            guid = "audit_comm",
                            title = "獨立董事與內部稽核主管及會計師之溝通情形",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "tw",
                            sortIndex = 3,
                            seo_description = "獨立董事與內部稽核主管及會計師之溝通情形",
                            seo_keywords = "獨立董事與內部稽核主管及會計師之溝通情形",
                             uri = "audit_comm",
                             category = "audit_master",
                          },
                            new download_category {
                            guid = "audit_comm",
                            title = "獨立董事與內部稽核主管及會計師之溝通情形",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "cn",
                            sortIndex = 3,
                            seo_description = "獨立董事與內部稽核主管及會計師之溝通情形",
                            seo_keywords = "獨立董事與內部稽核主管及會計師之溝通情形",
                             uri = "audit_comm",
                             category = "audit_master",
                          },



                                 new download_category {
                            guid = "audit_member",
                            title = "功能性委員會",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "tw",
                            sortIndex = 4,
                            seo_description = "功能性委員會",
                            seo_keywords = "功能性委員會",
                             uri = "audit_member",
                             category = "audit_master",
                          },
                            new download_category {
                            guid = "audit_member",
                            title = "功能性委员会",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "cn",
                            sortIndex = 4,
                            seo_description = "功能性委员会",
                            seo_keywords = "功能性委员会",
                             uri = "audit_member",
                             category = "audit_master",
                          },



                    #endregion


                            
                  new download_category {
                    guid = "organization_master",
                    title = "關係企業組織圖",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 6,
                    seo_description = "關係企業組織圖",
                    seo_keywords = "關係企業組織圖",
                     uri = "organization_master",
                      category = "0",
                  },
                    new download_category {
                    guid = "organization_master",
                    title = "关系企业组织图",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 6,
                    seo_description = "关系企业组织图",
                    seo_keywords = "关系企业组织图",
                     uri = "organization_master",
                      category = "0",
                  },


                    #region 關係企業組織圖
                new download_category {
                            guid = "organization",
                            title = "關係企業組織圖",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "tw",
                            sortIndex = 1,
                            seo_description = "關係企業組織圖",
                            seo_keywords = "關係企業組織圖",
                             uri = "organization",
                             category = "organization_master",
                          },
                            new download_category {
                            guid = "organization",
                            title = "关系企业组织图",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "cn",
                            sortIndex = 1,
                            seo_description = "关系企业组织图",
                            seo_keywords = "关系企业组织图",
                             uri = "organization",
                             category = "organization_master",
                          },


                              new download_category {
                            guid = "organization_service",
                            title = "各關係企業營運概況",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "tw",
                            sortIndex = 2,
                            seo_description = "各關係企業營運概況",
                            seo_keywords = "各關係企業營運概況",
                             uri = "organization_service",
                             category = "organization_master",
                          },
                            new download_category {
                            guid = "organization_service",
                            title = "各关系企业营运概况",
                            modifydate = DateTime.Now,
                            status = "Y",
                            create_date = DateTime.Now,
                            lang = "cn",
                            sortIndex = 2,
                            seo_description = "各关系企业营运概况",
                            seo_keywords = "各关系企业营运概况",
                             uri = "organization_service",
                             category = "organization_master",
                          },

                       


                    #endregion


                  new download_category {
                    guid = "investor",
                    title = "投資人資訊",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 7,
                    seo_description = "投資人資訊",
                    seo_keywords = "投資人資訊",
                     uri = "investor",
                      category = "0",
                  },
                    new download_category {
                    guid = "investor",
                    title = "投资人资讯",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 7,
                    seo_description = "投资人资讯",
                    seo_keywords = "投资人资讯",
                     uri = "investor",
                      category = "0",
                  },


                      new download_category {
                    guid = "corporate",
                    title = "法人說明會",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 8,
                    seo_description = "法人說明會",
                    seo_keywords = "法人說明會",
                     uri = "corporate",
                      category = "0",
                  },
                    new download_category {
                    guid = "corporate",
                    title = "法人说明会",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 8,
                    seo_description = "法人说明会",
                    seo_keywords = "法人说明会",
                     uri = "corporate",
                      category = "0",
                  },


                       new download_category {
                    guid = "monthly_report",
                    title = "月營業額報告",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 9,
                    seo_description = "月營業額報告",
                    seo_keywords = "月營業額報告",
                     uri = "monthly_report",
                      category = "0",
                  },
                    new download_category {
                    guid = "monthly_report",
                    title = "月营业额报告",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 9,
                    seo_description = "月营业额报告",
                    seo_keywords = "月营业额报告",
                     uri = "monthly_report",
                      category = "0",
                  },

                new download_category {
                    guid = "financial_report",
                    title = "財務報告",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 10,
                    seo_description = "財務報告",
                    seo_keywords = "財務報告",
                     uri = "financial_report",
                      category = "0",
                  },
                    new download_category {
                    guid = "financial_report",
                    title = "财务报告",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 10,
                    seo_description = "财务报告",
                    seo_keywords = "财务报告",
                     uri = "financial_report",
                      category = "0",
                  },

            };
            if (context.download_category.ToList().Count == 0)
            {
                download_category.ForEach(s => context.download_category.Add(s));
                context.SaveChanges();
            }



     

        }

        public List<downloads> csr()
        {
            List<string> temp = new List<string>
            {
                "2019",
                "2018",
                "2017",
                "2016",
                "2015",
                "2014",
                "2013",

            };

            List<string> temp_cn = new List<string>
            {
              "",
            };

            List<string> pic = new List<string>
            {
              "Styles/images/investor/csr/book-1.jpg",
              "Styles/images/investor/csr/book-2.jpg",
              "Styles/images/investor/csr/book-3.jpg",
              "Styles/images/investor/csr/book-4.jpg",
              "Styles/images/investor/csr/book-5.jpg",
              "Styles/images/investor/csr/book-6.jpg",
              "Styles/images/investor/csr/book-7.jpg",
            };

            List<string> startAt = new List<string>
            {
                "2019-01-01",
                "2018-01-01",
                "2017-01-01",
                "2016-01-01",
                "2015-01-01",
                "2014-01-01",
                "2013-01-01",

            };

            string category = "csr";
            

            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category+(i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,                  
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                    pic = pic[i].ToString(),
                    startdate = DateTime.Parse(startAt[i].ToString()),
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                    pic = pic[i].ToString(),
                    startdate = DateTime.Parse(startAt[i].ToString()),
                });

                i++;
            }

            return data;

            /*
            if (context.downloads.ToList().Count == 0)
            {
                data.ForEach(s => context.downloads.Add(s));
                context.SaveChanges();
            }
            */
        }


        public List<downloads> meeting()
        {
            List<string> temp = new List<string>
            {
                "2020年度年報",
                "2020年股東常會-議事錄",
                "2020年股東常會-議事手冊",
                "2020年股東常會-開會通知書",
                "2019年股東常會-議事錄",
                "2019年股東常會-議事手冊",
                "2018年股東常會-議事錄",
                "2018年股東常會-議事手冊",

            };

            List<string> startAt = new List<string>
            {
              "2020",
              "2020",
              "2020",
              "2020",
              "2019",
              "2019",
              "2018",
              "2018",
            };

            string category = "meeting";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });

                i++;
            }

            return data;

          /*  if (context.downloads.ToList().Count == 0)
            {
                data.ForEach(s => context.downloads.Add(s));
                context.SaveChanges();
            }*/
        }

        /// <summary>
        /// 董事會成員
        /// </summary>
        public List<downloads> diretors_member()
        {
            List<string> temp = new List<string>
            {
                "2019年度董事會、董監事成員及薪酬委員會 績效評估結果報告",              

            };

            List<string> startAt = new List<string>
            {
              "",
            };

            string category = "diretors_member";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                });

                i++;
            }

            return data;

            /*  if (context.downloads.ToList().Count == 0)
              {
                  data.ForEach(s => context.downloads.Add(s));
                  context.SaveChanges();
              }*/
        }

        /// <summary>
        /// 獨立董事選任資訊
        /// </summary>
        public List<downloads> diretors_info()
        {
            List<string> temp = new List<string>
            {
                "獨立董事選任資訊",

            };

            List<string> startAt = new List<string>
            {
            ""
            };

            string category = "diretors_info";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "en",
                });

                i++;
            }

            return data;

            /*  if (context.downloads.ToList().Count == 0)
              {
                  data.ForEach(s => context.downloads.Add(s));
                  context.SaveChanges();
              }*/
        }


        /// <summary>
        /// 董事會決議事項
        /// </summary>
        public List<downloads> diretors_resolution()
        {
            List<string> temp = new List<string>
            {
                "2020年董事會決議事項",
                "2019年董事會決議事項",
                "2018年董事會決議事項",
                "2017年董事會決議事項",
                "2016年董事會決議事項",
                "2015年董事會決議事項",
                "2014年董事會決議事項",
                "2013年董事會決議事項",

            };

            List<string> startAt = new List<string>
            {
              "2020",
              "2019",
              "2018",
              "2017",
              "2016",
              "2015",
              "2014",
              "2013",
            };

            string category = "diretors_resolution";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });

                i++;
            }


            return data;

            /*  if (context.downloads.ToList().Count == 0)
              {
                  data.ForEach(s => context.downloads.Add(s));
                  context.SaveChanges();
              }*/
        }


        /// <summary>
        /// 公司規章
        /// </summary>
        public List<downloads> regulations()
        {
            List<string> temp = new List<string>
            {
                "重要事項核決權限",
                "內部重大資訊處理暨防範內線交易管理作業程序",
                "薪資報酬委員會組織規程",
                "薪資報酬委員會組織規程",
                "董事會議事規範",
                "取得或處分資產處理程序",
                "背書保證作業程序",
                "資金貸與他人作業程序",
                "從事衍生性商品交易處理程序",
                "審計委員會組織規程",
                "董事會績效評估辦法",

            };

            List<string> startAt = new List<string>
            {
              "2021",
              "2020",
              "2020",
              "2020",
              "2020",
              "2020",
              "2020",
              "2020",
              "2020",
              "2019",
              "2019",
            };

            string category = "regulations";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });

                i++;
            }

            return data;

            /*  if (context.downloads.ToList().Count == 0)
              {
                  data.ForEach(s => context.downloads.Add(s));
                  context.SaveChanges();
              }*/
        }


        /// 內部稽核組織及運作
        /// </summary>
        public List<downloads> audit_master()
        {
            List<string> temp = new List<string>
            {
                "內部稽核組織及運作",

            };

            List<string> startAt = new List<string>
            {
          
            };

            string category = "audit_master";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                });

                i++;
            }

            return data;

            /*  if (context.downloads.ToList().Count == 0)
              {
                  data.ForEach(s => context.downloads.Add(s));
                  context.SaveChanges();
              }*/
        }

        /// 內部稽核組織及運作
        /// </summary>
        public List<downloads> audit_implement()
        {
            List<string> temp = new List<string>
            {
                "公司治理專(兼)職單位之運作及執行情形",

            };

            List<string> startAt = new List<string>
            {

            };

            string category = "audit_implement";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                });

                i++;
            }

            return data;
        }


        /// 獨立董事與內部稽核主管及會計師之溝通情形
        /// </summary>
        public List<downloads> audit_comm()
        {
            List<string> temp = new List<string>
            {
                "獨立董事與內部稽核主管及會計師之溝通方式",

            };

            List<string> startAt = new List<string>
            {

            };

            string category = "audit_comm";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                });

                i++;
            }
            return data;
        }

        /// 功能性委員會
        /// </summary>
        public List<downloads> audit_member()
        {
            List<string> temp = new List<string>
            {
                "功能性委員會",

            };

            List<string> startAt = new List<string>
            {

            };

            string category = "audit_member";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                });

                i++;
            }
            return data;
        }



        /// 關係企業組織圖
        /// </summary>
        public List<downloads> organization()
        {
            List<string> temp = new List<string>
            {
                "關係企業組織圖",

            };

            List<string> startAt = new List<string>
            {
                "2019",
            };

            string category = "organization";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });

                i++;
            }

            return data;
        }

        /// 各關係企業營運概況
        /// </summary>
        public List<downloads> organization_service()
        {
            List<string> temp = new List<string>
            {
                "各關係企業營運概況",

            };

            List<string> startAt = new List<string>
            {
                "2019",
            };

            string category = "organization_service";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });

                i++;
            }


            return data;
        }


        /// 投資人資訊
        /// </summary>
        public List<downloads> investor()
        {
            List<string> temp = new List<string>
            {
                "歷年股價",
                "股權結構",

            };

            List<string> startAt = new List<string>
            {
               
            };

            string category = "investor";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                });

                i++;
            }

            return data;
        }


        /// 法人說明會
        /// </summary>
        public List<downloads> corporate()
        {
            List<string> temp = new List<string>
            {
                "2020年12月14日法人說明會",
                "2020年11月10日至109年11月11日法人說明會",
                "2020年10月13日法人說明會",
                "2019年09月05日法人說明會",
                "2019年05月23日法人說明會",
                "2019年3月19日至 108年3月20日法人說明會。",
                "2018年11月28日法人說明會",
                "2018年09月05日法人說明會",
                "2018年07月09日-07月10日法人說明會",
                "2018年05月17日-05月18日法人說明會",
                "2018年05月16日法人說明會",
                "2018年04月16日法人說明會",
                "2018年03月15日法人說明會",
                "2017年11月21日-11月24日法人說明會",
                "2017年06月21日法人說明會",
                "2017年05月24日法人說明會",
                "2017年05月16日法人說明會",
                "2017年02月20日~24日 法人說明會",
                "2016年02月24日~25日 法人說明會",


            };

            List<string> startAt = new List<string>
            {
                "2020",
                "2020",
                "2020",
                "2019",
                "2019",
                "2019",
                "2018",
                "2018",
                "2018",
                "2018",
                "2018",
                "2018",
                "2018",
                "2017",
                "2017",
                "2017",
                "2017",
                "2017",
                "2016",

            };

            string category = "corporate";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });

                i++;
            }
            return data;
        }


        /// 月營業額報告
        /// </summary>
        public List<downloads> monthly_report()
        {
            List<string> temp = new List<string>
            {
                "2020年合併月營收報告",
                "2019年合併月營收報告",
                "2018年合併月營收報告",
                "2017年合併月營收報告",
                "2016年合併月營收報告",
                "2015年合併月營收報告",
                "2014年合併月營收報告",
                "2013年合併月營收報告",
                "2012年合併月營收報告",
                "2011年合併月營收報告",

            };

            List<string> startAt = new List<string>
            {
                "2020",              
                "2019",              
                "2018",                
                "2017",
                "2016",
                "2015",
                "2014",
                "2013",
                "2012",
                "2011",

            };

            string category = "monthly_report";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });

                i++;
            }
            return data;
        }



        /// 財務報告
        /// </summary>
        public List<downloads> financial_report()
        {
            List<string> temp = new List<string>
            {
                "2020年第三季合併財務報告暨會計師核閱報告",
                "2020年第二季合併財務報告暨會計師核閱報告",
                "2020年第一季合併財務報告暨會計師核閱報告",
                "2019年第四季合併財務報告暨會計師核閱報告",
                "2019年第四季財務報告書",
                "2019年第三季合併財務報告暨會計師核閱報告",
                "2019年第二季合併財務報告暨會計師核閱報告",
                "2019年第一季合併財務報告暨會計師核閱報告",
                "2018年第四季合併財務報告暨會計師核閱報告",
                "2018年第四季財務報告書",
                "2018年第三季合併財務報告暨會計師核閱報告",
                "2018年第二季合併財務報告暨會計師核閱報告",
                "2018年第一季合併財務報告暨會計師核閱報告",
                "2017年第四季合併財務報告暨會計師核閱報告",
                "2017年第四季財務報告書",
                "2017年第三季合併財務報告暨會計師核閱報告",
                "2017年第二季合併財務報告暨會計師核閱報告",
                "2017年第一季合併財務報告暨會計師核閱報告",

                "2016年第四季合併財務報告暨會計師核閱報告",
                "2016年第四季財務報告書",
                "2016年第三季合併財務報告暨會計師核閱報告",
                "2016年第二季合併財務報告暨會計師核閱報告",
                "2016年第一季合併財務報告暨會計師核閱報告",
            };

            List<string> startAt = new List<string>
            {
                "2020",              
                "2020",              
                "2020",                
                "2019",
                "2019",
                "2019",
                "2019",
                "2019",
                "2018",
                "2018",
                "2018",
                "2018",
                "2018",
                "2017",
                "2017",
                "2017",
                "2017",
                "2017",
                "2016",
                "2016",
                "2016",
                "2016",
                "2016",
            };

            string category = "financial_report";


            List<downloads> data = new List<downloads>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "tw",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });
                data.Add(new downloads
                {
                    guid = category + (i + 1).ToString(),
                    category = category,
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    file = "Upload/files/test.pdf",
                    lang = "cn",
                    startdate = DateTime.Parse(startAt[i].ToString() + "-01-01"),
                });

                i++;
            }

            return data;
        }


        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}