﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;
using Web.DB.Models;

namespace Web.Migrations.Seed
{
    public class LocationSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            data();
           
        }


        public void data()
        {

            List<string> temp = new List<string>
            {
                "台北總公司",
                "東莞胡連普光貿易有限公司",
                "胡連電子(越南)責任有限公司 【越南】",
                "東莞胡連電子科技有限公司【中國大陸】",
                "胡連科技製造有限責任公司【印尼】",
                "胡連公司【歐洲辦事處】Hu Lane Associate Incorporated",
                "胡連電子(南京)有限公司【中國大陸】",
                "胡連精密股份有限公司【香港分公司】",
                "胡連精密股份有限公司【上海聯絡處】",
                "胡連精密股份有限公司【武漢聯絡處】",
                "胡連精密股份有限公司【重慶聯絡處】",
                "胡連精密股份有限公司【華北聯絡處】",
                "胡連精密股份有限公司【蕪湖聯絡處】",
                "胡連精密股份有限公司【福州聯絡處】",
                "胡連精密股份有限公司【長春聯絡處】",
                "胡連精密股份有限公司【西安聯絡處】",
                "胡連精密股份有限公司【柳州聯絡處】",
                "胡連精密股份有限公司【長沙聯絡處】",
                "胡連精密股份有限公司【臺州聯絡處】",
                "胡連精密股份有限公司【鄭州聯絡處】",

            };

            List<string> temp_cn = new List<string>
            {
              "台北总公司",
                "东莞胡连普光贸易有限公司",
                "胡连电子(越南)责任有限公司 【越南】",
                "东莞胡连电子科技有限公司【中国大陆】",
                "胡连科技制造有限责任公司【印尼】",
                "胡连公司【欧洲办事处】Hu Lane Associate Incorporated",
                "胡连电子(南京)有限公司【中国大陆】",
                "胡连精密股份有限公司【香港分公司】",
                "胡连精密股份有限公司【上海联络处】",
                "胡连精密股份有限公司【武汉联络处】",
                "胡连精密股份有限公司【重庆联络处】",
                "胡连精密股份有限公司【华北联络处】",
                "胡连精密股份有限公司【芜湖联络处】",
                "胡连精密股份有限公司【福州联络处】",
                "胡连精密股份有限公司【长春联络处】",
                "胡连精密股份有限公司【西安联络处】",
                "胡连精密股份有限公司【柳州联络处】",
                "胡连精密股份有限公司【长沙联络处】",
                "胡连精密股份有限公司【台州联络处】",
                "胡连精密股份有限公司【郑州联络处】",
            };




            List<locations> data = new List<locations>();
            int i = 0;

            foreach(string title in temp)
            {

                data.Add(new locations
                {
                    guid = (i + 1).ToString(),
                    title = title,
                    content = MapPath("/Content/Templates/About/Locations/" + (i + 1).ToString() + ".html"),
                    modifydate = DateTime.Now,
                    map = "#",
                    email = "marketing@hulane.com.tw",
                    pic = "Styles/images/index/global-1.jpg",
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                }); ;
                data.Add(new locations
                {
                    guid = (i + 1).ToString(),
                    title = temp_cn[i].ToString(),
                    content = MapPath("/Content/Templates/About/Locations/" + (i + 1).ToString() + ".html"),
                    map = "#",
                    email = "marketing@hulane.com.tw",
                    pic = "Styles/images/index/global-1.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = (i + 1),
                });

                i++;
            }


            if (context.locations.ToList().Count == 0)
            {
                data.ForEach(s => context.locations.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}