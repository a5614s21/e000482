﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.DB.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class MemberSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {

            var Users = new List<member>
             {
                new member
                {
                    guid = "001",
                    username = "test001",
                    password = Web.Service.FunctionService.md5("test001"),
                    name = "會員1",
                    email = "m0205@minmax.tw",
                    phone = "",
                    mobile = "",
                    address = "",
                    note = "",
                    status = "Y",
                    logindate = DateTime.Now,
                    modifydate = DateTime.Now,
                    create_date = DateTime.Now
                },
                 new member
                {
                    guid = "002",
                    username = "test002",
                    password = Web.Service.FunctionService.md5("test002"),
                    name = "會員2",
                    email = "m0205@minmax.tw",
                    phone = "",
                    mobile = "",
                    address = "",
                    note = "",
                    status = "Y",
                    logindate = DateTime.Now,
                    modifydate = DateTime.Now,
                    create_date = DateTime.Now
                },


             };
            if (context.member.ToList().Count == 0)
            {
                Users.ForEach(s => context.member.Add(s));
                context.SaveChanges();
            }

            

            
        }

 

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}