﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.DB.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class NewsSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            category();
            data1("1");
            data2("2");
            data3("3");
            data4("4");
        }


        public void category()
        {
            //最新消息分類
            var news_category = new List<news_category>
            {
                 new news_category {
                    guid = "1",
                    title = "熱門訊息",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                    seo_description = "熱門訊息",
                    seo_keywords = "熱門訊息",
                  },
                 new news_category {
                    guid = "1",
                    title = "热门讯息",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 1,
                    seo_description = "热门讯息",
                    seo_keywords = "热门讯息",
                  },

                 new news_category {
                    guid = "2",
                    title = "最新展覽",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                    seo_description = "最新展覽",
                    seo_keywords = "最新展覽",
                  },
                 new news_category {
                    guid = "2",
                    title = "最新展览",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                  lang = "cn",
                    sortIndex = 2,
                    seo_description = "最新展览",
                    seo_keywords = "最新展览",
                  },
                  new news_category {
                    guid = "3",
                    title = "歷史展覽",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 3,
                    seo_description = "歷史展覽",
                    seo_keywords = "歷史展覽",
                  },
                    new news_category {
                    guid = "3",
                    title = "历史展览",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 3,
                    seo_description = "历史展览",
                    seo_keywords = "历史展览",
                  },
            };
            if (context.news_category.ToList().Count == 0)
            {
                news_category.ForEach(s => context.news_category.Add(s));
                context.SaveChanges();
            }
        }

        public void data1(string category)
        {
            //最新消息
            var news = new List<news>
            {
                 new news {
                    guid = category+"1",
                    title = "產品顏色標準制定公告",
                    category = category,
                    notes = "為了提供顧客更優質的產品服務，在持續改善計畫中，當前致力於產品顏色的制定，此將依據",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "產品顏色標準制定公告",
                    seo_keywords = "產品顏色標準制定公告",
                    startdate = DateTime.Parse("2020-07-22 00:00:00"),
                    pic = "/Styles/images/news/1.jpg",
                    tops = "Y",
                  },
                  new news {
                   guid = category+"1",
                    title = "產品顏色標準制定公告",
                    category = category,
                    notes = "為了提供顧客更優質的產品服務，在持續改善計畫中，當前致力於產品顏色的制定，此將依據",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "產品顏色標準制定公告",
                    seo_keywords = "產品顏色標準制定公告",
                    startdate = DateTime.Parse("2020-07-22 00:00:00"),
                    pic = "/Styles/images/news/1.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"2",
                    title = "歡迎參觀胡連2019年名古屋汽車技術週!",
                      notes = "為了提供顧客更優質的產品服務，在持續改善計畫中，當前致力於產品顏色的制定，此將依據",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "歡迎參觀胡連2019年名古屋汽車技術週!",
                    seo_keywords = "歡迎參觀胡連2019年名古屋汽車技術週!",
                    startdate = DateTime.Parse("2020-06-09 00:00:00"),
                    pic = "/Styles/images/news/2.jpg",
                    tops = "Y",
                  },
                  new news {
                     guid = category+"2",
                    title = "歡迎參觀胡連2019年名古屋汽車技術週!",
                      notes = "為了提供顧客更優質的產品服務，在持續改善計畫中，當前致力於產品顏色的制定，此將依據",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "歡迎參觀胡連2019年名古屋汽車技術週!",
                    seo_keywords = "歡迎參觀胡連2019年名古屋汽車技術週!",
                    startdate = DateTime.Parse("2020-06-09 00:00:00"),
                    pic = "/Styles/images/news/2.jpg",
                    tops = "Y",
                  },

                  new news {
                    guid = category+"3",
                    title = "產品顏色標準制定公告",
                      notes = "為了提供顧客更優質的產品服務，在持續改善計畫中，當前致力於產品顏色的制定，此將依據",
                     category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "產品顏色標準制定公告",
                    seo_keywords = "產品顏色標準制定公告",
                    startdate = DateTime.Parse("2020-03-31 00:00:00"),
                    pic = "/Styles/images/news/3.jpg",
                    tops = "Y",
                  },
                  new news {
                  guid = category+"3",
                    title = "产品颜色标准制定公告",
                      notes = "為了提供顧客更優質的產品服務，在持續改善計畫中，當前致力於產品顏色的制定，此將依據",
                     category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "产品颜色标准制定公告",
                    seo_keywords = "产品颜色标准制定公告",
                    startdate = DateTime.Parse("2020-03-31 00:00:00"),
                    pic = "/Styles/images/news/3.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"4",
                    title = "2019 9-10月份新品上市!",
                      notes = "為了提供顧客更優質的產品服務，在持續改善計畫中，當前致力於產品顏色的制定，此將依據",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "2019 9-10月份新品上市!",
                    seo_keywords = "2019 9-10月份新品上市!",
                    startdate = DateTime.Parse("2019-12-23 00:00:00"),
                    pic = "/Styles/images/news/4.jpg",
                    tops = "Y",
                  },

                   new news {
                   guid = category+"4",
                    title = "2019 9-10月份新品上市!",
                      notes = "為了提供顧客更優質的產品服務，在持續改善計畫中，當前致力於產品顏色的制定，此將依據",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "2019 9-10月份新品上市!",
                    seo_keywords = "2019 9-10月份新品上市!",
                    startdate = DateTime.Parse("2019-12-23 00:00:00"),
                    pic = "/Styles/images/news/4.jpg",
                    tops = "Y",
                  },
               

            };
            if (context.news.ToList().Count == 0)
            {
                news.ForEach(s => context.news.Add(s));
                context.SaveChanges();
            }
        }

        public void data2(string category)
        {
            //最新消息
            var news = new List<news>
            {
                 new news {
                    guid = category+"1",
                    title = "2020 5-6月份新品上市!",
                    category = category,
                    notes = "",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "2020 5-6月份新品上市",
                    seo_keywords = "2020 5-6月份新品上市",
                  startdate = DateTime.Parse("2020-07-22 00:00:00"),
                    pic = "/Styles/images/news/1.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"1",
                   title = "2020 5-6月份新品上市!",
                      category = category,
                    notes = "",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "2020 5-6月份新品上市",
                    seo_keywords = "2020 5-6月份新品上市",
                   startdate = DateTime.Parse("2020-07-22 00:00:00"),
                   pic = "/Styles/images/news/1.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"2",
                    title = "2020 3-4月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "2020 3-4月份新品上市!",
                    seo_keywords = "2020 3-4月份新品上市!",
                    startdate = DateTime.Parse("2020-06-09 00:00:00"),
                    pic = "/Styles/images/news/2.jpg",
                    tops = "Y",
                  },
                  new news {
                  guid = category+"2",
                    title = "2020 3-4月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "2020 3-4月份新品上市!",
                    seo_keywords = "2020 3-4月份新品上市!",
                    startdate = DateTime.Parse("2020-06-09 00:00:00"),
                    pic = "/Styles/images/news/2.jpg",
                    tops = "Y",
                  },

                  new news {
                    guid = category+"3",
                    title = "產品顏色標準制定公告",
                     category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "產品顏色標準制定公告",
                    seo_keywords = "產品顏色標準制定公告",
                    startdate = DateTime.Parse("2020-03-31 00:00:00"),
                    pic = "/Styles/images/news/3.jpg",
                    tops = "Y",
                  },
                  new news {
                  guid = category+"3",
                    title = "产品颜色标准制定公告",
                     category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "产品颜色标准制定公告",
                    seo_keywords = "产品颜色标准制定公告",
                    startdate = DateTime.Parse("2020-03-31 00:00:00"),
                    pic = "/Styles/images/news/3.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"4",
                    title = "2019 9-10月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "2019 9-10月份新品上市!",
                    seo_keywords = "2019 9-10月份新品上市!",
                    startdate = DateTime.Parse("2019-12-23 00:00:00"),
                    pic = "/Styles/images/news/4.jpg",
                    tops = "Y",
                  },

                   new news {
                   guid = category+"4",
                    title = "2019 9-10月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "2019 9-10月份新品上市!",
                    seo_keywords = "2019 9-10月份新品上市!",
                    startdate = DateTime.Parse("2019-12-23 00:00:00"),
                    pic = "/Styles/images/news/4.jpg",
                    tops = "Y",
                  },


            };
            if (context.news.ToList().Count == 0)
            {
                news.ForEach(s => context.news.Add(s));
                context.SaveChanges();
            }
        }

        public void data3(string category)
        {
            //最新消息
            var news = new List<news>
            {
                 new news {
                    guid = category+"1",
                    title = "2020 5-6月份新品上市!",
                    category = category,
                    notes = "",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "2020 5-6月份新品上市",
                    seo_keywords = "2020 5-6月份新品上市",
                  startdate = DateTime.Parse("2020-07-22 00:00:00"),
                    pic = "/Styles/images/news/1.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"1",
                   title = "2020 5-6月份新品上市!",
                      category = category,
                    notes = "",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "2020 5-6月份新品上市",
                    seo_keywords = "2020 5-6月份新品上市",
                   startdate = DateTime.Parse("2020-07-22 00:00:00"),
                   pic = "/Styles/images/news/1.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"2",
                    title = "2020 3-4月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "2020 3-4月份新品上市!",
                    seo_keywords = "2020 3-4月份新品上市!",
                    startdate = DateTime.Parse("2020-06-09 00:00:00"),
                    pic = "/Styles/images/news/2.jpg",
                    tops = "Y",
                  },
                  new news {
                  guid = category+"2",
                    title = "2020 3-4月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "2020 3-4月份新品上市!",
                    seo_keywords = "2020 3-4月份新品上市!",
                    startdate = DateTime.Parse("2020-06-09 00:00:00"),
                    pic = "/Styles/images/news/2.jpg",
                    tops = "Y",
                  },

                  new news {
                    guid = category+"3",
                    title = "產品顏色標準制定公告",
                     category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "產品顏色標準制定公告",
                    seo_keywords = "產品顏色標準制定公告",
                    startdate = DateTime.Parse("2020-03-31 00:00:00"),
                    pic = "/Styles/images/news/3.jpg",
                    tops = "Y",
                  },
                  new news {
                  guid = category+"3",
                    title = "产品颜色标准制定公告",
                     category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "产品颜色标准制定公告",
                    seo_keywords = "产品颜色标准制定公告",
                    startdate = DateTime.Parse("2020-03-31 00:00:00"),
                    pic = "/Styles/images/news/3.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"4",
                    title = "2019 9-10月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "2019 9-10月份新品上市!",
                    seo_keywords = "2019 9-10月份新品上市!",
                    startdate = DateTime.Parse("2019-12-23 00:00:00"),
                    pic = "/Styles/images/news/4.jpg",
                    tops = "Y",
                  },

                   new news {
                   guid = category+"4",
                    title = "2019 9-10月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "2019 9-10月份新品上市!",
                    seo_keywords = "2019 9-10月份新品上市!",
                    startdate = DateTime.Parse("2019-12-23 00:00:00"),
                    pic = "/Styles/images/news/4.jpg",
                    tops = "Y",
                  },


            };
            if (context.news.ToList().Count == 0)
            {
                news.ForEach(s => context.news.Add(s));
                context.SaveChanges();
            }
        }


        public void data4(string category)
        {
            //最新消息
            var news = new List<news>
            {
                 new news {
                    guid = category+"1",
                    title = "2020 5-6月份新品上市!",
                    category = category,
                    notes = "",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "2020 5-6月份新品上市",
                    seo_keywords = "2020 5-6月份新品上市",
                  startdate = DateTime.Parse("2020-07-22 00:00:00"),
                    pic = "/Styles/images/news/1.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"1",
                   title = "2020 5-6月份新品上市!",
                      category = category,
                    notes = "",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "2020 5-6月份新品上市",
                    seo_keywords = "2020 5-6月份新品上市",
                   startdate = DateTime.Parse("2020-07-22 00:00:00"),
                   pic = "/Styles/images/news/1.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"2",
                    title = "2020 3-4月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "2020 3-4月份新品上市!",
                    seo_keywords = "2020 3-4月份新品上市!",
                    startdate = DateTime.Parse("2020-06-09 00:00:00"),
                    pic = "/Styles/images/news/2.jpg",
                    tops = "Y",
                  },
                  new news {
                  guid = category+"2",
                    title = "2020 3-4月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "2020 3-4月份新品上市!",
                    seo_keywords = "2020 3-4月份新品上市!",
                    startdate = DateTime.Parse("2020-06-09 00:00:00"),
                    pic = "/Styles/images/news/2.jpg",
                    tops = "Y",
                  },

                  new news {
                    guid = category+"3",
                    title = "產品顏色標準制定公告",
                     category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "產品顏色標準制定公告",
                    seo_keywords = "產品顏色標準制定公告",
                    startdate = DateTime.Parse("2020-03-31 00:00:00"),
                    pic = "/Styles/images/news/3.jpg",
                    tops = "Y",
                  },
                  new news {
                  guid = category+"3",
                    title = "产品颜色标准制定公告",
                     category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "产品颜色标准制定公告",
                    seo_keywords = "产品颜色标准制定公告",
                    startdate = DateTime.Parse("2020-03-31 00:00:00"),
                    pic = "/Styles/images/news/3.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = category+"4",
                    title = "2019 9-10月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "2019 9-10月份新品上市!",
                    seo_keywords = "2019 9-10月份新品上市!",
                    startdate = DateTime.Parse("2019-12-23 00:00:00"),
                    pic = "/Styles/images/news/4.jpg",
                    tops = "Y",
                  },

                   new news {
                   guid = category+"4",
                    title = "2019 9-10月份新品上市!",
                   category = category,
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "2019 9-10月份新品上市!",
                    seo_keywords = "2019 9-10月份新品上市!",
                    startdate = DateTime.Parse("2019-12-23 00:00:00"),
                    pic = "/Styles/images/news/4.jpg",
                    tops = "Y",
                  },


            };
            if (context.news.ToList().Count == 0)
            {
                news.ForEach(s => context.news.Add(s));
                context.SaveChanges();
            }
        }


        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}