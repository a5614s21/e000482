﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;
using Web.DB.Models;

namespace Web.Migrations.Seed
{
    public class NotesSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            data();
           
        }

    
        public void data()
        {
            var notes_data = new List<notes_data>
            {
                 new notes_data {
                    guid = "1",
                    title = "客戶滿意度調查",
                    content = MapPath("/Content/Templates/Notes/survey.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "客戶滿意度調查",
                    seo_keywords = "客戶滿意度調查",
                  },
                 new notes_data {
                    guid = "1",
                    title = "客户满意度调查",
                    content = MapPath("/Content/Templates/Notes/survey.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "客户满意度调查",
                    seo_keywords = "人才客户满意度调查招募",
                  },
                 new notes_data {
                    guid = "2",
                    title = "隱私權政策",
                     content = MapPath("/Content/Templates/Notes/Privacy.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "隱私權政策",
                    seo_keywords = "隱私權政策",
                  },
                 new notes_data {
                    guid = "2",
                    title = "隐私权政策",
                    content = MapPath("/Content/Templates/Notes/Privacy.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "隐私权政策",
                    seo_keywords = "隐私权政策",
                  },
                 new notes_data {
                    guid = "3",
                    title = "使用者條款",
                    content = MapPath("/Content/Templates/Notes/Terms.html"),                
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "使用者條款",
                    seo_keywords = "使用者條款",
                  },
                 new notes_data {
                    guid = "3",
                    title = "使用者条款",
                    content = MapPath("/Content/Templates/Notes/Terms.html"),                  
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "使用者条款",
                    seo_keywords = "使用者条款",
                  },

                new notes_data {
                    guid = "4",
                    title = "利害關係人",
                    content = MapPath("/Content/Templates/Notes/stakeholder.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "利害關係人",
                    seo_keywords = "利害關係人",
                  },
                 new notes_data {
                    guid = "4",
                    title = "利害关系人",
                    content = MapPath("/Content/Templates/Notes/stakeholder.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "利害关系人",
                    seo_keywords = "利害关系人",
                  },
                
                     new notes_data {
                    guid = "5",
                    title = "投資人服務",
                    content = MapPath("/Content/Templates/Notes/service.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "投資人服務",
                    seo_keywords = "投資人服務",
                  },
                 new notes_data {
                    guid = "5",
                    title = "投资人服务",
                    content = MapPath("/Content/Templates/Notes/service.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    seo_description = "投资人服务",
                    seo_keywords = "投资人服务",
                  },
                 /*
                   new notes_data {
                    guid = "6",
                    title = "投資人專區-其他資訊",
                    content = MapPath("/Content/Templates/Notes/InvestorOthers.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "投資人專區-其他資訊",
                    seo_keywords = "投資人專區-其他資訊",
                  },
                 new notes_data {
                    guid = "6",
                    title = "投資人專區-其他資訊",
                    content = MapPath("/Content/Templates/Notes/InvestorOthers.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "投資人專區-其他資訊",
                    seo_keywords = "投資人專區-其他資訊",
                  },*/

            };
            if (context.notes_data.ToList().Count == 0)
            {
                notes_data.ForEach(s => context.notes_data.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}