﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.DB.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class ProblemSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
         

            List<problem_category> categoryList = category();
            List<problem> dataList = data();
         
                      
            /*

            if (context.problem_category.ToList().Count == 0)
            {
                categoryList.ForEach(s => context.problem_category.Add(s));
                context.SaveChanges();
            }*/
        }

        public List<problem_category> category()
        {
            List<string> temp = new List<string>
            {
                "一、產品品質",
                "二、產品交期",
                "三、產品回饋處理",
                "四、新品/技術服務",
                "五、產品目錄資訊/品牌服務",

            };

           

            List<problem_category> data = new List<problem_category>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new problem_category
                {
                    guid = "category" + (i + 1).ToString(),
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                    
                });
                data.Add(new problem_category
                {
                    guid = "category" + (i + 1).ToString(),
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "cn",

                });

                i++;
            }

    

            
            if (context.problem_category.ToList().Count == 0)
            {
                data.ForEach(s => context.problem_category.Add(s));
                context.SaveChanges();
            }

            return data;
        }

        public List<problem> data()
        {
            List<string> temp = new List<string>
            {
                "1.產品『首次送樣』，即能符合貴司之產品使用需求?",
                "2.連接器產品品質&功能，符合貴司使用需求?",
                "3.產品交貨包裝，符合貴司收貨品質需求?",
                "1.本公司人員是否適時告知您訂單的目前處理狀況?",
                "2.產品交貨:日期、數量、品項，是否依照雙方約定交貨?",
                "1.產品回饋處理，『異常回覆』是否能滿足貴司需求?",
                "2.產品回饋處理，『處理時效』是否滿足貴司需求?",
                "3.對於產品回饋處理，保持良好『持續追蹤服務』?",
                "1.本公司的產品種類 / 範圍，符合貴司需求 ?",
                "2.本公司之『新品開發件速度』，符合貴司約定之時程?",
                "3.本公司提供的『產品資料/新品資訊』，是否滿足貴司需求?",
                "1.產品型錄&產品規格資訊，是否滿足貴司需求?",
                "2.本公司網站內容，是否提供完整的內容資訊?",
                "3.本公司產品『價格、品質、服務』，相較於同業是否具有競爭力?",
                "4.本公司的業務服務態度是否滿意?",
                "5.您願意將公司產品介紹給同業?",

            };

            List<string> category = new List<string>
            {
                "category1",
                "category1",
                "category1",
                "category2",
                "category2",
                "category3",
                "category3",
                "category3",
                "category4",
                "category4",
                "category4",
                "category5",
                "category5",
                "category5",
                "category5",
                "category5",
            };


            List<problem> data = new List<problem>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new problem
                {
                    guid = (i + 1).ToString(),
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                    category = category[i]

                });
                data.Add(new problem
                {
                    guid = (i + 1).ToString(),
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "cn",
                    category = category[i]
                });

                i++;
            }




            if (context.problem.ToList().Count == 0)
            {
                data.ForEach(s => context.problem.Add(s));
                context.SaveChanges();
            }

            return data;
        }


        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}