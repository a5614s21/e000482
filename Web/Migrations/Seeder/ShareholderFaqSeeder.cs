﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;
using Web.DB.Models;

namespace Web.Migrations.Seed
{
    public class ShareholderFaqSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            data();
           
        }

    
        public void data()
        {

            List<string> temp = new List<string>
            {
                "胡連精密股份有限公司(以下簡稱胡連)何時設立?",
                "胡連是何時上櫃的?公司的股票代號為何？",
                "胡連的會計年度為何?",
                "您可與誰聯絡有關公司普通股之相關問題?",
                "民國109年股東常會何時召開?",
                "如何才能參與除權除息?",
                "配發股票股利時，是否會配發不足一股之畸零股？畸零股是否以發放現金來替代？",
                "如何可以查詢到公司歷年股利？",
                "胡連如何及何時發放股東股利?",
                "胡連的股利政策為何？",
            };
            List<string> editor = new List<string>
            {
               "<p class=\"mb-0\">本公司設立於中華民國六十六年七月九日。</p>",
               "<p class=\"mb-0\">請參考地圖及建議交通方式 ，請連結網址<a class=\"text-red ml-2\" href=\"http://www.hulane.com/tc/p8-career-4.asp\">http://www.hulane.com/tc/p8-career-4.asp</a></p>",
               "<img class=\"mb-3\" src=\"Styles/images/about/overview/img.png\" alt=\"胡連精密\"><p class=\"mb-0\">胡連精密成立於1977年，創立時從事金屬沖壓端子零件產品，於2004年涉入塑膠連接器產品，如今已成為汽機車業界專業連接器廠；並跨足醫療、太陽能等各方面領域。胡連不斷努力精進提高模具、製程等相關技術開發能力，包括點焊端子、電動車連接器、保險絲盒、壓著機、橡膠件之自製開發產品，並拓展銷售於全球市場。</p>",
               "<div class=\"row\"><div class=\"col-12 col-md-6 col-lg-3 mb-3\"><img src =\"Styles/images/index/global-1.jpg\" alt=\"胡連精密\"></div><div class=\"col-12 col-md-6 col-lg-3 mb-3\"><img src =\"Styles/images/index/global-2.jpg\" alt=\"胡連精密\"></div><div class=\"col-12 col-md-6 col-lg-3 mb-3\"><img src =\"Styles/images/index/global-1.jpg\" alt=\"胡連精密\"></div><div class=\"col-12 col-md-6 col-lg-3 mb-3\"><img src =\"Styles/images/index/global-2.jpg\" alt=\"胡連精密\"></div></div>",
               "<p class=\"mb-0\">胡連秉持「誠信務實、技術創新、客戶滿意、共榮共存、放眼天下」之經營理念；「同仁團隊合作、勇於挑戰、追求卓越」的企業精神、及四大發展方針『品質』、『專業』、『效率』、『服務』作為動力，持續向上成長。為滿足客戶的期望努力、求新求變實現 “Connecting Worldwide”，讓胡連連接器永續發展，連結全世界！</p>",
               "<p class=\"mb-3\">胡連精密成立於1977年，創立時從事金屬沖壓端子零件產品，於2004年涉入塑膠連接器產品，如今已成為汽機車業界專業連接器廠；並跨足醫療、太陽能等各方面領域。胡連不斷努力精進提高模具、製程等相關技術開發能力，包括點焊端子、電動車連接器、保險絲盒、壓著機、橡膠件之自製開發產品，並拓展銷售於全球市場。</p><img class=\"mb-0\" src=\"Styles/images/about/overview/img.png\" alt=\"胡連精密\">",
               "<div class=\"row mb-3\"><div class=\"col-12 col-lg-4 mb-2\"><img src =\"Styles/images/news/1.jpg\" alt=\"胡連精密\"></div><div class=\"col-12 col-lg-4 mb-2\"><img src =\"Styles/images/news/2.jpg\" alt=\"胡連精密\"></div><div class=\"col-12 col-lg-4 mb-2\"><img src =\"Styles/images/news/3.jpg\" alt=\"胡連精密\"></div></div><p class=\"mb-0\">胡連擁有數十年技術經驗及高效率之銷售服務團隊，以台北為總公司，大陸於南京及東莞、東南亞於越南、印尼設立工廠及銷售團隊，形成堅強全球團隊，在業界建立良好商譽。為提供可靠及實用的產品，除了管理體系通過多項國際品質管理認證外，產品亦符合RoHS等相關標準，給予客戶品質信賴的產品。</p>",
               "<p class=\"mb-3\">胡連精密成立於1977年，創立時從事金屬沖壓端子零件產品，於2004年涉入塑膠連接器產品，如今已成為汽機車業界專業連接器廠；並跨足醫療、太陽能等各方面領域。胡連不斷努力精進提高模具、製程等相關技術開發能力，包括點焊端子、電動車連接器、保險絲盒、壓著機、橡膠件之自製開發產品，並拓展銷售於全球市場。</p><img class=\"mb-0\" src=\"Styles/images/about/overview/img.png\" alt=\"胡連精密\">",
               "<p class=\"mb-3\">胡連精密成立於1977年，創立時從事金屬沖壓端子零件產品，於2004年涉入塑膠連接器產品，如今已成為汽機車業界專業連接器廠；並跨足醫療、太陽能等各方面領域。胡連不斷努力精進提高模具、製程等相關技術開發能力，包括點焊端子、電動車連接器、保險絲盒、壓著機、橡膠件之自製開發產品，並拓展銷售於全球市場。</p><img class=\"mb-0\" src=\"Styles/images/about/overview/img.png\" alt=\"胡連精密\">",
               "<p class=\"mb-3\">胡連精密成立於1977年，創立時從事金屬沖壓端子零件產品，於2004年涉入塑膠連接器產品，如今已成為汽機車業界專業連接器廠；並跨足醫療、太陽能等各方面領域。胡連不斷努力精進提高模具、製程等相關技術開發能力，包括點焊端子、電動車連接器、保險絲盒、壓著機、橡膠件之自製開發產品，並拓展銷售於全球市場。</p><img class=\"mb-0\" src=\"Styles/images/about/overview/img.png\" alt=\"胡連精密\">",
            };

            List<shareholder_faq> data = new List<shareholder_faq>();
            int i = 0;

            foreach(string title in temp)
            {
                
                data.Add(new shareholder_faq
                {
                    guid = (i+1).ToString(),
                    title = title,
                    notes = editor[i].ToString(),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                });
                data.Add(new shareholder_faq
                {
                    guid = (i + 1).ToString(),
                    title = title,
                    notes = editor[i].ToString(),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = (i + 1),
                });

                i++;
            }


            if (context.shareholder_faq.ToList().Count == 0)
            {
                data.ForEach(s => context.shareholder_faq.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}