﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.DB.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class SystemItemSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
         

            List<system_item_category> categoryList = category();
            List<system_item> dataList = data();
         
                      
            /*

            if (context.problem_category.ToList().Count == 0)
            {
                categoryList.ForEach(s => context.problem_category.Add(s));
                context.SaveChanges();
            }*/
        }

        public List<system_item_category> category()
        {
            List<string> temp = new List<string>
            {
                "部門",
                "職稱",
                "客戶區域",
                "國家",

            };

            List<string> guid = new List<string>
            {
                "department",
                "job",
                "area",
                "country",

            };




            List<system_item_category> data = new List<system_item_category>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new system_item_category
                {
                    guid = guid[i],
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                    
                });
                data.Add(new system_item_category
                {
                    guid = guid[i],
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "cn",

                });

                i++;
            }

    

            
            if (context.system_item_category.ToList().Count == 0)
            {
                data.ForEach(s => context.system_item_category.Add(s));
                context.SaveChanges();
            }

            return data;
        }

        public List<system_item> data()
        {
            List<string> temp = new List<string>
            {
               "採購",
               "品保",
               "生產",
               "行政",
               "業務",

               "負責人",
               "高階主管",
               "中階主管",
               "執行專員",
               "助理",

               "北",
               "中",
               "南",

               "台灣",
               "美國",

            };

            List<string> category = new List<string>
            {
                  "department",
               "department",
               "department",
               "department",
               "department",

                "job",
                "job",
                "job",
                "job",
                "job",


                "area",
                "area",
                "area",

                "country",
                "country",
             
            };


            List<system_item> data = new List<system_item>();
            int i = 0;

            foreach (string title in temp)
            {

                data.Add(new system_item
                {
                    guid = (i + 1).ToString(),
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                    category = category[i]

                });
                data.Add(new system_item
                {
                    guid = (i + 1).ToString(),
                    title = title,
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "cn",
                    category = category[i]
                });

                i++;
            }




            if (context.system_item.ToList().Count == 0)
            {
                data.ForEach(s => context.system_item.Add(s));
                context.SaveChanges();
            }

            return data;
        }


        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}