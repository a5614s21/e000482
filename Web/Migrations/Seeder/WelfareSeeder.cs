﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;
using Web.DB.Models;

namespace Web.Migrations.Seed
{
    public class WelfareSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            category();
            data();
           
        }

        public void category()
        {
            //最新消息分類
            var welfare_category = new List<welfare_category>
            {
                 new welfare_category {
                    guid = "1",
                    title = "選擇胡連",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                    seo_description = "選擇胡連",
                    seo_keywords = "選擇胡連",
                  },
                 new welfare_category {
                    guid = "1",
                    title = "选择胡连",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 1,
                    seo_description = "选择胡连",
                    seo_keywords = "选择胡连",
                  },

                 new welfare_category {
                    guid = "2",
                    title = "人才招募",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                    seo_description = "人才招募",
                    seo_keywords = "人才招募",
                  },
                 new welfare_category {
                    guid = "2",
                    title = "人才招募",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "cn",
                    sortIndex = 2,
                    seo_description = "人才招募",
                    seo_keywords = "人才招募",
                  },
               
            };
            if (context.welfare_category.ToList().Count == 0)
            {
                welfare_category.ForEach(s => context.welfare_category.Add(s));
                context.SaveChanges();
            }





        }


        public void data()
        {

            List<string> temp = new List<string>
            {
                "胡連福利",
                "培訓發展",

                "招募流程",
                "招募職缺",
                "招募連絡資訊",
                "如何前往胡連",

            };

            List<string> temp_cn = new List<string>
            {
                "胡连福利",
                "培训发展",
                "招募流程",
                "招募职缺",
                "招募连络资讯",
                "如何前往胡连",
            };


            List<string> category = new List<string>
            {
               "1",
               "1",
               "2",
               "2",
               "2",
               "2",
            };

            List<string> editor = new List<string>
            {
              MapPath("/Content/Templates/Welfare/01.html"),
              MapPath("/Content/Templates/Welfare/02.html"),
              MapPath("/Content/Templates/Welfare/03.html"),
              MapPath("/Content/Templates/Welfare/04.html"),
              MapPath("/Content/Templates/Welfare/05.html"),
              MapPath("/Content/Templates/Welfare/06.html"),
            };


            List<welfare> data = new List<welfare>();
            int i = 0;

            foreach(string title in temp)
            {
                
                data.Add(new welfare
                {
                    guid = (i+1).ToString(),
                    title = title,
                    category = category[i].ToString(),
                    content = editor[i].ToString(),     
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "tw",
                });
                data.Add(new welfare
                {
                    guid = (i + 1).ToString(),
                    title = temp_cn[i].ToString(),
                    category = category[i].ToString(),
                    content = editor[i].ToString(),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    sortIndex = (i + 1),
                    lang = "cn",
                });

                i++;
            }


            if (context.welfare.ToList().Count == 0)
            {
                data.ForEach(s => context.welfare.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}