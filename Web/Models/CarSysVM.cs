﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class CarSysVM
    {
        public int Id { get; set; }
        public int l3 { get; set; }
        public string Caption { get; set; }

        public List<CarSysVM> Childs { get; set; }
    }
}