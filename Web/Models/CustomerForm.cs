﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public partial class customerGeneralData
    {
        public string companyName { get; set; }
        public string contry { get; set; }
        public string location { get; set; }
        public string invoiceAddress { get; set; }
        public string deliveryAddress { get; set; }
        public string tel { get; set; }
        public string fax { get; set; }
        public string chairman { get; set; }
        public string contactPerson { get; set; }
        public string website { get; set; }
        public string email { get; set; }
        public string establishment { get; set; }
        public string stock { get; set; }
        public string license { get; set; }
        public string employees { get; set; }
        public string Overseas { get; set; }
    }

    public partial class bankAffiliatesData
    {
        public string bankName1 { get; set; }
        public string branch1 { get; set; }
        public string account1 { get; set; }
        public string remark1 { get; set; }
        public string bankName2 { get; set; }
        public string branch2 { get; set; }
        public string account2 { get; set; }
        public string remark2 { get; set; }
        public string bankName3 { get; set; }
        public string branch3 { get; set; }
        public string account3 { get; set; }
        public string remark3 { get; set; }
    }

    public partial class businessTypeData
    {
        public string businessType { get; set; }
        public string otherbusinessTypeText { get; set; }
    }

    public partial class top3BusinessContentsData
    {
        public string productsItem1 { get; set; }
        public string productsPrecent1 { get; set; }
        public string productsItem2 { get; set; }
        public string productsPrecent2 { get; set; }
        public string productsItem3 { get; set; }
        public string productsPrecent3 { get; set; }
    }

    public partial class top3CustomersData
    {
        public string customerItem1 { get; set; }
        public string customerPrecent1 { get; set; }
        public string customerItem2 { get; set; }
        public string customerPrecent2 { get; set; }
        public string customerItem3 { get; set; }
        public string customerPrecent3 { get; set; }
    }

    public partial class top3VendersData
    {
        public string venderItem1 { get; set; }
        public string venderPrecent1 { get; set; }
        public string venderItem2 { get; set; }
        public string venderPrecent2 { get; set; }
        public string venderItem3 { get; set; }
        public string venderPrecent3 { get; set; }
    }

    public partial class majorEquipmentData
    {
        public string list1 { get; set; }
        public string list2 { get; set; }
        public string list3 { get; set; }
        public string list4 { get; set; }
        public string list5 { get; set; }
        public string list6 { get; set; }
    }

    public partial class floorAreaData
    {
        public string headquarter { get; set; }
        public string branch { get; set; }
    }

    public partial class productModelData
    {
        public string CustomersText1 { get; set; }
        public string CustomersText2 { get; set; }
        public string CustomersText3 { get; set; }
        public string CustomersText4 { get; set; }
        public string CustomersText5 { get; set; }
        public string CustomersText6 { get; set; }
        public string CustomersText7 { get; set; }
        public string CustomersText8 { get; set; }
        public string CustomersText9 { get; set; }
    }

    public partial class companyType1Data
    {
        public string type1 { get; set; }
    }

    public partial class companyType2Data
    {
        public string type2 { get; set; }
    }

    public partial class cooperationData
    {
        public string cooperationItem1 { get; set; }
        public string cooperationPrecent1 { get; set; }
        public string cooperationItem2 { get; set; }
        public string cooperationPrecent2 { get; set; }
        public string cooperationItem3 { get; set; }
        public string cooperationPrecent3 { get; set; }
    }

    public partial class whitchMethodsData
    {
        public string methodsType { get; set; }
        public string othermethodsTypeText { get; set; }
    }
}