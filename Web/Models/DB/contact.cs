namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class contact
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string name { get; set; }//姓名


        public string department { get; set; }//部門

        public string email { get; set; } // E-Mail

        public string jobTitle { get; set; }//職稱

        public string eNews { get; set; }//eNews

        public string company { get; set; }//公司

        public string country { get; set; }//國家

        public string website { get; set; }//網址

        public string products { get; set; }//產品

        public string tel { get; set; }//電話


        public string message { get; set; }//其他意見

        public DateTime? modifydate { get; set; }        

        [StringLength(1)]
        public string status { get; set; }
 

        public DateTime? create_date { get; set; }


        [StringLength(30)]
        public string lang { get; set; }


    }
}
