namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("language")]
    public partial class language
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(64)]
        public string guid { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(50)]
        public string title { get; set; }

        [StringLength(10)]
        public string codes { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public int? sortIndex { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? date { get; set; }

        [StringLength(10)]
        public string lang { get; set; }
    }
}
