namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class locations
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

    

        public string content { get; set; }
        public string pic { get; set; }

        public string pic_alt { get; set; }


        public string map { get; set; }

        public string email { get; set; }

        public DateTime? modifydate { get; set; }

        public int? sortIndex { get; set; }

        [StringLength(1)]
        public string status { get; set; }
             

        public DateTime? create_date { get; set; }


        [StringLength(30)]
        public string lang { get; set; }

    }
}
