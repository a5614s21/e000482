namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class member
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }


        public string username { get; set; }

      
        public string password { get; set; }


    
        public string name { get; set; }

        public string company { get; set; }

     
        public string email { get; set; }

      
        public string phone { get; set; }

  
        public string mobile { get; set; }

      
        public string address { get; set; }

        public string note { get; set; }
        public string customer_general_information { get; set; }
        public string bank_affiliates { get; set; }
        public string businessType { get; set; }
        public string top3_businessContents { get; set; }
        public string top3_customers { get; set; }
        public string top3_venders { get; set; }
        public string major_equipmentList { get; set; }
        public string floorArea { get; set; }
        public string productModel { get; set; }
        public string companyType1 { get; set; }
        public string companyType2 { get; set; }
        public string cooperation { get; set; }
        public string whitchMethods { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? logindate { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }
    }
}
