namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class news
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string notes { get; set; }

        public string content { get; set; }

        public string video { get; set; }

        public DateTime? startdate { get; set; }

        public DateTime? enddate { get; set; }

        public DateTime? modifydate { get; set; }

        [StringLength(1)]
        public string sticky { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(64)]
        public string category { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string seo_keywords { get; set; }

        public string seo_description { get; set; }

        public string infinity { get; set; }

        [StringLength(30)]
        public string verify { get; set; }

        public string verify_info { get; set; }
        [StringLength(1)]
        public string tops { get; set; }
        public string file { get; set; }

    }
}
