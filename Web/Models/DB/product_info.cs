﻿namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class product_info
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public int product_id { get; set; }
        public string content { get; set; }
        public string newName { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }
        public string spec_pic { get; set; }
        public string paper_pic { get; set; }
        public string spec_paper { get; set; }
        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string seo_keywords { get; set; }

        public string seo_description { get; set; }
        public string carsysl4id { get; set; }
        public string carsysl4name { get; set; }
    }
}