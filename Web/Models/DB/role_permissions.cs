namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class role_permissions
    {
        [Key]
        public Guid guid { get; set; }

        [StringLength(64)]
        public string role_guid { get; set; }

        [StringLength(64)]
        public string permissions_guid { get; set; }

        [StringLength(1)]
        public string permissions_status { get; set; }
    }
}
