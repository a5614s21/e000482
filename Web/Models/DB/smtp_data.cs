namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class smtp_data
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        [StringLength(255)]
        public string host { get; set; }

        [StringLength(20)]
        public string port { get; set; }

        [StringLength(1)]
        public string smtp_auth { get; set; }

        [StringLength(255)]
        public string username { get; set; }

        [StringLength(255)]
        public string password { get; set; }

        [StringLength(255)]
        public string from_email { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }
    }
}
