namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class survey
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string no { get; set; }//客戶編號

        public string name { get; set; }//姓名


        public string company { get; set; }//公司資料

        public string email { get; set; } // E-Mail

        public string area { get; set; }//客戶區域

        public string industry { get; set; }//產業分類

        public string satisfaction { get; set; }//滿意度調查

        public string problem { get; set; }//綜合問題

        public string message { get; set; }//其他意見

        public DateTime? modifydate { get; set; }        

        [StringLength(1)]
        public string status { get; set; }
 

        public DateTime? create_date { get; set; }


        [StringLength(30)]
        public string lang { get; set; }


    }
}
