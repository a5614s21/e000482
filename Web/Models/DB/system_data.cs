namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class system_data
    {
        [Key]
        public Guid guid { get; set; }

        [StringLength(255)]
        public string title { get; set; }

        [StringLength(255)]
        public string login_title { get; set; }

        public string logo { get; set; }

        public string logo_alt { get; set; }

        [StringLength(50)]
        public string design_by { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }

        public string background { get; set; }

        public string background_alt { get; set; }
    }
}
