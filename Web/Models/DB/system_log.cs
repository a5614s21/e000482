namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class system_log
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        [StringLength(255)]
        public string title { get; set; }
        [StringLength(255)]
        public string ip { get; set; }

        [StringLength(255)]
        public string types { get; set; }

        [StringLength(255)]
        public string tables { get; set; }

        [StringLength(255)]
        public string tables_guid { get; set; }

        public string notes { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }

        public string username { get; set; }
    }
}
