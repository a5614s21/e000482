namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class system_menu
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        [StringLength(255)]
        public string title { get; set; }

        [StringLength(255)]
        public string category { get; set; }

        [StringLength(255)]
        public string tables { get; set; }

        public int? sortindex { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(255)]
        public string act_path { get; set; }

        [StringLength(255)]
        public string icon { get; set; }

        [StringLength(255)]
        public string area { get; set; }

        [StringLength(255)]
        public string category_table { get; set; }

        [StringLength(255)]
        public string index_view_url { get; set; }

        [StringLength(1)]
        public string can_add { get; set; }

        [StringLength(1)]
        public string can_edit { get; set; }

        [StringLength(1)]
        public string can_del { get; set; }
        public string prev_table { get; set; }
    }
}
