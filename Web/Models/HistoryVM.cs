﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class HistoryVM
    {
        public List<HistoryMenu> Menus { get; set; } = new List<HistoryMenu>();

        public HistoryMenu AddMenu(string name) 
        {
            var menu = new HistoryMenu() { Name = name, Years = new List<HistoryContent>() };

            Menus.Add(menu);

            return menu;
        }
    }

    public class HistoryMenu 
    {
        public string Name { get; set; }
        public List<HistoryContent> Years { get; set; }

        public void AddYearContent(string name,string content) 
        {
            Years.Add(new HistoryContent() { Name=name,Content = content });
        }
    }

    public class HistoryContent 
    {
        public string Name { get; set; }
        public string Content { get; set; }
    }
}