﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class LoginVM
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string VerificationCode { get; set; }
    }
}