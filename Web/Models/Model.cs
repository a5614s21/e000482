﻿namespace Web.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Web.DB.Models;

    public class Model : DbContext
    {
        // 您的內容已設定為使用應用程式組態檔 (App.config 或 Web.config)
        // 中的 'Model' 連接字串。根據預設，這個連接字串的目標是
        // 您的 LocalDb 執行個體上的 'Web.Models.Model' 資料庫。
        //
        // 如果您的目標是其他資料庫和 (或) 提供者，請修改
        // 應用程式組態檔中的 'Model' 連接字串。
        public Model()
            : base("name=Model")
        {
        }

      
        public virtual DbSet<web_data> web_data { get; set; }
        public virtual DbSet<language> language { get; set; }
        public virtual DbSet<smtp_data> smtp_data { get; set; }
        public virtual DbSet<role_permissions> role_permissions { get; set; }
        public virtual DbSet<roles> roles { get; set; }
        public virtual DbSet<system_data> system_data { get; set; }
        public virtual DbSet<system_log> system_log { get; set; }
        public virtual DbSet<system_menu> system_menu { get; set; }

        public virtual DbSet<user> user { get; set; }

        public virtual DbSet<user_role> user_role { get; set; }

        public virtual DbSet<ashcan> ashcan { get; set; }

        public virtual DbSet<firewalls> firewalls { get; set; }

        public virtual DbSet<mail_contents> mail_contents { get; set; }

        public virtual DbSet<google_analytics> google_analytics { get; set; }

        public virtual DbSet<news_category> news_category { get; set; }

        public virtual DbSet<news> news { get; set; }

        public virtual DbSet<abouts> abouts { get; set; }

        public virtual DbSet<notes_data> notes_data { get; set; }

        public virtual DbSet<business_faq> business_faq { get; set; }
        public virtual DbSet<courses> courses { get; set; }
        public virtual DbSet<course_category> course_category { get; set; }

        public virtual DbSet<locations> locations { get; set; }

        public virtual DbSet<downloads> downloads { get; set; }

        public virtual DbSet<download_category> download_category { get; set; }

        public virtual DbSet<welfare> welfare { get; set; }
        public virtual DbSet<welfare_category> welfare_category { get; set; }

        public virtual DbSet<survey> survey { get; set; }

        public virtual DbSet<contact> contact { get; set; }

        public virtual DbSet<shareholder_faq> shareholder_faq { get; set; }

        public virtual DbSet<inquire> inquire { get; set; }

        public virtual DbSet<advertise> advertise { get; set; }

        public virtual DbSet<member> member { get; set; }

        public virtual DbSet<history_category> history_category { get; set; }

        public virtual DbSet<history> history { get; set; }


        public virtual DbSet<problem_category> problem_category { get; set; }

        public virtual DbSet<problem> problem { get; set; }

        public virtual DbSet<system_item_category> system_item_category { get; set; }

        public virtual DbSet<system_item> system_item { get; set; }
        public virtual DbSet<product_info> product_info { get; set; }
        public virtual DbSet<index_content> index_content { get; set; }
        public virtual DbSet<carsysl2> carsysl2 { get; set; }
        public virtual DbSet<carsysl3> carsysl3 { get; set; }
        public virtual DbSet<carsysl4> carsysl4 { get; set; }
        public virtual DbSet<carsysl5> carsysl5 { get; set; }
        public virtual DbSet<mapcarsys> mapcarsys { get; set; }

        // 針對您要包含在模型中的每種實體類型新增 DbSet。如需有關設定和使用
        // Code First 模型的詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=390109。
    }
}