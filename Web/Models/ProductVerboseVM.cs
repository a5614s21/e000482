﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ProductVerboseVM
    {
        public string productReSize { get; set; }
        public string productReSerial { get; set; }
        public string productReCategory { get; set; }
        public string productReName { get; set; }
        public string productReType { get; set; }
        public string productReSex { get; set; }
        public string productReColor { get; set; }
        public string productReBag { get; set; }
        public string productReMaterial { get; set; }
        public string productRePlating { get; set; }
        public string productReSealable { get; set; }
        public string productReFeedType { get; set; }
        public string productReItemClass { get; set; }
        public string productData1 { get; set; }
        public string productData2 { get; set; }
        public string productData3 { get; set; }
        public string productData4 { get; set; }
        public string productData5 { get; set; }
        public string productData6 { get; set; }
        public string productData7 { get; set; }
        public string productData8 { get; set; }
        public string productData9 { get; set; }
        public string productData10 { get; set; }
        public string productData11 { get; set; }
        public string productData12 { get; set; }
        public string productData13 { get; set; }
        public string productData14 { get; set; }
        public string productData15 { get; set; }
        public string productData16 { get; set; }
        public string productData17 { get; set; }
        public string productData18 { get; set; }
        public string productData19 { get; set; }
        public string productData20 { get; set; }
        public string productData21 { get; set; }
        public decimal id { get; set; }
        public string name { get; set; }
        public string new_name { get; set; }
        public string type { get; set; }
        public string o_type { get; set; }
        public string material { get; set; }
        public string productImg { get; set; }
        public string tab { get; set; }
        public string plating { get; set; }
        public string leaking { get; set; }
        public string depth { get; set; }
        public string feed_type { get; set; }
        public string productSpc { get; set; }
        public string productPaper { get; set; }
        public string product3D { get; set; }
        public string productBook { get; set; }
        public string diameter_1 { get; set; }
        public string diameter_2 { get; set; }
        public string elongation { get; set; }
        public string e_init { get; set; }
        public string e_allow { get; set; }
        public string width_1 { get; set; }
        public string width_2 { get; set; }
        public string length_1 { get; set; }
        public string length_2 { get; set; }
        public string wire_size { get; set; }
        public string series { get; set; }

        //public int? width { get; set; }
        //public int? length { get; set; }
        //public int? height { get; set; }
        public float? width { get; set; }
        public float? length { get; set; }
        public float? height { get; set; }
        public string sizeS { get; set; }
        public string sizeR { get; set; }
        public string sizeT { get; set; }
        public string size
        {
            get
            {
                string result = string.Empty;
                if (length != null)
                    result += $"L:{length} ";
                if (width != null)
                    result += $"W:{width} ";
                if (height != null)
                    result += $"H:{height}";

                return result.Trim();
            }
        }
        public string item_class { get; set; }
        public string positions { get; set; }
        public string temper_operating { get; set; }
        public string seal_capability { get; set; }
        public string plate_form { get; set; }
        public string plate_thickness { get; set; }

        //排數
        public string rows { get; set; }

        public string sex_o { get; set; }

        public string sex
        {
            get
            {
                return sex_o;
            }
        }

        public string sealable_o { get; set; }

        public string sealable
        {
            get
            {
                return sealable_o;
            }
        }

        public string color_o { get; set; }
        public string color
        {
            get
            {
                return color_o;
            }
        }

        //耐熱等級
        public string flamm_rating { get; set; }
        public string diameter { get; set; }

        //單位包裝量
        public string quantity { get; set; }
        public string packaging_o { get; set; }

        public string packaging
        {
            get
            {
                return packaging_o;
            }
        }

        public bool showRows { get; set; }
        public bool showPositions { get; set; }

        public bool showSex { get; set; }
        public bool showSealable { get; set; }
        public bool showDiameter { get; set; }
        public bool showColor { get; set; }
        public bool showSColor { get; set; }
        public bool showFrontColor { get; set; }
        public bool showFlammRating { get; set; }
        public bool showTemperOperation { get; set; }
        public bool showQuantity { get; set; }
        public bool showPacking { get; set; }
        public bool showSealCapability { get; set; }
        public bool showPlateForm { get; set; }
        public bool showPlateThickness { get; set; }
        public bool showElectricity { get; set; }

        public virtual void SetShowable() { }

        public virtual void SetShowable_j(string type) { }
    }

    public class HousingVM : ProductVerboseVM
    {
        public override void SetShowable()
        {
            showRows = true;
            showPositions = true;
            showSex = true;
            showSealable = true;
            showDiameter = false;
            showColor = true;
            showFlammRating = true;
            showTemperOperation = true;
            showQuantity = true;
            showPacking = true;
            showSealCapability = true;
            showPlateForm = true;
            showPlateThickness = true;
        }
        public override void SetShowable_j(string type)
        {
            showRows = true;
            showPositions = true;
            showSex = true;
            showSealable = true;
            showDiameter = false;
            showColor = true;
            showFlammRating = true;
            showTemperOperation = true;
            showQuantity = true;
            showPacking = true;
            showSealCapability = true;
            showPlateForm = true;
            showPlateThickness = true;
            showElectricity = false;
        }
    }

    public class TerminalVM : ProductVerboseVM
    {
        public override void SetShowable()
        {
            showRows = false;
            showPositions = false;
            showSex = true;
            showSealable = true;
            showDiameter = true;
            showColor = false;
            showFlammRating = false;
            showTemperOperation = false;
            showQuantity = true;
            showPacking = true;
            showSealCapability = false;
            showPlateForm = false;
            showPlateThickness = false;
        }
        public override void SetShowable_j(string type)
        {
            showRows = false;
            showPositions = false;
            showSex = true;
            showSealable = true;
            showDiameter = true;
            showColor = false;
            showFlammRating = false;
            showTemperOperation = false;
            showQuantity = true;
            showPacking = true;
            showSealCapability = false;
            showPlateForm = false;
            showPlateThickness = false;
            showElectricity = false;
        }
    }

    public class RubberVM : ProductVerboseVM
    {
        public override void SetShowable()
        {
            showRows = false;
            showPositions = false;
            showSex = false;
            showSealable = false;
            showDiameter = true;
            showColor = true;
            showFlammRating = false;
            showTemperOperation = false;
            showQuantity = true;
            showPacking = false;
            showSealCapability = false;
            showPlateForm = false;
            showPlateThickness = true;
        }

        public override void SetShowable_j(string type)
        {
            showRows = false;
            showPositions = false;
            showSex = false;
            showSealable = false;
            showDiameter = false;
            showColor = true;
            showFlammRating = false;
            showTemperOperation = false;
            showQuantity = true;
            showPacking = false;
            showSealCapability = false;
            showPlateForm = false;
            showPlateThickness = true;
            showElectricity = true;
        }
    }

    public class SleeveVM : ProductVerboseVM
    {
        public override void SetShowable()
        {

            showRows = false;
            showPositions = false;
            showSex = false;
            showSealable = false;
            showDiameter = false;
            showColor = true;
            showFlammRating = false;
            showTemperOperation = true;
            showQuantity = true;
            showPacking = false;
            showSealCapability = false;
            showPlateForm = false;
            showPlateThickness = true;
        }
        public override void SetShowable_j(string type)
        {
            showRows = false;
            showPositions = false;
            showSex = false;
            showSealable = false;
            showDiameter = false;
            showColor = true;
            showFlammRating = false;
            showTemperOperation = true;
            showQuantity = true;
            showPacking = false;
            showSealCapability = false;
            showPlateForm = false;
            showPlateThickness = false;
            showElectricity = true;
        }
    }

    public class GrommetVM : ProductVerboseVM
    {
        public override void SetShowable()
        {
            showRows = false;
            showPositions = false;
            showSex = false;
            showSealable = false;
            showDiameter = false;
            showColor = true;
            showFlammRating = false;
            showTemperOperation = true;
            showQuantity = true;
            showPacking = false;
            showSealCapability = false;
            showPlateForm = false;
            showPlateThickness = false;
        }

        public override void SetShowable_j(string type)
        {
            showRows = false;
            showPositions = false;
            showSex = false;
            showSealable = false;
            showDiameter = false;
            showColor = true;
            showFlammRating = false;
            showTemperOperation = true;
            showQuantity = true;
            showPacking = false;
            showSealCapability = false;
            showPlateForm = false;
            showPlateThickness = false;
            showElectricity = false;
        }
    }
}