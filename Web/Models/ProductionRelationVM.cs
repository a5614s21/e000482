﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ProductionRelationVM
    {
        public string name { get; set; }

        public List<RelatedProduct> productions { get; set; } = new List<RelatedProduct>();

        public void AddProduct(decimal id, string name,string imgUrl)
        {
            RelatedProduct product = new RelatedProduct() { id = id, name = name,ImgUrl = imgUrl };
            productions.Add(product);
        }
    }

    public class RelatedProduct 
    {
        public decimal id { get; set; }
        public string name { get; set; }
        public string ImgUrl { get; set; }
    }
}