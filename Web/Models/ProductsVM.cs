﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ProductsVM
    {
        public int SelectedTab { get; set; }
        public List<ProductVM> Products { get; set; } = new List<ProductVM>();

        public void AddProduct(string itemNo,string category,string family,string size) 
        {
            ProductVM productVM = new ProductVM()
            {
                ItemNo = itemNo,
                Category = category,
                Family = family,
                Size = size
            };

            this.Products.Add(productVM);
        }
    }

    public class ProductVM
    {
        public string ItemNo { get; set; }
        public string Category { get;set;}
        public string Family { get; set; }
        public string Size { get; set; }
    }
}