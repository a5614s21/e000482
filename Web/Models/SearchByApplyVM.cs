﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class SearchByApplyVM
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}