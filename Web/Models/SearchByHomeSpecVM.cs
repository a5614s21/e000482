﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class SearchByHomeSpecVM
    {
        public string Keyword { get; set; }
        public string ProductTable { get; set; }
        public string ProductSizeType { get; set; }
        public string ProductClass { get; set; }
        public string ProductSerial { get; set; }
        public string ProductSex { get; set; }
        public string ProductSealable { get; set; }
        public string ProductPosition { get; set; }
        public string ProductTemper { get; set; }
        public string ProductDepth { get; set; }
        public string ProductDiameter { get; set; }
        public string ProductTab { get; set; }

        public SearchBySpecVM Build() 
        {
            return new SearchBySpecVM()
            {
                ProductTable = this.ProductTable,
                ProductSizeType = this.ProductSizeType,
                ProductClass = this.ProductClass,
                ProductSerial = this.ProductSerial,
                ProductSex = this.ProductSex,
                ProductSealable = this.ProductSealable,
                ProductTemper = this.ProductTemper,
                ProductPosition = this.ProductPosition,
                ProductDepth = this.ProductDepth,
                ProductDiameter = this.ProductDiameter,
                ProductTab = this.ProductTab
            };
        }
    }
}