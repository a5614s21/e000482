﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class SearchByKeyWordVM
    {
        public string Keyword { get; set; }
        public string PartNo { get; set; }
        public int FindWay { get; set; }
        public string TableKey { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }


}