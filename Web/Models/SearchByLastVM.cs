﻿using HuLane.Main.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class SearchByLastVM
    {
        public ProductTable Table { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}