﻿using HuLane.Main.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class SearchBySpecVM
    {
        public string ProductTable { get; set; }
        public string ProductSizeType { get; set; }
        public string ProductClass { get; set; }
        public string ProductSerial { get; set; }
        public string ProductSex { get; set; }
        public string ProductSealable { get; set; }
        public string ProductTemper { get; set; }
        public string ProductPosition { get; set; }
        public string ProductDepth { get; set; }
        public string ProductDiameter { get; set; }
        public MySize Size { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }

        public string Keyword { get; set; }
        public string ProductTab { get; set; }
    }

    public class MySize 
    {
        public Single? Wire_Size { get; set; }
        public Single? Width { get; set; }
        public Single? Length { get; set; }
        public Single? Height { get; set; }
        public Single? Diameter { get; set; }
        public Single? Diameter_1 { get; set; }
        public Single? Diameter_2 { get; set; }
    }
}