﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class SearchCategoryItem
    {
        public SearchCategoryItem(string key, string value) 
        {
            this.key = key;
            this.value = value;
        }
        public string key { get; set; }
        public string value { get; set; }

        
    }
}