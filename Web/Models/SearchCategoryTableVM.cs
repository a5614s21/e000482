﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class SearchCategoryTableVM
    {
        /// <summary>
        /// 尺寸類型
        /// </summary>
        public SearchCategoryVM sizeType { get; set; }
        /// <summary>
        /// 功能類別
        /// </summary>
        public SearchCategoryVM productClass { get; set; }
        /// <summary>
        /// 系列碼
        /// </summary>
        public SearchCategoryVM serial { get; set; }
        /// <summary>
        /// 公/母
        /// </summary>
        public SearchCategoryVM sex { get; set; }
        /// <summary>
        /// 防水
        /// </summary>
        public SearchCategoryVM sealable { get; set; }
        /// <summary>
        /// 孔數
        /// </summary>
        public SearchCategoryVM position { get; set; }
        /// <summary>
        /// 適用溫度
        /// </summary>
        public SearchCategoryVM temper { get; set; }
        /// <summary>
        /// 料厚
        /// </summary>
        public SearchCategoryVM depth { get; set; }
        /// <summary>
        /// 適用線徑
        /// </summary>
        public SearchCategoryVM diameter { get; set; }
        /// <summary>
        /// 尺寸
        /// </summary>
        public SearchCategoryVM size { get; set; }

        public SearchCategoryVM tab { get; set; }
    }
}