﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class SearchCategoryVM
    {
        public List<SearchCategoryItem> categoryItems { get; set; } = new List<SearchCategoryItem>();

        internal static SearchCategoryVM CreateSizeType(IDbConnection conn, string key,string lang)
        {
            var result = new SearchCategoryVM();
            var sql = string.Empty;
            var housingSql = "select distinct item_class from housing  where item_class <> ''";
            var terminalSql = "select distinct item_class from terminal  where item_class <> ''";
            var rubberSql = "select distinct item_class from rubber  where item_class <> ''";
            var sleeveSql = "select distinct item_class from sleeve  where item_class <> ''";
            var grommetSql = "select distinct item_class from grommet  where item_class <> ''";

            var housingSql_o = "select distinct item_class from housing  where item_class <> '' order by item_class";
            var terminalSql_o = "select distinct item_class from terminal  where item_class <> '' order by item_class";
            var rubberSql_o = "select distinct item_class from rubber  where item_class <> '' order by item_class";
            var sleeveSql_o = "select distinct item_class from sleeve  where item_class <> '' order by item_class";
            var grommetSql_o = "select distinct item_class from grommet  where item_class <> '' order by item_class";

            switch (key)
            {
                case "A":
                    sql = housingSql + " union " + terminalSql + " union " + rubberSql + " union " + sleeveSql + " union " + grommetSql;
                    break;
                case "H":
                    sql = housingSql_o;
                    break;
                case "T":
                    sql = terminalSql_o;
                    break;
                case "R":
                    sql = rubberSql_o;
                    break;
                case "S":
                    sql = sleeveSql_o;
                    break;
                case "G":
                    sql = grommetSql_o;
                    break;
            }

            var datas = conn.Query<string>(sql);

            foreach (var data in datas)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(data, "^[0-9].*$")) 
                {
                    result.categoryItems.Add(new SearchCategoryItem(data, data));
                }
            }
            return result;
        }

        internal static SearchCategoryVM CreateProductClass(IDbConnection conn, string key,string lang)
        {
            var result = new SearchCategoryVM();
            var sql = string.Empty;
            var housingSql = "select distinct item_class from housing  where item_class <> ''";
            var terminalSql = "select distinct item_class from terminal  where item_class <> ''";
            var rubberSql = "select distinct item_class from rubber  where item_class <> ''";
            var sleeveSql = "select distinct item_class from sleeve  where item_class <> ''";
            var grommetSql = "select distinct item_class from grommet  where item_class <> ''";

            switch (key)
            {
                case "A":
                    sql = housingSql + " union " + terminalSql + " union " + rubberSql + " union " + sleeveSql + " union " + grommetSql;
                    break;
                case "H":
                    sql = housingSql;
                    break;
                case "T":
                    sql = terminalSql;
                    break;
                case "R":
                    sql = rubberSql;
                    break;
                case "S":
                    sql = sleeveSql;
                    break;
                case "G":
                    sql = grommetSql;
                    break;
            }

            var datas = conn.Query<string>(sql);

            foreach (var data in datas)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(data, "^[^0-9].*$"))
                {
                    if (data == "N/A")
                    {
                        result.categoryItems.Add(new SearchCategoryItem(data, data));
                    }
                    else
                    {
                        var temp = data.Split('(');
                        if (lang == "en")
                        {
                            if (temp.Length == 1)
                            {
                                result.categoryItems.Add(new SearchCategoryItem(data, data));
                            }
                            else
                            {
                                var resultData = temp[0];

                                result.categoryItems.Add(new SearchCategoryItem(data, resultData));
                            }
                        }
                        else
                        {
                            if (temp.Length == 1)
                            {
                                result.categoryItems.Add(new SearchCategoryItem(data, data));
                            }
                            else
                            {
                                var result1 = temp[1];
                                var temp2 = temp[1].Split(')');
                                var resultData = temp2[0];

                                result.categoryItems.Add(new SearchCategoryItem(data, resultData));
                            }
                        }
                    }
                    
                }
            }
            return result;
        }

        internal static SearchCategoryVM CreateSerial(IDbConnection conn, string key, string lang)
        {
            if (key != "A" && key != "H")
                return null;

            var result = new SearchCategoryVM();
            var sql = "select distinct series from housing  where isnull(series,'') <> ''";

            var datas = conn.Query<string>(sql);

            foreach (var data in datas)
            {
                result.categoryItems.Add(new SearchCategoryItem(data, data));
            }
            return result;
        }

        internal static SearchCategoryVM CreateSex(string key, string lang)
        {
            if (key != "A" && key != "H" && key != "T")
                return null;

            var result = new SearchCategoryVM();

            if (lang == "en")
            {
                if (key == "T")
                {
                    result.categoryItems.Add(new SearchCategoryItem("Male(公)", "Male"));
                    result.categoryItems.Add(new SearchCategoryItem("Female(母)", "Female"));
                    result.categoryItems.Add(new SearchCategoryItem("N/A", "N/A"));
                }
                else
                {
                    result.categoryItems.Add(new SearchCategoryItem("Male(公端)", "Male"));
                    result.categoryItems.Add(new SearchCategoryItem("Female(母端)", "Female"));
                    result.categoryItems.Add(new SearchCategoryItem("N/A", "N/A"));
                }
            }
            else
            {
                if (key == "T")
                {
                    result.categoryItems.Add(new SearchCategoryItem("Male(公)", "公端"));
                    result.categoryItems.Add(new SearchCategoryItem("Female(母)", "母端"));
                    result.categoryItems.Add(new SearchCategoryItem("N/A", "N/A"));
                }
                else
                {
                    result.categoryItems.Add(new SearchCategoryItem("Male(公端)", "公端"));
                    result.categoryItems.Add(new SearchCategoryItem("Female(母端)", "母端"));
                    result.categoryItems.Add(new SearchCategoryItem("N/A", "N/A"));
                }
            }

            return result;

        }

        internal static SearchCategoryVM CreateSealable(string key, string lang)
        {
            if (key != "A" && key != "H" && key != "T")
                return null;

            var result = new SearchCategoryVM();

            if (lang == "en")
            {
                if (key == "T")
                {
                    result.categoryItems.Add(new SearchCategoryItem("Sealed(防水)", "Sealed"));
                    result.categoryItems.Add(new SearchCategoryItem("Unsealed(非防水)", "Unsealed"));
                    result.categoryItems.Add(new SearchCategoryItem("N/A", "N/A"));
                }
                else
                {
                    result.categoryItems.Add(new SearchCategoryItem("Sealed", "Sealed"));
                    result.categoryItems.Add(new SearchCategoryItem("Unsealed", "Unsealed"));
                    result.categoryItems.Add(new SearchCategoryItem("N/A", "N/A"));
                }         
            }
            else
            {
                if (key == "T")
                {
                    result.categoryItems.Add(new SearchCategoryItem("Sealed(防水)", "防水"));
                    result.categoryItems.Add(new SearchCategoryItem("Unsealed(非防水)", "非防水"));
                    result.categoryItems.Add(new SearchCategoryItem("N/A", "N/A"));
                }
                else
                {
                    result.categoryItems.Add(new SearchCategoryItem("Sealed", "防水"));
                    result.categoryItems.Add(new SearchCategoryItem("Unsealed", "非防水"));
                    result.categoryItems.Add(new SearchCategoryItem("N/A", "N/A"));
                }
            }

            return result;
        }

        internal static SearchCategoryVM CreateTemper(IDbConnection conn, string key, string lang)
        {
            if (key != "A" && key != "H")
                return null;

            var result = new SearchCategoryVM();

            var sql = "select distinct temper_operating from housing  where isnull(temper_operating,'') <> ''";

            var datas = conn.Query<string>(sql);

            List<Temperature> temps = new List<Temperature>();
            foreach (var data in datas)
            {
                string[] tempArray = null;
                if (data.IndexOf("~") > -1)
                    tempArray = data.Split('~');
                else 
                {
                    if (data.IndexOf('-') == 0) 
                    {
                        var first = data.Substring(0, data.LastIndexOf('-'));
                        var second = data.Substring(data.LastIndexOf('-') + 1);
                        tempArray = new string[] { first, second };
                    }
                    else
                        tempArray = data.Split('-');
                }

                if (tempArray.Length == 1)
                {
                    if (data == "NA")
                    {
                        temps.Add(new Temperature("0°C", "0°C", data));
                    }
                    else
                    {
                        temps.Add(new Temperature(tempArray[0], "0°C", data));
                    }
                }
                else
                {
                    temps.Add(new Temperature(tempArray[0], tempArray[1], data));
                }
                
            }

            temps = temps.OrderBy(s => s.First).ThenBy(s => s.Second).ToList();

            foreach (var temp in temps)
            {
                result.categoryItems.Add(new SearchCategoryItem(temp.Origin, temp.ToString()));
            }
            return result;
        }

        internal static SearchCategoryVM CreateSize(string key, string lang)
        {
            var result =  new SearchCategoryVM();
            var width = new SearchCategoryItem("W", "width");
            var length = new SearchCategoryItem("L", "length");
            var height = new SearchCategoryItem("H", "height");
            var diameter = new SearchCategoryItem("D", "diameter");
            var wire_size = new SearchCategoryItem("D", "wire_size");
            var diameter_1 = new SearchCategoryItem("D1", "diameter_1");
            var diameter_2 = new SearchCategoryItem("D2", "diameter_2");

            switch (key)
            {
                case "A":
                    result.categoryItems.Add(width);
                    result.categoryItems.Add(length);
                    result.categoryItems.Add(height);
                    result.categoryItems.Add(diameter);
                    result.categoryItems.Add(diameter_1);
                    result.categoryItems.Add(diameter_2);
                    break;
                case "H":
                    result.categoryItems.Add(width);
                    result.categoryItems.Add(length);
                    result.categoryItems.Add(height);
                    break;
                case "T":
                    result.categoryItems.Add(width);
                    result.categoryItems.Add(length);
                    result.categoryItems.Add(height);
                    result.categoryItems.Add(wire_size);
                    break;
                case "R":
                    result.categoryItems.Add(length);
                    result.categoryItems.Add(diameter_1);
                    result.categoryItems.Add(diameter_2);
                    break;
                case "S":
                    result.categoryItems.Add(length);
                    result.categoryItems.Add(diameter_1);
                    result.categoryItems.Add(diameter_2);
                    break;
                case "G":
                    result.categoryItems.Add(length);
                    result.categoryItems.Add(diameter_1);
                    result.categoryItems.Add(diameter_2);
                    break;

            }

            return result;
        }

        internal static SearchCategoryVM CreatePosition()
        {
            var result = new SearchCategoryVM();
            var posiotion_0 = new SearchCategoryItem("na", "N/A");
            var posiotion_1 = new SearchCategoryItem("1", "1");
            var posiotion_2 = new SearchCategoryItem("2", "2");
            var posiotion_3 = new SearchCategoryItem("3", "3");
            var posiotion_4 = new SearchCategoryItem("4", "4");
            var posiotion_5 = new SearchCategoryItem("5", "5");
            var posiotion_6 = new SearchCategoryItem("6", "6");
            var posiotion_7 = new SearchCategoryItem("7", "7");
            var posiotion_8 = new SearchCategoryItem("8", "8");
            var posiotion_9 = new SearchCategoryItem("9", "9");
            var posiotion_10 = new SearchCategoryItem("10", "10");
            var posiotion_11 = new SearchCategoryItem("11~12", "11 ~ 12");
            var posiotion_13 = new SearchCategoryItem("13~15", "13 ~ 15");
            var posiotion_16 = new SearchCategoryItem("16~17", "16 ~ 17");
            var posiotion_18 = new SearchCategoryItem("18~22", "18 ~ 22");
            var posiotion_23 = new SearchCategoryItem("23~29", "23 ~ 29");
            var posiotion_30 = new SearchCategoryItem("30~49", "30 ~ 49");
            var posiotion_50 = new SearchCategoryItem("50~∞", "50 ~ ∞");

            result.categoryItems.Add(posiotion_0);
            result.categoryItems.Add(posiotion_1);
            result.categoryItems.Add(posiotion_2); 
            result.categoryItems.Add(posiotion_3);
            result.categoryItems.Add(posiotion_4);
            result.categoryItems.Add(posiotion_5);
            result.categoryItems.Add(posiotion_6);
            result.categoryItems.Add(posiotion_7);
            result.categoryItems.Add(posiotion_8);
            result.categoryItems.Add(posiotion_9);
            result.categoryItems.Add(posiotion_10);
            result.categoryItems.Add(posiotion_11);
            result.categoryItems.Add(posiotion_13);
            result.categoryItems.Add(posiotion_16);
            result.categoryItems.Add(posiotion_18);
            result.categoryItems.Add(posiotion_23);
            result.categoryItems.Add(posiotion_30);
            result.categoryItems.Add(posiotion_50);
              

            return result;
        }

        internal static SearchCategoryVM CreateDepth()
        {
            var result = new SearchCategoryVM();
            result.categoryItems.Add(new SearchCategoryItem("0~0.15", "0 ~ 0.15"));
            result.categoryItems.Add(new SearchCategoryItem("0.15~0.2", "0.15 ~ 0.2"));
            result.categoryItems.Add(new SearchCategoryItem("0.2~0.25", "0.2 ~ 0.25"));
            result.categoryItems.Add(new SearchCategoryItem("0.25~0.3", "0.25 ~ 0.3"));
            result.categoryItems.Add(new SearchCategoryItem("0.3~0.35", "0.3 ~ 0.35")); 
            result.categoryItems.Add(new SearchCategoryItem("0.35~0.4", "0.35 ~ 0.4")); 
            result.categoryItems.Add(new SearchCategoryItem("0.4~0.45", "0.4 ~ 0.45"));
            result.categoryItems.Add(new SearchCategoryItem("0.45~0.5", "0.45 ~ 0.5"));
            result.categoryItems.Add(new SearchCategoryItem("0.5~0.6", "0.5 ~ 0.6"));
            result.categoryItems.Add(new SearchCategoryItem("0.6~0.7", "0.6 ~ 0.7")); 
            result.categoryItems.Add(new SearchCategoryItem("0.7~0.8", "0.7 ~ 0.8")); 
            result.categoryItems.Add(new SearchCategoryItem("0.8~0.9", "0.8 ~ 0.9")); 
            result.categoryItems.Add(new SearchCategoryItem("0.8~0.9", "0.8 ~ 0.9"));
            result.categoryItems.Add(new SearchCategoryItem("0.8~0.9", "0.8 ~ 0.9")); 
            result.categoryItems.Add(new SearchCategoryItem("2~∞", "2 ~ ∞"));

            return result;
        }

        internal static SearchCategoryVM CreateDiameter()
        {
            var result = new SearchCategoryVM();
            result.categoryItems.Add(new SearchCategoryItem("na", "N/A"));
            result.categoryItems.Add(new SearchCategoryItem("0.30~0.50", "0.30 ~ 0.50"));
            result.categoryItems.Add(new SearchCategoryItem("0.90~2.00", "0.90 ~ 2.00"));
            result.categoryItems.Add(new SearchCategoryItem("1.10~1.90", "1.10 ~ 1.90"));
            result.categoryItems.Add(new SearchCategoryItem("1.20~2.10", "1.20 ~ 2.10"));
            result.categoryItems.Add(new SearchCategoryItem("1.30~2.20", "1.30 ~ 2.20"));
            result.categoryItems.Add(new SearchCategoryItem("1.40~2.20", "1.40 ~ 2.20"));
            result.categoryItems.Add(new SearchCategoryItem("1.45~1.65", "1.45 ~ 1.65"));
            result.categoryItems.Add(new SearchCategoryItem("1.50~2.50", "1.50 ~ 2.50"));
            result.categoryItems.Add(new SearchCategoryItem("1.65~2.70", "1.65 ~ 2.70"));
            result.categoryItems.Add(new SearchCategoryItem("1.70~2.60", "1.70 ~ 2.60"));
            result.categoryItems.Add(new SearchCategoryItem("1.80~2.80", "1.80 ~ 2.80"));
            result.categoryItems.Add(new SearchCategoryItem("1.85~3.00", "1.85 ~ 3.00"));
            result.categoryItems.Add(new SearchCategoryItem("1.90~2.60", "1.90 ~ 2.60"));
            result.categoryItems.Add(new SearchCategoryItem("2.00~3.40", "2.00 ~ 3.40"));
            result.categoryItems.Add(new SearchCategoryItem("2.20~3.00", "2.20 ~ 3.00"));
            result.categoryItems.Add(new SearchCategoryItem("2.30~3.10", "2.30 ~ 3.10"));
            result.categoryItems.Add(new SearchCategoryItem("2.50~3.50", "2.50 ~ 3.50"));
            result.categoryItems.Add(new SearchCategoryItem("2.70~4.00", "2.70 ~ 4.00"));
            result.categoryItems.Add(new SearchCategoryItem("2.80~3.50", "2.80 ~ 3.50"));
            result.categoryItems.Add(new SearchCategoryItem("2.90~3.80", "2.90 ~ 3.80"));
            result.categoryItems.Add(new SearchCategoryItem("3.00~3.30", "3.00 ~ 3.30"));

            return result;
        }

        internal static SearchCategoryVM CreateTab()
        {
            var result = new SearchCategoryVM();
            result.categoryItems.Add(new SearchCategoryItem("0~0.3", "0 ~ 0.3"));
            result.categoryItems.Add(new SearchCategoryItem("0.30~0.49", "0.30 ~ 0.49"));
            result.categoryItems.Add(new SearchCategoryItem("0.50~0.59", "0.50 ~ 0.59"));
            result.categoryItems.Add(new SearchCategoryItem("0.60~0.63", "0.60 ~ 0.63"));
            result.categoryItems.Add(new SearchCategoryItem("0.64~0.79", "0.64 ~ 0.79"));
            result.categoryItems.Add(new SearchCategoryItem("0.80~0.99", "0.80 ~ 0.99"));
            result.categoryItems.Add(new SearchCategoryItem("1.00~1.99", "1.00 ~ 1.99"));
            result.categoryItems.Add(new SearchCategoryItem("2.00~3.99", "2.00 ~ 3.99"));
            result.categoryItems.Add(new SearchCategoryItem("4.00~5.99", "4.00 ~ 5.99"));
            result.categoryItems.Add(new SearchCategoryItem("6.00~∞", "6.00 ~ ∞"));

            return result;
        }
    }

    internal class Temperature 
    {
        public int First { get; }
        public int Second { get; }
        public string Origin { get; set; }
        public Temperature(string first,string second,string origin) 
        {
            var isFistNative = first.IndexOf('-') == 0;
            var isSecondNative = second.IndexOf('-') == 0;
            first = System.Text.RegularExpressions.Regex.Replace(first, "\\D+", "");
            second = System.Text.RegularExpressions.Regex.Replace(second, "\\D+", "");
            First = int.Parse(first) * (isFistNative? -1: 1);
            Second = int.Parse(second) * (isSecondNative ? -1: 1);
            Origin = origin;
        }

        public override string ToString()
        {
            return First + "~" + Second;
        }
    }
}