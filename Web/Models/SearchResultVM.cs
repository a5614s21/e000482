﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class SearchResultVM 
    {
        public int TotalCount { get; set; }
        public List<SearchResultDetailVM> PageDatas { get; set; } 
    }
    public class SearchResultDetailVM
    {
        public string reSex { get; set; }
        public string reSealable { get; set; }
        public string reMaterial { get; set; }
        public string rePlating { get; set; }
        public int th { get; set; }
        public string applicable_wire { get; set; }
        public string diameter_1 { get; set; }
        public string diameter_2 { get; set; }
        public Single wire_size { get; set; }
        public string productImg { get; set; }
        public string depth { get; set; }
        public string diameter { get; set; }
        public string plating { get; set; }
        public int id { get; set; }
        /// <summary>
        /// 產品品號
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 產品ID料號
        /// </summary>
        public string new_name { get; set; }
        public string type { get; set; }
        public string material { get; set; }
        public string color { get; set; }
        public string sex {
            get 
            {
                if (string.IsNullOrEmpty(sex_o))
                    return string.Empty;

                if (sex_o[0] == 'F')
                    return "母端";
                else if (sex_o[0] == 'M')
                    return "公端";
                else if (sex_o[0] == 'N')
                    return "N/A";
                else
                    return "";
            }
        }
        public string sex_o { get; set; }

        public string sealable 
        { 
            get 
            {
                if (string.IsNullOrEmpty(sealable_o))
                    return "";

                if (sealable_o[0] == 'S')
                    return "防水";
                else if (sealable_o[0] == 'U')
                    return "非防水";
                else if (sealable_o[0] == 'N')
                    return "N/A";
                else
                    return "";
            } 
        }
        public string sealable_o { get; set; }
        public string temper_operating { get; set; }
        public string positions { get; set; } = "";
        public string series { get; set; }
        public string item_class { get; set; }
        public Single height { get; set; }
        public Single width { get; set; }
        public Single length { get; set; }
    }
}