﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class StrongholdVM
    {
        public string Map { get; set; }

        public List<FactoryItem> Factories { get; set; } = new List<FactoryItem>();
    }

    public class FactoryItem 
    {
        public string Content { get; set; }
    }
}