namespace Web.Models
{


    public partial class companyData
    {
        public string title { get; set; }
        public string tel { get; set; }
        public string department { get; set; }
        public string jobTitle { get; set; }
    }
    public partial class areaData
    {
        public string area { get; set; }
        public string other { get; set; }
    }

    public partial class industryData
    {
        public string industry { get; set; }
        public string industry_other { get; set; }

        public string product { get; set; }
        public string product_other { get; set; }

        public string market { get; set; }
        public string market_other { get; set; }
    }

    public partial class problemData
    {
        public string comprehensive { get; set; }
        public string comprehensive_other { get; set; }

        public string env { get; set; }
        public string env_other { get; set; }

        public string company { get; set; }
        public string company_other { get; set; }
    }

    public partial class satisfactionData
    {
        public string A { get; set; }
        public string B { get; set; }

        public string C { get; set; }
        public string D { get; set; }

        public string E { get; set; }
        public string F { get; set; }
        public string G { get; set; }
        public string H { get; set; }
        public string I { get; set; }
        public string J { get; set; }
        public string K { get; set; }
        public string L { get; set; }
        public string M { get; set; }
        public string N { get; set; }
        public string O { get; set; }
        public string P { get; set; }
    }
}
