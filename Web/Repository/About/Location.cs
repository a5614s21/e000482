﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.DB.Models;

namespace Web.Repository.About
{
    public class Location : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '據點名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            //  main.Add("category", "[{'subject': '所屬分類','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'news_category'}]");
            // main.Add("notes", "[{'subject': '列表簡述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("content", "[{'subject': '據點內容','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("map", "[{'subject': 'Google Map','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','data':'名稱/連結','Val':'title/url','useLang':'Y'}]");
            main.Add("email", "[{'subject': 'Email','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','data':'名稱/連結','Val':'title/url','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("status", "[{'subject': '狀態','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
        
            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);
            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            //re.Add("pic", "圖片");
            re.Add("info", "內容摘要");
            re.Add("modifydate", "異動日期");
            //  re.Add("status", "狀態");
            re.Add("sortIndex", "排序");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }

        /// <summary>
        /// 全部
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        public static List<locations> All(string Lang)
        {
            Model DB = new Model();
            return DB.locations.Where(m => m.status == "Y").Where(m => m.lang == Lang).ToList();
        }

     
    }
}