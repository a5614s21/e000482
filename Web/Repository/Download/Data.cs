﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.DB.Models;

namespace Web.Repository.Download
{
    public class Data : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }
        public static Dictionary<String, Object> colFrom(dynamic data)
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            List<string> needPic = new List<string> { "stakeholder", "csr" };
            List<string> haveNext = new List<string> { "diretors" , "audit_master" , "organization_master" };

            data = (data == null) ? "" : data.ToString();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
      
            if (haveNext.IndexOf(data.ToString()) != -1)
            {
                main.Add("category", "[{'subject': '所屬分類','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'download_category','uri':'"+ data + "'}]");
            }
           
            //main.Add("infinity", "[{'subject': '相關連結','type': 'infinity','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','data':'名稱/連結','Val':'title/url','useLang':'Y'}]");
            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            if (needPic.IndexOf(data.ToString()) != -1)
            {
                media.Add("pic", "[{'subject': '列表圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高320 x 240 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            }
            media.Add("file", "[{'subject': '檔案下載','type': 'fileUploadFile','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','filetype': 'application/pdf','multiple': 'N','useLang':'Y'}]");

            // media.Add("video", "[{'subject': '影片上傳','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','filetype': 'video/mp4','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("startdate", "[{'subject': '發佈日期','type': 'dates','defaultVal': '"+DateTime.Now.ToString("yyyy/MM/dd")+ "','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
           // other.Add("enddate", "[{'subject': '下架日期','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
           // other.Add("tops", "[{'subject': '熱門訊息','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">啟用此選項文章將被顯示於首頁</small>','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");


            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;

        }


        public static Dictionary<String, Object> dataTableTitle(string guid = "")
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            List<string> needPic = new List<string> { "stakeholder" };

            re.Add("guid", "Y");
            if (needPic.IndexOf(guid) != -1)
            {
                re.Add("pic", "圖片");
            }
            List<string> haveNext = new List<string> { "diretors", "audit_master", "organization_master" };

            re.Add("info", "內容摘要");
            if (haveNext.IndexOf(guid.ToString()) != -1)
            {
                re.Add("category", "分類");
            }
            re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }

        /// <summary>
        /// 單筆
        /// </summary>
        /// <param name="Lang"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static downloads Single(string Lang, string guid)
        {
            Model DB = new Model();
            downloads data = DB.downloads.Where(m => m.status == "Y").Where(m => m.lang == Lang).Where(m => m.guid == guid).FirstOrDefault();
            return data;
        }

    }
}