﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Repository.Product
{
    public class ProductInfo
    {
        // GET: message

        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("newName", "[{'subject': '產品品號','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'readonly','notes': ''}]");
            //main.Add("carsysl4id", "[{'subject': '線束名稱','type': 'select','default': '','class': 'col-lg-10','required': '','notes': '','inherit':'carsysl2'}]");
            //main.Add("carsysl4name", "[{'subject': '線束內容','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'carsysl4'}]");
            main.Add("carsysl4id", "[{'subject': '線束預設值','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': ''}]");
            main.Add("carsysl4name", "[{'subject': '線束內容','type': 'selectMultiple','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'','Val':'','inherit':'carsysl4'}]");
            main.Add("content", "[{'subject': 'SEO預設值','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': ''}]");
            main.Add("seo_keywords", "[{'subject': 'SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");
            main.Add("seo_description", "[{'subject': 'SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': ''}]");


            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //media.Add("pic", "[{'subject': '產品圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議大小：1920 x 700 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("spec_pic", "[{'subject': '參考規格圖','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高:220 x 158 (px); 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("paper_pic", "[{'subject': '產品圖紙','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">限PDF</small>','filetype': 'application/pdf','multiple': 'N','useLang':'Y'}]");
            //media.Add("spec_paper", "[{'subject': '產品規格書','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">限PDF</small>','filetype': 'application/pdf','multiple': 'N','useLang':'Y'}]");
            //media.Add("banner", "[{'subject': 'Banner','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議大小：1920 x 430 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N'}]");

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();

            //other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");
            //other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            #region 隱藏欄位
            Dictionary<String, Object> hidden = new Dictionary<string, object>();


            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;

        }
        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            //re.Add("pic", "圖片");
            re.Add("newName", "產品品號");
            re.Add("seo_keywords", "SEO關鍵字");
            re.Add("action", "動作");

            return re;
        }



        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "newName");
            re.Add("orderByType", "desc");

            return re;
        }
    }
}