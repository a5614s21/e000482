﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Repository.System
{
    public class Data : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("title", "[{'subject': '後台網站標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("login_title", "[{'subject': '登入頁網站標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");

         
            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("logo", "[{'subject': 'LOGO','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高158 x 35 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'N'}]");
            media.Add("background", "[{'subject': '登入頁背景圖','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">1920 x 1280 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'N'}]");

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("design_by", "[{'subject': 'Design by','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'Minmax/E-creative','Val':'Minmax/E-creative','useLang':'N'}]");


            #endregion






            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

        

            return fromData;

        }


       


    
    }
}