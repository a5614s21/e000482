﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Repository.System
{
    public class Smtp : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("host", "[{'subject': '主機HOST','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            main.Add("port", "[{'subject': '發信PORT','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            main.Add("smtp_auth", "[{'subject': '是否有SLL認證','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'是/否','Val':'Y/N'}]");
            main.Add("username", "[{'subject': '信件帳號','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            main.Add("password", "[{'subject': '信件密碼','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            main.Add("from_email", "[{'subject': '來源Email','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

               //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();

         
            #endregion






            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

        

            return fromData;

        }


       


    
    }
}