﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.DB.Models;

namespace Web.Repository.System
{
    public class WebData : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("title", "[{'subject': '網站名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("url", "[{'subject': '網站網址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("name", "[{'subject': '聯絡人姓名','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("phone", "[{'subject': '電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("ext_num", "[{'subject': '分機','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("fax", "[{'subject': '傳真','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("servicemail", "[{'subject': '客服信箱','type': 'email','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
         
            main.Add("UrbanContactEmail", "[{'subject': '客戶滿意度調查收件信箱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如有多組信箱，請使用逗號來區分，以免信件發送失敗</small>','useLang':'Y'}]");
            main.Add("ContactEmail", "[{'subject': '客戶聯絡表單收件信箱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如有多組信箱，請使用逗號來區分，以免信件發送失敗</small>','useLang':'Y'}]");
            main.Add("CaseEmail", "[{'subject': '投資人意見留言板收件信箱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

           // main.Add("address", "[{'subject': '地址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
           // main.Add("facebook_url", "[{'subject': 'FaceBook粉絲團','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
          //  main.Add("global_url", "[{'subject': '地產集團員工網','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
          //  main.Add("mail_app_url", "[{'subject': '集團電子郵件平台','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

         //   main.Add("contact_info", "[{'subject': '開發洽詢資訊','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("seo_description", "[{'subject': 'SEO敘述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("seo_keywords", "[{'subject': 'SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            main.Add("token_url", "[{'subject': '線上建材設備選定系統連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("token_timeout", "[{'subject': 'Cookies TimeOut','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">以分鐘為單位，如未輸入則預設為2小時</small>','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;
        }
    }

    public class WebDataService
    {
        private Model _context = new Model();

        public web_data GetData()
        {
            try
            {
                var result = _context.web_data.Where(x => x.lang == "tw").SingleOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}