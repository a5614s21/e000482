﻿'use strict';

$(function () {
    $("header").each(function () {
        $("header .lang .toggle").click(function () {
            $(this).next().stop().slideToggle();
        });
        $(".menu-toggle").click(function () {
            $("html").toggleClass("menuOpen");
        });
        $(".gray_block").click(function () {
            $("html").removeClass("menuOpen");
        });
    });

    $("footer").each(function () {
        $("#goTop").on("click", function () {
            $("html, body").animate({ scrollTop: 0 }, 800);
        });
        $("footer .bg-red").hover(function () {
            $(this).addClass("active");
        }, function () {
            var $this = $(this);
            setTimeout(function () {
                $this.removeClass("active");
            }, 1000);

        });

    });

    $(".historyPage").each(function () {
        $(this).find(".nav li").click(function () {
            var $index = $(this).index();
            var targetblock = $("[data-block]").filter(function () {
                return $(this).attr("data-block") == $index;
            });
            $("html,body").animate({
                scrollTop: targetblock.offset().top - headerH
            }, 1000);
        });
    });

})