﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.DB.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace Web.AdminService
{
    public class DataTableListData : Controller
    {
        /// <summary>
        /// 調整DataTable資料輸出
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static List<Object> dataTableListData(dynamic list, string tables, string urlRoot, NameValueCollection requests , string guid = "")
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            Model DB = new Model();

            List<Object> re = new List<Object>();

            Dictionary<String, Object> dataTableRow = TablesService.dataTableTitle(tables , guid);//資料欄位

            string json = JsonConvert.SerializeObject(list, Formatting.Indented);//轉型(model to Json String)

            var Json1 = Newtonsoft.Json.Linq.JArray.Parse(json);//第一層列表轉json陣列
            string picurlRoot = urlRoot;
            if (urlRoot == "/")
            {
                urlRoot = "";
            }

            //取得選單一些基本資訊

            IQueryable<system_menu> system_menu_search = DB.system_menu.Where(m => m.tables == tables);
            if (requests["category"] != null && requests["category"].ToString() != "")
            {
                string category = requests["category"].ToString();
                string act_path = "list/" + category;
                system_menu_search = system_menu_search.Where(m => m.act_path == act_path);
            }

            var system_menu = system_menu_search.FirstOrDefault();
            if (system_menu == null)
            {
                system_menu = DB.system_menu.Where(m => m.tables == tables).FirstOrDefault();
            }

            if (guid != null && guid != "")
            {
                string act_path = "list/" + guid;
                system_menu = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();
            }

            int i = 0;
            foreach (var dataList in Json1)
            {
                Dictionary<String, Object> rowData = new Dictionary<string, object>();

                string thisGuid = dataList["guid"].ToString();

                rowData.Add("DT_RowId", "row_" + thisGuid);//欄位ID

                foreach (KeyValuePair<string, object> dataItem in dataTableRow)
                {
                    switch (dataItem.Key.ToString())
                    {
                        #region 鍵值

                        case "guid":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/checkbox.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$title}", tableTitleKey(tables, dataList));
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "pic":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                var key = dataList[dataItem.Key.ToString()].ToString();
                                var u = HttpUtility.UrlDecode(key);
                                u = u.Replace("//", "").Replace("///", "").Replace("Content/", "/Content/").Replace("//Content/", "/Content/").Replace("Styles/", "/Styles/").Replace("//Styles/", "/Styles/");
                                if (picurlRoot == "/")
                                {
                                    readText = readText.Replace("{$value}", u);
                                }
                                else
                                {
                                    readText = readText.Replace("{$value}", picurlRoot + u);
                                }

                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "icon":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                var key = dataList[dataItem.Key.ToString()].ToString();
                                var u = HttpUtility.UrlDecode(key);
                                u = u.Replace("//", "").Replace("///", "").Replace("Content/", "/Content/").Replace("//Content/", "/Content/").Replace("Styles/", "/Styles/").Replace("//Styles/", "/Styles/");
                                if (picurlRoot == "/")
                                {
                                    readText = readText.Replace("{$value}", u);
                                }
                                else
                                {
                                    readText = readText.Replace("{$value}", picurlRoot + u);
                                }

                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "list_pic":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                var key = dataList[dataItem.Key.ToString()].ToString();
                                var u = HttpUtility.UrlDecode(key);

                                u = u.Replace("//", "").Replace("///", "").Replace("Content/", "/Content/").Replace("//Content/", "/Content/").Replace("Styles/", "/Styles/").Replace("//Styles/", "/Styles/");

                                if (picurlRoot == "/")
                                {
                                    readText = readText.Replace("{$value}", u);
                                }
                                else
                                {
                                    readText = readText.Replace("{$value}", picurlRoot + u);
                                }

                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 內容摘要

                        case "info":

                            if (thisGuid != null && thisGuid != "")
                            {
                                if (tables != "shareholders")
                                {
                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/info.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);

                                    readText = readText.Replace("{$value}", dataList["title"].ToString());

                                    //置頂
                                    string sticky = "";
                                    if (dataList["sticky"] != null && dataList["sticky"].ToString() != "")
                                    {
                                        if (dataList["sticky"].ToString() == "Y")
                                        {
                                            sticky = "<span class=\"badge badge-pill badge-warning ml-2\">置頂</span>";
                                        }
                                    }
                                    readText = readText.Replace("{$sticky}", sticky);

                                    //今日異動
                                    string modifydate = "";
                                    if (dataList["modifydate"] != null && dataList["modifydate"].ToString() != "")
                                    {
                                        string date = DateTime.Parse(dataList["modifydate"].ToString()).ToString("yyyy-MM-dd");
                                        if (date == DateTime.Now.ToString("yyyy-MM-dd"))
                                        {
                                            modifydate = "<span class=\"badge badge-pill badge-info ml-2\">今日異動</span>";
                                        }
                                    }
                                    readText = readText.Replace("{$modifydate}", modifydate);

                                    //簡述
                                    string content = "";

                                    if (dataList["content"] != null && dataList["content"].ToString() != "")
                                    {
                                        content = Regex.Replace(dataList["content"].ToString(), "<.*?>", String.Empty);
                                        content = content.Replace(System.Environment.NewLine, String.Empty);
                                        content = content.Trim();
                                        if (content.Length > 50)
                                        {
                                            content = content.Substring(0, 50) + "...";
                                        }
                                        content = "<p class=\"m-0 p-0 text-pre-line\"><small>" + content + "</small></p>";
                                    }
                                    readText = readText.Replace("{$content}", content);

                                    string notes = "";//

                                    readText = readText.Replace("{$notes}", notes);

                                    //連結
                                    string addId = "";
                                    //dataTableCategory
                                    if (context.Session["dataTableCategory"] != null)
                                    {
                                        addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                    }
                                    readText = readText.Replace("{$url}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                                else
                                {
                                    string readText = "<div>" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }
                            break;

                        #endregion

                        #region 分類或其他上層對應

                        case "classic":
                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                try
                                {
                                    string categoryID = "";
                                    switch (tables)
                                    {
                                        case "cases":
                                            categoryID = dataList["classic"].ToString();
                                            readText = GetCategory.getCategory("cases_classic", categoryID);
                                            if (string.IsNullOrEmpty(readText))
                                            {
                                                readText = "無";
                                            }
                                            break;

                                        default:
                                            categoryID = dataList["classic"].ToString();
                                            readText = GetCategory.getCategory("cases_classic", categoryID);
                                            break;
                                    }
                                }
                                catch { }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        case "category":
                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                try
                                {
                                    string categoryID = "";
                                    switch (tables)
                                    {
                                        case "cases":
                                            categoryID = dataList["category"].ToString();
                                            readText = GetCategory.getCategory(system_menu.category_table, categoryID);
                                            if (string.IsNullOrEmpty(readText))
                                            {
                                                readText = "無";
                                            }
                                            break;

                                        default:
                                            categoryID = dataList["category"].ToString();
                                            readText = GetCategory.getCategory(system_menu.category_table, categoryID);
                                            break;
                                    }
                                }
                                catch { }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 複選分類

                        case "pluralCategory":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                if (tables == "user")
                                {
                                    string[] categoryArr = dataList["role_guid"].ToString().Split(',');
                                    for (int s = 0; s < categoryArr.Length; s++)
                                    {
                                        string categoryID = categoryArr[s].ToString();
                                        var category = DB.roles.Where(m => m.guid == categoryID).FirstOrDefault();
                                        if (category != null)
                                        {
                                            readText += category.title + "<br>";
                                        }
                                    }
                                }

                                readText = "<div>" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 審核者

                        case "user":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";

                                if (dataList["user_guid"] != null && dataList["user_guid"].ToString() != "")
                                {
                                    string categoryID = dataList["user_guid"].ToString();
                                    var category = DB.user.Where(m => m.guid == categoryID).FirstOrDefault();
                                    if (category != null)
                                    {
                                        readText = category.name;
                                    }
                                    else
                                    {
                                        readText = "查無審核者!";
                                    }
                                }
                                else
                                {
                                    readText = "未指定";
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 瀏覽次數

                        case "view":

                            /*  if (thisGuid != null && thisGuid != "")
                              {
                                  int record_log_qty = DB.record_log.Where(m => m.from == tables).Where(m => m.from_guid == thisGuid).Where(m => m.types == "view").Count();
                                  string readText = "<div class=\"text-center\">"+ record_log_qty.ToString() + "</div>";
                                  rowData.Add(dataItem.Key.ToString(), readText);
                              }*/
                            break;

                        #endregion

                        #region 日期

                        case "date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 日期

                        case "post_date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 發布日期

                        case "startdate":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 發布日期2

                        case "create_date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    if (tables == "member_log" || tables == "payment_apilog")
                                    {
                                        dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                                    }
                                    else
                                    {
                                        dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                    }
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 狀態

                        case "status":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/status.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "啟用";
                                string status = dataList[dataItem.Key.ToString()].ToString();

                                if (status == "N")
                                {
                                    style = "secondary";
                                    statusSubject = "停用";
                                }

                                List<string> contactUse = new List<string> { "survey" , "contact" };

                                if (contactUse.IndexOf(tables) != -1)
                                {
                                    statusSubject = "已處理";
                                    if (status == "N")
                                    {
                                        statusSubject = "未處理";
                                    }
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);
                                readText = readText.Replace("{$status}", status);
                                readText = readText.Replace("{$guid}", thisGuid);

                                List<string> otherUse = new List<string> { "survey", "contact","member", "inquire", "user" , "project_users" };

                                if (otherUse.IndexOf(tables) != -1)
                                {
                                    readText = readText.Replace("{$title}", dataList["name"].ToString());
                                }
                                else
                                {
                                    readText = readText.Replace("{$title}", dataList["title"].ToString());
                                }

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 討論區狀態

                        case "forum_status":

                            if (dataList["status"] != null && dataList["status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "待分派";
                                string status = dataList["status"].ToString();

                                if (status == "disable")
                                {
                                    style = "secondary";
                                    statusSubject = "停用";
                                }
                                if (status == "stagnate")
                                {
                                    style = "dark";
                                    statusSubject = "後續評估";
                                }
                                if (status == "success")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }
                                if (status == "warning")
                                {
                                    style = "warning";
                                    statusSubject = "處理中";
                                }
                                if (status == "end")
                                {
                                    style = "dark";
                                    statusSubject = "已解決";
                                }
                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 回覆狀態

                        case "re_status":

                            if (dataList["re_status"] != null && dataList["re_status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "尚未回覆";
                                string status = dataList["re_status"].ToString();

                                if (status == "Y")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "");
                            }
                            break;

                        #endregion

                        #region 角色

                        case "role":

                            if (dataList["role"] != null && dataList["role"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "primary";
                                string statusSubject = "回覆者";
                                string status = dataList["role"].ToString();

                                if (status == "Tongren")
                                {
                                    style = "secondary";
                                    statusSubject = "同仁";
                                }
                                if (status == "Asker")
                                {
                                    style = "danger";
                                    statusSubject = "提問者";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 屬性

                        case "type":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string readText = "";
                                if (tables == "holiday")
                                {
                                    string type = "休假";
                                    if (dataList[dataItem.Key.ToString()].ToString() == "duty")
                                    {
                                        type = "補上班";
                                    }
                                    readText = type;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 排序

                        case "sortIndex":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/sortIndex.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$guid}", thisGuid);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 成員數

                        case "user_qty":
                            if (dataList["guid"] != null)
                            {
                                string role_guid = dataList["guid"].ToString();
                                var data = DB.user.Where(m => m.role_guid.Contains(role_guid)).Where(m => m.username != "sysadmin").Count();
                                string readText = "<div>" + data + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 來源資料表名稱

                        case "tables":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string fromTable = dataList[dataItem.Key.ToString()].ToString();
                                var data = DB.system_menu.Where(m => m.tables == fromTable).Select(a => new { title = a.title }).FirstOrDefault();
                                rowData.Add(dataItem.Key.ToString(), data.title);
                            }
                            break;

                        #endregion

                        #region 動作

                        case "action":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/action.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$guid}", thisGuid);

                                //是否修改
                                if (system_menu.can_edit == "Y")
                                {
                                    readText = readText.Replace("{$editHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$editHide}", "style=\"display:none;\"");
                                }
                                //是否可刪除
                                dynamic permissions = context.Session["permissions"];//取得權限紀錄
                                if (system_menu.can_del == "Y" && permissions[system_menu.guid] == "F")
                                {
                                    readText = readText.Replace("{$delHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$delHide}", "style=\"display:none;\"");
                                }

                                string addId = "";
                                //dataTableCategory
                                if (context.Session["dataTableCategory"] != null)
                                {
                                    addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                }

                                //連結
                                readText = readText.Replace("{$editUrl}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);

                                if (system_menu.index_view_url != null && system_menu.index_view_url.ToString() != "")
                                {
                                    readText = readText.Replace("{$index_view_url}", "<a href=\"" + urlRoot + system_menu.index_view_url.ToString() + thisGuid + "\" target=\"_blank\" class=\"btn btn-outline-default btn-sm\" role=\"button\" aria-pressed=\"true\" title=\"瀏覽前端\"><i class=\"icon-eye3\"></i><span class=\"text-hide\">瀏覽</span></a>");
                                }
                                else
                                {
                                    readText = readText.Replace("{$index_view_url}", "");
                                }

                                readText = readText.Replace("{$title}", tableTitleKey(tables, dataList));

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 按鈕

                        case "button":
                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/button.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                switch (tables)
                                {
                                    case "urban_column":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/urban_column_files/list/" + thisGuid);
                                        break;

                                    case "cases":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/cases_progress/list/" + thisGuid);
                                        break;
                                }

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 預約賞屋

                        case "CasesReservation":
                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/button.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$url}", urlRoot + "/siteadmin/cases_reservation/list/" + thisGuid);
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion


                        #region 公司

                        case "company":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                if(FunctionService.IsValidJson(dataList[dataItem.Key.ToString()].ToString()))
                                {
                                    companyData obj = JsonConvert.DeserializeObject<companyData>(dataList[dataItem.Key.ToString()].ToString());

                                    string readText = "<div>" + obj.title + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                                else
                                {
                                    string readText = "<div>" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }

                             
                            }

                            break;

                        #endregion

                        case "username":
                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string username = FunctionService.aesDecryptBase64(dataList[dataItem.Key.ToString()].ToString(), "minmax");

                                
                                string readText = "<div>" + username + "</div>";
                                if (string.IsNullOrEmpty(username))
                                {
                                    readText = "<div>" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                }


                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        default:
                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string readText = "<div >" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                    }
                }

                re.Add(rowData);
            }

            return re;
        }

        /// <summary>
        /// 回傳資料表的標題或姓名
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string tableTitleKey(string tables, dynamic dataList)
        {
            Model DB = new Model();
            string re = "";
            switch (tables)
            {
                case "user":
                    re = dataList["name"];
                    break;

                default:
                    re = dataList["title"];
                    break;
            }
            return re;
        }


    }
}