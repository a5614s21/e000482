﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.DB.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Web.AdminService;
using System.Runtime.Remoting;

namespace Web.AdminService
{
    public class GetCategory : Controller
    {
        /// <summary>
        /// 取得分類(全部)
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getCategory(string tables, dynamic data = null, string category = "")
        {
            Model DB = new Model();
            dynamic re = null;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系
            string dataType = "";

            system_menu system_menu = DB.system_menu.Where(m => m.tables == tables).FirstOrDefault();

            if (data != null)
            {
                dataType = data.GetType().ToString();
            }

            switch (tables)
            {

                case "carsysl2":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.carsysl2.Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.carsysl2.Where(m => m.lang == defLang).Where(m => m.guid == Int32.Parse(guid)).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.caption;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;

                case "carsysl4":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.carsysl4.Where(m => m.lang == defLang && m.l3 != 12).ToList();
                            foreach (var item in re)
                            {
                                int l3 = item.l3;
                                if (l3 != 12)
                                {
                                    var tempL3 = (from a in DB.carsysl3
                                                  where a.guid == l3
                                                  select a).FirstOrDefault();
                                    var tempL2 = (from a in DB.carsysl2
                                                  where a.guid == tempL3.l2
                                                  select a).FirstOrDefault();

                                    item.caption = tempL2.caption.Split('(')[0] + " - " + item.caption.Split('(')[0];
                                }
                            }
                                
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.carsysl4.Where(m => m.lang == defLang).Where(m => m.guid == Int32.Parse(guid)).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.caption;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;

                case "history_category":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.history_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.history_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.title;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;
                //下載專區分類
                case "download_category":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            if (category != "")
                            {
                                re = DB.download_category.Where(m => m.status == "Y").Where(m => m.category == category).Where(m => m.lang == defLang).ToList();
                            }
                            else
                            {
                                re = DB.download_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                            }

                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.download_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.title;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;

                //人力資源
                case "welfare_category":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.welfare_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.welfare_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.title;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;

                //滿意度調查分類
                case "problem_category":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.problem_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.problem_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.title;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;

                //系統項目分類
                case "system_item_category":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.system_item_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.system_item_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.title;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;

                //客戶教育訓練
                case "course_category":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.course_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.course_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.title;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;
                //最新消息分類
                case "news_category":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.news_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.news_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.title;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;
                //群組
                case "roles":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.roles.Where(m => m.status == "Y").ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.roles.Where(m => m.status == "Y").Where(m => m.guid == guid).FirstOrDefault().title;
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;

                //使用者
                case "user":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.user.Where(m => m.status == "Y").ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.user.Where(m => m.status == "Y").Where(m => m.guid == guid).FirstOrDefault().name;
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;
            }

            return re;
        }
    }
}