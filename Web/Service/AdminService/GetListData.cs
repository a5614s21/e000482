﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.DB.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Web.AdminService;
using System.Data.SqlClient;
using System.Data;

namespace Web.AdminService
{
    public class GetListData
    {
        /// <summary>
        /// 取得列表資料提供給DataTable
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> getListData(string tables, NameValueCollection requests, string urlRoot, string guid = "")
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> list = new Dictionary<String, Object>();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            int skip = int.Parse(requests["start"].ToString());
            int take = int.Parse(requests["length"].ToString());
            if (take == -1)
            {
                take = 9999;
            }
            int iTotalDisplayRecords = 0;
            dynamic listData = null;
            dynamic sSearch = null;

            if (!string.IsNullOrEmpty(requests["search[value]"]))
            {
                sSearch = Newtonsoft.Json.Linq.JArray.Parse(requests["search[value]"].ToString());
                sSearch = sSearch[0];
            }

            //取得預設傳值
            Dictionary<string, string> defaultSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(requests["sSearch"]))
            {
                try
                {
                    var defaultSearchStr = requests["sSearch"].ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Split(new[] { '=' }));
                    foreach (var item in defaultSearchStr)
                    {
                        defaultSearch.Add(item[0], item[1]);
                    }
                }
                catch { }
            }

            string columns = requests["order[0][column]"];

            string orderByKey = requests["columns[" + columns + "][data]"].ToString();
            string orderByType = requests["order[0][dir]"].ToString();
            string orderBy = formatOrderByKey(tables, orderByKey) + " " + orderByType.ToUpper();

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //產品資訊
                case "product_info":

                    //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                    //conn.Open();
                    //SqlCommand cmd = new SqlCommand("select * from product", conn);
                    //SqlDataReader reader = cmd.ExecuteReader();

                    //while (reader.Read())
                    //{
                    //    var productid = Int32.Parse(reader[0].ToString());

                    //    var productNew = (from a in DB.product_info
                    //                      where a.id == productid
                    //                      select a).FirstOrDefault();
                    //    if (productNew == null)
                    //    {
                    //        product_info c = new product_info();
                    //        var newGid = Guid.NewGuid().ToString();
                    //        c.guid = newGid;
                    //        c.product_id = productid;
                    //        c.create_date = DateTime.Now;
                    //        c.lang = "tw";

                    //        SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                    //        conn2.Open();
                    //        string filecheck = "picture1";
                    //        string productfilename;
                    //        string img_sql = "select * from product_file where product_id= " + productid + " and filecheck= '" + filecheck + "'";
                    //        SqlCommand cmd2 = new SqlCommand(img_sql, conn2);
                    //        SqlDataReader reader2 = cmd2.ExecuteReader();
                            
                    //        if (reader2.Read())
                    //        {
                    //            productfilename = reader2[2].ToString();
                    //            if (productfilename != "")
                    //            {
                    //                var temp = productfilename.Split('.');
                    //                var productImg = "/Content/Upload/images/product/" + productid + "/" + temp[0] + "_WM." + temp[1];
                    //                c.pic = productImg;
                    //            }
                    //        }

                    //        conn2.Close();

                    //        SqlConnection conn3 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                    //        conn3.Open();
                    //        string filecheck2 = "picture2";
                    //        string productspc;
                    //        string img_sql2 = "select * from product_file where product_id= " + productid + " and filecheck= '" + filecheck2 + "'";
                    //        SqlCommand cmd3 = new SqlCommand(img_sql2, conn3);
                    //        SqlDataReader reader3 = cmd3.ExecuteReader();

                    //        if (reader3.Read())
                    //        {
                    //            productspc = reader3[2].ToString();
                    //            if (productspc != "")
                    //            {
                    //                var temp = productspc.Split('.');
                    //                var productSpc = "/Content/Upload/images/product/" + productid + "/" + temp[0] + "_WM." + temp[1];
                    //                c.spec_pic = productSpc;
                    //            }
                    //        }
                     
                    //        conn3.Close();

                    //        SqlConnection conn5 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                    //        conn5.Open();
                    //        string filecheck3 = "picture3";
                    //        string productpaper;
                    //        string img_sql3 = "select * from product_file where product_id= " + productid + " and filecheck= '" + filecheck3 + "'";
                    //        SqlCommand cmd5 = new SqlCommand(img_sql3, conn5);
                    //        SqlDataReader reader5 = cmd5.ExecuteReader();

                    //        if (reader5.Read())
                    //        {
                    //            productpaper = reader5[2].ToString();
                    //            if (productpaper != "")
                    //            {
                    //                var temp = productpaper.Substring(0, productpaper.Length - 4);
                    //                var temp2 = productpaper.Substring(productpaper.Length - 4);
                    //                var productPaper = "/Content/Upload/images/product/" + productid + "/" + temp + "_WM" + temp2;
                    //                c.paper_pic = productPaper;
                    //            }
                    //        }

                    //        conn5.Close();

                    //        DB.product_info.Add(c);
                    //        DB.SaveChanges();

                    //        product_info c2 = new product_info();
                    //        c2.guid = newGid;
                    //        c2.product_id = productid;
                    //        c2.create_date = DateTime.Now;
                    //        c2.lang = "cn";

                    //        DB.product_info.Add(c2);
                    //        DB.SaveChanges();
                    //    }
                    //}

                    if (model != null)
                    {
                        var data = model.Repository<product_info>();

                        var models = data.Reads();

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.newName.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //首頁content
                case "index_content":
                    if (model != null)
                    {
                        var data = model.Repository<index_content>();

                        var models = data.ReadsWhere(m => m.lang == defLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //客戶意見反饋
                case "feedBack":
                    if (model != null)
                    {
                        var data = model.Repository<problem>();

                        var models = data.ReadsWhere(m => m.lang == defLang && m.category == "category7");

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //系統項目分類
                case "system_item_category":
                    if (model != null)
                    {
                        var data = model.Repository<system_item_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //系統項目
                case "system_item":
                    if (model != null)
                    {
                        var data = model.Repository<system_item>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //問券問題
                case "problem_category":
                    if (model != null)
                    {
                        var data = model.Repository<problem_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //問券問題
                case "problem":
                    if (model != null)
                    {
                        var data = model.Repository<problem>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;


                //胡連沿革分類
                case "history_category":
                    if (model != null)
                    {
                        var data = model.Repository<history_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //線束2
                case "carsysl2":
                    if (model != null)
                    {
                        var data = model.Repository<carsysl2>();

                        var models = data.ReadsWhere(m => m.lang == defLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.ToList().Skip(skip).Take(take);
                    }
                    break;

                //線束對應產品
                case "mapcarsys":
                    if (model != null)
                    {
                        var data = model.Repository<mapcarsys>();

                        var models = data.ReadsWhere(m => m.lang == defLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.ToList().Skip(skip).Take(take);
                    }
                    break;

                //線束4
                case "carsysl4":
                    if (model != null)
                    {
                        var data = model.Repository<carsysl4>();

                        var models = data.ReadsWhere(m => m.lang == defLang);

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.ToList().Skip(skip).Take(take);
                    }
                    break;

                //胡連沿革
                case "history":
                    if (model != null)
                    {
                        var data = model.Repository<history>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;


                //會員
                case "member":
                    if (model != null)
                    {
                        var data = model.Repository<member>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.name.Contains(keywords) || m.company.Contains(keywords) || m.username.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //投資人意見留言板
                case "inquire":
                    if (model != null)
                    {
                        var data = model.Repository<inquire>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.name.Contains(keywords) || m.company.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //客戶聯絡表單
                case "contact":
                    if (model != null)
                    {
                        var data = model.Repository<contact>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.name.Contains(keywords) || m.company.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;


                //客戶滿意度調查
                case "survey":
                    if (model != null)
                    {
                        var data = model.Repository<survey>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.name.Contains(keywords) || m.no.Contains(keywords) || m.company.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //廣告
                case "advertise":
                    if (model != null)
                    {
                        //取得是否有下層
                        // List<string> _CategoriesId = DB.download_category.Where(m => m.category == guid).Select(x => x.guid).ToArray().ToList();


                        var data = model.Repository<advertise>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);



                        if (guid != "")
                        {
                            models = models.Where(m => m.category == guid);

                        }

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            /*  if (sSearch["category"].ToString() != "")
                              {
                                  string category = sSearch["category"].ToString();
                                  models = models.Where(m => m.category.Contains(category));
                              }*/
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //投資人Q&A
                case "shareholder_faq":
                    if (model != null)
                    {
                        var data = model.Repository<shareholder_faq>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //人力資源
                case "welfare":
                    if (model != null)
                    {
                        var data = model.Repository<welfare>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //人力資源分類
                case "welfare_category":
                    if (model != null)
                    {
                        var data = model.Repository<welfare_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;



                //下載專區分類
                case "download_category":
                    if (model != null)
                    {
                        var data = model.Repository<download_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang).Where(m => m.lang == defLang).Where(m => m.category == "audit_master");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //下載專區
                case "downloads":
                    if (model != null)
                    {
                        //取得是否有下層
                        List<string> _CategoriesId = DB.download_category.Where(m => m.category == guid).Select(x => x.guid).ToArray().ToList();


                        var data = model.Repository<downloads>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);



                        if (guid != "")
                        {
                            if (_CategoriesId.Count > 0)
                            {
                                models = models.Where(m => _CategoriesId.Contains(m.category));
                            }
                            else
                            {
                                models = models.Where(m => m.category == guid);
                            }

                        }

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //全球據點
                case "locations":
                    if (model != null)
                    {
                        var data = model.Repository<locations>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;


                //客戶教育訓練平台
                case "courses":
                    if (model != null)
                    {
                        var data = model.Repository<courses>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //客戶教育訓練平台分類
                case "course_category":
                    if (model != null)
                    {
                        var data = model.Repository<course_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //商務專區-FAQ問與答
                case "business_faq":
                    if (model != null)
                    {
                        var data = model.Repository<business_faq>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //最新消息分類
                case "news_category":
                    if (model != null)
                    {
                        var data = model.Repository<news_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //最新消息
                case "news":
                    if (model != null)
                    {
                        var data = model.Repository<news>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //群組管理
                case "roles":
                    if (model != null)
                    {
                        var data = model.Repository<roles>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //使用者
                case "user":
                    if (model != null)
                    {
                        var data = model.Repository<user>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.username != "sysadmin");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.username.Contains(keywords) || m.name.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.role_guid.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //防火牆
                case "firewalls":
                    if (model != null)
                    {
                        var data = model.Repository<firewalls>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //系統日誌
                case "system_log":
                    if (model != null)
                    {
                        var data = model.Repository<system_log>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.ip.Contains(keywords) || m.notes.Contains(keywords));
                            }
                            /*if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }*/
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //資源回收桶
                case "ashcan":
                    if (model != null)
                    {
                        var data = model.Repository<ashcan>();

                        var models = data.ReadsWhere(m => m.from_guid != "");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

            }

            list.Add("data", DataTableListData.dataTableListData(listData, tables, urlRoot, requests, guid));
            list.Add("iTotalDisplayRecords", iTotalDisplayRecords);

            return list;
        }

        /// <summary>
        /// 格式化排序欄位
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="orderByKey"></param>
        /// <returns></returns>
        public static string formatOrderByKey(string tables, string orderByKey)
        {
            string re = "";
            switch (orderByKey)
            {
                case "info":
                    re = "title";
                    break;

                case "view_info":
                    re = "nums";
                    break;

                case "forum_status":
                    re = "status";
                    break;

                case "action":
                    re = "guid";
                    break;

                case "user":
                    re = "user_guid";
                    break;

                case "category":
                    re = "category";
                    if (tables == "package_content")
                    {
                        re = "package_guid";
                    }
                    break;

                default:
                    re = orderByKey;
                    break;
            }
            return re;
        }
    }
}