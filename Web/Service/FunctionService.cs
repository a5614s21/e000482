﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.DB.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.Resources;
using System.Reflection;
using System.Globalization;
using System.Collections;
using System.Net.Mail;
using System.Web.Caching;
using System.Text.RegularExpressions;
using System.IO;
using Web.Controllers;
using System.Net;
using Web.Repository.System;
using System.Net.Http;
using Web.ServiceModels;

namespace Web.Service
{
    public class FunctionService : Controller
    {
        // GET: FunctionService

        //public static dynamic CallWebService_PayList(string UserID)
        //{
        //    try
        //    {
        //        System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;//更改較少資訊Cache使用
        //        CrpdtaService.WebServiceSoapClient service = new CrpdtaService.WebServiceSoapClient();

        //        var NewUserID = "";
        //        if(UserID.Length < 10)
        //        {
        //            NewUserID = UserID.PadRight(10, ' ');
        //        }
        //        else
        //        {
        //            NewUserID = UserID;
        //        }


        //        string CacheName = "GetClassPayList" + UserID;
        //        string testJson = "";
        //        string status = "";
        //        if(cacheContainer.Get(CacheName) == null)
        //        {
        //            string token = service.GenerateToken(UserID);
        //            testJson = service.GetClassPayList_ByUserID(token, NewUserID);
        //            cacheContainer.Insert(CacheName, testJson, null, DateTime.Now.AddHours(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);
        //            status = "getapi";
        //        }
        //        else
        //        {
        //            testJson = cacheContainer.Get(CacheName).ToString();
        //            status = "getcache";
        //        }

        //        testJson = ASCII2Str(testJson);


        //        //新增API呼叫紀錄
        //        PaymentApiLogService log = new PaymentApiLogService();
        //        var ip = FunctionService.GetIP();
        //        log.InsertLog(UserID, ip, status, testJson);

        //        return testJson;
        //    }
        //    catch(Exception ex)
        //    {
        //        return "Error";
        //    }
        //}


        /// <summary>
        /// 呼叫交易紀錄API
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        //public static dynamic CallWebService(string UserID = "B100431918")
        //{
        //    try
        //    {
        //        System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;//更改較少資訊Cache使用

        //        CrpdtaService.WebServiceSoapClient service = new CrpdtaService.WebServiceSoapClient();

        //        var newUserID = "";
        //        //newUserID = UserID;
        //        if (UserID.Length < 10)
        //        {
        //            newUserID = UserID.PadRight(10, ' ');
        //        }
        //        else
        //        {
        //            newUserID = UserID;
        //        }

        //        string CacheName = "GetClassPayList" + UserID;
        //        string testJson = "";

        //        var status = "";

        //        if (cacheContainer.Get(CacheName) == null)
        //        {
        //            string token = service.GenerateToken(UserID);
        //            testJson = service.GetClassPayList(token, newUserID);
        //            cacheContainer.Insert(CacheName, testJson, null, DateTime.Now.AddHours(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);
        //            status = "getapi";
        //        }
        //        else
        //        {
        //            testJson = cacheContainer.Get(CacheName).ToString();//自cache取出
        //            status = "getcache";
        //        }

        //        testJson = ASCII2Str(testJson);

        //        //新增API呼叫紀錄
        //        PaymentApiLogService log = new PaymentApiLogService();
        //        var ip = FunctionService.GetIP();
        //        log.InsertLog(UserID, ip, status, testJson);

        //        return testJson;
        //    }
        //    catch(Exception ex)
        //    {
        //        return "Error";
        //    }            
        //}


        /// <summary>
        /// 傳輸解密
        /// </summary>
        /// <param name="textAscii"></param>
        /// <returns></returns>
        public static string ASCII2Str(string textAscii)
        {
            try
            {
                int k = 0;//位元組移動偏移量

                byte[] buffer = new byte[textAscii.Length / 2];//儲存變數的位元組

                for (int i = 0; i < textAscii.Length / 2; i++)
                {
                    //每兩位合併成為一個位元組
                    buffer[i] = byte.Parse(textAscii.Substring(k, 2), System.Globalization.NumberStyles.HexNumber);
                    k = k + 2;
                }
                //將位元組轉化成漢字
                //return Encoding.Default.GetString(buffer);

                var e = Encoding.GetEncoding("big5");
                var result = e.GetString(buffer);

                return result;
            }
            catch (Exception ex)
            {
                // Log.WriteLog4("ASCII轉含中文字串異常" + ex.Message);
            }
            return "";
        }

        /// <summary>
        ///  Call API
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static dynamic CallApi(string url, string type, Dictionary<string, string> data)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "POST";
            if (type != "Info")
            {
                request.ContentType = "application/json";
            }
            else
            {
                request.Method = "GET";
            }

            if (type != "Login" && type != "NewPassword")
            {
                request.Headers.Add("Authorization", "Bearer " + data["jwtToken"].ToString());
            }

            if (type == "Password" || type == "NewPassword")
            {
                request.Method = "PATCH";
            }

            //必須透過ParseQueryString()來建立NameValueCollection物件，之後.ToString()才能轉換成queryString
            //NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
            string json = "";

            if (type != "Info")
            {
                data.Remove("jwtToken");//移除傳入的token

                try
                {
                    json = JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.None);
                    using (var reqStream = new StreamWriter(request.GetRequestStream()))
                    {
                        reqStream.Write(json);
                        reqStream.Flush();
                        reqStream.Close();
                    }//end using
                }
                catch (Exception e)
                {
                    Console.WriteLine("錯誤：" + e.Message.ToString());
                    return "Error:" + e.Message.ToString();
                }
            }

            //API回傳的字串
            string responseStr = "";

            try
            {
                //發出Request
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        responseStr = sr.ReadToEnd();
                    }//end using
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("錯誤：" + e.Message.ToString());
            }

            return responseStr;
        }

        /// <summary>
        /// 儲存LOG
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="files"></param>
        public static void saveLog(dynamic ex, string files)
        {
            try
            {
                string lines = "";
                string fileName = System.Web.Hosting.HostingEnvironment.MapPath("~") + "/Log/" + files;

                if (!System.IO.File.Exists(fileName))
                {
                    System.IO.File.Create(fileName);
                }

                if (System.IO.File.Exists(fileName))
                {
                    StreamReader str = new StreamReader(fileName);
                    str.ReadLine();
                    lines = str.ReadToEnd();
                    str.Close();
                }

                lines += DateTime.Now.ToString() + "：Message => " + ex.Message + "，";
                lines += $"Main exception occurs {ex}.";
                lines += "\r\n";
                System.IO.File.WriteAllText(fileName, lines);
            }
            catch
            {
            }
        }

        public static string getWebUrl()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            var trueUrl = context.Request.Url.AbsoluteUri;
            string webURL = "http://" + context.Request.ServerVariables["Server_Name"];
            if (context.Request.ServerVariables["Server_Name"] == "localhost")
            {
                webURL = webURL + ":59334";
                //webURL = "http://iis.24241872.tw/projects/public/e000482/test";
            }
            if (context.Request.ServerVariables["Server_Name"].IndexOf("iis.24241872.tw") != -1)
            {
                webURL = "http://iis.24241872.tw/projects/public/e000482/test";
                //webURL = "";
            }
            return webURL;
        }

        /// <summary>
        /// 縣市區域
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
       /* public static List<taiwan_city> TaiwanCitys(string city)
        {
            Model DB = new Model();
            List<taiwan_city> re = new List<taiwan_city>();

            if (city == "")
            {
                re = DB.taiwan_city.Where(m => m.lang == "tw").Where(m => m.lay == "1").ToList();
            }
            else
            {
                re = DB.taiwan_city.Where(m => m.lang == "tw").Where(m => m.city == city).Where(m => m.lay == "2").ToList();
            }

            return re;
        }*/

        /// <summary>
        /// 多圖處理
        /// </summary>
        /// <param name="pic"></param>
        /// <param name="pic_alt"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> PluralImage(string pic, string pic_alt)
        {
            Dictionary<String, Object> reData = new Dictionary<String, Object>();
            List<string> pics = pic.Split(',').ToList();

            List<string> pics_alt = new List<string>();

            List<string> pics_alt_temp = new List<string>();

            if (pic_alt != null && pic_alt != "")
            {
                pics_alt_temp = pic_alt.Split('§').ToList();
            }
            int i = 0;
            foreach (var item in pics)
            {
                if (pic_alt != null && pic_alt != "" && pics_alt_temp[i] != null)
                {
                    pics_alt.Add(pics_alt_temp[i]);
                }
                else
                {
                    pics_alt.Add("　");
                }

                i++;
            }

            reData.Add("pic", pics);
            reData.Add("pic_alt", pics_alt);

            return reData;
        }

        /// <summary>
        /// SEO資料格式化
        /// </summary>
        /// <param name="web_Data"></param>
        /// <param name="title"></param>
        /// <param name="seo_keywords"></param>
        /// <param name="seo_description"></param>
        /// <returns></returns>
        public static Dictionary<String, String> SEO(DB.Models.web_data web_Data, string title, string seo_keywords, string seo_description)
        {
            Dictionary<String, String> reData = new Dictionary<String, String>();

            reData.Add("title", web_Data.title);
            reData.Add("seo_keywords", web_Data.seo_keywords);
            reData.Add("seo_description", web_Data.seo_description);

            if (title != "")
            {
                reData["title"] = title + "|" + reData["title"].ToString();
            }

            if (seo_keywords != "")
            {
                reData["seo_keywords"] = seo_keywords;
            }

            if (seo_description != "")
            {
                reData["seo_description"] = seo_description;
            }

            return reData;
        }

        /// <summary>
        /// 取得語系黨
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static NameValueCollection getLangData(string lang)
        {
            NameValueCollection langData = new NameValueCollection();
            if (lang != "")
            {
                lang = "_" + lang;
            }
            ResourceManager rm = new ResourceManager("Web.App_GlobalResources.Resource" + lang, Assembly.GetExecutingAssembly());
            ResourceSet resourceSet = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
                langData.Add(entry.Key.ToString(), entry.Value.ToString());
            }

            return langData;
        }

        /// <summary>
        /// 格式化檔案更新日期
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        public static string FileModifyTime(string files)
        {
            string re = System.DateTime.Parse(System.IO.File.GetLastWriteTime(files).ToString()).ToString("yyyyMMddHHmmss");
            return re;
        }

        /// <summary>
        /// GUID樣式
        /// </summary>
        /// <returns></returns>
        public static string getGuid()
        {
            string GuidType = ConfigurationManager.ConnectionStrings["GuidType"].ConnectionString;

            if (GuidType == "System")
            {
                return Guid.NewGuid().ToString();
            }
            else
            {
                return DateTime.Now.ToString("yyMMddHHmmssff");
            }
        }

        /// <summary>
        /// 表單JSON轉換
        /// </summary>
        /// <param name="jsonText"></param>
        /// <returns></returns>
        public static NameValueCollection reSubmitFormDataJson(string jsonText)
        {
            NameValueCollection reData = new NameValueCollection();

            dynamic dynObj = JsonConvert.DeserializeObject(jsonText);
            foreach (var item in dynObj)
            {
                if ((string)item.name != "")
                {
                    if (reData[(string)item.name] == null)
                    {
                        string val = item.value;
                        reData.Add((string)item.name, val);
                    }
                    else
                    {
                        string val = reData[(string)item.name] + "," + item.value;
                        reData.Remove((string)item.name);
                        reData.Add((string)item.name, val);
                    }
                }
            }

            return reData;
        }

        /// <summary>
        /// 格式化圖片路徑
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string FormatImagePath(string content)
        {
            if (!string.IsNullOrEmpty(content))
            {
                return content.Replace("../", "")
                   .Replace("src=\"images", "src=\"" + getWebUrl() + "/images")
                   .Replace("src=\"/images", "src=\"" + getWebUrl() + "/images")
                   .Replace("src=\"Content/Upload/images", "src=\"" + getWebUrl() + "/Content/Upload/images")
                   .Replace("src=\"Content/Upload", "src=\"" + getWebUrl() + "/Content/Upload")
                   .Replace("src=\"Styles/images", "src=\"" + getWebUrl() + "/Styles/images")
                   .Replace("src=\"styles/images", "src=\"" + getWebUrl() + "/styles/images")
                   .Replace("href=\"/tw", "href=\"" + getWebUrl() + "/tw")
                   .Replace("href=\"/en", "href=\"" + getWebUrl() + "/en")
                   .Replace("srcset=\"Styles", "srcset=\"" + getWebUrl() + "/Styles")
                   .Replace("srcset=\"Content/Upload", "srcset=\"" + getWebUrl() + "/Content/Upload")
                   //.Replace("http://iis.youweb.tw/projects/public/e000482/test", getWebUrl())
                   ;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// md5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string md5(string str)
        {
            string reStr = "";

            byte[] Original = Encoding.Default.GetBytes(str); //將字串來源轉為Byte[]
            MD5 s1 = MD5.Create(); //使用MD5
            byte[] Change = s1.ComputeHash(Original);//進行加密
            reStr = Convert.ToBase64String(Change);//將加密後的字串從byte[]轉回string

            return reStr;
        }

        /// <summary>
        /// 後台登入判斷
        /// </summary>
        /// <param name="sysUsername"></param>
        /// <param name="adID"></param>
        /// <returns></returns>
        public static bool systemUserCheck()
        {
            Model DB = new Model();

            bool re = false;
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString; //後台登入記錄方式 Session or Cookie
            string username = "";

            if (context.Session["sysUsername"] != null || context.Request.Cookies["sysLogin"] != null)
            {
                if (adminCathType == "Session")
                {
                    if (context.Session["sysUsername"] != null && context.Session["sysUsername"].ToString() != "")
                    {
                        re = true;
                        username = System.Web.HttpContext.Current.Session["sysUsername"].ToString();
                    }
                }
                else
                {
                    if (context.Request.Cookies["sysLogin"] != null)
                    {
                        re = true;
                        HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                        string[] tempAccount = aCookie.Value.Split('&');
                        username = context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", ""));
                    }
                }

                if (username != "")
                {
                    //更新登入日期
                    try
                    {
                        var saveData = DB.user.Where(m => m.username == username).Where(m => m.status == "Y").FirstOrDefault();

                        saveData.logindate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        DB.SaveChanges();

                        getAdminPermissions(username);//權限
                    }
                    catch { }
                }
            }

            //IP判斷
            if (re == true && username != "sysadmin")
            {
                /*
                SOGOEntities DB = new SOGOEntities();
                var ipLock = DB.sogo_ip_lock.Where(m => m.status == "Y").ToList();

                if (ipLock.Count > 0)
                {
                    string ip = GetIP();
                    var ipLock2 = DB.sogo_ip_lock.Where(m => m.status == "Y").Where(m => m.ip == ip).ToList();
                    if (ipLock2.Count > 0)
                    {
                        re = true;
                    }
                    else
                    {
                        re = false;
                    }
                }*/
            }

            return re;
        }

        /// <summary>
        /// 回傳登入者帳號
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, String> ReUserData()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString; //後台登入記錄方式 Session or Cookie

            Dictionary<String, String> userData = new Dictionary<string, string>();

            if (adminCathType == "Session")
            {
                if (System.Web.HttpContext.Current.Session["sysUsername"] != null && System.Web.HttpContext.Current.Session["sysUsername"].ToString() != "")
                {
                    userData.Add("username", System.Web.HttpContext.Current.Session["sysUsername"].ToString());
                    userData.Add("guid", System.Web.HttpContext.Current.Session["sysUserGuid"].ToString());
                }
            }
            else
            {
                if (context.Request.Cookies["sysLogin"] != null)
                {
                    HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                    string[] tempAccount = aCookie.Value.Split('&');

                    userData.Add("username", context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", "").ToString()));
                    userData.Add("guid", context.Server.HtmlEncode(tempAccount[1].Replace("sysUserGuid=", "").ToString()));
                }
            }

            return userData;
        }

        /// <summary>
        /// 角色權限
        /// </summary>
        /// <param name="role_guid"></param>
        /// <returns></returns>
        public static NameValueCollection role_permissions(string role_guid)
        {
            Model DB = new Model();
            string site = ConfigurationManager.ConnectionStrings["site"].ConnectionString;
            var data = DB.role_permissions.Where(m => m.role_guid == role_guid).ToList();

            NameValueCollection re = new NameValueCollection();
            foreach (var item in data)
            {
                re.Add(item.permissions_guid, item.permissions_status);
            }
            return re;
        }

        /// <summary>
        /// 設定權限群組
        /// </summary>
        /// <param name="username"></param>
        public static void getAdminPermissions(string username)
        {
            Model DB = new Model();
            System.Web.HttpContext context = System.Web.HttpContext.Current;

            Dictionary<String, Object> permissions = new Dictionary<String, Object>();
            List<String> permissionsTop = new List<String>();

            context.Session.Remove("verify");

            //非系統權限
            if (username != "sysadmin")
            {
                //取得權限
                var tempUser = DB.user.Where(m => m.username == username).Select(a => new { role_guid = a.role_guid }).FirstOrDefault();
                if (tempUser != null)
                {
                    string role_guid = tempUser.role_guid;
                    var roleData = DB.roles.Where(m => m.guid == role_guid.ToString()).FirstOrDefault();
                    var perData = DB.role_permissions.Where(m => m.role_guid == role_guid.ToString()).ToList();

                    //審核權限

                   // context.Session.Add("verify", roleData.verify);

                    if (perData.Count > 0)
                    {
                        foreach (var item in perData)
                        {
                            string systemMenuGuid = item.permissions_guid;

                            var tempData = DB.system_menu.Where(m => m.guid == systemMenuGuid).Select(a => new { category = a.category }).FirstOrDefault();
                            if (tempData != null)
                            {
                                permissions.Add(item.permissions_guid, item.permissions_status);
                                if (permissionsTop.IndexOf(tempData.category) == -1)
                                {
                                    permissionsTop.Add(tempData.category);
                                }
                            }
                        }

                        context.Session.Remove("permissions");
                        context.Session.Remove("permissionsTop");

                        context.Session.Add("permissions", permissions);
                        context.Session.Add("permissionsTop", permissionsTop);
                    }
                }
            }
            else
            {
                context.Session.Add("verify", "N");

                var menuData = DB.system_menu.Where(m => m.category != "0").Select(a => new { category = a.category, guid = a.guid }).ToList();

                if (menuData.Count > 0)
                {
                    foreach (var item in menuData)
                    {
                        permissions.Add(item.guid, "F");
                        if (permissionsTop.IndexOf(item.category) == -1)
                        {
                            permissionsTop.Add(item.category);
                        }
                    }

                    context.Session.Remove("permissions");
                    context.Session.Remove("permissionsTop");

                    context.Session.Add("permissions", permissions);
                    context.Session.Add("permissionsTop", permissionsTop);
                }
            }
        }

        /// <summary>
        /// 取得頁碼
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="Allpage"></param>
        /// <param name="page"></param>
        /// <param name="inPage"></param>
        /// <param name="totalNum"></param>
        /// <param name="addpach"></param>
        /// <returns></returns>
        public static string getPageNum(string URL, int Allpage, int page, int totalNum, string addpach)
        {
            //addpach 附屬參數

            string PageList = "";

            int countPage = Allpage;
            int startPage = 1;
            int pageSec = Convert.ToInt16(Math.Ceiling((double)page / 5));

            if (Allpage > 1)
            {
                if (Allpage > 5)
                {
                    startPage = pageSec * 5 - 4;
                    if (startPage < Allpage)
                    {
                        countPage = startPage + 4;
                    }

                    if (page >= Allpage)
                    {
                        countPage = Allpage;
                    }
                }

                //上一頁
                if (Allpage > 1)
                {
                    if (page == 1)
                    {
                        // PageList += "<li class=\"prev\"><a class=\"icon-chevron-left\" href=\"javascript:void(0)\"></a></li>";
                    }
                    else
                    {
                        PageList += "<li class=\"prev-prev mx-1 mx-sm-2\">" +
                                       " <a class=\"flex-center\" href=\"" + URL + "page=1" + addpach + "\">" +
                                       "     <i class=\"icon-left-left text-blue225387 font-20\"></i>" +
                                       " </a>" +
                                   " </li>" +
                                   " <li class=\"prev ml-1 ml-sm-2 mr-3 mr-sm-4\">" +
                                   "     <a class=\"flex-center\" href=\"" + URL + "page=" + (page - 1).ToString() + addpach + "\">" +
                                     "       <i class=\"icon-left text-blue225387 font-20\"></i>" +
                                   "     </a>" +
                                   " </li>";


                        // PageList += "<li class=\"prev\"><a class=\"icon-chevron-left\" href=\"" + URL + "?page=" + (page - 1).ToString() + addpach + "\"></a></li>";
                    }
                }

               // PageList += "<div class=\"jump\"><ul class=\"ns\">";
                for (int p = startPage; p <= countPage; p++)
                {
                    if (p == page)
                    {
                        PageList += "<li class=\"num mx-1 mx-sm-2 active\"><a href=\"#\">" + p.ToString() + "</a></li>";
                    }
                    else
                    {
                        PageList += "<li class=\"num mx-1 mx-sm-2\" ><a href=\"" + URL + "page=" + p.ToString() + addpach + "\">" + p.ToString() + "</a></li>";
                    }
                }
             //   PageList += "</ul></div>";

                if ((Allpage - ((pageSec - 1) * 5)) >= 5)
                {
                    //PageList += "...... <a href=\"#1\" class=\"_pageLink\" data-rel=\"" + URL + "," + Allpage + "," + inPage + "," + Allpage + "," + totalNum + "," + orderBy + "\"><span>" + Allpage + "</span></a> ";
                }

                //下一頁
                if (Allpage > 1)
                {
                    if (Allpage > page)
                    {
                        // PageList += "<li class=\"next\"><a class=\"icon-chevron-right\" href=\"" + URL + "?page=" + (page + 1).ToString() + addpach + "\"></a></li>";


                        PageList += "<li class=\"next mx-1 mx-sm-2 ml-3 ml-sm-4 mr-1 mr-sm-2\">" +
                                        "<a class=\"flex-center\" href=\"" + URL + "page=" + (page + 1).ToString() + addpach + "\">" +
                                        "    <i class=\"icon-right text-blue225387 font-20\"></i>" +
                                       " </a>" +
                                  "  </li>" +
                                   " <li class=\"next-next mx-1 mx-sm-2\">" +
                                     "   <a class=\"flex-center\" href=\"" + URL + "page=" + Allpage + addpach + "\">" +
                                          "  <i class=\"icon-right-right text-blue225387 font-20\"></i>" +
                                     "   </a>" +
                                "    </li>";

                    }
                    else
                    {
                        //  PageList += "<li class=\"next\"><a href=\"#\"><i class=\"icon-right-open-mini\"></i></a></li>";
                        //  PageList += "<li class=\"next_all hidden\"><a href=\"#\"><i class=\"icon-angle-double-right\"></i></a></li>";
                    }
                }
            }

            return PageList;
        }

        /// <summary>
        /// 發送信件
        /// </summary>
        /// <param name="MailList"></param>
        /// <param name="content"></param>
        /// <param name="defLang"></param>
        public static void sendMail(List<string> MailList, string contentID, string defLang, NameValueCollection form)
        {
            Model DB = new Model();

            string webURL = ConfigurationManager.ConnectionStrings["APP_URL"].ConnectionString;

            web_data web_Data = DB.web_data.Where(model => model.lang == defLang).FirstOrDefault();//取得網站基本資訊
            Guid sysGuid = Guid.Parse("4795DABF-18DE-490E-9BB2-D57B4D99C127");

            system_data system_data = DB.system_data.Where(model => model.guid == sysGuid).FirstOrDefault();//取得網站基本資訊
            mail_contents mail_contents = DB.mail_contents.Where(m => m.guid == contentID).FirstOrDefault();

            string title = mail_contents.title;
            string content = mail_contents.content;




            content = content.Replace("{[title]}", title);
            content = content.Replace("{[websiteName]}", web_Data.title);
            string info = "";

            try
            {
                info = form["info"].ToString();
            }
            catch { }

            try
            {
                content = content.Replace("{[sysName]}", form["sysName"].ToString());
                form.Remove("sysName");
            }
            catch
            {
                content = content.Replace("{[sysName]}", "管理者");
            }

            content = content.Replace("{[websiteUrl]}", webURL);
            content = content.Replace("{[logo]}", webURL + "/" + system_data.logo);
            content = content.Replace("{[date]}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
            content = content.Replace("{[address]}", web_Data.address);
            content = content.Replace("{[phone]}", web_Data.phone);

            foreach (var key in form.AllKeys)
            {
                content = content.Replace("{[" + key + "]}", form[key].ToString());
            }

            //
            string smtp_use = ConfigurationManager.ConnectionStrings["smtp_use"].ConnectionString;//SMTP 正式/測試
            //string mailGuid = "b9732bfe-238d-4e4e-9114-8e6d30c34022";
            //if (smtp_use == "test")
            //{
            //    mailGuid = "test";
            //}
            var smtpData = DB.smtp_data.Where(m => m.guid == smtp_use).FirstOrDefault();

            string re = "";
            MailMessage msg = new MailMessage();
            //收件者，以逗號分隔不同收件者 ex "test@gmail.com,test2@gmail.com"
            msg.To.Add(string.Join(",", MailList.ToArray()));
            msg.From = new MailAddress(smtpData.from_email.ToString(), web_Data.title, System.Text.Encoding.UTF8);
            //郵件標題
            msg.Subject = title.ToString();
            //郵件標題編碼
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            //郵件內容
            msg.Body = content;
            msg.IsBodyHtml = true;
            msg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼
            msg.Priority = MailPriority.Normal;//郵件優先級
                                               //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port

            SmtpClient MySmtp = new SmtpClient(smtpData.host.ToString(), int.Parse(smtpData.port.ToString()));
            //設定你的帳號密碼
            MySmtp.Credentials = new System.Net.NetworkCredential(smtpData.username.ToString(), smtpData.password.ToString());
            //Gmial 的 smtp 使用 SSL
            if (smtpData.smtp_auth.ToString() == "Y")
            {
                MySmtp.EnableSsl = true;
            }
            else
            {
                MySmtp.EnableSsl = false;
            }

            try
            {
                MySmtp.Send(msg);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                Console.WriteLine("Exception caught in RetryIfBusy(): {0}", ex.ToString());
            }
        }



        /// <summary>
        /// 發送信件
        /// </summary>
        /// <param name="MailList"></param>
        /// <param name="title"></param>
        /// <param name="defLang"></param>
        /// <param name="form"></param>
        public static void sendMailIndex(List<string> MailList, string title, string defLang ,string contentUrl, FormCollection form )
        {
            Model DB = new Model();

            string webURL = ConfigurationManager.ConnectionStrings["APP_URL"].ConnectionString;

            web_data web_Data = DB.web_data.Where(model => model.lang == defLang).FirstOrDefault();//取得網站基本資訊
            Guid sysGuid = Guid.Parse("4795DABF-18DE-490E-9BB2-D57B4D99C127");

            system_data system_data = DB.system_data.Where(model => model.guid == sysGuid).FirstOrDefault();//取得網站基本資訊

            // string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "/Content/Mail/survey.html";
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + contentUrl;
            string content = System.IO.File.ReadAllText(path);





            content = content.Replace("{[title]}", title);
            content = content.Replace("{[websiteName]}", web_Data.title);
            string info = "";

            try
            {
                info = form["info"].ToString();
            }
            catch { }

            try
            {
                content = content.Replace("{[sysName]}", form["sysName"].ToString());
                form.Remove("sysName");
            }
            catch
            {
                content = content.Replace("{[sysName]}", "管理者");
            }

            content = content.Replace("{[websiteUrl]}", webURL);
            content = content.Replace("{[logo]}", webURL + "/" + system_data.logo);
            content = content.Replace("{[date]}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
            content = content.Replace("{[address]}", web_Data.address);
            content = content.Replace("{[phone]}", web_Data.phone);

            foreach (var key in form.AllKeys)
            {
                content = content.Replace("{[" + key + "]}", form[key].ToString());

                string comprehensive = "";
                for (int i = 1; i <= 11; i++)
                {
                    if (form.AllKeys.Contains("problem_comprehensive_" + i.ToString()))
                    {
                        comprehensive = comprehensive + form["problem_comprehensive_" + i.ToString()].ToString() + "," ?? "";
                    }

                }

                content = content.Replace("{[comprehensive]}", comprehensive.ToString());

                string env = "";
                for (int i = 1; i <= 6; i++)
                {
                    if (form.AllKeys.Contains("problem_env_" + i.ToString()))
                    {
                        env = env + form["problem_env_" + i.ToString()].ToString() + "," ?? "";
                    }
                }

                content = content.Replace("{[env]}", env.ToString());

            }

            //
            string smtp_use = ConfigurationManager.ConnectionStrings["smtp_use"].ConnectionString;//SMTP 正式/測試
          
            var smtpData = DB.smtp_data.Where(m => m.guid == smtp_use).FirstOrDefault();

            string re = "";
            MailMessage msg = new MailMessage();
            //收件者，以逗號分隔不同收件者 ex "test@gmail.com,test2@gmail.com"
            msg.To.Add(string.Join(",", MailList.ToArray()));
            msg.From = new MailAddress(smtpData.from_email.ToString(), web_Data.title, System.Text.Encoding.UTF8);
            //郵件標題
            msg.Subject = title.ToString();
            //郵件標題編碼
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            //郵件內容
            msg.Body = content;
            msg.IsBodyHtml = true;
            msg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼
            msg.Priority = MailPriority.Normal;//郵件優先級
                                               //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port

            SmtpClient MySmtp = new SmtpClient(smtpData.host.ToString(), int.Parse(smtpData.port.ToString()));
            //設定你的帳號密碼
            MySmtp.Credentials = new System.Net.NetworkCredential(smtpData.username.ToString(), smtpData.password.ToString());
            //Gmial 的 smtp 使用 SSL
            if (smtpData.smtp_auth.ToString() == "Y")
            {
                MySmtp.EnableSsl = true;
            }
            else
            {
                MySmtp.EnableSsl = false;
            }

            try
            {
                MySmtp.Send(msg);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                Console.WriteLine("Exception caught in RetryIfBusy(): {0}", ex.ToString());
            }
        }

        /// <summary>
        /// 回傳log標題
        /// </summary>
        /// <param name="FromData"></param>
        /// <param name="typeTitle"></param>
        /// <returns></returns>
        public static string reSysLogTitle(dynamic FromData, string typeTitle)
        {
            try
            {
                return typeTitle + "：" + FromData.title.ToString();
            }
            catch
            {
                try
                {
                    return typeTitle + "：" + FromData.name.ToString();
                }
                catch
                {
                    return typeTitle + "：" + FromData.username.ToString();
                }
            }
        }

        /// <summary>
        /// 發送審核信件
        /// </summary>
        /// <param name="data"></param>
        public static void SendVerifyEmail(Dictionary<string, string> data)
        {
            // string tables, string actType , string guid , string subject
            Model DB = new Model();
            //取得是否為審核資料表
            List<string> verifyUseTab = Web.Controllers.SiteadminController.verifyTables();//取得有審核的資料表
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            //審核通知信件
            if (verifyUseTab.IndexOf(data["tables"].ToString()) != -1)
            {
                web_data webData = DB.web_data.Where(m => m.lang == "tw").FirstOrDefault();//取得網站基本資訊
                List<string> MailList = new List<string>();
                NameValueCollection form = new NameValueCollection();

                //有審核權限下的使用者mail
                List<roles> roles = DB.roles.Where(m => m.verify == "Y").ToList();
                foreach (roles item in roles)
                {
                    List<user> user = DB.user.Where(m => m.role_guid == item.guid).ToList();
                    foreach (user subItem in user)
                    {
                        if (!string.IsNullOrEmpty(subItem.email) && MailList.IndexOf(subItem.email) == -1)
                        {
                            MailList.Add(subItem.email);
                        }
                    }
                }
                //通知審核者
                if (data["actType"].ToString() == "add")
                {
                    form.Add("sysName", "審核人員");
                    form.Add("info", "您有一則【" + data["subject"].ToString() + "】待審核，請登入後台後進行審核處理，謝謝!");
                    sendMail(MailList, "1", "tw", form);
                }
                else
                {
                }
            }
        }


        /// <summary>
        /// 無限欄位JSON拆解
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static Dictionary<string, List<string>> InfinityJsonFormat(string content)
        {
            Dictionary<string, string> re = new Dictionary<string, string>();

            Dictionary<string, List<string>> tempVal = new Dictionary<string, List<string>>();

            if (!string.IsNullOrEmpty(content))
            {
                List<SiteadminController.infinityLayoutModel> jsonData = JsonConvert.DeserializeObject<List<SiteadminController.infinityLayoutModel>>(content);

                foreach (SiteadminController.infinityLayoutModel valItem in jsonData)
                {
                    tempVal.Add(valItem.key, valItem.val);
                }
            }

            return tempVal;
        }

        /// <summary>
        /// 取得真實IP
        /// </summary>
        /// <returns></returns>
        public static string GetIP()
        {
            string ip;
            string trueIP = string.Empty;

            //先取得是否有經過代理伺服器
            ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ip))
            {
                //將取得的 IP 字串存入陣列
                string[] ipRange = ip.Split(',');

                //比對陣列中的每個 IP
                for (int i = 0; i < ipRange.Length; i++)
                {
                    //剔除內部 IP 及不合法的 IP 後，取出第一個合法 IP
                    if (ipRange[i].Trim().Substring(0, 3) != "10." &&
                        ipRange[i].Trim().Substring(0, 7) != "192.168" &&
                        ipRange[i].Trim().Substring(0, 7) != "172.16." &&
                        CheckIP(ipRange[i].Trim()))
                    {
                        trueIP = ipRange[i].Trim();
                        break;
                    }
                }
            }
            else
            {
                //沒經過代理伺服器，直接使用 ServerVariables["REMOTE_ADDR"]
                //並經過 CheckIP( ) 的驗證
                trueIP = CheckIP(System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]) ?
                     System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] : "";
            }

            return trueIP;
        }

        public static bool CheckIP(string strPattern)
        {
            // 繼承自：System.Text.RegularExpressions
            // regular: ^\d{1,3}[\.]\d{1,3}[\.]\d{1,3}[\.]\d{1,3}$
            Regex regex = new Regex("^\\d{1,3}[\\.]\\d{1,3}[\\.]\\d{1,3}[\\.]\\d{1,3}$");
            Match m = regex.Match(strPattern);

            return m.Success;
        }

        /// <summary>
        /// 字串加密(非對稱式)
        /// </summary>
        /// <param name="Source">加密前字串</param>
        /// <param name="CryptoKey">加密金鑰</param>
        /// <returns>加密後字串</returns>
        public static string aesEncryptBase64(string SourceStr, string CryptoKey)
        {
            string encrypt = "";
            try
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
                byte[] key = sha256.ComputeHash(Encoding.UTF8.GetBytes(CryptoKey));
                byte[] iv = md5.ComputeHash(Encoding.UTF8.GetBytes(CryptoKey));
                aes.Key = key;
                aes.IV = iv;

                byte[] dataByteArray = Encoding.UTF8.GetBytes(SourceStr);
                using (MemoryStream ms = new MemoryStream())
                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(dataByteArray, 0, dataByteArray.Length);
                    cs.FlushFinalBlock();
                    encrypt = Convert.ToBase64String(ms.ToArray());
                }
            }
            catch (Exception e)
            {
                return "";
            }
            return encrypt;
        }

        /// <summary>
        /// 字串解密(非對稱式)
        /// </summary>
        /// <param name="Source">解密前字串</param>
        /// <param name="CryptoKey">解密金鑰</param>
        /// <returns>解密後字串</returns>
        public static string aesDecryptBase64(string SourceStr, string CryptoKey)
        {
            string decrypt = "";
            try
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
                byte[] key = sha256.ComputeHash(Encoding.UTF8.GetBytes(CryptoKey));
                byte[] iv = md5.ComputeHash(Encoding.UTF8.GetBytes(CryptoKey));
                aes.Key = key;
                aes.IV = iv;

                byte[] dataByteArray = Convert.FromBase64String(SourceStr);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(dataByteArray, 0, dataByteArray.Length);
                        cs.FlushFinalBlock();
                        decrypt = Encoding.UTF8.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception e)
            {
                return "";
            }
            return decrypt;
        }
        /// <summary>
        /// check is json
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public static bool IsValidJson(string strInput)
        {
            if (string.IsNullOrWhiteSpace(strInput)) { return false; }
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = Newtonsoft.Json.Linq.JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    //Console.WriteLine(jex.Message);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    // Console.WriteLine(ex.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 發問問題列表參數格式化
        /// </summary>
        public class forumData
        {
            public string guid { get; set; } // or whatever
            public string title { get; set; }  // or whatever

            public string forum_categoryguid { get; set; }  // or whatever
            public string forum_subcategoryguid { get; set; }  // or whatever
            public string ad_id { get; set; }  // or whatever

            public string status { get; set; }  // or whatever

            public DateTime create_date { get; set; }  // or whatever

            public string permission { get; set; }  // or whatever

            public string look { get; set; }  // or whatever

            public string thumbsup { get; set; }  // or whatever

            public string ad_name { get; set; }  // or whatever
            public string ad_mail { get; set; }  // or whatever
            public string comment { get; set; }  // or whatever

            public string ad_imgurl { get; set; }  // or whatever
            public string ad_department { get; set; }  // or whatever

            public string ad_ext_num { get; set; }  // or whatever
            public string top_guid { get; set; }  // or whatever
        }
    }
}