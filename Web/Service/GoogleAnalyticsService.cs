﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Analytics.v3;
using System.Threading;
using System.Web.Mvc;
using Google.Apis.Services;
using System.Configuration;
using Web.Models;
using Web.DB.Models;
using Newtonsoft.Json;

namespace Web.Service
{
    public class GoogleAnalyticsService : Controller
    {
        private Model DB = new Model();
        public string GaProfileId = ConfigurationManager.ConnectionStrings["GaProfileId"].ConnectionString;//GaProfileId
        private ServiceAccountCredential credential = null;//驗證
        private string startDate = DateTime.Now.ToString("yyyy") + "-01-01";
        private string endDate = DateTime.Now.ToString("yyyy") + "-12-31";
        private string metrics = "";
        private string Dimensions = "";

        public void GetCredential()
        {
            GoogleAnalyticsService GaData = new GoogleAnalyticsService();
            credential = GaData.GoogleAnalytics();
        }


        public object TasksService { get; private set; }

        /// <summary>
        /// 取得登入憑證
        /// </summary>
        /// <returns></returns>
        public ServiceAccountCredential GoogleAnalytics()
        {
            string[] scopes = new string[] {
                AnalyticsService.Scope.Analytics,               // view and manage your Google Analytics data
                AnalyticsService.Scope.AnalyticsEdit,           // Edit and manage Google Analytics Account
                AnalyticsService.Scope.AnalyticsManageUsers,    // Edit and manage Google Analytics Users
                AnalyticsService.Scope.AnalyticsReadonly};      // View Google Analytics Data


            string serverPath = System.Web.Hosting.HostingEnvironment.MapPath("~/");
            var keyFilePath = serverPath + "Content/Siteadmin/API Project-8e05910ca2e2.p12";
            var serviceAccountEmail = "ga-130@api-project-155771781585.e-creative.tw.iam.gserviceaccount.com";
            var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
            var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
            {
                Scopes = scopes
            }.FromCertificate(certificate));

            //取得token
            string AuthenticationKey = "";
            if (credential.RequestAccessTokenAsync(CancellationToken.None).Result)
            {
                AuthenticationKey = credential.Token.AccessToken;

            }
            return credential;
        }




        /// <summary>
        /// 寫入年度數量
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.GaData ThisYear(ServiceAccountCredential credential)
        {

            var service = new AnalyticsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Analytics API Year",
            });

            string profileId = GaProfileId;
            string startDate = DateTime.Now.ToString("yyyy") + "-01-01";
            string endDate = DateTime.Now.ToString("yyyy") + "-12-31";
            string metrics = "ga:visits";
            DataResource.GaResource.GetRequest request = service.Data.Ga.Get(profileId, startDate, endDate, metrics);
            request.Dimensions = "ga:month";
            Google.Apis.Analytics.v3.Data.GaData GaData = request.Execute();

            return GaData;

        }


        /// <summary>
        /// 及時瀏覽人數
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.RealtimeData nowUser()
        {
            try
            {
                GetCredential();//設定驗證
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "RealtimeData",
                });
                string profileId = GaProfileId;
                DataResource.RealtimeResource.GetRequest request = service.Data.Realtime.Get(profileId, "rt:pageviews");

                Google.Apis.Analytics.v3.Data.RealtimeData realtimeData = request.Execute();

                return realtimeData;
            }
            catch (Exception ex)
            {
                return null;
            }

        }


        /// <summary>
        /// 回傳年度數量
        /// </summary>
        /// <returns></returns>
        public string ViewYear()
        {
            string json = "";
            try
            {
                List<Object> dic = new List<Object>();
                google_analytics GAThisYear = DB.google_analytics.Where(m => m.area == "ThisYear").FirstOrDefault();
                if (GAThisYear != null)
                {
                    dynamic mJObj = null;

                    if (!string.IsNullOrEmpty(GAThisYear.content.ToString()))
                    {
                        mJObj = Newtonsoft.Json.Linq.JArray.Parse(GAThisYear.content.ToString());
                    }


                    for (int i = 0; i <= 11; i++)
                    {
                        Dictionary<String, Object> dic2 = new Dictionary<string, object>();

                        var subObj = Newtonsoft.Json.Linq.JArray.Parse(mJObj[i].ToString());

                        dic2.Add("age", (i + 1) + "月");
                        dic2.Add("visits", subObj[1]);
                        dic.Add(dic2);
                    }
                }
                 json = JsonConvert.SerializeObject(dic, Formatting.Indented);
            }
            catch { }
         
            return json;
        }

        /// <summary>
        /// 回傳年度數量(目前月份)
        /// </summary>
        /// <returns></returns>
        public string ViewYearUseMonth(string month)
        {
            List<Object> dic = new List<Object>();

            google_analytics google_analytics = DB.google_analytics.Where(m => m.area == "ThisYear").FirstOrDefault();

            string visits = "0";
            if (google_analytics != null)
            {

                var mJObj = Newtonsoft.Json.Linq.JArray.Parse(google_analytics.content.ToString());

                for (int i = 0; i <= 11; i++)
                {

                    var subObj = Newtonsoft.Json.Linq.JArray.Parse(mJObj[i].ToString());
                    if ((i + 1).ToString() == month)
                    {
                        visits = subObj[1].ToString();
                    }

                }
            }
            return visits;
        }

        /// <summary>
        /// 瀏覽頁面與數量
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.GaData ReGaContent(GaParameterModel data)
        {
            startDate = data.startDate;
            endDate = data.endDate;
            metrics = data.metrics;
            Dimensions = data.Dimensions;

            return GetGaContent();
        }

        /// <summary>
        /// 共用取得函式
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="metrics"></param>
        /// <param name="Dimensions"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.GaData GetGaContent()
        {
            try
            {
                GetCredential();//設定驗證
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API",
                });

                string profileId = GaProfileId;
                DataResource.GaResource.GetRequest request = service.Data.Ga.Get(profileId, startDate, endDate, metrics);
                request.Dimensions = Dimensions;

                //request.Metrics
                Google.Apis.Analytics.v3.Data.GaData GaData = request.Execute();

                return GaData;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }

    public partial class GaParameterModel
    {
        public string startDate { get; set; }

        public string endDate { get; set; }
        public string metrics { get; set; }
        public string Dimensions { get; set; }
    }
}
