﻿using System;
using System.Collections.Generic;
using System.Linq;
using Web.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Dapper;

namespace Web.Service
{
    public class ProductService
    {
        protected IDbConnection dbConnection;

        public ProductService()
        {
            string connStr = ConfigurationManager.ConnectionStrings["productDB"].ConnectionString;
            dbConnection = new SqlConnection(connStr);

        }


        public List<SearchResultDetailVM> productAllData(List<string> id, SearchBySpecVM searchBySpecVM)
        {

            string connStr = ConfigurationManager.ConnectionStrings["productDB"].ConnectionString;
            IDbConnection dbConnection = new SqlConnection(connStr);

            var searchResult = new List<SearchResultDetailVM>();

            using (var conn = dbConnection)
            {
                searchResult.AddRange(getHousingResult(conn, searchBySpecVM));
                searchResult.AddRange(getTerminalResult(conn, searchBySpecVM));
                searchResult.AddRange(getRubberResult(conn, searchBySpecVM));
                searchResult.AddRange(getSleeveResult(conn, searchBySpecVM));
                searchResult.AddRange(getGrommetResult(conn, searchBySpecVM));

            }

            searchResult = searchResult.Where(m => id.Contains(m.id.ToString())).OrderBy(s => s.name).ToList();


            return searchResult;
        }

        private List<SearchResultDetailVM> getHousingResult(IDbConnection conn, SearchBySpecVM searchBySpecVM)
        {
            var builder = new SqlBuilder();

            builder.Where("p.[type] = 'Housing'");

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSizeType))
            {
                builder.Where("h.item_class = @productSizeType", new { productSizeType = searchBySpecVM.ProductSizeType });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductClass))
            {
                builder.Where("h.item_class = @productClass", new { productClass = searchBySpecVM.ProductClass });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSerial))
            {
                builder.Where("h.series = @productSerial", new { productSerial = searchBySpecVM.ProductSerial });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSex))
            {
                builder.Where("h.sex = @productSex", new { productSex = searchBySpecVM.ProductSex });
            }
            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSealable))
            {
                builder.Where("h.sealable = @productSealable", new { productSealable = searchBySpecVM.ProductSealable });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductPosition))
            {
                if (searchBySpecVM.ProductPosition.Contains("~") == false)
                {
                    int positions = 0;
                    if (int.TryParse(searchBySpecVM.ProductPosition, out positions))
                    {
                        //1,2,3,4,5,6,7,8,9,10
                        builder.Where("((DATALENGTH(h.positions) > 0  and TRY_CAST(h.positions AS INT) = @positions) or (DATALENGTH(h.positions) = 0 and TRY_CAST(SUBSTRING(p.name, 5, 2) AS INT) = @positions))", new { positions = positions });
                    }
                    else
                    {
                        //N/A
                        builder.Where("h.positions = 'N/A'");
                    }
                }
                else
                {
                    int start = 0, end = 0;
                    string[] positionStr = searchBySpecVM.ProductPosition.Split('~');
                    if (!int.TryParse(positionStr[0], out start))
                        throw new ArgumentOutOfRangeException($"孔數起始區間錯誤{positionStr[0]}");
                    if (int.TryParse(positionStr[1], out end))
                    {
                        //2個都數字
                        builder.Where("((DATALENGTH(h.positions) > 0  and (TRY_CAST(h.positions AS INT) >= @start and TRY_CAST(h.positions AS INT) <= @end)) or (DATALENGTH(h.positions) = 0 and (TRY_CAST(SUBSTRING(p.name, 5, 2) AS INT) >= @start and TRY_CAST(SUBSTRING(p.name, 5, 2) AS INT) <= @end)))", new { start = start, end = end });
                    }
                    else
                    {
                        //結尾是∞
                        builder.Where("((DATALENGTH(h.positions) > 0  and (TRY_CAST(h.positions AS INT) >= @start)) or (DATALENGTH(h.positions) = 0 and (TRY_CAST(SUBSTRING(p.name, 5, 2) AS INT) >= @start)))", new { start = start });
                    }
                }

            }


            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTemper))
            {
                builder.Where("h.temper_operating Like '%' +  @productTemper + '%'", new { productTemper = searchBySpecVM.ProductTemper });
            }

            if (searchBySpecVM.Size != null)
            {
                Single dimension = searchBySpecVM.Size.Width ?? 0 * searchBySpecVM.Size.Length ?? 0 * searchBySpecVM.Size.Height ?? 0;
                builder.Where("(h.width * h.length * h.height) = @dimension", new { dimension = dimension });
            }

            var builderTemplate =
                builder.AddTemplate("select p.id,p.name,p.new_name,'膠盒' as [type],h.series,h.positions,h.sex as sex_o,h.sealable as sealable_o,h.temper_operating,h.item_class,h.length,h.width,h.height,h.material from product p inner join housing h on p.id = h.product_id /**where**/");

            return
                conn.Query<SearchResultDetailVM>(builderTemplate.RawSql, builderTemplate.Parameters).ToList();
        }
        private List<SearchResultDetailVM> getTerminalResult(IDbConnection conn, SearchBySpecVM searchBySpecVM)
        {
            var builder = new SqlBuilder();

            builder.Where("p.[type] = 'Terminal'");

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSizeType))
            {
                builder.Where("h.item_class = @productSizeType", new { productSizeType = searchBySpecVM.ProductSizeType });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductClass))
            {
                builder.Where("h.item_class = @productClass", new { productClass = searchBySpecVM.ProductClass });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSex))
            {
                builder.Where("h.sex = @productSex", new { productSex = searchBySpecVM.ProductSex });
            }
            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSealable))
            {
                builder.Where("h.sealable = @productSealable", new { productSealable = searchBySpecVM.ProductSealable });
            }
            if (!string.IsNullOrEmpty(searchBySpecVM.ProductDepth))
            {
                float start = 0, end = 0;
                string[] depthStr = searchBySpecVM.ProductDepth.Split('~');
                if (!float.TryParse(depthStr[0], out start))
                    throw new ArgumentOutOfRangeException($"料厚起始值錯誤{depthStr[0]}");
                if (!float.TryParse(depthStr[1], out end))
                    throw new ArgumentOutOfRangeException($"料厚結束值錯誤{depthStr[1]}");

                if (!float.IsInfinity(end))
                {
                    builder.Where("TRY_CAST(h.depth AS float) >= @start and TRY_CAST(h.depth AS float) < @end", new { start = start, end = end });
                }
                else
                {
                    builder.Where("TRY_CAST(h.depth AS float) >= @start", new { start = start });
                }

            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductDiameter))
            {
                if (searchBySpecVM.ProductDiameter.Contains("~") == false)
                {
                    builder.Where("h.diameter = 'N/A'");
                }
                else
                {
                    float start = 0, end = 0;
                    string[] diameterStr = searchBySpecVM.ProductDiameter.Split('~');
                    if (!float.TryParse(diameterStr[0], out start))
                        throw new ArgumentOutOfRangeException($"適用線徑起始值錯誤{diameterStr[0]}");
                    if (!float.TryParse(diameterStr[1], out end))
                        throw new ArgumentOutOfRangeException($"適用線徑結束值錯誤{diameterStr[1]}");

                    builder.Where("TRY_CAST(h.diameter AS float) >= @start and TRY_CAST(h.diameter AS float) < @end", new { start = start, end = end });

                }
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTab))
            {
                float start = 0, end = 0;
                string[] tabStr = searchBySpecVM.ProductTab.Split('~');
                if (!float.TryParse(tabStr[0], out start))
                    throw new ArgumentOutOfRangeException($"新增TAB起始值錯誤{tabStr[0]}");
                if (!float.TryParse(tabStr[1], out end))
                    throw new ArgumentOutOfRangeException($"新增TAB結束值錯誤{tabStr[1]}");

                if (float.IsInfinity(end))
                {
                    builder.Where("TRY_CAST(h.tab AS float) >= @start", new { start = start });
                }
                else
                {
                    builder.Where("TRY_CAST(h.tab AS float) >= @start and TRY_CAST(h.tab AS float) < @end", new { start = start, end = end });
                }
            }

            if (searchBySpecVM.Size != null)
            {
                if (searchBySpecVM.Size.Width.HasValue)
                {
                    builder.Where("h.width = @width", new { width = searchBySpecVM.Size.Width });
                }
                if (searchBySpecVM.Size.Length.HasValue)
                {
                    builder.Where("h.length = @length", new { length = searchBySpecVM.Size.Length });
                }
                if (searchBySpecVM.Size.Height.HasValue)
                {
                    builder.Where("h.height = @height", new { height = searchBySpecVM.Size.Height });
                }
                if (searchBySpecVM.Size.Diameter.HasValue)
                {
                    builder.Where("h.diameter = @diameter", new { diameter = searchBySpecVM.Size.Diameter });
                }
            }

            var builderTemplate =
                builder.AddTemplate("select p.id,p.name,p.new_name,'端子' as [type],h.sex as sex_o,h.sealable as sealable_o,'N/A' as temper_operating,h.item_class,h.length,h.width,h.height,h.material from product p inner join terminal h on p.id = h.product_id /**where**/ ");

            return
                conn.Query<SearchResultDetailVM>(builderTemplate.RawSql, builderTemplate.Parameters).ToList();
        }
        private List<SearchResultDetailVM> getRubberResult(IDbConnection conn, SearchBySpecVM searchBySpecVM)
        {
            var builder = new SqlBuilder();

            builder.Where("p.[type] = 'Rubber'");

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSizeType))
            {
                builder.Where("h.item_class = @productSizeType", new { productSizeType = searchBySpecVM.ProductSizeType });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductClass))
            {
                builder.Where("h.item_class = @productClass", new { productClass = searchBySpecVM.ProductClass });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTemper))
            {
                builder.Where("h.temper_operating = @productTemper", new { productTemper = searchBySpecVM.ProductTemper });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductDiameter))
            {
                if (searchBySpecVM.ProductDiameter.Contains("~") == false)
                {
                    builder.Where("h.diameter = 'N/A'");
                }
                else
                {
                    float start = 0, end = 0;
                    string[] diameterStr = searchBySpecVM.ProductDiameter.Split('~');
                    if (!float.TryParse(diameterStr[0], out start))
                        throw new ArgumentOutOfRangeException($"適用線徑起始值錯誤{diameterStr[0]}");
                    if (!float.TryParse(diameterStr[1], out end))
                        throw new ArgumentOutOfRangeException($"適用線徑結束值錯誤{diameterStr[1]}");

                    builder.Where("TRY_CAST(h.diameter AS float) >= @start and TRY_CAST(h.diameter AS float) < @end", new { start = start, end = end });

                }
            }

            if (searchBySpecVM.Size != null)
            {
                if (searchBySpecVM.Size.Length.HasValue)
                {
                    builder.Where("h.length = @length", new { length = searchBySpecVM.Size.Length });
                }
                if (searchBySpecVM.Size.Diameter_1.HasValue)
                {
                    builder.Where("h.diameter_1 = @diameter_1", new { diameter_1 = searchBySpecVM.Size.Diameter_1 });
                }
                if (searchBySpecVM.Size.Diameter_2.HasValue)
                {
                    builder.Where("h.diameter_2 = @diameter_2", new { diameter_2 = searchBySpecVM.Size.Diameter_2 });
                }
            }

            var builderTemplate =
                builder.AddTemplate("select p.id,p.name,p.new_name,'防水栓' as [type],'N' as sex_o,'N' as sealable_o,h.temper_operating,h.item_class,h.length,h.width,0 as height,h.material from product p inner join rubber h on p.id = h.product_id /**where**/ ");

            return
                conn.Query<SearchResultDetailVM>(builderTemplate.RawSql, builderTemplate.Parameters).ToList();
        }
        private List<SearchResultDetailVM> getSleeveResult(IDbConnection conn, SearchBySpecVM searchBySpecVM)
        {
            var builder = new SqlBuilder();

            builder.Where("p.[type] = 'Sleeve'");

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSizeType))
            {
                builder.Where("h.item_class = @productSizeType", new { productSizeType = searchBySpecVM.ProductSizeType });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductClass))
            {
                builder.Where("h.item_class = @productClass", new { productClass = searchBySpecVM.ProductClass });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTemper))
            {
                builder.Where("h.temper_operating = @productTemper", new { productTemper = searchBySpecVM.ProductTemper });
            }

            if (searchBySpecVM.Size != null)
            {
                if (searchBySpecVM.Size.Length.HasValue)
                {
                    builder.Where("h.length = @length", new { length = searchBySpecVM.Size.Length });
                }
                if (searchBySpecVM.Size.Diameter_1.HasValue)
                {
                    builder.Where("h.diameter_1 = @diameter_1", new { diameter_1 = searchBySpecVM.Size.Diameter_1 });
                }
                if (searchBySpecVM.Size.Diameter_2.HasValue)
                {
                    builder.Where("h.diameter_2 = @diameter_2", new { diameter_2 = searchBySpecVM.Size.Diameter_2 });
                }
            }

            var builderTemplate =
                builder.AddTemplate("select p.id,p.name,p.new_name,'PVC絕緣護套' as [type],'N' as sex_o,'N' as sealable_o,h.temper_operating,h.item_class,h.length,h.width,h.height as height,h.material from product p inner join sleeve h on p.id = h.product_id /**where**/ ");

            return
                conn.Query<SearchResultDetailVM>(builderTemplate.RawSql, builderTemplate.Parameters).ToList();
        }


        private List<SearchResultDetailVM> getGrommetResult(IDbConnection conn, SearchBySpecVM searchBySpecVM)
        {
            var builder = new SqlBuilder();

            builder.Where("p.[type] = 'Grommet'");

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductSizeType))
            {
                builder.Where("h.item_class = @productSizeType", new { productSizeType = searchBySpecVM.ProductSizeType });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductClass))
            {
                builder.Where("h.item_class = @productClass", new { productClass = searchBySpecVM.ProductClass });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTemper))
            {
                builder.Where("h.temper_operating Like '%' + @productTemper + '%'", new { productTemper = searchBySpecVM.ProductTemper });
            }

            if (!string.IsNullOrEmpty(searchBySpecVM.ProductTemper))
            {
                builder.Where("h.temper_operating = @productTemper", new { productTemper = searchBySpecVM.ProductTemper });
            }

            if (searchBySpecVM.Size != null)
            {
                if (searchBySpecVM.Size.Length.HasValue)
                {
                    builder.Where("h.length = @length", new { length = searchBySpecVM.Size.Length });
                }
                if (searchBySpecVM.Size.Diameter_1.HasValue)
                {
                    builder.Where("h.diameter_1 = @diameter_1", new { diameter_1 = searchBySpecVM.Size.Diameter_1 });
                }
                if (searchBySpecVM.Size.Diameter_2.HasValue)
                {
                    builder.Where("h.diameter_2 = @diameter_2", new { diameter_2 = searchBySpecVM.Size.Diameter_2 });
                }
            }

            var builderTemplate =
                builder.AddTemplate("select p.id,p.name,p.new_name,'索環護套' as [type],'N' as sex_o,'N' as sealable_o,h.temper_operating,h.item_class,h.length,h.width,h.height as height,h.material from product p inner join grommet h on p.id = h.product_id /**where**/ ");

            return
                conn.Query<SearchResultDetailVM>(builderTemplate.RawSql, builderTemplate.Parameters).ToList();
        }


    }
}