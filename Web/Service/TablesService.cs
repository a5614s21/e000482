﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.DB.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;
using System.Data.SqlClient;
using System.Data;

namespace Web.Service
{
    public class TablesService
    {
        /// <summary>
        /// 取得DataTable欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic dataTableTitle(string tables , string guid = "")
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //產品資訊
                case "product_info":
                    re = Repository.Product.ProductInfo.dataTableTitle();
                    break;

                //首頁content
                case "index_content":
                    re = Repository.Advertise.IndexContent.dataTableTitle();
                    break;

                //客戶意見反饋
                case "feedBack":
                    re = Repository.Business.feedback.dataTableTitle();
                    break;

                //系統項目
                case "system_item":
                    re = Repository.SystemItem.Data.dataTableTitle();
                    break;

                //系統項目分類
                case "system_item_category":
                    re = Repository.SystemItem.Category.dataTableTitle();
                    break;


                //問券問題
                case "problem":
                    re = Repository.Business.Problem.dataTableTitle();
                    break;

                //問券問題分類
                case "problem_category":
                    re = Repository.Business.ProblemCategory.dataTableTitle();
                    break;


                //胡連沿革
                case "history":
                    re = Repository.History.Data.dataTableTitle();
                    break;

                //胡連沿革分類
                case "history_category":
                    re = Repository.History.Category.dataTableTitle();
                    break;

                //線束2
                case "carsysl2":
                    re = Repository.Product.carsysLevel2.dataTableTitle();
                    break;

                //線束產品對應
                case "mapcarsys":
                    re = Repository.Product.mapCarsys.dataTableTitle();
                    break;

                //線束2
                case "carsysl4":
                    re = Repository.Product.carsysLevel4.dataTableTitle();
                    break;

                //會員
                case "member":
                    try
                    {
                        Dictionary<String, Object> setCol = new Dictionary<string, object>();

                        setCol.Add("guid", "Y");
                        setCol.Add("username", "帳號");
                        setCol.Add("name", "姓名");
                        setCol.Add("company", "公司名稱");
                        setCol.Add("status", "狀態");
                        setCol.Add("create_date", "建立日期");
                        setCol.Add("action", "動作");

                        re = setCol;
                    }
                    catch { }



                    break;

                //投資人意見留言板
                case "inquire":
                    try
                    {
                        Dictionary<String, Object> setCol = new Dictionary<string, object>();

                        setCol.Add("guid", "Y");
                        setCol.Add("name", "姓名");
                        setCol.Add("company", "公司名稱");
                        setCol.Add("status", "狀態");
                        setCol.Add("create_date", "建立日期");
                        setCol.Add("action", "動作");

                        re = setCol;
                    }
                    catch { }



                    break;

                //客戶聯絡表單
                case "contact":
                    try
                    {
                        Dictionary<String, Object> setCol = new Dictionary<string, object>();

                        setCol.Add("guid", "Y");
                        setCol.Add("name", "姓名");
                        setCol.Add("company", "公司名稱");
                        setCol.Add("status", "狀態");
                        setCol.Add("create_date", "建立日期");
                        setCol.Add("action", "動作");

                        re = setCol;
                    }
                    catch { }



                    break;

                //客戶滿意度調查
                case "survey":
                    try
                    {
                        Dictionary<String, Object> setCol = new Dictionary<string, object>();

                        setCol.Add("guid", "Y");
                        setCol.Add("no", "客戶編號");
                        setCol.Add("name", "姓名");
                        setCol.Add("company", "公司名稱");
                        setCol.Add("status", "狀態");
                        setCol.Add("create_date", "建立日期");
                        setCol.Add("action", "動作");

                        re = setCol;
                    }
                    catch { }
                   


                    break;

                //廣告
                case "advertise":
                    re = Repository.Advertise.Data.dataTableTitle(guid);
                    break;

                //投資人Q&A

                case "shareholder_faq":
                    re = Repository.Investor.ShareholderFaq.dataTableTitle();
                    break;
                //人力資源分類
                case "welfare_category":
                    re = Repository.Welfare.Category.dataTableTitle();
                    break;

                //人力資源
                case "welfare":
                    re = Repository.Welfare.Data.dataTableTitle();
                    break;

                //下載專區
                case "downloads":
                    re = Repository.Download.Data.dataTableTitle(guid);
                    break;

                //下載專區分類
                case "download_category":
                    re = Repository.Download.Category.dataTableTitle();
                    break;



                //全球據點
                case "locations":
                    re = Repository.About.Location.dataTableTitle();
                    break;

                //客戶教育訓練平台
                case "courses":
                    re = Repository.Business.Courses.dataTableTitle();
                    break;

                //客戶教育訓練平台分類
                case "course_category":
                    re = Repository.Business.CourseCategory.dataTableTitle();
                    break;


                //商務專區-FAQ問與答

                case "business_faq":
                    re = Repository.Business.BusinessFaq.dataTableTitle();
                    break;

                //其他資訊
                case "notes_data":
                    re = Repository.Notes.NotesData.dataTableTitle();
                    break;
                //關於胡連
                case "abouts":
                    re = Repository.About.About.dataTableTitle();
                    break;
                //最新消息
                case "news":
                      re = Repository.News.Data.dataTableTitle();
                      break;

                  //最新消息分類
                  case "news_category":
                      re = Repository.News.Category.dataTableTitle();
                      break;
                      

                //系統日誌
                case "system_log":
                    re = Repository.System.Log.dataTableTitle();
                    break;
                //防火牆
                case "firewalls":
                    re = Repository.System.Firewalls.dataTableTitle();
                    break;
                //群組管理
                case "roles":
                    re = Repository.Admins.Roles.dataTableTitle();
                    break;

                //使用者
                case "user":
                    re = Repository.Admins.User.dataTableTitle();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.System.Ashcan.dataTableTitle();
                    break;

        
            }

            return re;
        }

        /// <summary>
        /// 取得預設排序
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy(string tables)
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //產品資訊
                case "product_info":
                    re = Repository.Product.ProductInfo.defaultOrderBy();
                    break;

                //首頁content
                case "index_content":
                    re = Repository.Advertise.IndexContent.defaultOrderBy();
                    break;

                //系統項目
                case "system_item":
                    re = Repository.SystemItem.Data.defaultOrderBy();
                    break;

                //系統項目分類
                case "system_item_category":
                    re = Repository.SystemItem.Category.defaultOrderBy();
                    break;
                //問券問題
                case "problem":
                    re = Repository.Business.Problem.defaultOrderBy();
                    break;

                //問券問題分類
                case "problem_category":
                    re = Repository.Business.ProblemCategory.defaultOrderBy();
                    break;

                //胡連沿革
                case "history":
                    re = Repository.History.Data.defaultOrderBy();
                    break;

                //胡連沿革分類
                case "history_category":
                    re = Repository.History.Category.defaultOrderBy();
                    break;

                //線束2
                case "carsysl2":
                    re = Repository.Product.carsysLevel2.defaultOrderBy();
                    break;

                //線束產品對應
                case "mapcarsys":
                    re = Repository.Product.mapCarsys.defaultOrderBy();
                    break;

                //線束4
                case "carsysl4":
                    re = Repository.Product.carsysLevel4.defaultOrderBy();
                    break;

                //廣告
                case "advertise":
                    re = Repository.Advertise.Data.defaultOrderBy();
                    break;


                //投資人Q&A

                case "shareholder_faq":
                    re = Repository.Investor.ShareholderFaq.defaultOrderBy();
                    break;
                //下載專區分類
                case "welfare_category":
                    re = Repository.Welfare.Category.defaultOrderBy();
                    break;

                //人力資源
                case "welfare":
                    re = Repository.Welfare.Data.defaultOrderBy();
                    break;
                //下載專區
                case "downloads":
                    re = Repository.Download.Data.defaultOrderBy();
                    break;

                //下載專區分類
                case "download_category":
                    re = Repository.Download.Category.defaultOrderBy();
                    break;
                //全球據點
                case "locations":
                    re = Repository.About.Location.defaultOrderBy();
                    break;

                //客戶教育訓練平台
                case "courses":
                    re = Repository.Business.Courses.defaultOrderBy();
                    break;

                //最新消息分類
                case "news_category":
                    re = Repository.News.Category.defaultOrderBy();
                    break;

                //客戶教育訓練平台分類
                case "course_category":
                    re = Repository.Business.CourseCategory.defaultOrderBy();
                    break;

                //商務專區-FAQ問與答
                case "business_faq":
                    re = Repository.Business.BusinessFaq.defaultOrderBy();
                    break;
                //其他資訊
                case "notes_data":
                    re = Repository.Notes.NotesData.defaultOrderBy();
                    break;
                //系統日誌
                case "system_log":
                    re.Clear();
                    re = Repository.System.Log.defaultOrderBy();
                    break;
                //防火牆
                case "firewalls":
                    re.Clear();
                    re = Repository.System.Firewalls.defaultOrderBy();
                    break;

      
            }
            return re;
        }

        /// <summary>
        /// 是否使用語系
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string useLang(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //產品資訊
                case "product_info":
                    re = Repository.Product.ProductInfo.useLang();
                    break;

                //首頁content
                case "index_content":
                    re = Repository.Advertise.IndexContent.useLang();
                    break;

                //客戶意見反饋
                case "feedBack":
                    re = Repository.Business.feedback.useLang();
                    break;

                //系統項目
                case "system_item":
                    re = Repository.SystemItem.Data.useLang();
                    break;

                //系統項目分類
                case "system_item_category":
                    re = Repository.SystemItem.Category.useLang();
                    break;
                //問券問題
                case "problem":
                    re = Repository.Business.Problem.useLang();
                    break;

                //問券問題分類
                case "problem_category":
                    re = Repository.Business.ProblemCategory.useLang();
                    break;
                //胡連沿革
                case "history":
                    re = Repository.History.Data.useLang();
                    break;

                //胡連沿革分類
                case "history_category":
                    re = Repository.History.Category.useLang();
                    break;

                //線束2
                case "carsysl2":
                    re = Repository.Product.carsysLevel2.useLang();
                    break;

                //線束產品對應
                case "mapcarsys":
                    re = Repository.Product.mapCarsys.useLang();
                    break;

                //線束4
                case "carsysl4":
                    re = Repository.Product.carsysLevel4.useLang();
                    break;

                //廣告
                case "advertise":
                    re = Repository.Advertise.Data.useLang();
                    break;

                //投資人Q&A
                case "shareholder_faq":
                    re = Repository.Investor.ShareholderFaq.useLang();
                    break;
                //下載專區分類
                case "welfare_category":
                    re = Repository.Welfare.Category.useLang();
                    break;

                //人力資源
                case "welfare":
                    re = Repository.Welfare.Data.useLang();
                    break;

                //下載專區
                case "downloads":
                    re = Repository.Download.Data.useLang();
                    break;

                //下載專區分類
                case "download_category":
                    re = Repository.Download.Category.useLang();
                    break;
                //全球據點
                case "locations":
                    re = Repository.About.Location.useLang();
                    break;

                //客戶教育訓練平台
                case "courses":
                    re = Repository.Business.Courses.useLang();
                    break;

                //客戶教育訓練平台分類
                case "course_category":
                    re = Repository.Business.CourseCategory.useLang();
                    break;
                // 商務專區 - FAQ問與答
                case "business_faq":
                    re = Repository.Business.BusinessFaq.useLang();
                    break;
                //其他資訊
                case "notes_data":
                    re = Repository.Notes.NotesData.useLang();
                    break;
                //關於胡連
                case "abouts":
                    re = Repository.About.About.useLang();
                    break;
                //最新消息
                case "news":
                    re = Repository.News.Data.useLang();
                    break;

                //最新消息分類
                case "news_category":
                    re = Repository.News.Category.useLang();
                    break;
               
                //系統日誌
                case "system_log":
                    re = Repository.System.Log.useLang();
                    break;

                //防火牆
                case "firewalls":
                    re = Repository.System.Firewalls.useLang();
                    break;
                //群組管理
                case "roles":
                    re = Repository.Admins.Roles.useLang();
                    break;
                //使用者
                case "user":
                    re = Repository.Admins.User.useLang();
                    break;

                //網站基本資料
                case "web_data":
                    re = Repository.System.WebData.useLang();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = Repository.System.Smtp.useLang();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.System.Ashcan.useLang();
                    break;
             
            }

            return re;
        }

        /// <summary>
        /// 取得列表說明
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic listMessage(string tables)
        {
            dynamic re = null;
            switch (tables)
            {
                //防火牆
                case "firewalls":
                    re = Repository.System.Firewalls.listMessage();
                    break;


                default:
                    re = "";
                    break;
            }

            return re;
        }

        /// <summary>
        /// 欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getColData(string tables, dynamic data)
        {
            dynamic re = null;
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //產品資訊
                case "product_info":
                    re = Repository.Product.ProductInfo.colFrom();
                    break;

                //首頁content
                case "index_content":
                    re = Repository.Advertise.IndexContent.colFrom();
                    break;

                //客戶意見反饋
                case "feedBack":
                    re = Repository.Business.feedback.colFrom();
                    break;

                //系統項目
                case "system_item":
                    re = Repository.SystemItem.Data.colFrom();
                    break;

                //系統項目分類
                case "system_item_category":
                    re = Repository.SystemItem.Category.colFrom();
                    break;
                //問券問題
                case "problem":
                    re = Repository.Business.Problem.colFrom();
                    break;

                //問券問題分類
                case "problem_category":
                    re = Repository.Business.ProblemCategory.colFrom();
                    break;
                //胡連沿革
                case "history":
                    re = Repository.History.Data.colFrom();
                    break;

                //胡連沿革分類
                case "history_category":
                    re = Repository.History.Category.colFrom();
                    break;

                //線束2
                case "carsysl2":
                    re = Repository.Product.carsysLevel2.colFrom();
                    break;

                //線束產品對應
                case "mapcarsys":
                    re = Repository.Product.mapCarsys.colFrom();
                    break;

                //線束4
                case "carsysl4":
                    re = Repository.Product.carsysLevel4.colFrom();
                    break;

                //廣告
                case "advertise":
                    re = Repository.Advertise.Data.colFrom(data);
                    break;


                //投資人Q&A
                case "shareholder_faq":
                    re = Repository.Investor.ShareholderFaq.colFrom();
                    break;
                //下載專區分類
                case "welfare_category":
                    re = Repository.Welfare.Category.colFrom();
                    break;

                //人力資源
                case "welfare":
                    re = Repository.Welfare.Data.colFrom();
                    break;

                //下載專區
                case "downloads":
                    re = Repository.Download.Data.colFrom(data);
                    break;

                //下載專區分類
                case "download_category":
                    re = Repository.Download.Category.colFrom();
                    break;
                //全球據點
                case "locations":
                    re = Repository.About.Location.colFrom();
                    break;

                //客戶教育訓練平台
                case "courses":
                    re = Repository.Business.Courses.colFrom();
                    break;

                //客戶教育訓練平台分類
                case "course_category":
                    re = Repository.Business.CourseCategory.colFrom();
                    break;
                //商務專區-FAQ問與答
                case "business_faq":
                    re = Repository.Business.BusinessFaq.colFrom();
                    break;
                //其他資訊
                case "notes_data":
                    re = Repository.Notes.NotesData.colFrom();
                    break;
                //關於胡連
                case "abouts":
                    re = Repository.About.About.colFrom();
                    break;
                //最新消息
                case "news":
                    re = Repository.News.Data.colFrom();
                    break;

                //最新消息分類
                case "news_category":
                    re = Repository.News.Category.colFrom();
                    break;
                  

                //系統日誌
                case "system_log":
                    re = Repository.System.Log.colFrom();
                    break;
                //防火牆
                case "firewalls":
                    re = Repository.System.Firewalls.colFrom();
                    break;
                //群組管理
                case "roles":
                    re = Repository.Admins.Roles.colFrom();
                    break;
                //使用者
                case "user":
                    re = Repository.Admins.User.colFrom();
                    break;

                //網站基本資料
                case "web_data":
                    re = Repository.System.WebData.colFrom();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = Repository.System.Smtp.colFrom();
                    break;

                //系統參數
                case "system_data":
                    re = Repository.System.Data.colFrom();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.System.Ashcan.colFrom();
                    break;

                //會員登入紀錄
                case "member_log":
                    re = Repository.System.MemberLog.colFrom();
                    break;

            
            }

            return re;
        }

        /// <summary>
        /// 取得資料庫資料
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getPresetData(string tables, string guid)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            string re = "";

            Dictionary<String, Object> reValData = new Dictionary<String, Object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //產品資訊
                case "product_info":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<product_info>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //首頁content
                case "index_content":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<index_content>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //客戶意見反饋
                case "feedBack":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<problem>();
                        var tempData = data.ReadsWhere(m => m.guid == guid && m.category == "category7");
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //系統項目
                case "system_item":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_item>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //系統項目分類
                case "system_item_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_item_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //問券問題
                case "problem":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<problem>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //問券問題分類
                case "problem_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<problem_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //胡連沿革
                case "history":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<history>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //胡連沿革分類
                case "history_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<history_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //線束2
                case "carsysl2":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<carsysl2>();
                        var tempData = data.ReadsWhere(m => m.guid == Int32.Parse(guid));
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //線束產品對應
                case "mapcarsys":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<mapcarsys>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //線束4
                case "carsysl4":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<carsysl4>();
                        var tempData = data.ReadsWhere(m => m.guid == Int32.Parse(guid));
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //廣告
                case "advertise":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<advertise>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //投資人Q&A
                case "shareholder_faq":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<shareholder_faq>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //人力資源分類
                case "welfare_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<welfare_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //人力資源
                case "welfare":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<welfare>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //下載專區
                case "downloads":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<downloads>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //下載專區分類
                case "download_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<download_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //全球據點
                case "locations":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<locations>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //客戶教育訓練平台
                case "courses":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<courses>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //客戶教育訓練平台分類
                case "course_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<course_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //商務專區-FAQ問與答
                case "business_faq":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<business_faq>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //其他資訊
                case "notes_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<notes_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //關於胡連
                case "abouts":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<abouts>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //最新消息
                case "news":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<news>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);

                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //最新消息分類
                case "news_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<news_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
             

                //系統日誌
                case "system_log":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_log>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //防火牆
                case "firewalls":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<firewalls>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //群組管理
                case "roles":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<roles>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //使用者
                case "user":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<user>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //網站基本資料
                case "web_data":
                    //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString);
                    //conn.Open();
                    //SqlCommand cmd = new SqlCommand("select * from product", conn);
                    //SqlDataReader reader = cmd.ExecuteReader();

                    //dynamic Model = null;//目前Model
                    //dynamic FromData = null;//表單資訊
                    ////FromData = JsonConvert.DeserializeObject<index_content>(form);
                    //string l5 = "";
                    //string l4 = "";
                    //string l3 = "";
                    //string l4Caption = "";
                    //string l2 = "";
                    //string l2Caption = "";
                    //string application = "";
                    //string endur = "";
                    //string seoDefault = "";


                    //Model = model.Repository<product_info>();

                    //while (reader.Read())
                    //{
                    //    var productid = Int32.Parse(reader[0].ToString());
                    //    var ptype = reader[2].ToString();

                    //    using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    //    {
                    //        string sql2 = "select * from map_carsys where product_id= " + productid;
                    //        using (SqlCommand cmd2 = conn2.CreateCommand())
                    //        {
                    //            conn2.Open();
                    //            cmd2.CommandText = sql2;
                    //            cmd2.CommandType = CommandType.Text;
                    //            SqlDataReader reader2 = cmd2.ExecuteReader();
                    //            while (reader2.Read())
                    //            {
                    //                l5 = reader2[2].ToString();
                    //            }
                    //        }
                    //    }

                    //    if (l5 != "")
                    //    {
                    //        using (SqlConnection conn3 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    //        {
                    //            string sql3 = "select * from carsys_l5 where id= " + l5;
                    //            using (SqlCommand cmd3 = conn3.CreateCommand())
                    //            {
                    //                conn3.Open();
                    //                cmd3.CommandText = sql3;
                    //                cmd3.CommandType = CommandType.Text;
                    //                SqlDataReader reader3 = cmd3.ExecuteReader();
                    //                while (reader3.Read())
                    //                {
                    //                    l4 = reader3[1].ToString();
                    //                }
                    //            }
                    //        }
                    //    }

                    //    if (l4 != "")
                    //    {
                    //        using (SqlConnection conn5 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    //        {
                    //            string sql5 = "select * from carsys_l4 where id= " + l4;
                    //            using (SqlCommand cmd5 = conn5.CreateCommand())
                    //            {
                    //                conn5.Open();
                    //                cmd5.CommandText = sql5;
                    //                cmd5.CommandType = CommandType.Text;
                    //                SqlDataReader reader5 = cmd5.ExecuteReader();
                    //                while (reader5.Read())
                    //                {
                    //                    l3 = reader5[1].ToString();
                    //                    l4Caption = reader5[2].ToString();
                    //                }
                    //            }
                    //        }
                    //    }

                    //    if (l3 != "")
                    //    {
                    //        using (SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    //        {
                    //            string sql6 = "select * from carsys_l3 where id= " + l3;
                    //            using (SqlCommand cmd6 = conn6.CreateCommand())
                    //            {
                    //                conn6.Open();
                    //                cmd6.CommandText = sql6;
                    //                cmd6.CommandType = CommandType.Text;
                    //                SqlDataReader reader6 = cmd6.ExecuteReader();
                    //                while (reader6.Read())
                    //                {
                    //                    l2 = reader6[1].ToString();
                    //                }
                    //            }
                    //        }
                    //    }

                    //    if (l2 != "")
                    //    {
                    //        using (SqlConnection conn7 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    //        {
                    //            string sql7 = "select * from carsys_l2 where id= " + l2;
                    //            using (SqlCommand cmd7 = conn7.CreateCommand())
                    //            {
                    //                conn7.Open();
                    //                cmd7.CommandText = sql7;
                    //                cmd7.CommandType = CommandType.Text;
                    //                SqlDataReader reader7 = cmd7.ExecuteReader();
                    //                while (reader7.Read())
                    //                {
                    //                    l2Caption = reader7[2].ToString();
                    //                }
                    //            }
                    //        }
                    //    }

                    //    using (SqlConnection conn8 = new SqlConnection(ConfigurationManager.ConnectionStrings["productDB"].ConnectionString))
                    //    {
                    //        if (ptype == "Housing")
                    //        {
                    //            string sql8 = "select * from housing where product_id= " + productid;
                    //            using (SqlCommand cmd8 = conn8.CreateCommand())
                    //            {
                    //                conn8.Open();
                    //                cmd8.CommandText = sql8;
                    //                cmd8.CommandType = CommandType.Text;
                    //                SqlDataReader reader8 = cmd8.ExecuteReader();
                    //                while (reader8.Read())
                    //                {
                    //                    application = reader8[16].ToString();
                    //                }
                    //            }
                    //            seoDefault = application;
                    //        }
                    //        else if (ptype == "Terminal")
                    //        {
                    //            string sql8 = "select * from terminal where product_id= " + productid;
                    //            using (SqlCommand cmd8 = conn8.CreateCommand())
                    //            {
                    //                conn8.Open();
                    //                cmd8.CommandText = sql8;
                    //                cmd8.CommandType = CommandType.Text;
                    //                SqlDataReader reader8 = cmd8.ExecuteReader();
                    //                while (reader8.Read())
                    //                {
                    //                    endur = reader8[9].ToString();
                    //                }
                    //            }
                    //            seoDefault = endur;
                    //        }
                    //    }

                    //    var productNew = (from a in DB.product_info
                    //                      where a.id == productid
                    //                      select a).FirstOrDefault();

                    //    Model = DB.product_info.Where(m => m.product_id == productid && m.lang == "en").FirstOrDefault();
                    //    Model.content = seoDefault;
                    //    if (l2Caption == "")
                    //    {
                    //        Model.carsysl4id = "";
                    //    }
                    //    else
                    //    {
                    //        Model.carsysl4id = l2Caption.Split('(')[0] + " - " + l4Caption.Split('(')[0];
                    //    }

                    //    DB.SaveChanges();

                    //    l5 = "";
                    //    l4 = "";
                    //    l3 = "";
                    //    l4Caption = "";
                    //    l2 = "";
                    //    l2Caption = "";
                    //    application = "";
                    //    endur = "";
                    //    seoDefault = "";
                    //}

                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<web_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //系統參數
                case "system_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_data>();
                        Guid newGuid = Guid.Parse(guid);
                        var tempData = data.ReadsWhere(m => m.guid == newGuid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<smtp_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //會員登入紀錄
             /*   case "member_log":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<member_log>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //API呼叫紀錄
                case "payment_apilog":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<payment_apilog>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;*/
            }

            return re;
        }

        /// <summary>
        /// 回傳標題
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getDataTitle(string tables, string guid)
        {
            Model DB = new Model();
            string re = "";
            switch (tables)
            {
                /*  case "investor_info":
                      try
                      {
                          investor_info data = Web.Repository.Investor.InvestorInfo.Single("tw", guid);
                          re = data.title;
                      }
                      catch { }

                      break;


                  case "urban_column_files":
                      try
                      {
                          urban_column data = Repository.Urban.UrbanColumn.Single("tw", guid);
                          re = "[" + data.title + "]";
                      }
                      catch { }
                      break;

                  case "cases_progress":
                      try
                      {
                          cases data = Repository.Cases.CasesData.Single("tw", guid);
                          if (data != null)
                          {
                              re = "[" + data.title + "]";
                          }
                      }
                      catch { }
                      break;

                  case "cases_reservation":
                      try
                      {
                          cases data = Repository.Cases.CasesData.Single("tw", guid);
                          re = "[" + data.title + "]";
                      }
                      catch { }
                      break;*/

                case "abouts":
                    try
                    {
                        abouts data = Repository.About.About.Single("tw", guid);
                        re = data.title;
                    }
                    catch { }
                    break;

                case "downloads":
                    try
                    {
                        download_category data = Repository.Download.Category.Single("tw", guid);
                        re = data.title;
                    }
                    catch { }
                    break;
            }

            return re;
        }

        /// <summary>
        /// 複選選單選取判斷用
        /// </summary>
        /// <param name="tabels"></param>
        /// <param name="user_guid"></param>
        /// <param name="forum_guid"></param>
        /// <returns></returns>
        public static string selectMultipleSelected(string tabels, string user_guid, string forum_guid)
        {
            Model DB = new Model();

            string selected = "";
            /*
            switch(tabels)
            {
                case "forum_message":

                    int data = DB.forum_message.Where(m=>m.user_guid == user_guid).Where(m=>m.forum_guid == forum_guid).Select(a => new { guid = a.guid }).Count();
                    if(data > 0)
                    {
                        selected = " selected";
                    }
                    break;
            }
            */

            return selected;
        }

        /// <summary>
        /// 新增 修改
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="form"></param>
        /// <param name="Field"></param>
        /// <returns></returns>
        public static string saveData(string tables, string form, string Field, string guid, string actType, Dictionary<String, Object> FormObj = null)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            dynamic Model = null;//目前Model
            dynamic Model2 = null;//目前Model
            dynamic Model3 = null;//目前Model
            dynamic FromData = null;//表單資訊
            dynamic FromData2 = null;//表單資訊
            dynamic FromData3 = null;//表單資訊
            string lang = "";
            system_log system_Log = new system_log();//系統紀錄用

            var formArray = JsonConvert.DeserializeObject<Dictionary<string, object>>(form);

            string def_re_mail = "";

            try
            {
                def_re_mail = formArray["def_re_mail"].ToString();
            }
            catch { }

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //產品資訊
                case "product_info":
                    FromData = JsonConvert.DeserializeObject<product_info>(form);
                    Model = model.Repository<product_info>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.product_info.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }

                    var temp_pic = (from a in DB.product_info
                                    where a.guid == guid
                                    select a.pic).FirstOrDefault();

                    FromData.pic = temp_pic;
                    var temp_productid = (from a in DB.product_info
                                    where a.guid == guid
                                    select a.product_id).FirstOrDefault();

                    FromData.product_id = temp_productid;

                    string templang = FromData.lang;
                    
                    if (FormObj.ContainsKey("carsysl4name"))
                    {
                        var l4idlist = FormObj["carsysl4name"].ToString();
                        var l4idsplit = l4idlist.Split(',');

                        var delDefaultProductSysList = (from a in DB.mapcarsys
                                                        where a.product_id == temp_productid
                                                        select a).ToList();
                        foreach (var item in delDefaultProductSysList)
                        {
                            DB.mapcarsys.Remove(item);
                            DB.SaveChanges();
                        }

                        foreach (var data in l4idsplit)
                        {
                            var temp = Int32.Parse(data);
                            var level5 = (from a in DB.carsysl5
                                          where a.l4 == temp
                                          select a).FirstOrDefault();

                            var isData = (from a in DB.mapcarsys
                                          where a.product_id == temp_productid && a.lang == templang && a.l5 == temp
                                          select a).FirstOrDefault();

                            if (isData == null)
                            {
                                FromData2 = JsonConvert.DeserializeObject<mapcarsys>(form);
                                Model2 = model.Repository<mapcarsys>();
                                FromData2.guid = Guid.NewGuid().ToString();
                                FromData2.product_id = FromData.product_id;
                                FromData2.l5 = level5.id;
                                FromData2.status = "Y";
                                FromData2.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                FromData2.lang = FromData.lang;
                                Model2.Create(FromData2);
                                Model2.SaveChanges();
                            }
                            else
                            {
                                var delProductSysList = (from a in DB.mapcarsys
                                                         where a.product_id == temp_productid
                                                         select a).ToList();
                                foreach (var item in delProductSysList)
                                {
                                    DB.mapcarsys.Remove(item);
                                    DB.SaveChanges();
                                }

                                FromData2 = JsonConvert.DeserializeObject<mapcarsys>(form);
                                Model2 = model.Repository<mapcarsys>();
                                FromData2.guid = Guid.NewGuid().ToString();
                                FromData2.product_id = FromData.product_id;
                                FromData2.l5 = level5.id;
                                FromData2.status = "Y";
                                FromData2.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                FromData2.lang = FromData.lang;
                                Model2.Create(FromData2);
                                Model2.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        var delProductSysList = (from a in DB.mapcarsys
                                                 where a.product_id == temp_productid
                                                 select a).ToList();
                        foreach (var item in delProductSysList)
                        {
                            DB.mapcarsys.Remove(item);
                            DB.SaveChanges();
                        }
                    }

                    
                    break;

                //首頁content
                case "index_content":
                    FromData = JsonConvert.DeserializeObject<index_content>(form);
                    Model = model.Repository<index_content>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.index_content.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //客戶意見反饋
                case "feedBack":
                    FromData = JsonConvert.DeserializeObject<problem>(form);

                    
                    if (FromData.notes == null)
                    {
                        string tempguid = FromData.guid;
                        FromData.notes = (from a in DB.problem
                                             where a.guid == tempguid && a.lang == "tw"
                                          select a.notes).FirstOrDefault();
                    }
                    if (FromData.status == null)
                    {
                        string tempguid = FromData.guid;
                        FromData.status = (from a in DB.problem
                                             where a.guid == tempguid && a.lang == "tw"
                                           select a.status).FirstOrDefault();
                    }
                    if (FromData.category == null)
                    {
                        string tempguid = FromData.guid;
                        FromData.category = (from a in DB.problem
                                             where a.guid == tempguid && a.lang == "tw"
                                             select a.category).FirstOrDefault();
                    }
                    if (FromData.sortIndex == null)
                    {
                        string tempguid = FromData.guid;
                        FromData.sortIndex = (from a in DB.problem
                                             where a.guid == tempguid && a.lang == "tw"
                                              select a.sortIndex).FirstOrDefault();
                    }

                    Model = model.Repository<problem>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.problem.Where(m => m.guid == guid && m.lang == lang && m.category == "category7").FirstOrDefault();
                    }
                    break;

                //系統項目
                case "system_item":
                    FromData = JsonConvert.DeserializeObject<system_item>(form);
                    Model = model.Repository<system_item>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.system_item.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //系統項目分類
                case "system_item_category":
                    FromData = JsonConvert.DeserializeObject<system_item_category>(form);
                    Model = model.Repository<system_item_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.system_item_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //問券問題
                case "problem":
                    FromData = JsonConvert.DeserializeObject<problem>(form);
                    Model = model.Repository<problem>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.problem.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //問券問題分類
                case "problem_category":
                    FromData = JsonConvert.DeserializeObject<problem_category>(form);
                    Model = model.Repository<problem_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.problem_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //胡連沿革
                case "history":
                    FromData = JsonConvert.DeserializeObject<history>(form);
                    Model = model.Repository<history>();

                    if (FromData.sortIndex == null)
                    {
                        string tempguid = FromData.guid;
                        FromData.sortIndex = (from a in DB.history
                                              where a.guid == tempguid && a.lang == "tw"
                                              select a.sortIndex).FirstOrDefault();
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.history.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //胡連沿革分類
                case "history_category":
                    FromData = JsonConvert.DeserializeObject<history_category>(form);
                    Model = model.Repository<history_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.history_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //線束2
                case "carsysl2":
                    FromData = JsonConvert.DeserializeObject<carsysl2>(form);
                    Model = model.Repository<carsysl2>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.carsysl2.Where(m => m.guid == Int32.Parse(guid)).FirstOrDefault();
                    }
                    break;

                //線束產品對應
                case "mapcarsys":
                    FromData = JsonConvert.DeserializeObject<mapcarsys>(form);
                    Model = model.Repository<mapcarsys>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.mapcarsys.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //線束4
                case "carsysl4":
                    FromData = JsonConvert.DeserializeObject<carsysl4>(form);
                    Model = model.Repository<carsysl4>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.carsysl4.Where(m => m.guid == Int32.Parse(guid)).FirstOrDefault();
                    }
                    break;

                //投資人意見留言板
                case "inquire":
                    FromData = JsonConvert.DeserializeObject<inquire>(form);
                    Model = model.Repository<inquire>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.inquire.Where(m => m.guid == guid ).FirstOrDefault();
                    }
                    break;
                //客戶聯絡表單
                case "contact":
                    FromData = JsonConvert.DeserializeObject<contact>(form);
                    Model = model.Repository<contact>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.contact.Where(m => m.guid == guid ).FirstOrDefault();
                    }
                    break;

                //客戶滿意度調查
                case "survey":
                    FromData = JsonConvert.DeserializeObject<survey>(form);
                    Model = model.Repository<survey>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.survey.Where(m => m.guid == guid ).FirstOrDefault();
                    }
                    break;

                //會員
                case "member":
                    FromData = JsonConvert.DeserializeObject<member>(form);
                    Model = model.Repository<member>();
                    if (Field != "")
                    {
                        guid = FromData.guid;

                        Model = DB.member.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //廣告
                case "advertise":
                    FromData = JsonConvert.DeserializeObject<advertise>(form);
                    Model = model.Repository<advertise>();
                     
                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "tw" || FromData.lang == "en" || FromData.lang == "cn")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp;
                        }
                    }

                    if (FromData.sortIndex == null)
                    {
                        string tempguid = FromData.guid;
                        FromData.sortIndex = (from a in DB.advertise
                                              where a.guid == tempguid && a.lang == "tw"
                                              select a.sortIndex).FirstOrDefault();
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.advertise.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //投資人Q&A
                case "shareholder_faq":
                    FromData = JsonConvert.DeserializeObject<shareholder_faq>(form);
                    Model = model.Repository<shareholder_faq>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.shareholder_faq.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //人力資源分類
                case "welfare_category":
                    FromData = JsonConvert.DeserializeObject<welfare_category>(form);
                    Model = model.Repository<welfare_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.welfare_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //人力資源
                case "welfare":
                    FromData = JsonConvert.DeserializeObject<welfare>(form);
                    Model = model.Repository<welfare>();
                    if (FromData.status.Length > 1)
                    {
                        if (FromData.status.Length > 2)
                        {
                            FromData.status = FromData.status.Substring(2);
                        }
                        else
                        {
                            FromData.status = FromData.status.Substring(1);
                        }
                    }
                    if (FromData.sortIndex == null)
                    {
                        string tempguid = FromData.guid;
                        FromData.sortIndex = (from a in DB.welfare
                                              where a.guid == tempguid && a.lang == "tw"
                                              select a.sortIndex).FirstOrDefault();
                        if (FromData.sortIndex == null)
                        {
                            FromData.sortIndex = 1;
                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.welfare.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //下載專區
                case "downloads":
                    FromData = JsonConvert.DeserializeObject<downloads>(form);
                    Model = model.Repository<downloads>();

                    if (FromData.file != null && FromData.file != "")
                    {
                        var splitstatementfile = FromData.file.Split('/');
                        string statementtemp = "";
                        int deleteindex = 0;
                        for (var i = 0; i < splitstatementfile.Length; i++)
                        {
                            if (splitstatementfile[i] == "Content")
                            {
                                deleteindex = i;
                            }
                        }

                        for (var j = 0; j < deleteindex; j++)
                        {
                            splitstatementfile[j] = "";
                        }

                        for (var k = 0; k < splitstatementfile.Length; k++)
                        {
                            if (splitstatementfile[k] != "")
                            {
                                statementtemp = statementtemp + "/" + splitstatementfile[k];
                            }
                        }

                        FromData.file = statementtemp;
                    }

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "tw" || FromData.lang == "en" || FromData.lang == "cn")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp;
                        }
                    }

                    //if (FromData.pic == null)
                    //{
                    //    string tempguid = FromData.guid;
                    //    FromData.pic = (from a in DB.downloads
                    //                         where a.guid == tempguid
                    //                         select a.pic).FirstOrDefault();
                    //}
                    if (FromData.category != null)
                    {
                        var categorySplit = FromData.category.Split(',');
                        if (categorySplit.Length > 1)
                        {
                            FromData.category = categorySplit[1];
                        }
                    }

                    if (FromData.sortIndex == null)
                    {
                        string tempguid = FromData.guid;
                        FromData.sortIndex = (from a in DB.downloads
                                        where a.guid == tempguid
                                        select a.sortIndex).FirstOrDefault();
                        if (FromData.sortIndex == null)
                        {
                            FromData.sortIndex = 1;
                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.downloads.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //下載專區分類
                case "download_category":
                    FromData = JsonConvert.DeserializeObject<download_category>(form);
                    Model = model.Repository<download_category>();
                    FromData.category = "audit_master";

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.download_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //全球據點
                case "locations":
                   
                    FromData = JsonConvert.DeserializeObject<locations>(form);
                    Model = model.Repository<locations>();

                    if (FromData.sortIndex == null)
                    {
                        string tempguid = FromData.guid;
                        FromData.sortIndex = (from a in DB.locations
                                              where a.guid == tempguid && a.lang == "tw"
                                              select a.sortIndex).FirstOrDefault();
                        if (FromData.sortIndex == null)
                        {
                            FromData.sortIndex = 1;
                        }
                    }

                    if (FromData.status.Length > 1)
                    {
                        if (FromData.status.Length > 2)
                        {
                            FromData.status = FromData.status.Substring(2);
                        }
                        else
                        {
                            FromData.status = FromData.status.Substring(1);
                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.locations.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //客戶教育訓練平台
                case "courses":
                    FromData = JsonConvert.DeserializeObject<courses>(form);
                    Model = model.Repository<courses>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.courses.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //客戶教育訓練平台分類
                case "course_category":
                    FromData = JsonConvert.DeserializeObject<course_category>(form);
                    Model = model.Repository<course_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.course_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //商務專區-FAQ問與答
                case "business_faq":
                    FromData = JsonConvert.DeserializeObject<business_faq>(form);
                    Model = model.Repository<business_faq>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.business_faq.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //其他資訊
                case "notes_data":
                    FromData = JsonConvert.DeserializeObject<notes_data>(form);
                    Model = model.Repository<notes_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.notes_data.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //關於胡連
                case "abouts":
                    FromData = JsonConvert.DeserializeObject<abouts>(form);
                    Model = model.Repository<abouts>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.abouts.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //最新消息
                case "news":
                    FromData = JsonConvert.DeserializeObject<news>(form);
                    Model = model.Repository<news>();

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "tw" || FromData.lang == "en" || FromData.lang == "cn")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp;
                        }
                    }

                    if (FromData.file != null && FromData.file != "")
                    {
                        var splitstatementfile = FromData.file.Split('/');
                        string statementtemp = "";
                        int deleteindex = 0;
                        for (var i = 0; i < splitstatementfile.Length; i++)
                        {
                            if (splitstatementfile[i] == "Content")
                            {
                                deleteindex = i;
                            }
                        }

                        for (var j = 0; j < deleteindex; j++)
                        {
                            splitstatementfile[j] = "";
                        }

                        for (var k = 0; k < splitstatementfile.Length; k++)
                        {
                            if (splitstatementfile[k] != "")
                            {
                                statementtemp = statementtemp + "/" + splitstatementfile[k];
                            }
                        }

                        FromData.file = statementtemp.Substring(1);
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //最新消息分類
                case "news_category":

                    FromData = JsonConvert.DeserializeObject<news_category>(form);
                    Model = model.Repository<news_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news_category.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }

                    break;
             
                //系統日誌
                case "system_log":

                    FromData = JsonConvert.DeserializeObject<system_log>(form);
                    Model = model.Repository<system_log>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.system_log.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //防火牆
                case "firewalls":

                    FromData = JsonConvert.DeserializeObject<firewalls>(form);
                    Model = model.Repository<firewalls>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.firewalls.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //群組管理
                case "roles":

                    FromData = JsonConvert.DeserializeObject<roles>(form);
                    Model = model.Repository<roles>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.roles.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;

                //網站基本資料
                case "web_data":
                    FromData = JsonConvert.DeserializeObject<web_data>(form);
                    Model = model.Repository<web_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.web_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    FromData = JsonConvert.DeserializeObject<smtp_data>(form);
                    Model = model.Repository<smtp_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.smtp_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //資源回收桶
                case "ashcan":

                    FromData = JsonConvert.DeserializeObject<ashcan>(form);
                    Model = model.Repository<ashcan>();
                    if (Field != "")
                    {
                        Guid NewGuid = Guid.Parse(FromData.guid.ToString());
                        Model = DB.ashcan.Where(m => m.guid == NewGuid).FirstOrDefault();
                    }

                    break;
                //使用者
                case "user":

                    FromData = JsonConvert.DeserializeObject<user>(form);
                    Model = model.Repository<user>();

                    if (FromData.password != null && FromData.password != "")
                    {
                        FromData.password = FunctionService.md5(FromData.password);
                    }
                    //無輸入密碼進入修改情況下
                    if (FromData.password == "" && FromData.guid != "")
                    {
                        FromData.password = FormObj["defPassword"].ToString();
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.user.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;

                //系統參數
                case "system_data":

                    FromData = JsonConvert.DeserializeObject<system_data>(form);
                    Model = model.Repository<system_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Guid newGuid = Guid.Parse(guid);
                        Model = DB.system_data.Where(m => m.guid == newGuid).FirstOrDefault();
                    }

                    break;
            }
            try
            {
                FromData.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            catch
            { }

           

            if (actType == "add")
            {
                FromData.guid = guid;
                FromData.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                Model.Create(FromData);
                Model.SaveChanges();

                system_Log.title = FunctionService.reSysLogTitle(FromData, "新增");//回傳log標題
            }
            else
            {
                if (Field == "")
                {
                    DateTime re_data = DateTime.Now;
                    if (tables.IndexOf("contacts") != -1 || tables == "cases_reservation")
                    {
                        FromData.re_date = re_data;
                    }

                    //假設非審核者修改
                    System.Web.HttpContext context = System.Web.HttpContext.Current;
                    try
                    {
                        if (context.Session["verify"].ToString() == "N")
                        {
                            List<string> verifyUseTab = Web.Controllers.SiteadminController.verifyTables();//取得有審核的資料表
                            if (verifyUseTab.IndexOf(tables) != -1)
                            {
                                FromData.verify = "E";//一旦修改就是改為未審核
                            }
                        }
                    }
                    catch { }

                    Model.Update(FromData);
                    Model.SaveChanges();

                    if (tables != "product_info" && tables != "index_content")
                    {
                        system_Log.title = FunctionService.reSysLogTitle(FromData, "修改");    //回傳log標題
                    }
                                                                                         //Model = FromData;
                                                                                         //DB.SaveChanges();

                    //回覆信件
                    if ((tables.IndexOf("contacts") != -1 || tables == "cases_reservation") && FromData.status == "Y")
                    {
                        web_data webData = DB.web_data.Where(m => m.lang == "tw").FirstOrDefault();//取得網站基本資訊
                        List<string> MailList = new List<string>();

                        if (!string.IsNullOrEmpty(formArray["email"].ToString()))
                        {
                            MailList = formArray["email"].ToString().Split(',').ToList();
                        }

                        /* var jss = new JavaScriptSerializer();
                         var dict = jss.Deserialize<Dictionary<string, string>>(form);*/

                        string mailContentID = "";

                        if (tables == "contacts")
                        {
                            mailContentID = "4";
                        }
                        if (tables == "urban_contacts")
                        {
                            mailContentID = "5";
                        }
                        if (tables == "cases_reservation")
                        {
                            mailContentID = "7";
                        }

                        NameValueCollection nvc = null;
                        if (formArray != null && FromData.re_mail == "Y" && def_re_mail == "N" && MailList.Count > 0)
                        {
                            nvc = new NameValueCollection(formArray.Count);
                            foreach (var k in formArray)
                            {
                                nvc.Add(k.Key, k.Value.ToString());
                            }
                            nvc.Add("re_date", re_data.ToString("yyyy-MM-dd"));
                            nvc.Add("sysName", nvc["name"].ToString());

                            FunctionService.sendMail(MailList, mailContentID, "tw", nvc);

                            /*  string sendGuid = FromData.guid;
                              Model = DB.contacts.Where(m => m.guid == sendGuid).FirstOrDefault();
                              Model.is_send = "Y";
                              Model.re_date = DateTime.Now;
                              DB.SaveChanges();*/
                        }
                    }
                }
                else
                {
                    //單一欄位修改
                    switch (Field)
                    {
                        case "status":
                            Model.status = FromData.status;
                            break;

                        case "sortIndex":
                            Model.sortIndex = FromData.sortIndex;
                            break;

                        case "logindate":
                            Model.logindate = FromData.logindate;
                            break;
                    }

                    DB.SaveChanges();
                }
            }

            if (FormObj != null)
            {
                #region 權限

                if (FormObj.ContainsKey("permissions") && FormObj["permissions"] != null && FormObj["permissions"].ToString() != "")
                {
                    NameValueCollection permissions = FunctionService.reSubmitFormDataJson(FormObj["permissions"].ToString());

                    role_permissions role_permissions_add = new role_permissions();

                    var delData = DB.role_permissions.Where(m => m.role_guid == guid).ToList();

                    foreach (var item in delData)
                    {
                        DB.role_permissions.Remove((role_permissions)item);
                    }

                    foreach (string key in permissions)
                    {
                        //Guid newGuid = Guid.NewGuid();
                        role_permissions_add = new role_permissions();
                        role_permissions_add.guid = Guid.NewGuid();
                        role_permissions_add.role_guid = guid;
                        role_permissions_add.permissions_guid = key.ToString();
                        role_permissions_add.permissions_status = permissions[key].ToString();
                        DB.role_permissions.Add(role_permissions_add);

                        if (key.ToString() == "3d7ffc73-f7f4-4b24-b4c6-7c35f6f129cb")
                        {
                            //預約鑑賞
                            role_permissions_add = new role_permissions();
                            role_permissions_add.guid = Guid.NewGuid();
                            role_permissions_add.role_guid = guid;
                            role_permissions_add.permissions_guid = "715db7df-0f81-4811-a420-f7113162ac87";
                            role_permissions_add.permissions_status = permissions[key].ToString();
                            DB.role_permissions.Add(role_permissions_add);

                            //工程進度
                            role_permissions_add = new role_permissions();
                            role_permissions_add.guid = Guid.NewGuid();
                            role_permissions_add.role_guid = guid;
                            role_permissions_add.permissions_guid = "e5bc75bb-2e96-48b8-a759-6889cee11ae9";
                            role_permissions_add.permissions_status = permissions[key].ToString();
                            DB.role_permissions.Add(role_permissions_add);
                        }

                        if (key.ToString() == "069c6fe0-eacf-44aa-b873-647975028848")
                        {
                            //都更相關檔案
                            role_permissions_add = new role_permissions();
                            role_permissions_add.guid = Guid.NewGuid();
                            role_permissions_add.role_guid = guid;
                            role_permissions_add.permissions_guid = "898f874e-9ec6-4946-aa45-5b8b24b828ea";
                            role_permissions_add.permissions_status = permissions[key].ToString();
                            DB.role_permissions.Add(role_permissions_add);
                        }

                        DB.SaveChanges();
                    }
                }

                #endregion
            }

            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            //系統紀錄
            try
            {
                if (FromData.lang == defLang || FromData.lang == null)
                {
                    system_Log.ip = FunctionService.GetIP();
                    system_Log.types = "";
                    system_Log.tables = tables;
                    system_Log.tables_guid = guid;
                    system_Log.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    system_Log.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    system_Log.guid = Guid.NewGuid().ToString();
                    Dictionary<String, String> ReUserData = FunctionService.ReUserData();
                    system_Log.username = ReUserData["username"].ToString();
                    DB.system_log.Add(system_Log);
                    DB.SaveChanges();
                    /*  if (getData["username"].ToString() != "sysadmin")
                      {
                      }*/
                }
            }
            catch { }

            //寄發審核通知
            try
            {
                if (FromData.lang == defLang)
                {
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    data.Add("tables", tables);
                    data.Add("actType", actType);
                    data.Add("guid", guid);
                    data.Add("subject", "<a href=\"" + FunctionService.getWebUrl() + "/siteadmin/" + tables + "/edit/" + guid + "\">" + system_Log.title + "</a>");
                    FunctionService.SendVerifyEmail(data);
                }
            }
            catch { }

            return guid;
        }
    }
}