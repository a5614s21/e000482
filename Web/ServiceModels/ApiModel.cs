namespace Web.ServiceModels
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// 登入回傳
    /// </summary>
    public partial class Login
    {
        public string isSuccess { get; set; }

        public string state { get; set; }

        public string message { get; set; }

        public LoginResult result { get; set; }
    }

    /// <summary>
    /// 回傳Token
    /// </summary>
    public partial class LoginResult
    {
        public string jwtToken { get; set; }
    }

    /// <summary>
    /// 會員資料
    /// </summary>
    public partial class Info
    {
        public string isSuccess { get; set; }

        public string state { get; set; }

        public string message { get; set; }

        public InfoResult result { get; set; }
    }

    /// <summary>
    /// 會員資料(購屋列表)
    /// </summary>
    public partial class InfoResult
    {
        public List<InfoData> preSale { get; set; }
    }

    /// <summary>
    /// 會員資料(詳細內容)
    /// </summary>
    public partial class InfoData
    {
        public string hmmcu { get; set; }

        public string mcdc { get; set; }

        public string hmbuno { get; set; }
        public string hmhsn { get; set; }
        public string hmhnpl { get; set; }
        public string hI75T8DTH { get; set; }
        public string cP75T8DTCP { get; set; }
        public string cmdtcca { get; set; }
        public string abalph { get; set; }
        public string abtax { get; set; }
        public string pC59CARDN { get; set; }
        public string pceftb { get; set; }
        public string pcefte { get; set; }
        public string pceftB_Datetime { get; set; }
        public string pceftE_Datetime { get; set; }

        public string hmhsta { get; set; }

        public string hmbufl { get; set; }
    }

    /// <summary>
    /// 修改密碼
    /// </summary>
    public partial class Password
    {
        public string isSuccess { get; set; }

        public string state { get; set; }

        public string message { get; set; }
    }

    public partial class PaymentModel
    {
        public string WFKCOO { get; set; }
        public string WFDCTO { get; set; }
        public string WFDOCO { get; set; }
        public string WFMCU { get; set; }//*工地代號
        public string WFBUNO { get; set; }
        public string WFHSN { get; set; }
        public string WFTWIDNO { get; set; }
        public string WFGLRC { get; set; }
        public string WFNLIN { get; set; }
        public string WFDL01 { get; set; }
        public string WFDL010 { get; set; }
        public string WFDL011 { get; set; }
        public string WFRECDATE { get; set; }
        public string WFAMTLA { get; set; }
        public string WFURDT { get; set; }
        public string WFAMTHIT { get; set; }
    }

    public partial class PaymentListModel
    {
        public string WFMCU { get; set; }   //工地代號

        public string WFDL01 { get; set; }  //名稱

        public string WFDL010 { get; set; } //戶名
    }



    public partial class PaymentDataModel
    {
        public string WFMCU { get; set; }   //工地代號

        public string WFNLIN { get; set; }  //資料排序數字

        public string WFDL01 { get; set; }  //名稱

        public string WFDL010 { get; set; } //戶名

        public string WFDL011 { get; set; } //期別

        public string WFRECDATE { get; set; }   //應繳日期

        public string WFAMTLA { get; set; } //合約金額

        public string WFURDT { get; set; }  //實繳日期

        public string WFAMTHIT { get; set; }    //已繳金額
    }
}