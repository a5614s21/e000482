﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Web;

namespace Web.ServiceModels
{
    /// <summary>
    /// 搜尋引勤來源數量
    /// </summary>
    public partial class FullReferrerModel
    {

        public string name { get; set; }

        public int num { get; set; }
    }

    /// <summary>
    /// 頁面瀏覽數量
    /// </summary>
    public partial class PageViewModel
    {

        public string url { get; set; }
        public string val { get; set; }
        public int views { get; set; }
        public string session { get; set; }
        public string times { get; set; }
        public string title { get; set; }
    }
    /// <summary>
    /// 瀏覽年齡層
    /// </summary>
    public partial class AgeViewModel
    {

        public string age { get; set; }

        public int visits { get; set; }
    }

    /// <summary>
    /// 瀏覽器使用
    /// </summary>
    public partial class BrowserViewModel
    {

        public string name { get; set; }

        public int session { get; set; }
    }

    /// <summary>
    /// 查詢關鍵字
    /// </summary>
    public partial class KeywordViewModel
    {

        public string keyword { get; set; }

        public int num { get; set; }
    }

    /// <summary>
    /// 地區
    /// </summary>
    public partial class CountryViewModel
    {

        public string code { get; set; }
        public string name { get; set; }
    
        public int value { get; set; }
        public string color { get; set; }
     
    }


    /// <summary>
    /// 流量來源
    /// </summary>
    public partial class MediumViewModel
    {
        public string source { get; set; }
     
        public int litres { get; set; } 

    }

    /// <summary>
    /// 流量來源(細項)
    /// </summary>
    public partial class TrafficViewModel
    {
        public string date { get; set; }
     
        public int value { get; set; } 

    }


    /// <summary>
    /// 流量來源
    /// </summary>
    public partial class SessionsViewModel
    {
        public string source { get; set; }
     
        public int num { get; set; } 

    }


}
