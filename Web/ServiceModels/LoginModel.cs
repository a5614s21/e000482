﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Web.Models;
using Web.Repository.System;

namespace Web.ServiceModels
{
    //public static class TokenCrypto
    //{
    //    //產生 HMACSHA256 雜湊
    //    public static string ComputeHMACSHA256(string data, string key)
    //    {
    //        var keyBytes = Encoding.UTF8.GetBytes(key);
    //        using (var hmacSHA = new HMACSHA256(keyBytes))
    //        {
    //            var dataBytes = Encoding.UTF8.GetBytes(data);
    //            var hash = hmacSHA.ComputeHash(dataBytes, 0, dataBytes.Length);
    //            return BitConverter.ToString(hash).Replace("-", "").ToUpper();
    //        }
    //    }

    //    //AES 加密
    //    public static string AESEncrypt(string data, string key, string iv)
    //    {
    //        var keyBytes = Encoding.UTF8.GetBytes(key);
    //        var ivBytes = Encoding.UTF8.GetBytes(iv);
    //        var dataBytes = Encoding.UTF8.GetBytes(data);
    //        using (var aes = Aes.Create())
    //        {
    //            aes.Key = keyBytes;
    //            aes.IV = ivBytes;
    //            aes.Mode = CipherMode.CBC;
    //            aes.Padding = PaddingMode.PKCS7;
    //            var encryptor = aes.CreateEncryptor();
    //            var encrypt = encryptor
    //                .TransformFinalBlock(dataBytes, 0, dataBytes.Length);
    //            return Convert.ToBase64String(encrypt);
    //        }
    //    }

    //    //AES 解密
    //    public static string AESDecrypt(string data, string key, string iv)
    //    {
    //        var keyBytes = Encoding.UTF8.GetBytes(key);
    //        var ivBytes = Encoding.UTF8.GetBytes(iv);
    //        var dataBytes = Convert.FromBase64String(data);
    //        using (var aes = Aes.Create())
    //        {
    //            aes.Key = keyBytes;
    //            aes.IV = ivBytes;
    //            aes.Mode = CipherMode.CBC;
    //            aes.Padding = PaddingMode.PKCS7;
    //            var decryptor = aes.CreateDecryptor();
    //            var decrypt = decryptor.TransformFinalBlock(dataBytes, 0, dataBytes.Length);
    //            return Encoding.UTF8.GetString(decrypt);
    //        }
    //    }
    //}

    //public class LoginTokenService
    //{
    //    private Model _context = new Model();

    //    public void InsertToken(Token data)
    //    {
    //        try
    //        {
    //            if (data != null)
    //            {
    //                logintoken l = new logintoken();
    //                l.access_token = data.access_token;
    //                l.refresh_token = data.refresh_token;
    //                l.timeout_date = DateTime.Now;

    //                _context.Entry(l).State = System.Data.Entity.EntityState.Added;
    //                _context.SaveChanges();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //        }
    //    }

    //    public void DeleteData(string refresh_token)
    //    {
    //        try
    //        {
    //            var result = _context.logintoken.Where(x => x.refresh_token == refresh_token).SingleOrDefault();
    //            _context.Entry(result).State = System.Data.Entity.EntityState.Deleted;
    //            _context.SaveChanges();
    //        }
    //        catch (Exception ex)
    //        {
    //        }
    //    }

    //    public logintoken GetData(string refresh_token)
    //    {
    //        try
    //        {
    //            var result = _context.logintoken.Where(x => x.refresh_token == refresh_token).SingleOrDefault();
    //            return result;
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }
    //}

    //public class TokenManager
    //{
    //    //金鑰
    //    public string key = "95a3f17715-89fa10600q-ak0e24f5fi-u2r9f9861e-07yha09f14-bqs47fr5n7-vt3e4h8ye5";

    //    //產生 ToKen
    //    public Token Create(string userid)
    //    {
    //        try
    //        {
    //            //Model
    //            WebDataService WebData = new WebDataService();
    //            var web_data = WebData.GetData();

    //            var exp = web_data.token_seconds;  //過期時間(分鐘)
    //            exp = exp * 60;

    //            var payload = new Payload
    //            {
    //                user_id = userid,
    //                //Unix 時間戳
    //                exp = Convert.ToInt32((DateTime.Now.AddSeconds((double)exp) - new DateTime(1970, 1, 1)).TotalSeconds)
    //            };

    //            var json = JsonConvert.SerializeObject(payload);
    //            var base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
    //            var iv = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);

    //            //使用 AES 加密 Payload
    //            var encrypt = TokenCrypto.AESEncrypt(base64, key.Substring(0, 16), iv);

    //            //取得簽章
    //            var signature = TokenCrypto.ComputeHMACSHA256(iv + "." + encrypt, key.Substring(0, 64));

    //            return new Token
    //            {
    //                //Token 為 iv + encrypt + signature，並用 . 串聯
    //                access_token = iv + "." + encrypt + "." + signature,

    //                //Refresh Token 使用GUID產生
    //                refresh_token = Guid.NewGuid().ToString().Replace("-", ""),
    //                expires_in = exp.GetValueOrDefault(),
    //            };
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }

    //    //刷新 ToKen
    //    public Token Refresh(string token)
    //    {
    //        try
    //        {
    //            LoginTokenService LoginToken = new LoginTokenService();
    //            var data = LoginToken.GetData(token);

    //            //解密
    //            var split = data.access_token.Split('.');
    //            var iv = split[0];
    //            var encrypt = split[1];
    //            var signature = split[2];

    //            //使用AES 解密Payload
    //            var base64 = TokenCrypto.AESDecrypt(encrypt, key.Substring(0, 16), iv);
    //            var json = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
    //            var payload = JsonConvert.DeserializeObject<Payload>(json);

    //            //產生一組新的Token 和 Refresh Token
    //            var t = Create(payload.user_id);

    //            //刪除舊有
    //            LoginToken.DeleteData(token);

    //            //新增
    //            LoginToken.InsertToken(t);

    //            return t;
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }

    //    //取得使用者資訊
    //    public UserData GetUserIfLogin(string token)
    //    {
    //        try
    //        {
    //            LoginTokenService LoginToken = new LoginTokenService();
    //            var Data = LoginToken.GetData(token);

    //            var split = Data.access_token.Split('.');
    //            var iv = split[0];
    //            var encrypt = split[1];
    //            var signature = split[2];

    //            var message = "驗證成功";
    //            var auth = true;

    //            //檢查簽章是否正確
    //            if (signature != TokenCrypto.ComputeHMACSHA256(iv + "." + encrypt, key.Substring(0, 64)))
    //            {
    //                message = "Signature_Error";
    //                auth = false;
    //            }

    //            //使用AES 解密Payload
    //            var base64 = TokenCrypto.AESDecrypt(encrypt, key.Substring(0, 16), iv);
    //            var json = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
    //            var payload = JsonConvert.DeserializeObject<Payload>(json);

    //            //檢查是否過期
    //            if (payload.exp < Convert.ToInt32((DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds))
    //            {
    //                message = "DateTime_Error";
    //                auth = false;
    //            }

    //            UserData result = new UserData
    //            {
    //                user_id = payload.user_id,
    //                message = message,
    //                auth = auth
    //            };

    //            return result;
    //        }
    //        catch (Exception ex)
    //        {
    //            //UserData result = new UserData
    //            //{
    //            //    user_id = "",
    //            //    message = "驗證失敗",
    //            //    auth = false
    //            //};

    //            //return result;
    //            return null;
    //        }
    //    }
    //}

    //public class Token
    //{
    //    //Token
    //    public string access_token { get; set; }

    //    //Refresh Token
    //    public string refresh_token { get; set; }

    //    //幾秒過期
    //    public int expires_in { get; set; }
    //}

    //public class Payload
    //{
    //    //使用者資訊
    //    public string user_id { get; set; }

    //    //過期時間
    //    public int exp { get; set; }
    //}

    //public class UserData
    //{
    //    //使用者資訊
    //    public string user_id { get; set; }

    //    //Token
    //    //public string access_token { get; set; }

    //    //Refresh Token
    //    //public string refresh_token { get; set; }

    //    //訊息
    //    public string message { get; set; }

    //    public bool auth { get; set; }
    //}
}