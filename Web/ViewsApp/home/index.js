import Vue from 'vue'
import HomeMain from './HomeMain.vue'
import ProductAdvanceSearch from './ProductAdvanceSearch.vue'
import NewProductList from './NewProductList.vue'
import AboutMe from './AboutMe.vue'
import GlobalSites from './GlobalSites.vue'
import ProductApply from './ProductApplication.vue'
import CarConnector from './CarConnector.vue'
import NewsAndEvent from './NewsAndEvent.vue'

import axios from 'axios'

import AOS from "aos"
import "aos/dist/aos.css"

Vue.component('select-picker', {
    template: '<select class="firstHide style-1 style-1-1 red-tri w-100" v-model="selectedItem">' +
        '<option value="">{{ title }}</option>' +
        '<option v-bind:value="option.key" v-for="option in options">{{ option.value }}</option>' +
        '</select>',
    name: 'selectpicker',
    props: ['title','options'],
    data() {
        return {
            selectedItem: ""
        }
    },
    mounted: function () {
        $(this.$el).selectpicker();
    },
    watch: {
        options: function (val) {
            this.selectedItem = "";
        }
    },
    updated: function () {
        this.$nextTick(function () {
            if (this.options === null) {
                $(this.$el).selectpicker('hide');
            } else {
                $(this.$el).selectpicker('show');
                $(this.$el).selectpicker('refresh');
            }
        });
    }
});

Vue.component('new-product-list-slick', {
    template:
        '<div class="slider max820 arrows-style-1 dots-style-1 px-0" data-aos="fade-up">' +
            '<div class="mb-3" v-for="item in datas">' +
                '<div class="row flex-wrap mx-0">' +
                    '<div class="col-12 col-md-7 pl-0 pl-md-5 pr-0">' +
                        '<div class= "px-5 px-md-0 pl-xxl-4">' +
                            /*'<img src="latestPorudct1 | appPathFormat" alt="胡連精密">' +*/
                              '<img v-bind:src="item.productImg" alt="胡連精密">' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-12 col-md-5 px-0 pt-md-4">' +
                        '<p class="font-20 font-weight-bold mb-2">{{ item.new_name }}</p>' +
                        '<ul class="noneStyle text-646464 mb-4 pb-2">' +
                            '<li class="mb-1">{{productNewName}} : {{ item.name }}</li>' +
                            '<li class="mb-1">{{productBoxData2}} : {{ item.type }}</li>' +
                            '<li class="mb-1">{{productSerial}} : {{ item.item_class }}</li>' +
                            '<li class="mb-1">{{productSize}} / {{ item.size }}</li>' +
                        '</ul>' +
                        '<a :href="' + '\'product?id=\'' + '+' + 'item.id" class="btn-style-1 bg-black text-white btn-dec mx-auto ml-md-0">{{moreTitle}}</a>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'
    ,
    name: 'NewProductListSlick',
    props: ["datas"],
    data() {
        return {
            selectedItem: "",
            moreTitle: "",
            productBoxData2: "",
            productNewName: "",
            productSerial: "",
            productSize:"",
            latestPorudct1: "/Styles/images/index/latestPorudct-1.png"
        }
    },
    created: function () {
        AOS.init({
            duration: 500,
            offset: 10,
            mirror: true,
        });
        this.$axios.get($('#langPath').val() +'/products/getProductsCategories')
            .then((res) => {
                console.log(res.data);
                this.moreTitle = res.data.moreName;
                this.productBoxData2 = res.data.productBoxData2;
                this.productNewName = res.data.productNewName;
                this.productSerial = res.data.productSerial;
                this.productSize = res.data.productSize;
            })
            .catch((error) => {
                console.error(error)
            })
    },
    beforeUpdate: function () {
        if ($(this.$el).hasClass('slick-initialized')) {
            $(this.$el).slick('unslick');
        }

    },
    updated: function () {
        $(this.$el).slick({
            dots: true,
            prevArrow: " <button class='slick-prev slick-arrow text-hover-red text-black' type='button'><i class='icon-left-big font-24'></i></button>",
            nextArrow: " <button class='slick-next slick-arrow text-hover-red text-black' type='button'><i class='icon-right-big font-24'></i></button>",
            responsive: [
                {
                    breakpoint: 576,
                    settings: {
                        arrows: false
                    },
                },
            ],
        });
    }
});

Vue.component('banner-slider', {
    template:
        '<div class="slider">' +
            '<div class="position-relative">' +
                '<div class="imgFill h-100" data-query="size">' +
                    '<picture>' +
                        '<source media="(min-width: 576px)" :srcset="banner1 | appPathFormat" />' +
                        '<source media="(max-width: 575px)" :srcset="mobilebanner1 | appPathFormat" />' +
                        '<img src="#" alt="胡連精密" />' +
                    '</picture>' +
                '</div>' +
                '<div class="text text-white position-absolute">' +
                    '<p class="font-80 font-weight-bold mb-md-4">' +
                        '<span class="d-block">Professional</span>' +
                        '<span class="d-block">Connector</span>' +
                        '<span class="d-block">Manufacture</span>' +
                    '</p>' +
                    '<p class="font-24">全球專業汽車業連接器和端子供應商</p>' +
                    '<a :href="productspecurl | appPathFormat" class="d-table Heebo font-15 font-weight-bold les2 border-white rounded-25 bg-hover-white text-hover-red py-2 py-sm-3 px-4 mt-3 mt-md-5">ALL PRODUCTS</a>' +
                '</div>' +
                '<div class="circle-group position-absolute">' +
                    '<div class="circle absolute-center">' +
                        '<div class="wave wave-1"></div>' +
                        '<div class="wave wave-2"></div>' +
                        '<div class="wave wave-3"></div>' +
                        '<div class="wave wave-4"></div>' +
                        '<div class="wave wave-5"></div>' +
                        '<div class="wave wave-6"></div>' +
                        '<div class="wave wave-7"></div>' +
                        '<div class="wave wave-8"></div>' +
                        '<div class="wave wave-9"></div>' +
                        '<div class="wave wave-10"></div>' +
                    '</div>' +
                    '<div class="circle-bg bg-white rounded-circle position-relative">' +
                        '<img class="position-relative" :src="radiusproduct | appPathFormat" alt="胡連精密">' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="position-relative">' +
                '<div class="imgFill h-100" data-query="size">' +
                    '<picture>' +
                        '<source media="(min-width: 576px)" :srcset="banner2 | appPathFormat" />' +
                        '<source media="(max-width: 575px)" :srcset="mobilebanner2 | appPathFormat" />' +
                        '<img src="#" alt="胡連精密" />' +
                    '</picture>' +
                '</div>' +
            '</div>' +
            '<div class="position-relative">' +
                '<div class="imgFill h-100" data-query="size">' +
                    '<picture>' +
                        '<source media="(min-width: 576px)" :srcset="banner3 | appPathFormat" />' +
                        '<source media="(max-width: 575px)" :srcset="mobilebanner3 | appPathFormat" />' +
                        '<img src="#" alt="胡連精密" />' +
                    '</picture>' +
                '</div>' +
            '</div>' +
        '</div>',
    name: 'BannerSlider',
    data() {
        return {
            banner1: "/Styles/images/index/banner-1.jpg",
            mobilebanner1: "/Styles/images/index/mobile-banner1.png",
            radiusproduct: "/Styles/images/index/radius-product.png",
            banner2: "/Styles/images/index/banner-2.jpg",
            mobilebanner2: "/Styles/images/index/mobile-banner2.png",
            banner3: "/Styles/images/index/banner-3.jpg",
            mobilebanner3: "/Styles/images/index/mobile-banner3.png",
            productspecurl: "/products/spec"
        }
    },
    created: function () {
        AOS.init({
            duration: 500,
            offset: 10,
            mirror: true,
        });
    },
    mounted: function () {
        $(this.$el).slick({
            dots: true,
            arrows: false,
        });

        $(this.$el).on("beforeChange", function () {
            $(".index-banner-wrapper .slider")
                .find(".fadeInUp")
                .removeClass("fadeInUp");
        });

        $(this.$el)
            .on("afterChange", function () {
                bannerTextEffect();
            })
            .trigger("afterChange");
    },
    methods: {
        bannerTextEffect: function () {
            var $this = this.$el.find(".slick-current");
            $this.find(".font-80").find("span").eq(0).addClass("fadeInUp");
            setTimeout(function () {
                $this.find(".font-80").find("span").eq(1).addClass("fadeInUp");

            }, 50);
            setTimeout(function () {
                $this.find(".font-80").find("span").eq(2).addClass("fadeInUp");
            }, 100);
            setTimeout(function () {
                $this.find(".font-24").addClass("fadeInUp");
            }, 150);

        }
    }
});

Vue.component('news-and-event-slick', {
    template:
        '<div class="slider dots-style-1" data-aos="fade-up">' +
            '<div class="px-sm-3 px-lg-0">' +
                '<h3 class="hidden">產品顏色標準制定公告</h3>' +
                '<a href="new" class="row text-hover-red mx-0 mx-xl-3">' +
                    '<span class="col-12 col-lg-5 d-block img px-0">' +
                        '<img class="w-100" :src="news1 | appPathFormat" alt="胡連精密">' +
                    '</span>' +
                    '<span class="col-12 col-lg-7 d-block pl-0 pl-lg-4 pr-0 pr-lg-3 pr-xl-0 pt-3 pt-lg-0 pb-3 pb-md-0">' +
                        '<span class="d-block font-20 font-weight-bold lh14 mb-2 pb-1">產品顏色標準制定公告</span>' +
                        '<span class="text-646464 line3 mb-2 mb-md-3">為了提供顧客更優質的產品服務，在持續改善計畫中，當前致力於產品顏色的制定，此將依據...</span>' +
                        '<span class="d-block text-red font-weight-bold">2020-03-31</span>' +
                    '</span>' +
                '</a>' +
            '</div>' +
            '<div class="px-sm-3 px-lg-0">' +
                '<h3 class="hidden">歡迎參觀胡連2019年名古屋汽車技術週!</h3>' +
                '<a href="new" class="row text-hover-red mx-0 mx-xl-3">' +
                    '<span class="col-12 col-lg-5 d-block img px-0">' +
                        '<img class="w-100" :src="news2 | appPathFormat" alt="胡連精密">' +
                    '</span>' +
                    '<span class="col-12 col-lg-7 d-block pl-0 pl-lg-4 pr-0 pr-lg-3 pr-xl-0 pt-3 pt-lg-0 pb-3 pb-md-0">' +
                        '<span class="d-block font-20 font-weight-bold lh14 mb-2 pb-1">歡迎參觀胡連2019年名古屋汽車技術週!</span>' +
                        '<span class="text-646464 line3 mb-2 mb-md-3">現場將展示汽機車連接器新品、電動車連接器、車用電子設備以及新開發應用領域的連接器產品</span>' +
                        '<span class="d-block text-red font-weight-bold">2019-08-12</span>' +
                    '</span>' +
                '</a>' +
            '</div>' +
            '<div class="px-sm-3 px-lg-0">' +
                '<h3 class="hidden">歡迎蒞臨胡連2019年印尼電動車，智慧車秀！</h3>' +
                '<a href="new" class="row text-hover-red mx-0 mx-xl-3">' +
                    '<span class="col-12 col-lg-5 d-block img px-0">' +
                        '<img class="w-100" :src="news3 | appPathFormat" alt="胡連精密">' +
                    '</span>' +
                    '<span class="col-12 col-lg-7 d-block pl-0 pl-lg-4 pr-0 pr-lg-3 pr-xl-0 pt-3 pt-lg-0 pb-3 pb-md-0">' +
                        '<span class="d-block font-20 font-weight-bold lh14 mb-2 pb-1">歡迎蒞臨胡連2019年印尼電動車，智慧車秀！</span>' +
                        '<span class="text-646464 line3 mb-2 mb-md-3">現場將展出年度新品、保險絲盒、電動車連接器等產品，以及新應用類別新品。</span>' +
                        '<span class="d-block text-red font-weight-bold">2019-08-11</span>' +
                    '</span>' +
                '</a>' +
            '</div>' +
            '<div class="px-sm-3 px-lg-0">' +
                '<h3 class="hidden">產品顏色標準制定公告</h3>' +
                '<a href="new" class="row text-hover-red mx-0 mx-xl-3">' +
                    '<span class="col-12 col-lg-5 d-block img px-0">' +
                        '<img class="w-100" :src="news1 | appPathFormat" alt="胡連精密">' +
                    '</span>' +
                    '<span class="col-12 col-lg-7 d-block pl-0 pl-lg-4 pr-0 pr-lg-3 pr-xl-0 pt-3 pt-lg-0 pb-3 pb-md-0">' +
                        '<span class="d-block font-20 font-weight-bold lh14 mb-2 pb-1">產品顏色標準制定公告</span>' +
                        '<span class="text-646464 line3 mb-2 mb-md-3">為了提供顧客更優質的產品服務，在持續改善計畫中，當前致力於產品顏色的制定，此將依據...</span>' +
                        '<span class="d-block text-red font-weight-bold">2020-03-31</span>' +
                    '</span>' +
                '</a>' +
            '</div>' +
        '</div>',
    name: 'NewsAndEventSlick',
    data() {
        return {
            news1: "/Styles/images/index/news-1.jpg",
            news2: "/Styles/images/index/news-2.jpg",
            news3: "/Styles/images/index/news-3.jpg"
        }
    },
    created: function () {
        AOS.init({
            duration: 500,
            offset: 10,
            mirror: true,
        });
    },
    mounted: function () {
        var slider = $(this.$el).slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: $('.index-news .slick-prev'),
            nextArrow: $('.index-news .slick-next'),
            responsive: [
                {
                    breakpoint: 1400,
                    settings: {
                        slidesToShow: 2,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                    },
                },
                {
                    breakpoint: 576,
                    settings: {
                        dots: true,
                        arrows: false,
                        slidesToShow: 1,
                    },
                },
            ],
        });
    }
});

Vue.component('product-application-slick', {
    template:
        '<div class="slider arrows-style-1" data-aos="fade-up">' +
            '<div class="pb-4 pb-sm-5">' +
                '<div class="jqimgFill">' +
                    '<img :src="applications1 | appPathFormat" alt="胡連精密">' +
                '</div>' +
                '<p class="font-20 font-weight-bold pt-4 mt-2 mt-sm-3 pb-sm-2">機車產業應用</p>' +
                '<a href="#" class="d-table font-weight-bold border-red bg-hover-red text-red text-hover-white rounded-25 lh2 py-2 px-4 mx-auto mb-2">深入了解</a>' +
            '</div>' +
            '<div class="pb-4 pb-sm-5">' +
                '<div class="jqimgFill">' +
                    '<img :src="applications2 | appPathFormat" alt="胡連精密">' +
                '</div>' +
                '<p class="font-20 font-weight-bold pt-4 mt-2 mt-sm-3 pb-sm-2">新能源汽車產業應用</p>' +
                '<a href="#" class="d-table font-weight-bold border-red bg-hover-red text-red text-hover-white rounded-25 lh2 py-2 px-4 mx-auto mb-2">深入了解</a>' +
            '</div>' +
            '<div class="pb-4 pb-sm-5">' +
                '<div class="jqimgFill">' +
                    '<img :src="applications3 | appPathFormat" alt="胡連精密">' +
                '</div>' +
                '<p class="font-20 font-weight-bold pt-4 mt-2 mt-sm-3 pb-sm-2">新能源汽車產業應用</p>' +
                '<a href="#" class="d-table font-weight-bold border-red bg-hover-red text-red text-hover-white rounded-25 lh2 py-2 px-4 mx-auto mb-2">深入了解</a>' +
            '</div>' +
            '<div class="pb-4 pb-sm-5">' +
                '<div class="jqimgFill">' +
                    '<img :src="applications4 | appPathFormat" alt="胡連精密">' +
                '</div>' +
                '<p class="font-20 font-weight-bold pt-4 mt-2 mt-sm-3 pb-sm-2">壓著模具</p>' +
                '<a href="#" class="d-table font-weight-bold border-red bg-hover-red text-red text-hover-white rounded-25 lh2 py-2 px-4 mx-auto mb-2">深入了解</a>' +
            '</div>' +
            '<div class="pb-4 pb-sm-5">' +
                '<div class="jqimgFill">' +
                    '<img src="styles/images/index/applications-1.jpg" alt="胡連精密">' +
                '</div>' +
                '<p class="font-20 font-weight-bold pt-4 mt-2 mt-sm-3 pb-sm-2">機車產業應用</p>' +
                '<a href="#" class="d-table font-weight-bold border-red bg-hover-red text-red text-hover-white rounded-25 lh2 py-2 px-4 mx-auto mb-2">深入了解</a>' +
            '</div>' +
        '</div>',
    name: 'ProductApplicationSlick',
    data() {
        return {
            applications1: "/Styles/images/index/applications-1.jpg",
            applications2: "/Styles/images/index/applications-2.jpg",
            applications3: "/Styles/images/index/applications-3.jpg",
            applications4: "/Styles/images/index/applications-4.jpg"
        }
    },
    created: function () {
        AOS.init({
            duration: 500,
            offset: 10,
            mirror: true,
        });
    },
    mounted: function () {
        $(this.$el).slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            prevArrow: " <button class='slick-prev slick-arrow text-black rounded-circle text-white' type='button'><i class='icon-left-big font-24'></i></button>",
            nextArrow: " <button class='slick-next slick-arrow text-black rounded-circle text-white' type='button'><i class='icon-right-big font-24'></i></button>",
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                    },
                },
            ],
        })
    }
});

Vue.component('global-sites-slick', {
    template:
        '<div class="slider mt-sm-5 mb-lg-5 pb-sm-5 pb-xl-0" data-aos="fade-up">' +
            '<div>' +
                '<div class="position-relative bg-white rounded-15 p-1">' +
                    '<img class="rounded-15 w-100" :src="global1 | appPathFormat" alt="胡連精密">' +
                        '<span class="position-absolute font-17 font-weight-bold">台北新廠</span>' +
                '</div>' +
            '</div>' +
            '<div>' +
                '<div class="position-relative bg-white rounded-15 p-1">' +
                    '<img class="rounded-15 w-100" :src="global2 | appPathFormat" alt="胡連精密">' +
                        '<span class="position-absolute font-17 font-weight-bold">南京廠</span>' +
                '</div>' +
            '</div>' +
            '<div>' +
                '<div class="position-relative bg-white rounded-15 p-1">' +
                    '<img class="rounded-15 w-100" :src="global1 | appPathFormat" alt="胡連精密">' +
                        '<span class="position-absolute font-17 font-weight-bold">台北新廠</span>' +
                '</div>' +
            '</div>' +
        '</div>',
    name: 'GlobalSitesSlick',
    data() {
        return {
            global1: "/Styles/images/index/global-1.jpg",
            global2: "/Styles/images/index/global-2.jpg"
        }
    },
    created: function () {
        AOS.init({
            duration: 500,
            offset: 10,
            mirror: true,
        });
    },
    mounted: function () {
        $(this.$el).slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow: " <button class='slick-prev slick-arrow text-white' type='button'><i class='icon-left-big font-24'></i></button>",
            nextArrow: " <button class='slick-next slick-arrow text-white' type='button'><i class='icon-right-big font-24'></i></button>",
            responsive: [
                {
                    breakpoint: 1600,
                    settings: {
                        slidesToShow: 1,
                    },
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                    },
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                        dots: true
                    },
                },
            ],
        })
    }
});

Vue.filter('appPathFormat', function (val) {
    return $("#appPath").val() + val;
})

const axiosConfig = {
    baseURL: $("#appPath").val(),
    timeout: 30000,
};

Vue.prototype.$axios = axios.create(axiosConfig)

new Vue({
    el: '#app',
    components: {
        HomeMain,
        ProductAdvanceSearch,
        NewProductList,
        AboutMe,
        GlobalSites,
        ProductApply,
        CarConnector,
        NewsAndEvent
    },
    data(){
        return {
            
        }
    },
    created: function () {
        AOS.init({
            duration: 500,
            offset: 10,
            mirror: true,
        });
    },
    mounted: function () {
        //initSlick();
        //initLastestSlick();
    },
    updated: function () {
        
       
    },
    methods: {
        
    }
})

