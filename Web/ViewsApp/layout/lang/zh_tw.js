﻿export default {
    __intro: '產品介紹',
    __about: '關於胡連',
    __business: '商務專區',
    __news: '最新訊息',
    __stakeholder: '投資人專區',
    __hr: '人力資源'
}