﻿import Vue from 'vue'
import ProductMain from './ProductMain.vue'
import ProductBox from './ProductBox.vue'
import ProductRelation from './ProductRelation.vue'
import ThreeDDialog from './ThreeDDialog.vue'

import AOS from "aos";
import "aos/dist/aos.css";

import axios from 'axios'

Vue.component('product-info', {
    template:
        '<div class="scrollbarX mb-4" data-aos="fade-up">' +
        '<div class= "wrapper-style">' +
        '<div class="dec"></div>' +
        '<div class="d-flex text-center">' +
        '<div class="item" v-if="product.rows">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData1}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.rows}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.positions">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData2}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.positions}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.productReMaterial">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData3}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.productReMaterial}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.tab">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">TAB(mm)</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.tab}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.productRePlating">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData4}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.productRePlating}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.depth">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData5}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.depth}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.showSex">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData6}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.productReSex}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.showSealable">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData7}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.productReSealable}}</div>' +
        '</div>' +

        '<div class="item" v-if="product.showFrontColor">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData8}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.productReColor}}</div>' +
        '</div>' +

        '<div class="item" v-if="product.showDiameter">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData9}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.diameter}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.showElectricity">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData10}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.diameter}}</div>' +
        '</div>' +

        '<div class="item" v-if="product.showSColor">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData8}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.productReColor}}</div>' +
        '</div>' +

        '<div class="item" v-if="product.temper_operating">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData11}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.temper_operating}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.showQuantity">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData12}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.quantity}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.showPacking">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData13}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.productReBag}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.productReFeedType">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData14}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.productReFeedType}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.flamm_rating">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData15}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.flamm_rating}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.seal_capability">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData16}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.seal_capability}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.leaking">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData17}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.leaking}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.plate_form">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData18}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.plate_form}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.plate_thickness">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData19}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.plate_thickness}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.e_init">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData20}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.e_init}}</div>' +
        '</div>' +
        '<div class="item" v-if="product.e_allow">' +
        '<div class="border-C8C8C8 flex-center bg-white py-2 px-3">{{product.productData21}}</div>' +
        '<div class="border-C8C8C8 bg-blueEDF4FF py-2 px-3">{{product.e_allow}}</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>',
    name: 'product-nfo',
    props: ['product'],
    data() {
        return {

        }
    }
});

Vue.filter('appPathFormat', function (val) {
    return $("#appPath").val() + val;
})

Vue.filter('appPathFormatTw', function (val) {
    return $("#appPathtw").val() + val;
})

const axiosConfig = {
    baseURL: $("#appPath").val(),
    timeout: 30000,
};

Vue.prototype.$axios = axios.create(axiosConfig)


new Vue({
    el: '#product',
    components: {
        ProductMain,
        ProductBox,
        ProductRelation,
        ThreeDDialog
    },
    data() {
        return {
            productid: -1,
            productData: {},
            productRelations: [],
            productUrl: this.$options.filters.appPathFormat("/products"),
            productUrlTw: this.$options.filters.appPathFormatTw("/products")
        }
    },
    created: function () {
        let uri = window.location.search.substring(1);
        // console.log(window.location.pathname);
        //  let params = new URLSearchParams(uri);
        //  this.productid = params.get("id");

        let pathTemp = window.location.pathname.split('/');
        this.productid = pathTemp[pathTemp.length - 1];

        AOS.init({
            duration: 500,
            offset: 10,
            mirror: true,
        });

    },
    mounted: function () {
        initScrollbar();
        this.getProductData();
        this.getProductRelations();
    },
    methods: {
        getProductData: function () {
            console.log(this.productid);
            this.$axios.post($('#langPath').val() + '/product/get', { id: this.productid })
                .then((res) => {
                    console.log(res.data);
                    this.productData = res.data;
                })
                .catch((error) => {
                    console.error(error)
                })

        },
        getProductRelations: function () {
            this.$axios.post($('#langPath').val() + '/product/getRelations', { id: this.productid })
                .then((res) => {
                    console.log(res.data);
                    this.productRelations = res.data;
                })
                .catch((error) => {
                    console.error(error)
                })
        },
        handleZoomToggle: function (zoomToggle) {
            if (zoomToggle.zoomInit) {
                $(".productImg").imagezoomsl({
                    innerzoommagnifier: true,
                    classmagnifier: window.external ? window.navigator.vendor === "Yandex" ? "" : "round-loupe" : "",
                    zoomrange: [1.5, 1.5],
                    zoomstart: 1,
                    magnifiersize: [250, 250]
                });
            }
            else {
                if (!zoomToggle.zoom) {
                    $(".round-loupe").addClass("opacity0");
                    $(".tracker").addClass("d-none");
                }
                else {
                    $(".round-loupe").removeClass("opacity0");
                    $(".tracker").removeClass("d-none");
                }
            }
        },
        handleOpen3DDialog: function () {
            $('#productModal').modal('toggle');
        },
        handleOpenDownloadDialog: function () {
            $('#downloadModal').modal('toggle');
        },
        handleOpenPaperDialog: function () {
            $('#paperModal').modal('toggle');
        },
        handleOpenSpcDialog: function () {
            $('#spcModal').modal('toggle');
        },
    }
})