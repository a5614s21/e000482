﻿import Vue from 'vue'
import ProductsMain from './ProductsMain.vue'
import SearchTab from './SearchTab.vue'
import SearchBySpec from './SearchBySpec.vue'
import SearchByApply from './SearchByApply.vue'
import SearchByKeyword from './SearchByKeyword.vue'
import SearchByLast from './SearchByLast.vue'
import SearchResult from './SearchResult.vue'
import Pagination from './Pagination.vue'
import axios from 'axios'
import AOS from "aos";
import "aos/dist/aos.css";
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'

Vue.use(AOS);
Vue.use(PerfectScrollbar);

Vue.component('search-by-apply-category', {
    template:
        '<div class="scrollbarY content w-100">' +
            '<div>' +
                '<div v-for="(item,index) in applyCategories" class="category bg-blue-gradient-3 bg-hover-yellow border-white rounded-10 overflow-hidden cursor opacity08 py-1 py-md-2 px-3 mb-md-2" v-bind:class= "{active:activeCategory == index}" @click="handleCategoryClick(index,item)">' +
                    '<span class="flex-justify-space-between align-center position-relative z-1 px-md-1">' +
                        '<span>{{ item.Caption }}</span>' +
                        '<i class="icon-right-big font-20"></i>' +
                    '</span>' +
                '</div>' +
            '</div>' +
        '</div>',
    name: 'SearchByApplyCategory',
    data: function () {
        return {
            applyCategories: [],
            activeCategory: JSON.parse($("#homeSearch").val()).Id,
        }
    },
    created: function () {
        console.log(JSON.parse($("#homeSearch").val()).Id);
        this.$axios.get('/products/GetL2CarSysIncludeL3')
            .then((res) => {
                this.applyCategories = res.data
                this.handleCategoryClick(JSON.parse($("#homeSearch").val()).Id, this.applyCategories[JSON.parse($("#homeSearch").val()).Id]);
            })
            .catch((error) => {
                console.error(error)
            })
    },
    mounted: function () {
        $(this.$el).mCustomScrollbar({
            axis: "y",
        });
    },
    methods: {
        handleCategoryClick: function (index, item) {
            console.log(index);
            console.log(item);
            this.activeCategory = index;
            this.$emit('l2selected',item.Childs,item.Caption)
        },
        l2selected: function (l3items,name) {
            this.l3items = l3items;
            this.l2name = name;
        },
    },
    updated: function () {
        $(this.$el).mCustomScrollbar({
            axis: "y",
        });
    }
});

Vue.filter('appPathFormat', function (val) {
    console.log(val);
    //return $("#appPath").val() + val;
    return val;
})

const axiosConfig = {
    baseURL: $("#appPath").val(),
    timeout: 30000,
};

Vue.prototype.$axios = axios.create(axiosConfig)

new Vue({
    el: '#app',
    components: {
        ProductsMain,
        SearchTab,
        SearchBySpec,
        SearchByApply,
        SearchByKeyword,
        SearchByLast,
        SearchResult,
        Pagination
    },
    data() {
        return {
            tabIndex: 0,
            isauth: false,
            homeSearchItems: null,
            keyword: ""
        }
    },
    created: function () {
        AOS.init();

        this.isauth = isAuthed();
        this.tabIndex = getTabIndex();
        this.homeSearchItems = JSON.parse($("#homeSearch").val());
        this.keyword = $("#homeSearchKeyword").val();
    },
    updated: function () {
        this.$nextTick(function () {
            this.initScroll();
        })
    },
    methods: {
        tabIndexChange: function (newIndex) {
            this.tabIndex = newIndex;  
        },
        initScroll: function () {
            $(".scrollbarX").mCustomScrollbar({
                axis: "x",
                scrollEasing: "easeOutCirc",
                mouseWheel: "auto",
                autoDraggerLength: true,
                advanced: {
                    updateOnBrowserResize: true,
                    updateOnContentResize: true
                } // removed extra commas  
            });
            $(".scrollbarY").mCustomScrollbar({
                axis: "y",
            });
        }
    }
})